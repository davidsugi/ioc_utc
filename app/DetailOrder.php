<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Addon;
use App\Facility;
use App\Bundle;
use App\Folio;
use App\DEtailFacility;
use Carbon\carbon;
use H;

class DetailOrder extends Model
{
    protected $guarded = [];
    protected $dates = ['check_in','check_out'];
    protected $hidden = ['created_at','updated_at','amount','amount2'];

      public function getdurasiAtauJumlahAttribute()
   {
       return $this->amount;
   }
   public function getOrangAttribute()
   {
       return $this->amount2;
   }

    public function order()
    {
        return $this->BelongsTo('App\Order');
    }

    public function folio()
    {
    return $this->hasMany('App\Folio');
    }
    public function item()
    {
        return $this->morphTo();
    }

    public function getpriceLabelAttribute()
    {
        return 'Rp. ' . number_format($this->price, 0, '', '.');
    }
    public function getNameLabelAttribute()
    {
        $obj = Addon::find($this->item_id);
        if($this->item_type=="App\\Facility") {
            $obj=Facility::find($this->item_id);        
        }
        else if($this->item_type=="App\\Bundle"){
         $obj = Bundle::find($this->item_id);

        }
        if($obj)
            return $obj->name;    
        
            return "-";
    }
    public function getpersonLabelAttribute(){
        if($this->item_type=="App\\Bundle"){
            $pers= Bundle::where("id",$this->item_id)->first();            
        }
        else{
             $pers= Facility::where("id",$this->item_id)->first();            
        }
        if($this->amount2==0 || $pers->per_person==0) {
            return "-";
        }
        else if($pers->per_person==1 && $this->amount2 < $pers->minimum){
            return "<s style='color:red'>".$this->amount2."</s> ".$pers->minimum;
        }
        return $this->amount2;
    }

    public function getPerPersonLabelAttribute()
    {
         if($this->item_type=="App\\Bundle"){
            $pers= Bundle::where("id",$this->item_id)->first();            
        }
        else{
             $pers= Facility::where("id",$this->item_id)->first();            
        }
        return $pers->per_person;
        
    }

    public function gettotalLabelAttribute()
    {
        $prc = $this->price*$this->amount;
        if($this->amount2!=null) {
            $prc=$prc*$this->amount2;
        }
        $diskon=$this->disc;
        if($this->disc_type==0)
            $diskon=$prc*($this->disc/100);
        $prc = $prc - $diskon;
        return $prc;
    }
    public function getselectLabelAttribute()
    {
        return $this->id." - ".$this->namelabel;
    }
    public function getstatusLabelAttribute()
    {   
        $text=["Booked","Check In","Check Out","Canceled"];

        return $text[$this->status];
    }
    public function getdurationLabelAttribute()
    {
        $cin = Folio::where("detail_order_id", $this->id)->where("folio_type", "LIKE", "%Facility")->orderBy("check_in", "ASC")->first();        
        if($cin)
            return $cin->check_in->diffInDays($cin->check_out);
        return intval($this->amount);
    }
    public function getdiscLabelAttribute()
    {
        if($this->disc_type==0){
            return $this->disc." %";
        }
        return H::rupiah($this->disc);
       
    }
    public function getidHelperLabelAttribute()
    {
        if($this->item_type=="App\\Facility") {
            return "F-".$this->item_id;   
        }
        else if($this->item_type=="App\\Bundle"){
            return "B-".$this->item_id;               
        }
        return "A-".$this->item_id;
    }
    public function getcheckinLabelAttribute(){
        // return $this->id;
       $cin = Folio::where("detail_order_id", $this->id)->where("folio_type", "LIKE", "%Facility")->orderBy("check_in", "ASC")->first();
       if($cin)
        return $cin->check_in;
        
        return "-";
    }
    public function getcheckoutLabelAttribute(){
       $cin = Folio::where("detail_order_id", $this->id)->where("folio_type", "LIKE", "%Facility")->orderBy("check_out", "DESC")->first();
       if($cin) 
        return $cin->check_out;
        return "-";
    }
    protected $appends = ['durasi_atau_jumlah','orang'];
    

}
