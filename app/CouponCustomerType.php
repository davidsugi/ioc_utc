<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponCustomerType extends Model
{
    public function coupon()
    {
        return $this->BelongsTo('App\Coupon');
    }
    public function customer_type()
    {
        return $this->BelongsTo('App\CustomerType');
    }
}
