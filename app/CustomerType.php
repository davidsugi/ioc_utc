<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerType extends Model
{
    protected $guarded=[];
     protected $hidden = [
        'id','created_at','updated_at'
    ];
    public function coupon_customer_type()
    {
        return $this->hasMany('App\CouponCustomerType');
    }
    public function customer()
    {
        return $this->hasMany('App\Customer');
    }
    public function special_price()
    {
        return $this->hasMany('App\SpecialPrice');
    }
}
