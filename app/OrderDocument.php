<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDocument extends Model
{
    protected $guarded = [];

    public function order()
    {
        return $this->BelongsTo('App\Order');
    }
}
