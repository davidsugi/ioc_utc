<?php

namespace App;
use Carbon\carbon;
use App\GlobalSetting;

class Helpers
{
    public static function globs($key,$val=null)
    {
        if($val!=null) {
            $glob=GlobalSetting::where("key", "LIKE", "%".$key."%")->first();
            $glob->value=$val;
            $glob->save();
        }
        else{
            $glob=GlobalSetting::where("key", "LIKE", "%".$key."%")->first();
            return $glob->value;
        }
    }

    public static function rupiah($nums)
    {
        return 'Rp. ' . number_format( $nums, 0 , '' , '.' );
    }


    public static function date_title($datestart,$dateend)
    {
        $returns=Carbon::now()->format('d-m-Y');
        if($datestart!=null || $dateend!=null){
            if($datestart!=$dateend){
                $returns= Carbon::parse($datestart)->format('d-m-Y');
                $returns.=" - ";
                $returns.= Carbon::parse($dateend)->format('d-m-Y');

                if($datestart==null){
                    $returns=" ~ ".Carbon::parse($dateend)->format('d-m-Y');
                }
                if($dateend==null){
                    $returns= Carbon::parse($datestart)->format('d-m-Y');
                    $returns.=" ~ ";
                }
            }
            else{
                $returns= Carbon::parse($datestart)->format('d-m-Y');
            
            }

           
        }
        return $returns;
    }


}
?>