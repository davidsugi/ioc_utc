<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded =[];
    protected $hidden = [
        'password', 'remember_token','created_at','update_at'
    ];
    protected $appends=['category_label'];

    public function scopeActive($query)
    {
        return $query->where('blacklist',0);
    }
    public function getprovinceIdAttribute()
    {
        if($this->city)
          return $this->city->province;
    }
    public function getcategoryLabelAttribute()
    {
        $category=['Pribadi','Swasta/Perusahaan','BUMN/Bank/Dinas','Pendidikan'];
        if($this->customer_type){
            if($this->customer_type->name=="External")
                return $category[$this->category-1];

        }
            return "-";
    }
    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

    public function Order()
    {
        return $this->hasMany('App\Order');
    }

    public function customer_type()
    {
        return $this->BelongsTo('App\CustomerType');
    }
    public function getNameLabelAttribute()
    {
        if($this->company_name == "")
            return $this->name;
        return $this->company_name;
    }
}
