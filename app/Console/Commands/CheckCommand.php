<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use Carbon\carbon;
use App\DetailOrder;
use App\Addon;
use App\Folio;

class CheckCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking expired order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Investigating Rotten Orders');
        $order = Order::where('status',"<",3)->get();
        foreach($order as $ord){
            $dt = Carbon::now()->diffInDays($ord->date);
            if($dt >= $ord->expiry){
                $this->info("rotten orders found! order no ".$ord->id);
                $ord->status=4;
                $adds = DetailOrder::where("order_id",$ord->id)->where("item_type","LIKE","%Addon")->get();        
                if(count($ord->folio)>0){
                    foreach ($adds as $add) {
                        $ad =Addon::find($add->item_id);
                        $ad->borrowed = $ad->borrowed-$add->amount;
                        $ad->save();
                    }

                    foreach ($ord->folio as $fol) {
                        $fol->active=0;
                        $fol->save();
                    }
                }
        // $ord->save();
                $ord->save();                
            }
        }

    }
}
