<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $guarded =[];

    public function addon()
    {
        return $this->BelongsTo('App\Addon','addon_id');
    }

    public function getitemLabelAttribute()
    {
        return json_decode($this->description);
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = json_encode($value);
    }
    public function getDescriptionLabelAttribute()
    {
        $str="";
        foreach($this->item_label as $item){
            $str.=$item.", ";
        }
        
        return rtrim($str,", ");
    }
}
