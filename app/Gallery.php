<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded =[];

    public function galleriable()
    {
        return $this->morphTo();
    }
    
    protected $galleriable_types = [
        'facility' => \App\Facility::class,
        'educationtour' => \App\EducationTour::class,
    ];

    public function scopeCode($query,$param)
    {
    return $query->where('kode', 'LIKE' ,'%'.$param.'%');
    }
}
