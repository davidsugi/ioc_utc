<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpecialPrice;
use App\CustomerType;
use App\Addon;
use App\Facility;
use App\Bundle;

class SpecialPriceController extends Controller
{
    public function index(Request $request){
        $special_prices = SpecialPrice::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $special_prices  = $special_prices->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");
                }
            }
        }
        $special_prices=$special_prices->orderBy('id','DESC')->paginate(20);
        return view('special_price.index')->with('special_prices',$special_prices)->with('filter',$filter);
    }
    public function create(Request $req){
        
        $special_price = new SpecialPrice;
        $special_price->special_type = $req->u;
        $special_price->special_id = $req->id;        
        $cst = CustomerType::all()->pluck("name","id");
        $fac = Facility::pluck("name","id");                    
        if($req->u=="App\\Facility"){
            $fac = Facility::where("id",$req->id)->pluck("name","id");            
        }
        else if($req->u=="App\\Addon"){
            $fac = Addon::where("id",$req->id)->pluck("name","id");
        }
        else if($req->u=="App\\Bundle"){
            $fac = Bundle::where("id",$req->id)->pluck("name","id");            
        }
        return view('special_price._form')
        ->with('customer_types',$cst)
        ->with('facility',$fac)
        ->with('special_price',$special_price);
        
    }

    public function store(Request $request){
        // return $request;
        $special_price = new SpecialPrice;
       $check = SpecialPrice::orderBy('id','DESC')->first();
                        if($check)
                            $special_price->id = $check->id+1;
        $special_price->fill($request->all());
        $special_price->save();
        $request->session()->flash('toast', 'Harga Spesial berhasil ditambahkan!');
        if($special_price->special_type=="App\\Facility"){
             return redirect()->route('facilities.show',$special_price->special_id);
        }
        else if($special_price->special_type=="App\\Addon"){
             return redirect()->route('addons.show',$special_price->special_id);                    
        }
        else if($special_price->special_type=="App\\Bundle"){
            return redirect()->route('bundles.show',$special_price->special_id);                    
        }
        return redirect()->route('special_prices.index');
    }
    public function edit($id){
        $special_price = SpecialPrice::find($id);   
        $cst = CustomerType::all()->pluck("name","id");
        $fac = Facility::pluck("name","id");                    
        if($special_price->special_type=="App\\Facility"){
            $fac = Facility::where("id",$special_price->special_id)->pluck("name","id");            
        }
        else if($special_price->special_type=="App\\Addon"){
            $fac = Addon::where("id",$special_price->special_id)->pluck("name","id");
        }
        else if($special_price->special_type=="App\\Bundle"){
            $fac = Bundle::where("id",$special_price->special_id)->pluck("name","id");            
        }
        return view('special_price._form')
        ->with('customer_types',$cst)
        ->with('facility',$fac)
        ->with('special_price',$special_price);

    }
    public function update($id, Request $request){
        $special_price = SpecialPrice::find($id);
          $special_price->fill($request->all());
        $special_price->save();
        $request->session()->flash('toast', 'Harga Spesial berhasil ditambahkan!');
        if($special_price->special_type=="App\\Facility"){
             return redirect()->route('facilities.show',$special_price->special_id);
        }
        else if($special_price->special_type=="App\\Addon"){
             return redirect()->route('addons.show',$special_price->special_id);                    
        }
        else if($special_price->special_type=="App\\Bundle"){
            return redirect()->route('bundles.show',$special_price->special_id);                    
        }
        
        $request->session()->flash('toast', 'Harga Spesial  berhasil diubah!');
        return redirect()->route('special_prices.index');
    }
    public function show(SpecialPrice $special_price){
        return view('special_price.show')->with('special_price',$special_price);
    }
    public function destroy(Request $request, SpecialPrice $special_price){
        $tmp= $special_price;
        $special_price->delete();
        $request->session()->flash('toast', 'Harga Spesial berhasil dihapus!');
        if($tmp->special_type=="App\\Facility"){
             return redirect()->route('facilities.show',$tmp->special_id);
        }
        else if($tmp->special_type=="App\\Addon"){
             return redirect()->route('addons.show',$tmp->special_id);                    
        }
        else if($tmp->special_type=="App\\Bundle"){
            return redirect()->route('bundles.show',$tmp->special_id);                    
        }
    }
}
