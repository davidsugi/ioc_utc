<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Facility;
use App\CustomPrice;
use Illuminate\Http\Request;

class FacilityCustomPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Facility $facility, Request $req)
    {
        // return $req;
        $custom_price = new CustomPrice;
        $type = $req->type;
        return view('facilities_custom_prices.create', compact('custom_price', 'facility','type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Facility $facility)
    {
        // return $request;
        $custom_price = new CustomPrice;
        $chk=CustomPrice::orderBy('id','DESC')->first();
        if($chk)
            $custom_price->id = $chk->id +1;
        
        $array=explode('-', $request->rangedate);
        $startdate=Carbon::createFromFormat('d/m/Y H:i:s ', $array[0]);
        $enddate=Carbon::createFromFormat(' d/m/Y H:i:s', $array[1]);
        $custom_price->startdate=$startdate;
        $custom_price->enddate=$enddate;
        $custom_price->price = $request->price;
        $custom_price->price_id = $facility->id;
        $custom_price->price_type= "App\\Facility";
        $custom_price->save();
        $request->session()->flash('toast', 'Harga berhasil ditambahkan!');
        return redirect('/facilities/'.$facility->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Facility $facility, CustomPrice $custom_price)
    {
        return view('facilities_custom_prices.create', compact('custom_price', 'facility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility, CustomPrice $custom_price)
    {
        $array=explode('-', $request->rangedate);
        $startdate=Carbon::createFromFormat('d/m/Y H:i:s ', $array[0]);
        $enddate=Carbon::createFromFormat(' d/m/Y H:i:s', $array[1]);
        $custom_price->startdate=$startdate;
        $custom_price->enddate=$enddate;
        $custom_price->price = $request->price;
        $custom_price->save();
        $request->session()->flash('toast', 'Harga berhasil diubah!');
        return redirect('/facilities/'.$facility->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Facility $facility, CustomPrice $custom_price)
    {
        try{
            $custom_price->delete();
            $request->session()->flash('toast', 'Harga berhasil dihapus');

        }
        catch(\Illuminate\Database\QueryException $ex){
            $request->session()->flash('toast', 'Harga gagal dihapus'.substr($ex->getMessage(), 0, 15));

        };       
        return redirect('/facilities/'.$facility->id);  
    }
}
