<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bundle;
use App\Gallery;
class BundleGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Bundle $bundle)
    {
        $gallery = new Gallery;
        return view('bundle_galleries.create', compact('gallery', 'bundle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Bundle $bundle)
    {
        $file = $request->file('image');
        $filename = $file->getClientOriginalName();
        $path = public_path().'/image/';
        $file->move($path, $filename);

        $gallery = new Gallery;
        $check = Gallery::orderBy('id','DESC')->first();
            if($check)
                $gallery->id = $check->id+1;
        $check = Gallery::orderBy('id','DESC')->first();
            if($check)
                $gallery->id = $check->id+1;

        $gallery->image_path = $filename;
        $gallery->title = $request->title;
        $gallery->description = $request->description;
        $gallery->galleriable_id = $bundle->id;
        $gallery->galleriable_type = 'Bundle';
        $gallery->save();
        $gallery->slug = str_slug($gallery->id.'_'.$gallery->title, '_');
        $gallery->save();
        $request->session()->flash('toast', 'Gallery berhasil ditambahkan!');
        
        return redirect('/bundles/'.$bundle->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Bundle $bundle, Gallery $gallery)
    {
        return view('bundle_galleries.create', compact('gallery', 'bundle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bundle $bundle, Gallery $gallery)
    {
        if($request->file('image')){
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/image/';
            $file->move($path, $filename);
            $gallery->image_path = $filename;            
        }

        $gallery->title = $request->title;
        $gallery->description = $request->description;
        $gallery->slug = str_slug($gallery->id.'_'.$gallery->title, '_');
        $gallery->save();
        $request->session()->flash('toast', 'Gallery berhasil diubah!');
        return redirect('/bundles/'.$bundle->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Bundle $bundle, Gallery $gallery)
    {
        try {
            $gallery->delete();
            $request->session()->flash('toast', 'Gallery berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Gallery gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        return redirect('/bundles/'.$bundle->id);  
    }
}
