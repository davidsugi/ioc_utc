<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use Auth;

class ActivtiyController extends Controller
{
    public function index(Request $request)
    {
        $activitys = Activity::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $activitys  = $activitys->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");
                }
            }
        }
        $activitys=$activitys->orderBy('id','DESC')->paginate(10);
        return view('activity.index')
        ->with('activitys', $activitys)
        ->with('filter',$filter);
    }
    public function create(){
        $activity = new Activity;
        return view('activity._form')->with('activity', $activity);
    }

     public function store(Request $request)
    {
        // return $request->description;
            $filename = "";
        if($request->hasFile('image')){
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/image/';
            $file->move($path, $filename);
        }


        $activtiy = new Activity;
        $check = Activity::orderBy('id','DESC')->first();
        if($check)
            $activtiy->id = $check->id+1;

        $input = $request->except(['image','files']);
        $activtiy->fill($input);
        $activtiy->image = $filename;

        $activtiy->user_id = Auth::user()->id;
        $activtiy->description = $request->description;

        
        $activtiy->slug = str_slug($activtiy->title, '_');        
        $activtiy->save();

        $request->session()->flash('toast', 'Data Aktivitas  berhasil diubah!');
        
        return redirect()->route('activities.index');
        

    }

    public function edit($id){
        $activity = Activity::find($id);
        return view('activity._form')->with('activity',$activity);
    }
    public function update($id, Request $request){
        $activity = Activity::find($id);
          $input = $request->except(['files']);
        $activity->fill($input);
        $activity->user_id = Auth::user()->id;
        $activity->slug = str_slug($activity->title, '_');
                        
        $activity->save();

        $request->session()->flash('toast', 'Data Aktivitas  berhasil diubah!');
        return redirect()->route('activities.show',['id'=>$activity->id]);
    }
    public function show(Activity $activity){
        return view('activity.show')->with('activity', $activity);
    }
    public function destroy(Request $request, Activity $activity){

        try {
        $activity->delete();
        $request->session()->flash('toast', 'Data Aktivitas berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Data Aktivitas Gagal dihapus!');
         }
        return redirect()->route('activities.index');
    }

    public function updateImage(Request $request, Activity $activtiy)
    {
        $file = $request->file('image');
        $filename = $file->getClientOriginalName();
        $path = public_path().'/image/';
        $file->move($path, $filename);
        
        $activtiy->image = $filename;
        
        $activtiy->save();
        $request->session()->flash('toast', 'Data Aktivitas  berhasil diubah!');
        return redirect()->route('activities.show',['id'=>$activtiy->id]);
    }

       public function editImage(Activity $activtiy)
    {
        return view('activity.createImage', compact('activtiy'));
    }


}
