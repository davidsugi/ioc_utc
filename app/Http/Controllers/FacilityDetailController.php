<?php

namespace App\Http\Controllers;

use App\FacilityDetail;
use App\Facility;
use App\FacilityAttribute;

use Illuminate\Http\Request;

class FacilityDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $facility_detail = FacilityDetail::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $facility_detail = $facility_detail->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }
        $facility = Facility::pluck('name', 'id')->all();
        $attribute = FacilityAttribute::pluck('name', 'id')->all();
        $facility_detail = $facility_detail->paginate(10);
        return view('facility_details.index', compact('facility_detail', 'filter', 'facility', 'attribute'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $facility_detail = new FacilityDetail;
        $facility = Facility::pluck('name', 'id')->all();
        $attribute = FacilityAttribute::pluck('name', 'id')->all();
        return view('facility_details.create', compact('facility_detail', 'facility', 'attribute'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $facility_detail = new FacilityDetail;
        $check = FacilityDetail::orderBy('id','DESC')->first();
                if($check)
                    $facility_detail->id = $check->id+1;
        $facility_detail->facility_id = $request->facility_id;
        $facility_detail->attribute_id = $request->attribute_id;
        $facility_detail->value = $request->value[$request->tipe];
        $facility_detail->save();
        $request->session()->flash('toast', 'Detail Fasilitas berhasil ditambahkan');
        return redirect('/facilities/'.$facility_detail->facility_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FacilityDetail $facilityDetail
     * @return \Illuminate\Http\Response
     */
    public function show(FacilityDetail $facility_detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FacilityDetail $facilityDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(FacilityDetail $facility_detail)
    {
        $facility = Facility::pluck('name', 'id')->all();
        $attribute = FacilityAttribute::pluck('name', 'id')->all();
        return view('facility_details.create', compact('facility_detail', 'facility', 'attribute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\FacilityDetail      $facilityDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FacilityDetail $facility_detail)
    {
        
        $facility_detail->facility_id = $request->facility_id;
        $facility_detail->attribute_id = $request->attribute_id;
        $facility_detail->value = $request->value[$request->tipe];
        $facility_detail->save();
        $request->session()->flash('toast', 'Detail Fasilitas berhasil diubah');
        return redirect('/facilities/'.$facility_detail->facility_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FacilityDetail $facilityDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,FacilityDetail $facility_detail)
    {
        try{
            $facility_detail->delete();
            $request->session()->flash('toast', 'Detail Fasilitas berhasil dihapus');

        }
        catch(\Illuminate\Database\QueryException $ex){
            $request->session()->flash('toast', 'Detail Fasilitas gagal dihapus'.substr($ex->getMessage(), 0, 15));

        };
        return redirect('/facility_details');
    }
}
