<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\DetailOrder;
use App\Facility;
use App\Customer;
use App\CustomerType;
use App\FacilityType;
use Carbon\carbon;
use App\Folio;
use App\CustomSchedule;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return "Aaaa";
        $cstTyp= CustomerType::all();
        $cus=[];
        foreach ($cstTyp as $cstt) {
            $tmp=[];     
            if($cstt->name=="External"){
                $cuscat = Customer::where('customer_type_id',$cstt->id)->select('category')->distinct()->get();
                foreach( $cuscat as $cat){
                    $tmp=[];
                    $cuss = Customer::where('customers.customer_type_id','=',$cstt->id)->where('customers.category',$cat->category)->first();
                    for($i=1;$i<=12;$i++){
                        $ord = Order::where('customers.customer_type_id','=',$cstt->id)->where('customers.category',$cat->category)->join('customers', 'customers.id', '=', 'orders.customer_id')->whereMonth("orders.date",$i)->count();
                    
                        $dt = Carbon::createFromFormat("n",$i);
                        $tmp = array_add($tmp,$dt->format("M"),$ord);
                    }
                    $cus = array_add($cus,$cuss->category_label,$tmp);    
                }
            } 
            else{
                for($i=1;$i<=12;$i++){
                    $ord = Order::where('customers.customer_type_id','=',$cstt->id)->join('customers', 'customers.id', '=', 'orders.customer_id')->whereMonth("orders.date",$i)->count();
                    $dt = Carbon::createFromFormat("n",$i);
                    $tmp = array_add($tmp,$dt->format("M"),$ord);
                }
                $cus = array_add($cus,$cstt->name,$tmp);    
            }
                    
        }
        $ords = DetailOrder::whereYear("created_at",date("Y"))->where("item_type","LIKE","%Facility")->get();
        $facs=[];
        foreach ($ords as $or) {
            $fa = Facility::find($or->item_id);
            $tmp=[];                
            if($fa){

            $ord = Order::where('detail_orders.item_id','=',$fa->id)->where("detail_orders.item_type","LIKE","%Facility")->join('detail_orders', 'orders.id', '=', 'detail_orders.order_id')->whereYear("orders.date",date("Y"))->count();
            $facs = array_add($facs,$fa->name,$ord);  
            }            
        }
        // return $facs;
        // return $cus;

        $custom_schedule = CustomSchedule::query();
        $folios =Folio::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value) && $key=='startdate') {
                    $startdate=Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
                    $custom_schedule = $custom_schedule->where($key, '>=', $startdate);
                    $folios = $custom_schedule->where('check_in', '>=', $startdate);
                }
                if(!empty($value) && $key=='enddate') {
                    $enddate=Carbon::createFromFormat('Y-m-d', $value)->endOfDay();
                    $custom_schedule = $custom_schedule->where($key, '<=', $enddate);
                    $folios = $custom_schedule->where('check_out', '>=', $startdate);
                    
                }
            }
        }
        $carbonevent = array();
        // return $custom_schedule;
        $adds = clone $folios;
        $adds = $adds->where("folio_type","LIKE","%Addon")->orderBy('id','DESC')->paginate(10);        
        $folios = Folio::join("detail_facilities","folio_id","detail_facilities.id")->join("detail_orders","detail_order_id","detail_orders.id")->join("orders","order_id","orders.id")->join("customers","customer_id","customers.id")->where("folio_type","LIKE","%DetailFacility")->selectRaw("null as id,check_in as start, check_out as end, detail_facilities.name as facility, customers.name as description, orders.id as order_id, orders.order_code as order_code");
        $custom_schedule = $custom_schedule->join("detail_facilities","detail_facility_id","detail_facilities.id")->selectRaw("custom_schedules.id, startdate as start, enddate as end, detail_facilities.name as facility, description, null as order_id, null as order_code")->union($folios)->get();
        
        // return $carbonevent;
        // return view('detail_facilities_custom_schedules.index', compact('custom_schedule', 'filter', 'detail_facility'));
        return view('home')
        ->with('cus',$cus)
        ->with('adds',$adds)
        ->with('schedules',$custom_schedule)
        ->with('facs',$facs);
    }
}
