<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Menu;
use DB; 
use Illuminate\Validation\ValidationException;
use Illuminate\Database\QueryException;
use App\Addon;


class MenuController extends Controller
{
    public function index(Addon $addon,Request $request)
    {
        $menus = Menu::query();
        $menus=$menus->where('addon_id',$addon->id);
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $menus  = $menus->where(DB::raw("lower(".$key.")"), "LIKE", "%".strtolower($request->q).'%');
                }
            }
        }
        $menus=$menus->orderBy('id','DESC')->paginate(20);
        return view('menu.index')
        ->with('menus', $menus)
        ->with('addon', $addon)
        ->with('filter',$filter);
    }
    public function create(Addon $addon){
        $menu = new Menu;
        
        $addons=Addon::where('id',$addon->id)->pluck('name','id');

        return view('menu._form')->with('addons',$addons)->with('addon',$addon)->with('makanan',[])->with('menu', $menu);
    }

    public function store(Request $request,Addon $addon){
        DB::beginTransaction();
        $errors = [];
        //$errors = array_add($errors, 'request_field', 'Terapat field Yang kurang tepat');
        //if($chk){
        //    DB::rollBack();
        //    throw ValidationException::withMessages($errors);
        //} 
        $menu = new Menu;
        $menu->fill($request->all());
        $menu->save();
        DB::commit();
        $request->session()->flash('toast', 'Menu berhasil ditambahkan!');
        return redirect()->route('addons.menus.index',$addon->id);
    }
    public function edit($id,$addon){
        $menu = Menu::find($id);
        $addon = Addon::find($addon);
        $addons=Addon::where('id',$addon->id)->pluck('name','id');
        $makanan=[];
        foreach($menu->item_label as $mak){
            
            $makanan=array_add($makanan,$mak,$mak);
        }
        // return $makanan;
        return view('menu._form')->with('addons',$addons)->with('addon',$addon)->with('makanan',$makanan)->with('menu',$menu);
    }
    public function update(Addon $addon,$id,Request $request){
        DB::beginTransaction();
        $menu = Menu::find($id);
        $menu->fill($request->all());
        $errors = [];
        //$errors = array_add($errors, 'request_field', 'Terapat field Yang kurang tepat');
        //if($chk){
        //    DB::rollBack();
        //    throw ValidationException::withMessages($errors);
        //} 
        $menu->save();
        DB::commit();
        $request->session()->flash('toast', 'Menu  berhasil diubah!');
        return redirect()->route('addons.menus.index',$addon);
    }
    public function show(Menu $menu,Addon $addon){
        return view('menu.show')->with('menu', $menu);
    }
    public function destroy(Addon $addon,Menu $menu, Request $request ){
        // return $addon;
        try {
            $menu->delete();
            $request->session()->flash('toast', 'Menu berhasil dihapus!');
        } catch(QueryException $ex){ 
            $request->session()->flash('error', 'Menu gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        return redirect()->route('addons.menus.index',$addon);
    }
    //API CONTROLLER FUNCTION
    public function api_create(Request $request)
    {
        $menu = Menu::create($request->all());
        return response()->json([
            'code' => 200,
            'status' => true,
            'message' => "Success",
            'data' => $menu
        ], 200);
    }

    public function api_show(Request $request)
    {
        if(!$request->id){
            $menu = Menu::all();   
        } else {
            $menu = Menu::findOrFail($request->id);
        }
        return response()->json([
            'code' => 200,
            'status' => true,
            'message' => "Success",
            'data' => $menu
        ], 200);
    }

    public function api_update(Request $request, $id)
    {
        $menu = Menu::find($id);
        if(!$menu){
            return response()->json([
                'code' => 200,
                'status' => false,
                'message' => "Failed"
            ], 200);
        }
        $menu->update($request->all());

        return response()->json([
            'code' => 200,
            'status' => true,
            'message' => "Success",
            'data' => $menu
        ], 200);
    }

    public function api_delete(Request $request, $id)
    {
        $menu = Menu::find($id);
        if(!$menu){
            return response()->json([
                'code' => 200,
                'status' => false,
                'message' => "Failed"
            ], 200);
        }
        Menu::destroy($id);
        
        return response()->json([
            'code' => 200,
            'status' => true,
            'message' => "Success"
        ], 200);
    }
}