<?php

namespace App\Http\Controllers;

use App\FacilityType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
class FacilityTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $facility_type = FacilityType::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $facility_type = $facility_type->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }
        
        $facility_type = $facility_type->paginate(20);
        return view('facility_types.index', compact('facility_type', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $facility_type = new FacilityType;
        return view('facility_types.create', compact('facility_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $facility_type = new FacilityType;
        $check = FacilityType::orderBy('id','DESC')->first();
            if($check)
                $facility_type->id = $check->id+1;
        $facility_type->fill($request->all());
        $facility_type->save();
        $facility_type->slug = str_slug($facility_type->name, '_');
        $facility_type->save();
        $request->session()->flash('toast', 'Tipe berhasil ditambahkan!');
        
        return redirect('/facility_types');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FacilityType $facility_type
     * @return \Illuminate\Http\Response
     */
    public function show(FacilityType $facility_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FacilityType $facility_type
     * @return \Illuminate\Http\Response
     */
    public function edit(FacilityType $facility_type)
    {
        return view('facility_types.create', compact('facility_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\FacilityType        $facility_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FacilityType $facility_type)
    {
        $facility_type->fill($request->all());
        $facility_type->slug = str_slug($facility_type->name, '_');
        $facility_type->save();
        $request->session()->flash('toast', 'Tipe berhasil diubah!');
        
        return redirect('/facility_types');     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FacilityType $facility_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(FacilityType $facility_type, Request $request)
    {
        try {
            $facility_type->delete();
            $request->session()->flash('toast', 'Tipe berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Tipe gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/facility_types');
    }

    public function get(Request $request)
    {
        $type = FacilityType::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($request->q)."%")->pluck('name', 'id');
        return $type;
    }
}