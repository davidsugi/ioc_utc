<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EducationTour;
use App\Gallery;
use Intervention\Image\ImageManagerStatic as Image;

class EducationTourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $education_tour = EducationTour::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $education_tour = $education_tour->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }
        

        $education_tour = $education_tour->paginate(10);
        return view('education_tours.index', compact('education_tour', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $education_tour = new EducationTour;
        return view('education_tours.create', compact('education_tour'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $input = $request->except(['images','files']);
        
        $educaton_tour = new EducationTour;
        $check = EducationTour::orderBy('id','DESC')->first();
                if($check)
                    $educaton_tour->id = $check->id+1;
        $educaton_tour->fill($input);
        $educaton_tour->save();
        $educaton_tour->slug = str_slug($educaton_tour->id.'_'.$educaton_tour->title, '_');
        $dom = new \DomDocument();
        $dom->loadHtml($request->description);
        $images = $dom->getElementsByTagName('img');
        foreach($images as $img){
			$src = $img->getAttribute('src');
			
			// if the img source is 'data-url'
			if(preg_match('/data:image/', $src)){
				
				// get the mimetype
				preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
				$mimetype = $groups['mime'];
				
				// Generating a random filename
				$filename = uniqid();
				$filepath = "/image/$filename.$mimetype";
	
				// @see http://image.intervention.io/api/
				$image = Image::make($src)
				  // resize if required
				  /* ->resize(300, 200) */
				  ->encode($mimetype, 100) 	// encode file to the specified mimetype
				  ->save(public_path($filepath));
				
				$new_src = asset($filepath);
				$img->removeAttribute('src');
				$img->setAttribute('src', $new_src);
			} // <!--endif
        } // <!--endforeach
        
        $educaton_tour->description = $dom->saveHTML();

        // return $educaton_tour;
        $educaton_tour->save();
        $file = $request->file('images');
        foreach($file as $f){
            $filename = $f->getClientOriginalName();
            $path = public_path().'/image/';
            $f->move($path, $filename);
            
            $gallery = new Gallery;
             $check = Gallery::orderBy('id','DESC')->first();
                if($check)
                    $gallery->id = $check->id+1;
            $gallery->image_path = $filename;
            $gallery->galleriable_id = $educaton_tour->id;
            $gallery->galleriable_type = 'EducationTour';            
            $gallery->save();
            $gallery->slug = str_slug($gallery->id.'_'.$gallery->image, '_');
            $gallery->save();
        }
        $request->session()->flash('toast', 'Eduwisata berhasil ditambahkan!');
        return redirect('/education_tours/'.$educaton_tour->id);      
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(EducationTour $education_tour)
    {

        $gallery = Gallery::query();
        $gallery = $gallery->where('galleriable_type', '=', 'EducationTour');
        $gallery = $gallery->where('galleriable_id', '=', $education_tour->id);
        $gallery = $gallery->paginate(5, ['*'], 'gallery');

        return view('education_tours.show', compact('education_tour', 'gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationTour $education_tour)
    {
        return view('education_tours.create', compact('education_tour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EducationTour $education_tour)
    {
        $education_tour->fill($request->all());
        $education_tour->slug = str_slug($education_tour->id.'_'.$education_tour->title, '_');
        $education_tour->save();
        $request->session()->flash('toast', 'Eduwisata berhasil diubah!');
        
        return redirect('/education_tours/'.$education_tour->id); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationTour $education_tour, Request $request)
    {
        try {
            $education_tour->delete();
            $request->session()->flash('toast', 'Eduwisata berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Eduwisata gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/education_tours');
    }
}
