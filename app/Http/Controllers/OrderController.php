<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\DetailFacility;
use App\Facility;
use App\Addon;
use App\DetailOrder;
use App\Customer;
use Carbon\Carbon;
use App\Invoice;
use App\Bundle;
use App\Folio;
use H;
use App\BundleDetail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use DB;
use App\Mail\Email;
use Illuminate\Support\Facades\Mail;
use App\User;
use Auth;

class OrderController extends Controller
{

    public function index(Request $request){  
        $all= Order::all()->count();
        $confirmed = Order::where("status",">=",1)->where("status","<",3)->count();
        $finished = Order::where("status",">=",3)->where("status","<",4)->count();
        // return $finished;

        $orders = Order::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    if($key=="start_date") {
                        $orders  = $orders->whereDate("date", '>=', $value);
                    }
                    else if($key=="name") {
                        $custid = Customer::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($value)."%")->orwhere(DB::raw("lower(company_name)"), "LIKE", "%".strtolower($value)."%")->pluck('id');  
                        $orders  = $orders->whereIn("customer_id", $custid);
                    }
                    else{
                        $orders  = $orders->whereDate("date", '<=', $value);
                    }
                }
            }
        }
        $status=[];
        if(isset($request->status)){
            $orders = $orders->whereIn("status",$request->status); 
            $status=$request->status;
        }
        // return $status;
        $orders= $orders->orderBy('id','DESC')->paginate(50);
        
        return view('orders.index')
        ->with('filter', $filter)
        ->with('status', $status)
        ->with('all', $all)
        ->with('confirmed', $confirmed)
        ->with('finished', $finished)
        ->with('orders', $orders);
    }
    public function create(){  
        $fac = Facility::find(23);
        $all_array=[];
        $facilities = Facility::all();
        $bundles = Bundle::all();
        $i=0;
        foreach ($facilities as $facs) {
            $all_array[ "F-".$facs->id ] = $facs->name;
        }
        foreach ($bundles as $bundl) {
            $all_array[ "B-".$bundl->id ] = $bundl->name; 
        }

        $customers = Customer::all()->pluck("nameLabel", "id");
        $adds= Addon::pluck("name", "id");        
        $order = new Order;
        $order->menginap=1;
         
        return view('orders._form')
        ->with('customers', $customers)
        ->with('facs', $all_array)
        ->with('adds', $adds)
        ->with('order', $order);
    }

    public function Json($jsons,$arraytoloop,$customer_name,$prod_name,$amount,$diskon=0)
    {
        foreach($arraytoloop as $key => $li){
            $arrayresult=[];
            $arrayresult=array_add($arrayresult,'pengguna',$customer_name);
            $itemm = json_decode($key);
            $category = $itemm[0];
            $days = $li;
            $price = $itemm[1];
            $prdnm=$prod_name."(".$category.")";
            $arrayresult=array_add($arrayresult,'item',$prdnm);
            $arrayresult=array_add($arrayresult,'jmlitem',$amount);
            $arrayresult=array_add($arrayresult,'jmlhari',$li);
            $arrayresult=array_add($arrayresult,'biaya',$price);
            $arrayresult=array_add($arrayresult,'diskon',$diskon);
            $jsons[]=$arrayresult;
        }
        return $jsons;
    }

    public function store(Request $req)
    {
        // return $req->all();
        DB::beginTransaction();
        $errors =[];        
        $order = new Order;
        $order->fill($req->except("per_person","facil","note","ids","ida","anote","adisc_type","fdisc_type","disc_type","adate", "price","duration", "addons", "amount", "check_in", "person", "discon", "subta","subt","coden","disca", "taxa"));        
        $order->menginap=isset($req->menginap)? 1:0;
        if($req->globaldisc==null)
            $order->globaldisc=0; 
        $check = Order::orderBy('id','DESC')->first();
            if($check)
                $order->id = $check->id+1;
        if($req->disc==null){
                $order->disc = 0;
            }
            $order->save();
                    $date_start = $order->date->startOfMonth();
        $date_end = $order->date->endOfMonth();
        $order_counter = Order::where('customer_id', $order->customer_id)->whereBetween('date', [$date_start,$date_end])->count();
        $order->order_code = "ORD/".str_pad($order->customer_id,3,"0",STR_PAD_LEFT)."/".$order->date->format("d/m/y")."/".str_pad($order_counter,3,"0",STR_PAD_LEFT);
        $order->user_id = Auth::user()->id;
        $order->save();
        $cst= Customer::find($order->customer_id);        
        $i=1;
        $tot=0;
        $arra=[];
        $jsons=[];
        $amountsjson=[];
        $tar=[];
        if($req->facil){
            foreach ($req->facil as $fac) {
                // return $fac;
                if($fac==null)
                { continue; }
                $subtot=0;
                $fol= new Folio;
                $check = Folio::orderBy('id','DESC')->first();
                if($check)
                $fol->id = $check->id+1;

                $detail= new DetailOrder;
                $check = DetailOrder::orderBy('id','DESC')->first();
                if($check)
                $detail->id = $check->id+1;

                $detail->disc_type=$req->fdisc_type[$i];

                $id = explode("-",$fac);
                $detail->item_id = $id[1];
                $detail->amount= $req->duration[$i];
                $detail->amount2= 1;
                $detail->id = DetailOrder::orderBy('id','DESC')->first()->id+1;
                $detail->note= $req->note[$i];
                if($req->person[$i]!=null) {$detail->amount2= $req->person[$i];
                }
                $detail->disc=0;
                if( $req->discon[$i]!=null) { $detail->disc= $req->discon[$i];
                }
                $detail->order_id=$order->id;
                $detail->save();

                if($id[0]=="F"){
                    $item = Facility::find($id[1]);
                    $fol->folio_type = "App\\DetailFacility";
                    $thiid=1;
                    if( count($item->available($req->check_in[$i],$req->duration[$i])) > 0){
                        $thiid= $item->available($req->check_in[$i],$req->duration[$i])[0]->id;
                    }
                    else{
                        if(!array_key_exists ($i, $errors)){
                            $errors=array_add($errors,$i,"Fasilitas Tidak tersedia");
                         }
                    }
                    $fol->folio_id =$thiid;                
                    $tmps = $item->calDetprice($req->check_in[$i],$req->duration[$i],$detail->amount2,$detail->disc,$order->menginap,$cst->customer_type_id);   
                        
                    $subtot = $item->calprice($req->check_in[$i],$req->duration[$i],$detail->amount2,$order->menginap,$cst->customer_type_id);                    
                   
                    $persdiv = $detail->amount2;
                    $tojson= $detail->amount2;
                    if($item->per_person==1 && $persdiv < $item->minimum){
                        $persdiv= $item->minimum;
                        $tojson= $item->minimum;
                    }
                    if($item->per_person==0){
                        $persdiv=1;
                        $tojson=1;
                    }
                    $diskon=$detail->disc;
                    if($req->fdisc_type[$i]==0){
                        $diskon=($subtot*$req->discon[$i]/100);
                    }
                    $jsons=$this->Json($jsons,$tmps,$cst->name,$item->name,$tojson,$diskon);

                    $detail->price=  $subtot/$persdiv/$detail->amount;
                    $detail->item_type="App\\Facility";   
                    $detail->subtotal = $subtot-$diskon;
                    // if(Carbon::createFromFormat("Y-m-d", $req->check_in[$i])->isPast() && !Carbon::createFromFormat("Y-m-d", $req->check_in[$i])->isToday()){
                    //     $errors=array_add($errors,"dt".$i,"Tanggal Check in sudah lewat!");                    
                    // }
                    $fol->check_in=$req->check_in[$i];
                    $fol->check_out= Carbon::createFromFormat("Y-m-d", $req->check_in[$i])->adddays($req->duration[$i])->format("Y-m-d");
                    // if(!isset($req->menginap)){
                    //     $fol->check_out= $req->check_in[$i];
                    // }
                    
                    $fol->amount =  $req->duration[$i];
                    $fol->detail_order_id = $detail->id;    
                    // return  $req->duration[$i];        
                    $fol->save();
                }
                else{
                    $items = Bundle::find($id[1]);
                    $detail->item_type = "App\\Bundle"; 
                    $item = BundleDetail::where("bundle_id",$id[1])->get();
                    foreach ($item as $bundl) {
                        $fol= new Folio;
                        $check = Folio::orderBy('id','DESC')->first();
                        if($check)
                            $fol->id = $check->id+1;
                        if($bundl->bundl_type=="App\\Addons"){
                            $itm= Addon::find($bundl->bundl_id); 
                            // $fol->price= $itm->price; 
                            $fol->folio_type = "App\\Addon";       
                            $fol->amount = 1;
                        }
                        else{
                            $fol->folio_type = "App\\DetailFacility"; 
                            $facs = Facility::find($bundl->bundl_id);
                            $thiid=1;
                            if(count($facs->available($req->check_in[$i],$req->duration[$i])) > 0){
                                $thiid= $facs->available($req->check_in[$i],$req->duration[$i])[0]->id;
                            }
                            else{
                                if(!array_key_exists ($i, $errors)){
                                    $errors=array_add($errors,$i,"Fasilitas Tidak Tersedia");
                                }
                                $trigger=$i;
                            }
                            $facid =$thiid;   
                            $itm= DetailFacility::find($facid);
                            // $fol->price= $itm->facility->price;  
                            $fol->amount =  $req->duration[$i];
                        }  
                        $fol->folio_id= $itm->id;                    
                        $fol->check_in=$req->check_in[$i];
                        $fol->check_out= Carbon::createFromFormat("Y-m-d", $req->check_in[$i])->adddays($req->duration[$i])->format("Y-m-d");
                        // if(!isset($req->menginap)){
                        //     $fol->check_out= $req->check_in[$i];
                        // }
                        $fol->detail_order_id = $detail->id;
                        $fol->save();
                    }      
                $tmp=[];
                $tmps = $items->calDetprice($req->check_in[$i],$req->duration[$i],$detail->amount2,$detail->disc,$order->menginap,$cst->customer_type_id);  
                
                $subtot = $items->calprice($req->check_in[$i],$req->duration[$i],$detail->amount2,$order->menginap,$cst->customer_type_id);  
                
                $diskon=$detail->disc;
                if($req->fdisc_type[$i]==0){
                    $diskon=($subtot*$req->discon[$i]/100);
                }
                $persdiv = $detail->amount2;
                    $tojson= $detail->amount2;
                    if($items->per_person==1 && $persdiv < $items->minimum){
                        $tojson= $items->minimum;
                        $persdiv= $items->minimum;
                    }
                if($items->per_person==0){
                        $persdiv=1;
                        $tojson=1;
                    }
                $detail->price= $subtot/$persdiv/$detail->amount;
                $detail->subtotal=$subtot-$diskon;
                    
                $jsons=$this->Json($jsons,$tmps,$cst->name,$items->name,$persdiv,$diskon);

            }
                $detail->save();  
                $i++;
            }
        }
            // return $jsons;

            $i=1;
            if($req->addons){
                foreach ($req->addons as $fac) {
                    $item = Addon::find($fac);
                    $detail= new DetailOrder;
                    $detail->disc_type=$req->adisc_type[$i];
                    $check = DetailOrder::orderBy('id','DESC')->first();
                        if($check)
                            $detail->id = $check->id+1;
                    $detail->item_id = $fac;
                    $detail->amount= $req->amount[$i];
                    $detail->disc=0;
                    if( $req->disca[$i]!=null) {  
                        $detail->disc= $req->disca[$i];
                    }
                    $item->borrowed = $item->borrowed + $req->amount[$i];
                    if($item->borrowed > $item->stock && $item->limited==1){
                        $errors=array_add($errors,"a-".$i,"Addon tidak bisa dipinjam, karena stok kurang");
                        $trigger=$i;
                    }

                    $item->save();  
                    $detail->price= $item->price;
                    if( $req->price[$i]!=null) {  
                        $detail->price= $req->price[$i];
                    }
                    $tmp=[];
                    
                    $tmp = array_add($tmp,json_encode(['Addons',$detail->price]),1);
                    // $tmps = array_add($tmps,json_encode($tmp),$detail->amount);
                    $arra = array_add($arra,$item->name,json_encode($tmps));
                    
                    $detail->order_id=$order->id;
                    $detail->item_type = "App\\Addon";
                    // $detail->subtotal = $item->calprice($req->amount[$i],$detail->disc,$order->menginap,$cst->customer_type_id);
                     $diskon=$detail->disc;
                    if($detail->disc_type==0){
                        $diskon=(($detail->price * $detail->amount) * ($detail->disc/100));
                    }      
                    $detail->subtotal = ($detail->price * $detail->amount) - $diskon;
                    $jsons=$this->Json($jsons,$tmp,$cst->name,$item->name,$detail->amount,$diskon);            
                                     
                    $detail->save();
                    $fol= new Folio;       
                    $check = Folio::orderBy('id','DESC')->first();
                        if($check)
                            $fol->id = $check->id+1;
                    $date=Carbon::parse($req->adate[$i])->format("Y-m-d H:i:s");
                    $fol->check_in=$date;
                    $fol->check_out=$date;
                    $fol->description = $req->anote[$i];
                    $fol->folio_id= $fac;
                    $fol->folio_type = "App\\Addon";
                    $fol->amount =  $req->amount[$i];
                    // $fol->price= $item->price;
                    $fol->detail_order_id = $detail->id;
                    $fol->save();
                    $i++; 
                }
            }
            $order->other=json_encode($jsons);
            $order->save();
            if(count($errors)>0){
                DB::rollBack();
                throw ValidationException::withMessages($errors);
            }
            $req->session()->flash('toast', 'Order berhasil ditambah!');
            $user= User::where("emailable",1)->get();
            foreach($user as $usr){
                Mail::to($usr->email)->send(new Email($order));
                if(Mail::failures()){
                // $string="There is some Errors, Try Again Later";
                }                
            }
            // $string="Succesfuly emailed to admin";
            // return $arra;
            
            DB::commit();
            return redirect()->route('orders.show',["id"=> $order->id]);            
           // return $emptys;
    }
    public function edit($id)
    {          
        $order = Order::find($id);
        $customers = Customer::all()->pluck("nameLabel", "id");
        $adds= Addon::pluck("name", "id");
         $array=[];
        $facilities = Facility::all();
        $bundles = Bundle::all();
        $i=0;
        foreach ($facilities as $facs) {
            $tmp = [];
            $array = array_add($array, "F-".$facs->id , $facs->name);
            $i++;
        }
        foreach ($bundles as $bundl) {
            $tmp = [];            
            $array = array_add($array  ,"B-".$bundl->id , $bundl->name);
            $i++;
        }
        $addd= DetailOrder::where("order_id", $id)->where("item_type", "LIKE", "%Addon")->get();
        $facd= DetailOrder::where("order_id", $id)->where("item_type", "LIKE", "%Facility")->orwhere("item_type", "LIKE", "%Bundle")->where("order_id", $id)->get();
        $ids = DetailOrder::where("order_id", $id)->where("item_type", "LIKE", "%Facility")->orwhere("item_type", "LIKE", "%Bundle")->where("order_id", $id)->pluck("id");
        $ida = DetailOrder::where("order_id", $id)->where("item_type", "LIKE", "%Addon")->pluck("id");     
        return view('orders._form')
        ->with('adds', $adds)
        ->with('addd', $addd)
        ->with('facs', $array)
        ->with('facd', $facd)
        ->with('ids', $ids)
        ->with('ida', $ida)
        ->with('customers', $customers)
        ->with('order', $order);
    }
    public function update($id, Request $req)
    {
        // return "aaa";
        // return $req;
        DB::beginTransaction();    
        $errors =[];                       
        $order = Order::find($id);
        $order->fill($req->except("per_person","facil", "note", "duration","price", "addons","anote","adisc_type","fdisc_type","disc_type","adate","disc_type", "amount", "ids", "ida", "check_in", "person","discon", "subta","subt","coden","disca", "taxa"));
        $order->menginap=isset($req->menginap)? 1:0;        
        if($req->globaldisc==null)
            $order->globaldisc=0; 
        $order->save();
        $detid = DetailOrder::where("order_id",$order->id);
        $deldet = clone $detid;
        $addonupdate= clone $detid;
        $cst= Customer::find($order->customer_id);        
        //redopreovious transaction
        $addonupdate = $addonupdate->where("item_type","LIKE","%Addon")->get();
        foreach ($addonupdate as $key) {
            $adds = Addon::find($key->item_id);
            $adds->borrowed = $adds->borrowed - $key->amount;
            $adds->save();
        }
        $delfol = Folio::whereIn("detail_order_id", $detid->pluck("id"))->delete(); 
        $deldet->delete(); 
        $i=1;


        $arra=[];
        $jsons=[];
        $amountsjson=[];

        if(isset($req->facil)){
        foreach ($req->facil as $fac) {
            $detail= new DetailOrder;    
            $detail->note= $req->note[$i];
            $detail->disc_type=$req->fdisc_type[$i];
            $check = DetailOrder::orderBy('id','DESC')->first();
                if($check)
                    $detail->id = $check->id+1;
            $fol= new Folio;       
            $fol->id=  Folio::orderBy('id','DESC')->first()->id+1;               
             $subtot=0;
                // $detail= new DetailOrder;
                $id = explode("-",$fac);
                $detail->item_id = $id[1];
                $detail->amount= $req->duration[$i];
                $detail->amount2= 1;
                if($req->person[$i]!=null) {$detail->amount2= $req->person[$i];
                }
                $detail->disc=0;
                if( $req->discon[$i]!=null) { $detail->disc= $req->discon[$i];
                }
                $detail->order_id=$order->id;
                $detail->save();

                if($id[0]=="F"){
                    $item = Facility::find($id[1]);
                    $fol->folio_type = "App\\DetailFacility";
                    $thiid=1;
                    if( count($item->available($req->check_in[$i],$req->duration[$i])) >  0){
                        $thiid= $item->available($req->check_in[$i],$req->duration[$i])[0]->id;
                    }
                    else{
                        // return "sadasdasdasda";
                        if(!array_key_exists ($i-1, $errors)){
                            $errors=array_add($errors,$i,"Fasilitas Tidak tersedia");
                         }
                    }
                    $fol->folio_id =$thiid;                
                    // $fol->price= $item->facility->price; 
                    
                $fol->check_in=$req->check_in[$i];
                $fol->check_out= Carbon::createFromFormat("Y-m-d", $req->check_in[$i])->adddays($req->duration[$i])->format("Y-m-d");
                    // if(!isset($req->menginap)){
                    //     $fol->check_out= $req->check_in[$i];
                    // }
                $fol->amount =  $req->duration[$i];
                $fol->detail_order_id = $detail->id;            
                $fol->save();
                $tmps = $item->calDetprice($req->check_in[$i],$req->duration[$i],$detail->amount2,$detail->disc,$order->menginap,$cst->customer_type_id);   

                 $subtot = $item->calprice($req->check_in[$i],$req->duration[$i],$detail->amount2,$order->menginap,$cst->customer_type_id);   
                    $persdiv = $detail->amount2;
                    $tojson= $detail->amount2;

                    if($item->per_person==1 && $persdiv < $item->minimum){
                        $persdiv= $item->minimum;
                        $tojson= $item->minimum;

                    }
                    if($item->per_person==0){
                        $persdiv=1;
                        $tojson=1;

                    }

                    $diskon=$detail->disc;
                    if($detail->disc_type==0){
                        $diskon=($subtot * ($detail->disc/100));
                    }      
                    $jsons=$this->Json($jsons,$tmps,$cst->name,$item->name,$tojson,$diskon);
                    $detail->subtotal = $subtot - $diskon;
                               
                    $detail->price=  $subtot/$persdiv/$detail->amount;
                    $detail->item_type = "App\\Facility";   
                
                }
                else{
                    $items = Bundle::find($id[1]);
                    // $detail->price= $item->price;  
                    $detail->item_type = "App\\Bundle"; 
                    $item = BundleDetail::where("bundle_id",$id[1])->get();
                    foreach ($item as $bundl) {
                        $fol= new Folio;
                         $check = Folio::orderBy('id','DESC')->first();
                        if($check)
                            $fol->id = $check->id+1;
                        if($bundl->bundl_type=="App\\Addons"){
                            $itm= Addon::find($bundl->bundl_id); 
                            // $fol->price= $itm->price; 
                            $fol->folio_type = "App\\Addon";   
                            $fol->amount = 1;

                        }
                        else{
                            $fol->folio_type = "App\\DetailFacility"; 
                            $facs = Facility::find($bundl->bundl_id);
                            // if($facs->per_person==0)
                            //     $detail->amount2=1;
                            $thiid=1;
                            if(count($facs->available($req->check_in[$i],$req->duration[$i])) > 0){
                                $thiid= $facs->available($req->check_in[$i],$req->duration[$i])[0]->id;
                            }
                            else{
                                if(!array_key_exists ($i, $errors)){
                                    $errors=array_add($errors,$i,"Fasilitas Tidak Tersedia");
                                }
                                $trigger=$i;
                            }
                            $facid =$thiid;   
                            $itm= DetailFacility::find($facid);
                            $fol->amount =  $req->duration[$i];

                            // $fol->price= $itm->facility->price;                         
                        }  
                        $fol->folio_id= $itm->id;                    
                        $fol->check_in=$req->check_in[$i];
                        $fol->check_out= Carbon::createFromFormat("Y-m-d", $req->check_in[$i])->adddays($req->duration[$i])->format("Y-m-d");
                        // if(!isset($req->menginap)){
                        //     $fol->check_out= $req->check_in[$i];
                        // }
                        $fol->detail_order_id = $detail->id;
                        $fol->save();
                    }                
                    $tmp=[];
                $tmps = $items->calDetprice($req->check_in[$i],$req->duration[$i],$detail->amount2,$detail->disc,$order->menginap,$cst->customer_type_id);  
                
                     $subtot = $items->calprice($req->check_in[$i],$req->duration[$i],$detail->amount2,$order->menginap,$cst->customer_type_id);  
                        $persdiv = $detail->amount2;
                            if($items->per_person==1 && $persdiv < $items->minimum){
                                $persdiv= $items->minimum;
                            }
                            if($items->per_person==0){
                                $persdiv=1;
                                $tojson=1;
                            }
                         $diskon=$detail->disc;
                        if($detail->disc_type==0){
                            $diskon=($subtot * ($detail->disc/100));
                        }      
                        $detail->subtotal = $subtot - $diskon;
                                
                    $detail->price=$subtot/$persdiv/$detail->amount; 
                    $jsons=$this->Json($jsons,$tmps,$cst->name,$items->name,$persdiv,$diskon);

                }
                // $detail->subtotal = ($detail->price * $detail->amount * $detail->amount2) - (($detail->price * $detail->amount * $detail->amount2) * ($detail->disc/100));            
                $detail->save();            
                $i++;
        }
    }
        $i=1;

        if(isset($req->addons)){
        foreach ($req->addons as $fac) {
            $item = Addon::find($fac);
            $detail= new DetailOrder;
            $detail->disc_type=$req->adisc_type[$i];
            $check = DetailOrder::orderBy('id','DESC')->first();
                if($check)
                    $detail->id = $check->id+1;
            $detail->item_id = $fac;
            $detail->amount= $req->amount[$i];
            $item->borrowed = $item->borrowed + $req->amount[$i];
            if($item->borrowed > $item->stock && $item->limited==1){
                $errors=array_add($errors,"a-".$i,"Addon tidak bisa dipinjam, karena stok kurang");
                $trigger=$i;
            }
            $item->save();           
            $detail->price= $item->price;
            if( $req->price[$i]!=null) {  
                $detail->price= $req->price[$i];
            }
            $detail->order_id=$order->id;
            $detail->subtotal= $req->subta[$i];            
            $detail->disc= $req->disca[$i];
            $detail->item_type = "App\\Addon";
            $diskon=$detail->disc;
            if($detail->disc_type==0){
                $diskon=(($detail->price * $detail->amount) * ($detail->disc/100));
            }
            $tmp=[];
            $tmp = array_add($tmp,json_encode(['Addons',$detail->price]),1);
            $arra = array_add($arra,$item->name,json_encode($tmps));
            $jsons=$this->Json($jsons,$tmp,$cst->name,$item->name,$detail->amount,$diskon);            
                    
            $detail->subtotal = ($detail->price * $detail->amount) - $diskon;                        
            $detail->save();
            $del= Folio::where("detail_order_id", $detail->id)->delete();
            $fol= new Folio;   
            $check = Folio::orderBy('id','DESC')->first();
                if($check)
                    $fol->id = $check->id+1;
            $date=Carbon::parse($req->adate[$i])->format("Y-m-d H:i:s");            
            $fol->check_in=$date;
            $fol->check_out=$date;
            $fol->description = $req->anote[$i];
            $fol->folio_id= $fac;
            $fol->folio_type = "App\\Addon";
            $fol->amount =  $req->amount[$i];
            // $fol->price= $item->price;
            $fol->detail_order_id = $detail->id;            
            $fol->save();
            $i++;
        }
    }
    $order->other=json_encode($jsons);
    $order->save();
             if(count($errors)>0){
                DB::rollBack();
                throw ValidationException::withMessages($errors);
            }
        if($order->payableLabel>0){
            if($order->paid_label==0)
                $order->lunas=0;
            else
                $order->lunas=1;
        }
        elseif($order->payableLabel<0){
            $order->lunas=3;
        }
        else{
            $order->lunas=2;
        }
            $order->save();
        

        $req->session()->flash('toast', 'Order berhasil diubah!');
        $user= User::where("emailable",1)->get();
        $order->status=5;
            // foreach($user as $usr){
            //     Mail::to($usr->email)->send(new Email($order));
            //     if(Mail::failures()){
            //     // $string="There is some Errors, Try Again Later";
            //     }                
            // }

        DB::commit();        
        return redirect()->route('orders.show',["id"=> $order->id]);
    }
    public function show(Order $order)
    {
        $facil = DetailOrder::where("item_type", "LIKE", "%Facility%")->where("order_id", "=", $order->id)->orwhere("item_type", "LIKE", "%Bundle%")->where("order_id", "=", $order->id)->get();
        $add = DetailOrder::where("order_id", "=", $order->id)->where("item_type", "LIKE", "%Addon%")->get(); 
        // $order->status=5;
        // return (new Email($order))->render();
        
        return view('orders.show')
        ->with('facils', $facil)
        ->with('adds', $add)
        ->with('order', $order);
    }
    public function destroy(Order $order, Request $req)
    {
        $order->status=4;
        $dords = DetailOrder::where("order_id", $order->id)
        ->where("item_type", "LIKE", "%Addon")->get();    
        foreach($dords as $dord){
            if(count($dord->folio)>0){
                    $ad =Addon::find($dord->item_id);
                    $ad->borrowed = $ad->borrowed - $dord->amount;
                    $ad->save();
            }
        } 
        $dords = DetailOrder::where("order_id", $order->id)
        ->where("item_type", "LIKE", "%Facility")->get();  
        foreach($dords as $dord){
        foreach ($dord->folio as $fol) {
                    $fol->active=0;
                    $fol->save();
                }
            }

        $order->save();
        $user= User::where("emailable",1)->get();
            foreach($user as $usr){
                Mail::to($usr->email)->send(new Email($order));
                if(Mail::failures()){
                // $string="There is some Errors, Try Again Later";
                }                
            }
        $req->session()->flash('toast', 'Order berhasil dibatalkan!');
        return redirect()->route('orders.show',$order->id);
    }

    public function stat(Request $req)
    {
        
        $ord= Order::find($req->id);
        $fols = Folio::whereIn("detail_order_id",$ord->detail_order->pluck("id"))->get();
        $adds = DetailOrder::where("order_id",$ord->id)->where("item_type","LIKE","%Addon")->get();
    
        $ord->status++;
        if($ord->status==3){
            if($ord->lunas!=2){
                return redirect()->route('orders.invoices.create',["order"=> $ord->id,"checkout"=>1]);                
            }
            foreach ($adds as $add) {
                $ad =Addon::find($add->item_id);
                $ad->borrowed = $ad->borrowed-$add->amount;
                $ad->save();
            }
            foreach ($fols as $fol) {
                $fol->active=0;
                // $fol->actual_out=date("Y-m-d");                
                $fol->save();
            }

        }
        else if($ord->status==2){
            foreach ($fols as $fol) {
                // $fol->actual_in=date("Y-m-d");
                $fol->save();
            }
        }
        $ord->save();
        // $user= User::where("emailable",1)->get();
        //     foreach($user as $usr){
        //         Mail::to($usr->email)->send(new Email($ord));
        //         if(Mail::failures()){
        //         // $string="There is some Errors, Try Again Later";
        //         }                
        //     }
        return redirect()->route('orders.show',["id"=> $req->id]);        
    }

    public function folio($order,Request $req){
        // return $req;
        $folios = Folio::query();
        $filter = [];
        $ord= DetailOrder::where("order_id",$order)->pluck("id");
        $folios = $folios->whereIn("detail_order_id",$ord);
        if(isset($req->filter)) {
            $filter = $req->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    if($key=="check_in")
                        $folios  = $folios->whereDate('check_out','>=',$value);
                    if($key=='check_out')
                         $folios  = $folios->whereDate('check_in','<=',$value);
                }
            }
        }
        $fac= clone $folios;
        
        $fac = $fac->where("folio_type","LIKE","%DetailFacility")->orderBy('id','DESC')->paginate(10,['*'], 'page');       
        $add=$folios->where("folio_type","LIKE","%Addon")->orderBy('id','DESC')->paginate(10,['*'], 'add_page');
        
        return view('folio.index')
        ->with('order',$order)
        ->with('adds',$add)
        ->with('facs',$fac)
        ->with('filter',$filter);
    }
    
    //api
    public function get(Request $req)
    {
        $cus = Customer::where("name", "LIKE", "%".$req->q."%")->pluck("id");
        $ord = Order::whereIn("customer_id", $cus)->orWhere("id", "LIKE", "%".$req->q."%")->get()->pluck("selectLabel", "id");
        return $ord;
    }

    public function getDet(Request $req)
    {
        $add = Addon::where("name", "LIKE", "%".$req->q)->active()->pluck("id");
        $fac = DetailFacility::where("name", "LIKE", "%".$req->q)->pluck("id");
        $arr = DetailOrder::where("order_id", $req->b)->whereIn("item_id", $fac)->where("item_type", "App\\DetailFacility")->orWhere("order_id", $req->b)->WhereIn("item_id", $add)->where("item_type", "App\\Addon")->get()->pluck("selectLabel", "id");
        return $arr;
    }

    public function getPrice(Request $req){
        $cst=0;
        if($req->id==""){
            return 0;
        }
        if($req->cs){
            $cust =Customer::find($req->cs);
            $cst = $cust->customer_type_id;
        }
        // return $cst;
        $id = explode("-",$req->id);
        if($id[0]=="B"){
            $fac= Bundle::find($id[1]);
        }
        else{
            $fac= Facility::find($id[1]);            
        }
        $tot = $fac->calPrice($req->t,$req->d,$req->p,$req->s,$cst);
        if($req->dtyp==0){
            $tot -= $tot * $req->c/100;
        }
        else{
            $tot -= $req->c;
        }
        return $tot;
    }

    public function getAddonPrice(Request $req){
        $cst=0;
        if($req->id==""){
            return 0;
        }
        if($req->cs){
            $cust =Customer::find($req->cs);
            $cst = $cust->customer_type_id;
        }
        // return $cst;
        $fac= Addon::find($req->id);
        $tot = $fac->calPrice($req->a,$req->s,$cst);
         if($req->dtyp==0){
            $tot -= $tot * $req->c/100;
        }
        else{
            $tot -= $req->c;
        }
        return $tot;
    }

    public function getSch(Request $req)
    {
        $ord = DetailOrder::where("item_type", "LIKE", "%DetailFacility")->get();
        $data = [];
        foreach ($ord as $key => $value) {
            $data[$key] = [ "title"=>$value->order->orderLabel." - ".$value->statusLabel,
                            "start"=>$value->check_in->format("Y-m-d"),
                            "end"=>$value->check_out->format("Y-m-d"),
                            "allDay"=>false,
                            "backgroundColor"=> $value->status == 1 ? "#009600" : "#f39c12",
                            // "borderColor"=>"#000000" 
                        ];    
        }
        
        return json_encode($data);
    }
    //api
    //param: start_date, end_date, month, year, day, id,state(1234), today(1), 
    public function getOrder(Request $req){
        $ord = Order::query();
        if($req->start_date) 
            $ord = $ord->whereDate("date", '>=', $req->start_date);
        if($req->end_date)
            $ord  = $ord->whereDate("date", '<=', $req->end_date);   
        if($req->date)
            $ord  = $ord->whereDate("date", '=', $req->date);   
        if($req->month)
            $ord  = $ord->whereMonth("date", '=', $req->month);   
        if($req->year)
            $ord  = $ord->whereYear("date", '=', $req->year);   
        if($req->day)
            $ord  = $ord->whereDay("date", '=', $req->day);           
        if($req->id)
            $ord  = $ord->where("id", '=', $req->id);                   
        if($req->state)
            $ord  = $ord->where("status", '=', $req->state);                           
        if($req->today)
            $ord  = $ord->whereDate("date", '=', carbon::now()->format("Y-m-d"));
        
         return   $ord->with('detail_order', 'customer', 'invoice', 'coupon','detail_order.item','customer.customer_type','customer.city' )->get();
    }
}
