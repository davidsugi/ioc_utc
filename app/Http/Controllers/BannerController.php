<?php

namespace App\Http\Controllers;
use App\Http\Requests\BannerRequest;
use App\Banner;
use App\BannerLocation;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $banner = Banner::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $banner = $banner->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }
        
        $banner = $banner->paginate(10);
        return view('banners.index', compact('banner', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banner = new Banner;
        $bannerLocation = BannerLocation::pluck('name', 'id')->all();
        return view('banners.create', compact('banner', 'bannerLocation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request)
    {
        $banner = new Banner;
        $banner->fill($request->all());

        $check = Banner::orderBy('id','DESC')->first();
        if($check)
            $banner->id = $check->id+1;
        if($request->file('image_desktop')){ 
            $file = $request->file('image_desktop');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/image/';
            $file->move($path, $filename);
            $banner->image_desktop_path=$filename;
        }
        if($request->file('image_mobile')){ 
            $file = $request->file('image_mobile');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/image/';
            $file->move($path, $filename);
            $banner->image_mobile_path=$filename;
        }
        $banner->save();
        $request->session()->flash('toast', 'Banner berhasil ditambahkan!');
        
        return redirect('/banners');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        $bannerLocation = BannerLocation::pluck('name', 'id')->all();
        return view('banners.create', compact('banner', 'bannerLocation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Banner              $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        $banner->fill($request->all());
        $file = $request->file('image_desktop');
              if($request->file('image_desktop')){ 
            $file = $request->file('image_desktop');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/image/';
            $file->move($path, $filename);
            $banner->image_desktop_path=$filename;
        }
        if($request->file('image_mobile')){ 
            $file = $request->file('image_mobile');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/image/';
            $file->move($path, $filename);
            $banner->image_mobile_path=$filename;
        }
        $banner->save();
        
        $request->session()->flash('toast', 'Banner berhasil diubah!');
        
        return redirect('/banners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner, Request $request)
    {
        try {
            $banner->delete();
            $request->session()->flash('toast', 'Banner berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Banner gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/banners');        
    }
}
