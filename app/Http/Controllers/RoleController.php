<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

//Importing laravel-permission models
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
 use DB; 
 use Illuminate\Validation\ValidationException; 
 use Illuminate\Support\Facades\Validator;

 
class RoleController extends Controller
{
    public function index(Request $request)
    {
        $roles = Role::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $roles  = $roles->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }
        $roles=$roles->orderBy('id','DESC')->paginate(30);
        return view('role.index')
        ->with('roles', $roles)
        ->with('filter',$filter);
    }
    public function create(){
        $role = new Role;
        $permissions = Permission::all()->pluck('name','name');
        return view('role._form')
        ->with('permissions', $permissions)
        ->with('role', $role);
    }
    public function store(Request $request){
        DB::beginTransaction();
        
        $role = new Role;
        $check = Role::orderBy('id','DESC')->first();
                        if($check)
                            $role->id = $check->id+1;
        $role->fill($request->except('permissions'));
        $role->save();
        if (!empty($request->permissions)) {        
            $role->syncPermissions($request->permissions);
        }   
        DB::commit();
        $request->session()->flash('toast', 'Jabatan berhasil ditambahkan!');
        return redirect()->route('roles.index');
    }
    public function edit($id){
        $role = Role::find($id);
        $permissions = Permission::all()->pluck('name','name');

        return view('role._form')
        ->with('permissions', $permissions)
        ->with('role',$role);
    }
    public function update($id, Request $request){
        DB::beginTransaction();
        $role = Role::find($id);
        $role->fill($request->except('permissions'));
        $role->save();
        if (!empty($request->permissions)) {        
            $role->syncPermissions($request->permissions);
        }        
        else {
            $role->permissions()->detach();
        }
        DB::commit();
        $request->session()->flash('toast', 'Jabatan  berhasil diubah!');
        return redirect()->route('roles.index');
    }
    public function show(Role $role){
        return view('role.show')->with('role', $role);
    }
    public function destroy(Request $request, Role $role){
        $role->delete();
        $request->session()->flash('toast', 'Jabatan berhasil dihapus!');
        return redirect()->route('roles.index');
    }
}
