<?php

namespace App\Http\Controllers;

use App\FacilityAttribute;
use Illuminate\Http\Request;

class FacilityAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDataType(Request $request)
    {
        $datatype = FacilityAttribute::find($request->id);
        return $datatype->datatype;
    }
    public function index(Request $request)
    {
        $facility_attribute = FacilityAttribute::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $facility_attribute = $facility_attribute->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }
        
        $facility_attribute = $facility_attribute->paginate(10);
        return view('facility_attributes.index', compact('facility_attribute', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $facility_attribute = new FacilityAttribute;
        return view('facility_attributes.create', compact('facility_attribute'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $facility_attribute = new FacilityAttribute;
        $facility_attribute->fill($request->all());
        $check = FacilityAttribute::orderBy('id','DESC')->first();
                if($check)
                    $facility_attribute->id = $check->id+1;
        $facility_attribute->save();
        
        $facility_attribute->slug = str_slug($facility_attribute->id.'_'.$facility_attribute->name, '_');
        $facility_attribute->save();
        $request->session()->flash('toast', 'Attribute berhasil ditambahkan');
        return redirect('/facility_attributes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FacilityAttribute $facilityAttribute
     * @return \Illuminate\Http\Response
     */
    public function show(FacilityAttribute $facility_attribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FacilityAttribute $facilityAttribute
     * @return \Illuminate\Http\Response
     */
    public function edit(FacilityAttribute $facility_attribute)
    {
        return view('facility_attributes.create', compact('facility_attribute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\FacilityAttribute   $facilityAttribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FacilityAttribute $facility_attribute)
    {
        $facility_attribute->fill($request->all());
        $facility_attribute->slug = str_slug($facility_attribute->id.'_'.$facility_attribute->name, '_');
        $facility_attribute->save();
        $request->session()->flash('toast', 'Attribute berhasil diubah');
        return redirect('/facility_attributes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FacilityAttribute $facilityAttribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,FacilityAttribute $facility_attribute)
    {
        try {
            $facility_attribute->delete();
            $request->session()->flash('toast', 'Attribute berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Attribute gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/facility_attributes');
    }
}
