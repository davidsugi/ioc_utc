<?php

namespace App\Http\Controllers;
use App\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $unit = Unit::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $unit = $unit->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }
        
        $unit = $unit->paginate(10);
        return view('units.index', compact('unit', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unit = new Unit;
        return view('units.create', compact('unit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unit = new Unit;
        $check = Unit::orderBy('id','DESC')->first();
            if($check)
                $unit->id = $check->id+1;
        $unit->fill($request->all());
        $unit->save();
        $request->session()->flash('toast', 'Unit berhasil ditambahkan');
        return redirect('/units');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        return view('units.create', compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Unit $unit)
    {
        $unit->fill($request->all());
        $unit->save();
        $request->session()->flash('toast', 'Unit berhasil diubah');
        return redirect('/units');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Unit $unit)
    {
        try{
            $unit->delete();
            $request->session()->flash('toast', 'Unit berhasil dihapus');
        }
        catch(\Illuminate\Database\QueryException $ex){
            $request->session()->flash('toast', 'Unit gagal dihapus'.substr($ex->getMessage(), 0, 15));
        }
        return redirect('/units');
    }
}
