<?php

namespace App\Http\Controllers;
use App\Facility;
use App\DetailFacility;
use Illuminate\Http\Request;

class FacilityDetailFacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Facility $facility)
    {
        $detail_facility = new DetailFacility;
        return view('facilities_detail_facilities.create', compact('detail_facility', 'facility'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Facility $facility)
    {
        $detail_facility = new DetailFacility;
        $check = DetailFacility::orderBy('id','DESC')->first();
                if($check)
                    $detail_facility->id = $check->id+1;
        $detail_facility->name=$request->name;
        $detail_facility->facility_id = $facility->id;
        $detail_facility->save();
        $request->session()->flash('toast', 'List berhasil ditambahkan');
        return redirect('/facilities/'.$facility->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Facility $facility, DetailFacility $detail_facility)
    {
        //bagian e VERRELL. tampilin calendar
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Facility $facility, DetailFacility $detail_facility)
    {
        return view('facilities_detail_facilities.create', compact('detail_facility', 'facility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Facility $facility, DetailFacility $detail_facility)
    {
        $detail_facility->name=$request->name;
        $detail_facility->save();
        $request->session()->flash('toast', 'List berhasil diubah');
        return redirect('/facilities/'.$facility->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Facility $facility, DetailFacility $detail_facility )
    {
        try{
            $detail_facility->delete();
            $request->session()->flash('toast', 'List Fasilitas berhasil dihapus');

        }
        catch(\Illuminate\Database\QueryException $ex){
            $request->session()->flash('toast', 'List Fasilitas gagal dihapus'.substr($ex->getMessage(), 0, 15));

        };       
        return redirect('/facilities/'.$facility->id);
    }
}
