<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // return view('users.test');
        $user = User::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $user = $user->whereRaw("CAST(".$key." as TEXT) ilike '%".strtolower($value)."%'"); 
                }
            }
        }
        
        $user = $user->paginate(10);
        return view('users.index', compact('user', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        $user = new User();
        $roles = Role::all()->pluck('name','name');

        return view('users.create', compact('user','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Validator::make(
            $request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            ]
        )->validate();

        $user = new User;
        $check = User::orderBy('id','DESC')->first();
            if($check)
                $user->id = $check->id+1;
        $user->fill($request->except('role'));
        $user->password = bcrypt($request->password);
        $user->emailable = 0;      
        $user->save();
  
        if(isset($request->emailable))
            $user->emailable = $request->emailable;        
        if(!empty($request->role)){        
            $user->syncRoles($request->role);
        }
        $request->session()->flash('toast', 'User berhasil ditambahkan!');
        
        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show')->with('user',$user); 

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $roles = Role::all()->pluck('name','name');
        $user = User::find($id);
        return view('users.create', compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;
        Validator::make(
            $request->all(), [
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
            ]
        )->validate();


        $user = User::find($id);
        $user->fill($request->except('role'));    
      
        if(isset($request->emailable))
            $user->emailable = $request->emailable;
        else
            $user->emailable =0;
          if(!empty($request->role)){        
            $user->syncRoles($request->role);
        }
        $user->save();
        $request->session()->flash('toast', 'User berhasil diubah!');
        
        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try {
            $user = User::find($id);
            $user->delete();
            $request->session()->flash('toast', 'User berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'User gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/users');
    }

    public function change_password_view(User $user)
    {
        return view('users.form_password')->with(['user' => $user]);
    }
   
    public function change_password_store(Request $request, User $user)
    {
        $user->password = bcrypt($request->password);
        $user->save();

        $request->session()->flash('toast', 'Password User berhasil diubah!');
        return redirect()->route('users.show', $user->id);
    }
}
