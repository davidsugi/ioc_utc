<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bundle;
use App\BundleDetail;
use App\Facility;
use App\DetailFacility;
use App\Addon;
use App\Gallery;
use App\DetailOrder;
use DB;
use Illuminate\Validation\ValidationException;


class BundleController extends Controller
{
    public function index(Request $request)
    {
        $bundles = Bundle::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $bundles  = $bundles->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");                        

                }
            }
        }
        $bundles=$bundles->orderBy('id','DESC')->paginate(20);
        
        return view('bundle.index')
        ->with('filter', $filter)
        ->with('bundles', $bundles);
    }
    public function create(){
        $bundle = new Bundle;
        $array=[];
        $facs= Facility::all()->pluck("id", "name");
        $add = Addon::all()->pluck("id", "name");
        foreach ($facs as $key=>$val) {
            $array = array_add($array, "F-".$val, $key);
        }
        foreach ($add as $key=>$val) {
            $array = array_add($array, "A-".$val, $key);
        }
        
        return view('bundle._form')
        ->with('facs', $array)
        ->with('bundle', $bundle);
    }
    public function store(Request $request)
    {
           DB::beginTransaction();          
        // return $request;
        $errors=[];                                                
        $bundle = new Bundle;
        $check = Bundle::orderBy('id','DESC')->first();
        if($check)
            $bundle->id = $check->id+1;
        $bundle->per_person = 0;     
        $bundle->frontend = 0;                                   
        $bundle->fill($request->except('detail','images'));
        if($request->per_person=="on"){
            $bundle->per_person = 1;
        }
        if($request->frontend=="on"){
            $bundle->frontend = 1;
        }
        $bundle->slug = str_slug($bundle->name, '_');   
        $chk=Bundle::where('slug',$bundle->slug)->first();
        
         if($chk){
                 $errors=array_add($errors,"name","Nama Paket Sudah Pernah Digunakan!");                                        
                DB::rollBack();
                throw ValidationException::withMessages($errors);
            }

        $bundle->save();
        if(count($request->detail)>0){
            foreach($request->detail as $key=>$det){
                if($key==0){
                    continue;
                }
                $dets = new BundleDetail;
                 $check = BundleDetail::orderBy('id','DESC')->first();
                if($check)
                    $dets->id = $check->id+1;
                $str=explode("-",$det);     
                $dets->bundl_type = "App\Addons";
                if($str[0]=="F"){
                    $dets->bundl_type = "App\Facility";
                }
                $dets->bundl_id=$str[1];
                $dets->bundle_id=$bundle->id;
                $dets->save();
            }
        }
        if($request->hasFile('images')){
            $file = $request->file('images');
            if(count($file)){
                foreach($file as $f){
                $filename = $f->getClientOriginalName();
                $path = public_path().'/image/';
                $f->move($path, $filename);
                
                $gallery = new Gallery;
                $check = Gallery::orderBy('id','DESC')->first();
                    if($check)
                        $gallery->id = $check->id+1;
                $gallery->image_path = $filename;
                $gallery->galleriable_id = $bundle->id;
                $gallery->galleriable_type = 'Bundle';
                $gallery->save();
                $gallery->slug = str_slug($gallery->id.'_'.$gallery->title, '_');
                $gallery->save();
                }
            }
        }

        DB::commit();
        
        $request->session()->flash('toast', 'Paket berhasil ditambahkan!');
        return redirect()->route('bundles.index');
    }
    public function edit($id)
    {
        $bundle = Bundle::find($id);
        $array=[];
        $fac = Facility::all()->pluck("id", "name");
        $add = Addon::pluck("id", "name");
        foreach ($fac as $key=>$val) {
            $array = array_add($array, "F-".$val, $key);
        }
        foreach ($add as $key=>$val) {
            $array = array_add($array, "A-".$val, $key);
        }

        
        // return $bundle->bundle_detail->pluck("id_helper_label");
        return view('bundle._form')
        ->with('facs', $array)
        ->with('bundle', $bundle);
    }
    public function update($id, Request $request)
    {
        DB::beginTransaction();                  
        $bundle = Bundle::find($id);
        $bundle->fill($request->except('detail','ids'));
        $bundle->frontend = 0;  
        $bundle->per_person = 0;
        $bundle->slug = str_slug($bundle->name, '_');   
        
        if($request->per_person){
            $bundle->per_person = 1;
        }                    
        if($request->frontend){
            $bundle->frontend = 1;
        }
        $chk=Bundle::where('slug',$bundle->slug)->where('id','<>',$bundle->id)->first();
        $errors=[];
        
         if($chk){
                
                 $errors=array_add($errors,"name","Nama Paket Sudah Pernah Digunakan!");                                        
                DB::rollBack();
                throw ValidationException::withMessages($errors);
            }


        $bundle->save();
        $detbun = BundleDetail::where("bundle_id",$bundle->id)->delete();
        if(count($request->detail)>0){
            foreach($request->detail as $key => $det){
                if($key==0){
                    continue;
                }
                $dets = new BundleDetail;
                 $check = BundleDetail::orderBy('id','DESC')->first();
                if($check)
                    $dets->id = $check->id+1;
                $str=explode("-",$det);     
                $dets->bundl_type = "App\Addons";
                if($str[0]=="F"){
                    $dets->bundl_type = "App\Facility";
                }
                
                $dets->bundl_id=$str[1];
                $dets->bundle_id=$bundle->id;
                $dets->save();
            }
        }
        DB::commit();

        $request->session()->flash('toast', 'Paket  berhasil diubah!');
        return redirect()->route('bundles.index');
    }
    public function show(Bundle $bundle)
    {
        
        $gallery = Gallery::query();
        $gallery = $gallery->where('galleriable_type', '=', 'Bundle');
        $gallery = $gallery->where('galleriable_id', '=', $bundle->id);
        $gallery = $gallery->paginate(5, ['*'], 'gallery');

        return view('bundle.show', compact('bundle', 'gallery'));
    }
    public function destroy(Request $request, Bundle $bundle)
    {
        try {
                $chk = DetailOrder::where("item_id",$bundle->id)->where("item_type","LIKE","%Bundle")->count();        
                if($chk>0){
                    $request->session()->flash('error', 'Paket gagal dihapus. Bundle Telah dipakai dalam order');
                    return redirect()->route('bundles.index');
                                
                }
                $detbun = BundleDetail::where("bundle_id",$bundle->id)->delete();        
                $bundle->delete();
                $request->session()->flash('toast', 'Paket berhasil dihapus!');
                $bundle->delete();
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('error', 'Paket gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
    
        return redirect()->route('bundles.index');
    }

    public function getforbundle(Request $req)
    {
        $array=[];
        if($req->q="") {
            return [];  
        }
        $facs= Facility::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($req["q"])."%")->get()->pluck("id", "name");
        $add = Addon::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($req["q"])."%")->limit(10)->pluck("id", "name");
        foreach ($facs as $key=>$val) {
            $array = array_add($array, "F-".$val, $key);
        }
        foreach ($add as $key=>$val) {
            $array = array_add($array, "A-".$val, $key);
        }
        return $array;
    }

}
