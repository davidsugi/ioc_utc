<?php

namespace App\Http\Controllers;
use App\FacilityDetail;
use App\Facility;
use App\FacilityAttribute;
use Illuminate\Http\Request;

class FacilityFacilityDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Facility $facility)
    {
        $facility_detail = new FacilityDetail;
        $attribute = FacilityAttribute::pluck('name', 'id')->all();
        return view('facilities_facility_details.create', compact('facility_detail', 'facility', 'attribute'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Facility $facility)
    {
        // return $request;
        $facility_detail = new FacilityDetail;
        $check = FacilityDetail::orderBy('id','DESC')->first();
                if($check)
                    $facility_detail->id = $check->id+1;
        $facility_detail->facility_id = $facility->id;
        $facility_detail->attribute_id = $request->attribute_id;
        $facility_detail->value = $request->value[$request->tipe];
        $facility_detail->save();
        $request->session()->flash('toast', 'Detail Fasilitas berhasil ditambahkan');
        return redirect('/facilities/'.$facility->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Facility $facility, FacilityDetail $facility_detail)
    {
        $attribute = FacilityAttribute::pluck('name', 'id')->all();
        return view('facilities_facility_details.create', compact('facility_detail', 'facility', 'attribute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility, FacilityDetail $facility_detail)
    {

        $facility_detail->facility_id = $facility->id;
        $facility_detail->attribute_id = $request->attribute_id;
        $facility_detail->value = $request->value[$request->tipe];
        $facility_detail->save();
        $request->session()->flash('toast', 'Detail Fasilitas berhasil ditambahkan');
        return redirect('/facilities/'.$facility->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Facility $facility, FacilityDetail $facility_detail)
    {
        try{
            $facility_detail->delete();
            $request->session()->flash('toast', 'Detail Fasilitas berhasil dihapus');

        }
        catch(\Illuminate\Database\QueryException $ex){
            $request->session()->flash('toast', 'Detail Fasilitas gagal dihapus'.substr($ex->getMessage(), 0, 15));

        };       
        return redirect('/facilities/'.$facility->id);  
    }
}
