<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use H;
use DB;
class FoodSettingController extends Controller
{
    function build_sorter($key) {
        return function ($a, $b) use ($key) {
            return strnatcmp($a[$key], $b[$key]);
        };
    }
    public function index(Request $request)
    {
        if($request->dev!=ENV('DEV_KEY',""))
            return redirect()->route('home');   
        $menu=json_decode(H::globs('menu'));
        $lastdayMenu=json_decode(H::globs('last_menu'));
        return view('addons.setting')
        ->with('menus', $menu)
        ->with('lastdayMenus', $lastdayMenu);
    }

    public function create(Request $req){
        if($req->dev!=ENV('DEV_KEY',""))
            return redirect()->route('home');
        return view('addons._formset')->with('menu',[])->with('type',$req->type);
    }
    public function store(Request $req){
        DB::beginTransaction();
        if($req->typef==1){
            $menu=json_decode(H::globs('menu'));
            $menu[]=[$req->type,$req->title,$req->day,$req->time,0];
            usort($menu, $this->build_sorter(3));
            usort($menu, $this->build_sorter(2));
            $menu=json_decode(H::globs('menu',json_encode($menu)));
        }
        else{
            $menu=json_decode(H::globs('last_menu'));
            $menu[]=[$req->type,$req->title,$req->day,$req->time,1];
            usort($menu, $this->build_sorter(3));
            usort($menu, $this->build_sorter(2));
            $menu=json_decode(H::globs('last_menu',json_encode($menu)));
        }
        DB::commit();
        $req->session()->flash('toast', 'Setting berhasil ditambahkan!');
        return redirect()->route('food_settings.index');
    }
    public function edit($id,Request $request){
        if($request->dev!=ENV('DEV_KEY',""))
            return redirect()->route('home');
        if($request->type==1){        
            $menu=json_decode(H::globs('menu'));
        }
        else{
            $menu=json_decode(H::globs('last_menu'));
        }
        $editval=$menu[$id];
        return view('addons._formset')->with('menu',$editval)->with('type',$request->type);

    }
    public function update($id, Request $request){
        DB::beginTransaction();
        if($request->typef==1){
            $menu=json_decode(H::globs('menu'));
            $menu[$id]=[$req->type,$req->title,$req->day,$req->time,0];
            usort($menu, $this->build_sorter(3));
            usort($menu, $this->build_sorter(2));
            $menu=json_decode(H::globs('menu',json_encde($menu)));
        }
        else{
            $menu=json_decode(H::globs('last_menu'));
            $menu[$id]=[$req->type,$req->title,$req->day,$req->time,1];
            usort($menu, $this->build_sorter(3));
            usort($menu, $this->build_sorter(2));
            $menu=json_decode(H::globs('last_menu',json_encde($menu)));
        }
        DB::commit();
        $request->session()->flash('toast', 'Setting berhasil diubah!');
        return redirect()->route('food_settings.index');
    }

    public function destroy(Request $request,$food_setting){
         try {
           
            $request->session()->flash('toast', 'Setting berhasil dihapus!');
        } catch(QueryException $ex){ 
            $request->session()->flash('error', 'Setting gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        return redirect()->route('food_settings.index');
    }
}
