<?php

namespace App\Http\Controllers;

use App\Testimony;
use Illuminate\Http\Request;

class TestimonyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $testimony = Testimony::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $testimony = $testimony->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }
        
        $testimony = $testimony->paginate(10);
        return view('testimonies.index', compact('testimony', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $testimony = new Testimony;
        return view('testimonies.create', compact('testimony'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $testimony = new Testimony;
        $check = Testimony::orderBy('id','DESC')->first();
                        if($check)
                            $testimony->id = $check->id+1;
        $testimony->fill($request->all());
        $testimony->save();
        $request->session()->flash('toast', 'Testimoni berhasil ditambahkan!');
        
        return redirect('/testimonies');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testimony $testimony
     * @return \Illuminate\Http\Response
     */
    public function show(Testimony $testimony)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimony $testimony
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimony $testimony)
    {
        return view('testimonies.create', compact('testimony'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Testimony           $testimony
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimony $testimony)
    {
        $testimony->fill($request->all());
        $testimony->save();
        $request->session()->flash('toast', 'Testimoni berhasil diubah');
        return redirect('/testimonies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimony $testimony
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimony $testimony, Request $request)
    {
        try{
            $testimony->delete();
            $request->session()->flash('toast', 'Testimoni berhasil dihapus!');
        }
        catch(\Illuminate\Database\QueryException $ex){
            $request->session()->flash('toast', 'Testimoni gagal dihapus'.substr($ex->getMessage(), 0, 15));

        }
        return redirect('/testimonies');
    }
}
