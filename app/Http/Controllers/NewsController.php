<?php

namespace App\Http\Controllers;

use Auth;
use App\News;
use App\Category;
use App\CategoryNews;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;


class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $news = News::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $news = $news->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }

        $news = $news->paginate(10);
        return view('news.index', compact('news', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $news = new News;
        $category = Category::pluck('name', 'id')->all();
        return view('news.create', compact('news', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('image');
        $filename = $file->getClientOriginalName();
        $path = public_path().'/image/';
        $file->move($path, $filename);

        $news = new News;
         $check = News::orderBy('id','DESC')->first();
            if($check)
                $news->id = $check->id+1;

        $input = $request->except(['categories','image','files']);
        $news->fill($input);
        $news->image = $filename;

        $news->user_id = Auth::user()->id;
               $dom = new \DomDocument();
        $dom->loadHtml($request->content);
        $images = $dom->getElementsByTagName('img');
        foreach($images as $img){
			$src = $img->getAttribute('src');
			
			// if the img source is 'data-url'
			if(preg_match('/data:image/', $src)){
				
				// get the mimetype
				preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
				$mimetype = $groups['mime'];
				
				// Generating a random filename
				$filename = uniqid();
				$filepath = "/image/$filename.$mimetype";
	
				// @see http://image.intervention.io/api/
				$image = Image::make($src)
				  // resize if required
				  /* ->resize(300, 200) */
				  ->encode($mimetype, 100) 	// encode file to the specified mimetype
				  ->save(public_path($filepath));
				
				$new_src = asset($filepath);
				$img->removeAttribute('src');
				$img->setAttribute('src', $new_src);
			} // <!--endif
        } // <!--endforeach
        
        $news->content = $dom->saveHTML();
        $news->slug = str_slug($news->title, '_');        
        $news->save();

        foreach($request->categories as $c){
            $cnews= new CategoryNews;
            $check = CategoryNews::orderBy('id','DESC')->first();
            if($check)
                $cnews->id = $check->id+1;

            $cnews->news_id=$news->id;
            $cnews->category_id=$c;
            $cnews->save();
        }
        return redirect('/news/'.$news->id);
        

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        return view('news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News $news
     * @return \Illuminate\Http\Response
     */
    public function editImage(News $news)
    {
        return view('news.createImage', compact('news'));
    }
    public function edit(News $news)
    {
        $category = Category::pluck('name', 'id')->all();
        return view('news.create', compact('news', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\News                $news
     * @return \Illuminate\Http\Response
     */
    public function updateImage(Request $request, News $news)
    {
        $file = $request->file('image');
        $filename = $file->getClientOriginalName();
        $path = public_path().'/image/';
        $file->move($path, $filename);
        
        $news->image = $filename;
        
        $news->save();
        return redirect('/news/'.$news->id);
    }
    public function update(Request $request, News $news)
    {
        $input = $request->except(['categories']);
        $news->fill($input);
        $news->user_id = Auth::user()->id;
        $news->slug = str_slug($news->title, '_');                
        $news->save();
        $news->category()->detach();
        foreach($request->categories as $c){
            $cnews= new CategoryNews;
            $check = CategoryNews::orderBy('id','DESC')->first();
            if($check)
                $cnews->id = $check->id+1;

            $cnews->news_id=$news->id;
            $cnews->category_id=$c;
            $cnews->save();
        }
        return redirect('/news/'.$news->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,News $news)
    {
        try {

            $news->category()->detach();
            $news->delete();
            $request->session()->flash('toast', 'Berita berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Berita gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/news');
    }
}
