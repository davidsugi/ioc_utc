<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Auth;

//Importing laravel-permission models
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
 use DB; 
 use Illuminate\Validation\ValidationException; 
 use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    public function index(Request $request)
    {
        $permissions = Permission::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $permissions  = $permissions->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }
        $permissions=$permissions->orderBy('id','DESC')->paginate(20);
        return view('permission.index')
        ->with('permissions', $permissions)
        ->with('filter',$filter);
    }
    public function create(Request $req){
        if($req->dev!=ENV('DEV_KEY',""))
            return redirect()->route('home');
        $permission = new Permission;
        return view('permission._form')->with('permission', $permission);
    }
    public function store(Request $request){
        DB::beginTransaction();

        $permission = new Permission;
        $check = Permission::orderBy('id','DESC')->first();
        if($check)
            $permission->id = $check->id+1;
        $permission->fill($request->all());
        $permission->guard_name="web";
        $permission->save();
        DB::commit();
        $request->session()->flash('toast', 'Hak Akses berhasil ditambahkan!');
        return redirect()->route('permissions.index');
    }
    public function edit($id){
        $permission = Permission::find($id);
        return view('permission._form')->with('permission',$permission);
    }
    public function update($id, Request $request){
        DB::beginTransaction();
        $permission = Permission::find($id);
        $permission->fill($request->all());
     
        $permission->save();
        DB::commit();
        $request->session()->flash('toast', 'Hak Akses  berhasil diubah!');
        return redirect()->route('permission.index');
    }
    public function show(Permission $permission){
        return view('permission.show')->with('permission', $permission);
    }
    public function destroy(Request $request, Permission $permission){
        $permission->delete();
        $request->session()->flash('toast', 'Hak Akses berhasil dihapus!');
        return redirect()->route('permission.index');
    }
}
