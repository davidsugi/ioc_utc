<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Folio;
use App\DetailFacility;
use App\Addon;
use Illuminate\Support\Facades\Auth;
use Barryvdh\Snappy\Facades\SnappyPdf;
use App\Order;
use Carbon\carbon;
use App\Facility;

class FolioController extends Controller
{
    public function index(Request $request){
        $folios = Folio::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    if($key=="check_in")
                        $folios  = $folios->whereDate('check_out','>=',$value);
                    elseif($key=='check_out')
                         $folios  = $folios->whereDate('check_in','<=',$value);
                }
            }
        }
        $fac= clone $folios;
        $fac = $fac->where("folio_type","LIKE","%DetailFacility")->orderBy('check_in','DESC')->paginate(10,['*'], 'page');    
        $add=$folios->where("folio_type","LIKE","%Addon")->orderBy('check_in','DESC')->paginate(10,['*'], 'add_page');
        
        return view('folio.index')
        ->with('order',"all")
        ->with('adds',$add)
        ->with('facs',$fac)
        ->with('facs',$fac)
        ->with('filter',$filter);
    }
    public function create(){
        $folio = new Folio;
        return view('folio.create')->with('folio',$folio);
    }
    public function store(Request $request){
        $folio = new Folio;
        $check = Folio::orderBy('id','DESC')->first();
            if($check)
                $folio->id = $check->id+1;
        $folio->fill($request->all());
        $folio->save();
        $request->session()->flash('toast', 'Folio berhasil ditambahkan!');
        return redirect()->route('folios.index');
    }
    public function edit($id){
        $folio = Folio::find($id);
        $dur= $folio->check_in->diffInDays($folio->check_out);
        $facil= Facility::find($folio->folio->facility->id);
        $facilities=$facil->availableFolio($folio->check_in->format("Y-m-d"),$dur,$folio->folio_id)->pluck('name','id');

        return view('folio.edit')->with('folio',$folio)->with('facilities',$facilities);
    }
    public function update($id, Request $request){
        $folio = Folio::find($id);
        $folio->folio_id=$request->folio_id;
        $folio->save();
        $request->session()->flash('toast', 'Folio  berhasil diubah!');
        return redirect()->route('folios.index');
    }
    public function show(Folio $folio){
        return view('folio.show')->with('folio',$folio);
    }
    public function destroy(Request $request, Folio $folio){
        $folio->delete();
        $request->session()->flash('toast', 'Folio berhasil dihapus!');
        $folio->delete();
        return redirect()->route('folios.index');
    }

    public function getCalendar(Request $request)
    {
        $carbonevent = array();
        $folios = Folio::where("active",1);
        $colors = ['#095663','#53058e','#0b9131','#005c5e',
        '#2b7003','#f2d818','#a30cc9','#ce0caa',
        '#81a004','#44037a','#cad104','#c90692',
        '#ce9504','#11ad9b','#646d00','#0b9e3c',
        '#0c457a','#039b87','#5d8c05','#4809a0',
        '#099923','#04578e','#e08114','#e002cd',
        '#ce650a','#d800bf','#0b567f'];
        if($request->id!="all"){
            $ord = Order::find($request->id);
            $folios = $folios->whereIn("detail_order_id",$ord->detail_order->pluck("id"));
        }
        else{
            $folios=$folios->where("folio_type","App\\DetailFacility"); 
        }
        $folios=$folios->orderBy('detail_order_id','DESC')->get();
        // return $carbonevent;
        foreach($folios as $folio){
            $tmp = [];
            $ord= Order::find($folio->detail_order->order_id);            
            if($folio->folio_type=="App\\DetailFacility"){
                $detf = DetailFacility::find($folio->folio_id);   
                $tmp[]=$detf->name." - ".$ord->customer->nameLabel;   
                $tmp[]=$colors[$ord->id%27];                     
               
                $tmp[]= $folio->check_in->format("Y-m-d")." 12:00:00";
                if($ord->menginap!=1){
                    $tmp[]= $folio->check_out_label->format("Y-m-d")." 00:00:00";
                }
                else{
                    $tmp[]= $folio->check_out_label." 12:00:00";
                }
                $tmp[] = route('orders.show',$ord->id);
                $tmp[] = true;
            }
            else if($folio->folio_type=="App\\Addon"){
                $detf = Addon::find($folio->folio_id);  
                $tmp[]=$folio->name_label." - ".$ord->customer->nameLabel;   
                if($detf->type==1||$detf->type==2){
                    $tmp[]="#3c8dbc";
                }
                else{
                    $tmp[]="#49b75b";
                }
                $tmp[]= $folio->check_in->format("Y-m-d H:i:s");
                $tmp[]= $folio->check_out_label->format("Y-m-d H:i:s");
                $tmp[] = route('orders.show',$ord->id);
                $tmp[] = true;

            }
            // return $tmp;
            $carbonevent[] = $tmp;
        }

        return $carbonevent;
    }

    public function print(Request $request)
    {
                // return $request;
  
        // $notin = Order::whereDate("date","<",$request->filter['start'])->orwhereDate("date",">",$request->filter['end']);
        $facs = Folio::where("folio_type","LIKE","%Facility");
        $addons = Folio::where("folio_type","LIKE","%Addon%");
        $result=[];
        // return $result;
        $dated = Carbon::now()->format("Y-m-d");    
        if(isset($request['order']) && $request['order']!='all'){
            $facs=$facs->join('detail_orders','detail_orders.id','detail_order_id')->where('order_id',$request['order']);
            $addons=$addons->join('detail_orders','detail_orders.id','detail_order_id')->where('order_id',$request['order']);
        }    
        if($request['start']!=null && $request['end']!=null){
            $dated=$request['start']." - ".$request['end'];
            $facs=$facs->whereDate('check_in','<=',$request['end'])->whereDate('check_out','>=',$request['start']);
            $addons=$addons->whereDate('check_in','<=',$request['end'])->whereDate('check_out','>=',$request['start']);
        
        }
        else {
            if($request['end']!=null){
                $dated= "~".$request['end'];                     
                $facs=$facs->where('check_in','<',$request['end']);
                $addons=$addons->where('check_in','<',$request['end']);
        
            }
            else if($request['start']!=null){
                $dated= $request['start']."~";                
                $facs=$facs->where('check_out','>',$request['start']);
                 $addons=$addons->where('check_out','>',$request['start']);
            }
        }
        $facs=$facs->get();
        $addons=$addons->get();
        
        // return view('folio.print')->with(['facs' => $facs,'addons' => $addons, 'dated'=>$dated]);
        $pdf = SnappyPdf::loadView('folio.print', ['facs' => $facs,'addons' => $addons,  'dated'=>$dated]);
        // ->setPaper('letter','landscape');
        return $pdf->download('Folio ['. $dated .'].pdf');
    }
}
