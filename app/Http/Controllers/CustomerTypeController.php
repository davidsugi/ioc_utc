<?php

namespace App\Http\Controllers;
use App\CustomerType;
use Illuminate\Http\Request;

class CustomerTypeController extends Controller
{
     //api
    public function get(Request $request)
    {
        $cus = CustomerType::whereRaw("CAST($key as TEXT) ilike '%" . strtolower($request->q) . "%'")->pluck('name', 'id');
        return $cus;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customer_type = CustomerType::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $customer_type = $customer_type->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                }
            }
        }
        
        $customer_type = $customer_type->paginate(10);
        return view('customer_types.index', compact('customer_type', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer_type = new CustomerType;
        return view('customer_types.create', compact('customer_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer_type = new CustomerType;
        $check = CustomerType::orderBy('id','DESC')->first();
            if($check)
                $customer_type->id = $check->id+1;
        $customer_type->fill($request->all());
        $customer_type->save();
        $request->session()->flash('toast', 'Tipe Cutomer berhasil ditambahkan');
        return redirect('/customer_types');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerType $customer_type)
    {
        return view('customer_types.create', compact('customer_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerType $customer_type)
    {
        $customer_type->fill($request->all());
        $customer_type->save();
        $request->session()->flash('toast', 'Tipe Customer berhasil diubah');
        return redirect('/customer_types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerType $customer_type, Request $request)
    {
        try {
            $customer_type->delete();
            $request->session()->flash('toast', 'Tipe Customer berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Tipe Customer gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/customer_types');
    }
}
