<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDocument;
use File;

class OrderDocumentController extends Controller
{
    public function index(Request $request,Order $order)
    {
        $order_documents = OrderDocument::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $order_documents  = $order_documents->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");
                }
            }
        }
        $order_documents=$order_documents->where('order_id',$order->id)->orderBy('id','DESC')->paginate(20);
        return view('order_document.index')
        ->with('ord',$order_documents)
        ->with('order',$order)
        ->with('filter',$filter);
    }
    public function create(Order $order){
        $order_document = new OrderDocument;
        return view('order_document._form')
        ->with('order',$order)
        ->with('order_document',$order_document);
    }
    public function store(Request $request, Order $order){
        // return $request;
       $path = public_path().'/document';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        $file = $request->file('path');
                $filename = $file->getClientOriginalName();
                $pat = public_path().'/document/';
                $file->move($pat, $filename);
        // foreach($request->file('path') as $path){
        //         $file = $request->file('path');
        //         $filename = $file->getClientOriginalName();
        //         $pat = public_path().'/document/';
        //         $file->move($pat, $filename);
        // }
        // return "o";

        $order_document = new OrderDocument;
        $chk=OrderDocument::orderBy('id','DESC')->first();
        if($chk)
            $order_document->id = $chk->id +1;
        
        $order_document->fill($request->except(['path']));
        $order_document->path = $filename;
        $order_document->order_id = $order->id;
        $order_document->save();
        $request->session()->flash('toast', 'Dokumen Order berhasil ditambahkan!');
        return redirect()->route('orders.order_documents.index',["order"=>$order]);
    }
    public function edit($id){
        $order_document = OrderDocument::find($id);
        return view('order_document._form')->with('order_document',$order_document);
    }
    public function update($id, Request $request){
        $order_document = OrderDocument::find($id);
        $order_document->fill($request->all());
        $order_document->save();
        $request->session()->flash('toast', 'Dokumen Order  berhasil diubah!');
        return redirect()->route('orders.order_documents.index');
    }
    public function show(OrderDocument $order_document){
        return view('order_document.show')->with('order_document',$order_document);
    }
    public function destroy(Request $request, Order $order, OrderDocument $order_document){
        // $order_document->delete();
        $request->session()->flash('toast', 'Dokumen Order berhasil dihapus!');
        $order_document->delete();
        return redirect()->route('orders.order_documents.index',$order);
    }
}
