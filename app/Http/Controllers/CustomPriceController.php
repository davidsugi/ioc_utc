<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Facility;
use App\Bundle;
use App\Addon;
use App\CustomPrice;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
// use DB;

class CustomPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $custom_prices = CustomPrice::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    if($key=="startdate") {
                        $custom_prices  = $custom_prices->whereDate("startdate",">=",$value);
                    }
                    else if($key=="enddate") {
                        $custom_prices  = $custom_prices->whereDate("enddate","<=",$value);
                    }
                    else{
                        $custom_prices  = $custom_prices->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                    }
                }
            }
        }
        $custom_prices=$custom_prices->orderBy("id","DESC")->paginate(30);
        return view('custom_prices.index')
        ->with('filter', $filter)
        ->with('custom_prices', $custom_prices);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req){
        // return $req->u;
        
        $custom_price = new CustomPrice;
        $custom_price->price_type = $req->u;
        $facility = Facility::pluck('name','id')->all();        
        if($req->u=="App\\Addon"){
            $facility = Addon::where("id",$req->id)->pluck('name','id');  
            $custom_price->price_id = $req->id;
            // return $facility[$custom_price->price_id];          
        }
        else if($req->u=="App\\Bundle"){
            $facility = Bundle::where("id",$req->id)->pluck('name','id');
            $custom_price->price_id = $req->id;                    
        }

        return view('custom_prices.create', compact('custom_price', 'facility'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        // return $request;
        try {
            if($request->price_type=="all"){
            $fac=Facility::all();
            $add=Addon::all();
            $bundle=Bundle::all();
            foreach($fac as $f){
                $custom_price = new CustomPrice;
                $check = CustomPrice::orderBy('id','DESC')->first();
                if($check)
                    $custom_price->id = $check->id+1;
                $array=explode('-', $request->rangedate);
                $startdate=Carbon::createFromFormat('d/m/Y H:i:s ', $array[0]);
                $enddate=Carbon::createFromFormat(' d/m/Y H:i:s', $array[1]);
                $custom_price->startdate=$startdate;
                $custom_price->enddate=$enddate;
                $custom_price->price_id = $f->id;
                $custom_price->price_type = "App\\Facility";
                $custom_price->price = $request->price;
                $custom_price->save();
            }
            foreach($add as $a){
                $custom_price = new CustomPrice;
               $check = CustomPrice::orderBy('id','DESC')->first();
                if($check)
                    $custom_price->id = $check->id+1;
                $array=explode('-', $request->rangedate);
                $startdate=Carbon::createFromFormat('d/m/Y H:i:s ', $array[0]);
                $enddate=Carbon::createFromFormat(' d/m/Y H:i:s', $array[1]);
                $custom_price->startdate=$startdate;
                $custom_price->enddate=$enddate;
                $custom_price->price_id = $a->id;
                $custom_price->price_type = "App\\Addon";
                $custom_price->price = $request->price;
                $custom_price->save();
            }
            foreach($bundle as $b){
                $custom_price = new CustomPrice;
                $check = CustomPrice::orderBy('id','DESC')->first();
                                if($check)
                                    $custom_price->id = $check->id+1;
                $array=explode('-', $request->rangedate);
                $startdate=Carbon::createFromFormat('d/m/Y H:i:s ', $array[0]);
                $enddate=Carbon::createFromFormat(' d/m/Y H:i:s', $array[1]);
                $custom_price->startdate=$startdate;
                $custom_price->enddate=$enddate;
                $custom_price->price_id = $b->id;
                $custom_price->price_type = "App\\Bundle";
                $custom_price->price = $request->price;
                $custom_price->save();
            }
        }
        else{
                $custom_price = new CustomPrice;
                $check = CustomPrice::orderBy('id','DESC')->first();
                if($check)
                    $custom_price->id = $check->id+1;
                $array=explode('-', $request->rangedate);
                $startdate=Carbon::createFromFormat('d/m/Y H:i:s ', $array[0]);
                $enddate=Carbon::createFromFormat(' d/m/Y H:i:s', $array[1]);
                $custom_price->startdate=$startdate;
                $custom_price->enddate=$enddate;
                $custom_price->price_id = $request->price_id;
                $custom_price->price_type = $request->price_type;
                $custom_price->price = $request->price;
                $custom_price->save();
        }
        $request->session()->flash('toast', 'Harga berhasil ditambahkan!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('error', 'Harga gagal ditambahkan. ' . substr($ex->getMessage(), 0, 15));
        }
        return redirect('/custom_prices');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomPrice $customPrice
     * @return \Illuminate\Http\Response
     */
    public function show(CustomPrice $custom_price)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomPrice $customPrice
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomPrice $custom_price)
    {   
        
        $facility = Facility::pluck('name', 'id')->all();      
        if($custom_price->price_type=="App\\Addon"){
            $facility = Addon::where("id",$custom_price->price_id)->pluck('name','id');  
            // $custom_price->price_id = $custom_price->id;
            // return $facility[$custom_price->price_id];          
        }
        else if($custom_price->price_type=="App\\Bundle"){
            $facility = Bundle::where("id",$custom_price->price_id)->pluck('name','id');
            // $custom_price->price_id = $custom_price->id;                    
        }
        return view('custom_prices.create', compact('custom_price', 'facility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\CustomPrice         $customPrice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomPrice $custom_price)
    {
        try {
                // $custom_price = new CustomPrice;
                $check = CustomPrice::orderBy('id','DESC')->first();
                if($check)
                    $custom_price->id = $check->id+1;
                $array=explode('-', $request->rangedate);
                $startdate=Carbon::createFromFormat('d/m/Y H:i:s ', $array[0]);
                $enddate=Carbon::createFromFormat(' d/m/Y H:i:s', $array[1]);
                $custom_price->startdate=$startdate;
                $custom_price->enddate=$enddate;
                $custom_price->price_id = $request->price_id;
                $custom_price->price_type = $request->price_type;
                $custom_price->price = $request->price;
                $custom_price->save();
                $request->session()->flash('toast', 'Harga berhasil diUbah!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('error', 'Harga gagal ditambahkan. ' . substr($ex->getMessage(), 0, 15));
        }

       
        $request->session()->flash('toast', 'Harga berhasil diubah!');
        
        return redirect()->route('custom_prices.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomPrice $customPrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,CustomPrice $custom_price)
    {
        try {
            $custom_price->delete();
            $request->session()->flash('toast', 'Harga berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Harga gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }

        return redirect('/custom_schedules');
    }
}
