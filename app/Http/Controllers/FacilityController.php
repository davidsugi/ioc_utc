<?php

namespace App\Http\Controllers;

use App\Facility;
use App\FacilityType;
use App\FacilityDetail;
use App\DetailFacility;
use App\CustomPrice;
use App\Unit;
use App\Bundle;
use App\Gallery;
use App\Folio;
use App\Addon;
use App\SpecialPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\carbon;
use DB;
use Illuminate\Validation\ValidationException;


class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // return  $request->filter;
        $facility = Facility::query();
        $type = FacilityType::pluck('name', 'id')->all();
        $ava = DetailFacility::rentable(date("Y-m-d"),1)->get();
        // $avaid = clone $ava;
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            $dur=1;
            if($filter['duration']>0){
                $dur=$filter['duration'];
            }
            $avaid = DetailFacility::rentable(date("Y-m-d"),$dur)->get();            
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    if($key=="available"){
                        $avaid = $avaid->pluck("facility_id");
                        if($value=="1"){
                            $facility = $facility->whereIn("id",$avaid);  
                            // return $avaid;                          
                        }
                        else{
                            $facility = $facility->whereNotIn("id",$avaid);                            
                        }
                    }
                    else if($key=="date"){
                        $date = DetailFacility::rentable($value,$dur)->get();
                        $avaid = clone $date;  
                        if($filter['available']==0){
                            $avaid = $avaid->pluck("facility_id");
                            $facility = $facility->whereIn("id",$avaid); 
                        }                      
                        
                    }
                    else if($key=="duration"){

                    }
                    else if($key=="capacity"){
                        $facility = $facility->where($key,">=", $value)->where("minimum","<=",$value);                                               
                    }
                    else{
                        $facility = $facility->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");
                    }
                }
            }
        }
        // if($request["available"]==1){
        //     $detfac = DetailFacility::rentable(date("Y-m-d"),1)->get()->pluck("facility_id");
        //     $facility= $facility->whereIn("id",$detfac);
        // }
        $all = Facility::all()->count();
        $detf = DetailFacility::all()->count();
        $ava = $ava->count();        
        $facility = $facility->paginate(10);
        return view('facilities.index', compact('facility','all','detf','ava','filter', 'type'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $facility = new Facility;
        $type = FacilityType::pluck('name', 'id')->all();
        $unit = Unit::pluck('name', 'id')->all();
        $facility->front_end=1;
        $facility->back_end=1;

        return view('facilities.create', compact('facility', 'type', 'unit'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();          
        
        $input = $request->except(['images','files']);

        $facility = new Facility;
        $facility->fill($input);
        $facility->no_stay_weekday_price= $request->no_stay_weekday_price?:0;
        $facility->weekend_price= $request->weekend_price?:0;

        $check = Facility::orderBy('id','DESC')->first();
                if($check)
                    $facility->id = $check->id+1;
         if($request->front_end=="on"){
            $facility->front_end = 1;
        }
          if($request->back_end=="on"){
            $facility->back_end = 1;
        }
        
        $facility->save();
        $facility->slug = str_slug($facility->name, '_');
        $chk=Facility::where('slug',$facility->slug)->first();
        $errors=[];
         if($chk){
                 $errors=array_add($errors,"name","Nama Fasilitas Sudah Pernah Digunakan!");                                        
                DB::rollBack();
                throw ValidationException::withMessages($errors);
            }

        $facility->save();
        if($request->Hasfile('images')){
        $file = $request->file('images');
            if(count($file)){
                foreach($file as $f){
                $filename = $f->getClientOriginalName();
                $path = public_path().'/image/';
                $f->move($path, $filename);
                
                $gallery = new Gallery;
                $check = Gallery::orderBy('id','DESC')->first();
                    if($check)
                        $gallery->id = $check->id+1;
                $gallery->image_path = $filename;
                $gallery->galleriable_id = $facility->id;
                $gallery->galleriable_type = 'Facility';
                $gallery->save();
                $gallery->slug = str_slug($gallery->id.'_'.$gallery->title, '_');
                $gallery->save();
                }
            }
        }
        DB::commit();
        
        $request->session()->flash('toast', 'Facility berhasil ditambahkan!');
        return redirect('/facilities/'.$facility->id);      
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Facility $facility
     * @return \Illuminate\Http\Response
     */
    public function show(Facility $facility){
        $facilityDetail = FacilityDetail::query();
        $facilityDetail = $facilityDetail->where('facility_id', '=', $facility->id);
        $facilityDetail = $facilityDetail->paginate(5, ['*'], 'attribute');
        $gallery = Gallery::query();
        $gallery = $gallery->where('galleriable_type', '=', 'Facility');
        $gallery = $gallery->where('galleriable_id', '=', $facility->id);
        $gallery = $gallery->orderBy('id','ASC')->paginate(5, ['*'], 'gallery');

        $detail_facility = DetailFacility::query();
        $detail_facility = $detail_facility->where('facility_id', '=', $facility->id);
        $detail_facility = $detail_facility->paginate(5, ['*'], 'detail');

        $custom_price = CustomPrice::query();
        $custom_price = $custom_price->where('price_id', '=', $facility->id);
        $custom_price = $custom_price->where('price_type', 'LIKE', "%Facility");
        $custom_price = $custom_price->paginate(5, ['*'], 'price');

        $special_prices = SpecialPrice::query();
        $special_prices = $special_prices->where('special_id', '=', $facility->id);
        $special_prices = $special_prices->where('special_type', 'LIKE', "%Facility");
        $special_prices = $special_prices->paginate(5);

        return view('facilities.show', compact('facility', 'facilityDetail', 'gallery', 'detail_facility', 'custom_price',  'special_prices'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Facility $facility
     * @return \Illuminate\Http\Response
     */
    public function edit(Facility $facility){
        $type = FacilityType::pluck('name', 'id')->all();
        $unit = Unit::pluck('name', 'id')->all();
        return view('facilities.create', compact('facility', 'type', 'unit'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Facility            $facility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility)
    {
           DB::beginTransaction();                  
        $facility->fill($request->all());
        $facility->front_end = 0;  
        $facility->back_end = 0;                          
          if($request->front_end){
            $facility->front_end = 1;
        }
          if($request->back_end){
            $facility->back_end = 1;
        }
        $facility->slug = str_slug($facility->id.'_'.$facility->name, '_');
        $facility->slug = str_slug($facility->name, '_');
        $chk=Facility::where('slug',$facility->slug)->where('id','<>',$facility->id)->first();
        $errors=[];
         if($chk){
                 $errors=array_add($errors,"name","Nama Fasilitas Sudah Pernah Digunakan!");                                        
                DB::rollBack();
                throw ValidationException::withMessages($errors);
            }
        
        $facility->save();
        DB::commit();
        $request->session()->flash('toast', 'Facility berhasil diubah!');
        
        return redirect('/facilities/'.$facility->id);        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Facility $facility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facility $facility,Request $request)
    {
        try {
            $facility->delete();
            $request->session()->flash('toast', 'Facility berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Facility gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/facilities');
    }

    //api
    public function getDep(Request $request){
        $fac = Facility::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($request->q)."%")->active()->pluck("id");
        $dfac = DetailFacility::whereIn("facility_id", $fac)->orwhere(DB::raw("lower(name)"), "LIKE", "%".strtolower($request->q)."%")->get()->pluck("nameLabel", "id");
        return $dfac;
    }

    public function get(Request $req){
        // $br = Folio::booked(carbon::now()->addDays(z)->format("Y-m-d"), carbon::now()->addDays(30)->format("Y-m-d"))->get();
        // return $br;
        $array=[];
        // if($req->q="") {
        //     return [];  
        // }
        if($req["d"]!=null){
            $dur = $req["d"];
        }
        else{
            $dur = 1;
        }
        if($req["p"]!=null){
            $prs = $req["p"];
        }
        else{
            $prs = 1;
        }
        if($req["c"]!=null){
            $disc = $req["c"];
        }
        else{
            $disc = 0;
        }
        
        $facs= Facility::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($req["q"])."%")->active()->get();
        $bundles = Bundle::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($req["q"])."%")->active()->get();
        $i=0;
        foreach ($facs as $facs) {
            $tmp = [];
            $tmp = array_add($tmp , "id", "F-".$facs->id);
            $tmp = array_add($tmp , "name", $facs->name);
            $tmp = array_add($tmp , "per_person", $facs->per_person);
            $tmp = array_add($tmp , "price", $facs->price);
            $array = array_add($array, $i, $tmp);
            $i++;
        }
        foreach ($bundles as $bundl) {
            $tmp = [];            
            $tmp = array_add($tmp , "id", "B-".$bundl->id);            
            $tmp = array_add($tmp , "name", $bundl->name);
            $tmp = array_add($tmp , "per_person", $bundl->per_person);
            $tmp = array_add($tmp , "price", $bundl->price);
            $array = array_add($array,$i, $tmp);
            $i++;
        }
        return $array;
    }
    public function getBuyable(Request $req){
        
        $array=[];
        if($req->q="") {
            return [];  
        }
        $facilities= Facility::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($request->q)."%")->active()->limit(10)->get();
        $bundles = Bundle::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($request->q)."%")->active()->get();
        $addons = Addon::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($request->q)."%")->active()->get();
        $i=0;
        foreach ($facilities as $facs) {
            $tmp = [];
            $tmp = array_add($tmp , "id", "F-".$facs->id);
            $tmp = array_add($tmp , "name", $facs->name);
            $tmp = array_add($tmp , "per_person", $facs->per_person);
            $tmp = array_add($tmp , "price", $facs->price);
            $array = array_add($array, $i, $tmp);
            $i++;
        }
        foreach ($bundles as $bundl) {
            $tmp = [];            
            $tmp = array_add($tmp , "id", "B-".$bundl->id);            
            $tmp = array_add($tmp , "name", $bundl->name);
            $tmp = array_add($tmp , "per_person", $bundl->per_person);
            $tmp = array_add($tmp , "price", $bundl->price);
            $array = array_add($array,$i, $tmp);
            $i++;
        }
           foreach ($addons as $addon) {
            $tmp = [];            
            $tmp = array_add($tmp , "id", "A-".$addon->id);            
            $tmp = array_add($tmp , "name", $addon->name);
            $tmp = array_add($tmp , "per_person", 0);
            $tmp = array_add($tmp , "price", $addon->price);
            $array = array_add($array,$i, $tmp);
            $i++;
        }
        return $array;
    }

    //api
    public function getUnit(Request $req)
    {
        $str = explode("-",$req->q);
        return $str;
        if($str[0]=="F"){

        }
        else{
            $dfac = DetailFacility::where("id", "=", $req->q)->first();             
        }
        if($dfac!=[]) {
            return json_encode(array($dfac->facility->unitLabel,$dfac->facility->per_person));
        }
    }
}
