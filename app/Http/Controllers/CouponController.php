<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coupon;
use App\CouponFacility;
use App\CouponFacilityType;
use App\CouponCustomer;
use App\CouponCustomerType;
use App\DetailFacility;
use App\Addon;
use App\Customer;
use App\Facility;
use App\Bundle;
use App\FacilityType;
use App\CustomerType;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;
use DB;

class CouponController extends Controller
{
    public function index(Request $request)
    {
        $coupons = Coupon::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    if($key=="start") {
                        $coupons  = $coupons->whereDate("start",">=",$value);
                    }
                    else if($key=="expiry") {
                        $coupons  = $coupons->whereDate("expiry","<=",$value);
                    }
                    else{
                        $coupons  = $coupons->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
                    }
                }
            }
        }
        $coupons=$coupons->paginate(30);
        return view('coupons.index')
        ->with('filter', $filter)
        ->with('coupons', $coupons);
    }
    public function create()
    {
        $coupon = new Coupon;
        return view('coupons._form')->with('coupon', $coupon);
    }
    public function store(Request $request)
    {
        DB::beginTransaction();  
        $errors=[];      
        $coupon = new Coupon;
        $check = Coupon::orderBy('id','DESC')->first();
            if($check)
                $coupon->id = $check->id+1;

        $coupon->fill($request->except('facility', 'ex_facil', 'ftype', 'ex_ftype', 'user', 'ex_user', 'role', 'ex_role'));
        if($coupon->min_spend > $coupon->max_spend){
            DB::rollBack();
            $errors=array_add($errors,"max_spend","Harga maksimum harus lebih besar dari harga minimum");            
            $errors=array_add($errors,"min_spend","Harga minimum harus lebih kecil dari harga maximum");            
        }
        $coupon->save();
        foreach ((array) $request->facility as $fac) {
            $fa = new CouponFacility;
            $check = CouponFacility::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $id = explode("-",$fac);
            if($id[0]=="F"){
                $facil= Facility::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Facility";
            }
            else if($id[0]=="A"){
                $facil= Addon::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Addon";                
            }
            else{
                 $facil= Bundle::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Bundle";                
            }
           
            $fa->save();
        }
        foreach ((array) $request->ex_facil as $fac) {
            $fa = new CouponFacility;
            $check = CouponFacility::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $id = explode("-",$fac);
            if($id[0]=="F"){
                $facil= Facility::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Facility";
            }
            else if($id[0]=="A"){
                $facil= Addon::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Addon";                
            }
            else{
                 $facil= Bundle::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Bundle";                
            }
           $check = CouponFacility::where("facs_type",$fa->facs_type)->where("facs_id",$fa->facs_id)->where("coupon_id",$fa->coupon_id)->get();
           if(count($check)>0){
             $errors=array_add($errors,"ex_facil","Fasilitas yang di include tidak bisa di exclude");                           
           }
           $fa->exclude = 1;
            $fa->save();
        }
        //coupon facility
        foreach ((array) $request->ftype as $fac) {
            $fa = new CouponFacilityType;
            $check = CouponFacilityType::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $fa->facility_type_id=$fac;
            $fa->save();
        }
        foreach ((array) $request->ex_ftype as $fac) {

            $fa = new CouponFacilityType;
            $check = CouponFacilityType::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $fa->facility_type_id=$fac;
            $check = CouponFacilityType::where("facility_type_id",$fa->facility_type_id)->where("coupon_id",$fa->coupon_id)->get();
            $fctye = FacilityType::find($fa->facility_type_id);
            $chk2 = CouponFacility::whereIn("facs_id",$fctye->facility->pluck("id"))->where("facs_type","LIKE","%Facility")->where("exclude","0")->count();
            if(count($check)>0){
                $errors=array_add($errors,"ex_ftype","Tipe Fasilitas yang di include tidak bisa di exclude");                           
            }
            if($chk2 >0){
                $errors=array_add($errors,"ex_ftype","Fasilitas yang di include memiliki Tipe Fasilitas yang akan diexclude");                                           
            }
            $fa->exclude = 1;
            $fa->save();
        }

        //coupon user
        foreach ((array) $request->user as $csr) {
            $fa = new CouponCustomer;
            $check = CouponCustomer::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $fa->customer_id=$csr;
            $fa->save();
        }
        foreach ((array) $request->ex_user as $csr) {
            $fa = new CouponCustomer;
            $check = CouponCustomer::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $fa->customer_id=$csr;
            $check = CouponCustomer::where("customer_id",$fa->customer_id)->where("coupon_id",$fa->coupon_id)->get();
            if(count($check)>0){
                $errors=array_add($errors,"ex_user","Pelanggan yang di include tidak bisa di exclude");                           
            }
            $fa->exclude = 1;
            $fa->save();
        }
        //coupon customer type
        foreach ((array) $request->role as $rl) {
            $role= new CouponCustomerType;
            $check = CouponCustomerType::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;
            $role->coupon_id=$coupon->id;
            $role->customer_type_id=$rl;
            $role->save();
        }
        //coupon customer type
        foreach ((array) $request->ex_role as $rl) {
            $role= new CouponCustomerType;
            $check = CouponCustomerType::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $role->coupon_id=$coupon->id;
            $role->customer_type_id=$rl;
            $check = CouponCustomerType::where("customer_type_id",$role->customer_type_id)->where("coupon_id",$role->coupon_id)->get();
            $ctyp = CustomerType::find($rl);
            $check2 = CouponCustomer::whereIn("id",$ctyp->pluck("id"))->where("exclude",0)->count();
            
            if(count($check)>0){
                $errors=array_add($errors,"ex_role","Jenis Pelanggan yang di include tidak bisa di exclude");                           
            }
            if($check2>0){
                $errors=array_add($errors,"ex_role","Pelanggan yang di include memiliki jenis pelanggan yang di exclude");                                           
            }
            $role->exclude=1;
            $role->save();
        }

         if(count($errors)>0){
                DB::rollBack();
                throw ValidationException::withMessages($errors);
            }
        $request->session()->flash('toast', 'Kupon berhasil ditambahkan!');
        DB::commit();
        return redirect()->route('coupons.index');
    }
    public function edit($id)
    {
        $coupon = Coupon::find($id);
    
        return view('coupons._form')->with('coupon', $coupon);
    }
    public function update($id, Request $request)
    {
        $coupon = Coupon::find($id);
         DB::beginTransaction();  
        $errors=[];      

        $coupon->fill($request->except('facility', 'ex_facil', 'ftype', 'ex_ftype', 'user', 'ex_user', 'role', 'ex_role'));
        if($coupon->min_spend > $coupon->max_spend){
            DB::rollBack();
            $errors=array_add($errors,"max_spend","Harga maksimum harus lebih besar dari harga minimum");            
            $errors=array_add($errors,"min_spend","Harga minimum harus lebih kecil dari harga maximum");            
        }
        $coupon->save();
        foreach ((array) $request->facility as $fac) {
            $fa = new CouponFacility;
            $check = CouponFacility::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $id = explode("-",$fac);
            if($id[0]=="F"){
                $facil= Facility::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Facility";
            }
            else if($id[0]=="A"){
                $facil= Addon::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Addon";                
            }
            else{
                 $facil= Bundle::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Bundle";                
            }
           
            $fa->save();
        }
        foreach ((array) $request->ex_facil as $fac) {
            $fa = new CouponFacility;
            $check = CouponFacility::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $id = explode("-",$fac);
            if($id[0]=="F"){
                $facil= Facility::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Facility";
            }
            else if($id[0]=="A"){
                $facil= Addon::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Addon";                
            }
            else{
                 $facil= Bundle::find($id[1]);
                $fa->facs_id=$facil->id;
                $fa->facs_type="App\\Bundle";                
            }
           $check = CouponFacility::where("facs_type",$fa->facs_type)->where("facs_id",$fa->facs_id)->where("coupon_id",$fa->coupon_id)->get();
           if(count($check)>0){
             $errors=array_add($errors,"ex_facil","Fasilitas yang di include tidak bisa di exclude");                           
           }
           $fa->exclude = 1;
            $fa->save();
        }
        //coupon facility
        foreach ((array) $request->ftype as $fac) {
            $fa = new CouponFacilityType;
            $check = CouponFacilityType::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $fa->facility_type_id=$fac;
            $fa->save();
        }
        foreach ((array) $request->ex_ftype as $fac) {

            $fa = new CouponFacilityType;
            $check = CouponFacilityType::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $fa->facility_type_id=$fac;
            $check = CouponFacilityType::where("facility_type_id",$fa->facility_type_id)->where("coupon_id",$fa->coupon_id)->get();
            $fctye = FacilityType::find($fa->facility_type_id);
            $chk2 = CouponFacility::whereIn("facs_id",$fctye->facility->pluck("id"))->where("facs_type","LIKE","%Facility")->where("exclude","0")->count();
            if(count($check)>0){
                $errors=array_add($errors,"ex_ftype","Tipe Fasilitas yang di include tidak bisa di exclude");                           
            }
            if($chk2 >0){
                $errors=array_add($errors,"ex_ftype","Fasilitas yang di include memiliki Tipe Fasilitas yang akan diexclude");                                           
            }
            $fa->exclude = 1;
            $fa->save();
        }

        //coupon user
        foreach ((array) $request->user as $csr) {
            $fa = new CouponCustomer;
            $check = CouponCustomer::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $fa->customer_id=$csr;
            $fa->save();
        }
        foreach ((array) $request->ex_user as $csr) {
            $fa = new CouponCustomer;
            $check = CouponCustomer::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;

            $fa->coupon_id=$coupon->id;
            $fa->customer_id=$csr;
            $check = CouponCustomer::where("customer_id",$fa->customer_id)->where("coupon_id",$fa->coupon_id)->get();
            if(count($check)>0){
                $errors=array_add($errors,"ex_user","Pelanggan yang di include tidak bisa di exclude");                           
            }
            $fa->exclude = 1;
            $fa->save();
        }
        //coupon customer type
        foreach ((array) $request->role as $rl) {
            $role= new CouponCustomerType;
            $check = CouponCustomerType::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;
            $role->coupon_id=$coupon->id;
            $role->customer_type_id=$rl;
            $role->save();
        }
        //coupon customer type
        foreach ((array) $request->ex_role as $rl) {
            $role= new CouponCustomerType;
            $check = CouponCustomerType::orderBy('id','DESC')->first();
            if($check)
                $fa->id = $check->id+1;
                
            $role->coupon_id=$coupon->id;
            $role->customer_type_id=$rl;
            $check = CouponCustomerType::where("customer_type_id",$role->customer_type_id)->where("coupon_id",$role->coupon_id)->get();
            $ctyp = CustomerType::find($rl);
            $check2 = CouponCustomer::whereIn("id",$ctyp->pluck("id"))->where("exclude",0)->count();
            
            if(count($check)>0){
                $errors=array_add($errors,"ex_role","Jenis Pelanggan yang di include tidak bisa di exclude");                           
            }
            if($check2>0){
                $errors=array_add($errors,"ex_role","Pelanggan yang di include memiliki jenis pelanggan yang di exclude");                                           
            }
            $role->exclude=1;
            $role->save();
        }

         if(count($errors)>0){
                DB::rollBack();
                throw ValidationException::withMessages($errors);
            }
        $request->session()->flash('toast', 'Kupon berhasil ditambahkan!');
        DB::commit();
        return redirect()->route('coupons.index');
    }
    public function show(Coupon $coupon)
    {
        return view('coupons.show')->with('coupon', $coupon);
    }
    public function destroy(Request $request, Coupon $coupon)
    {
        CouponCustomer::where("coupon_id",$coupon->id)->delete();
        CouponCustomerType::where("coupon_id",$coupon->id)->delete();
        CouponFacilityType::where("coupon_id",$coupon->id)->delete();
        CouponFacility::where("coupon_id",$coupon->id)->delete();
        $coupon->delete();
        $request->session()->flash('toast', 'Kupon berhasil dihapus!');
        $coupon->delete();
        return redirect()->route('coupons.index');
    }

    public function get(Request $req){
        $cup=Coupon::where("code", "LIKE", $req->coden)->first();
        if(sizeof($cup)<=0) {
            return json_encode(array("Tidak Ada Kupon Tersebut",0,0,0));            
        }
        if($cup->expiry->diffInDays(Carbon::now())>0 && $cup->expiry->isPast()) {
            return json_encode(array("Kupon Gagal digunakan! Kupon Sudah Expired",0,0,0));
        }
        if($cup->start->diffInDays(Carbon::now())>0 && $cup->start->isFuture()) {
            return json_encode(array("Kupon Gagal digunakan! Kupon Tidak Berlaku",0,0,0));
         }
        $cst = Customer::find($req->customer_id);        
        $cus = $cup->coupon_customer;
        $count=0;
        $match=0;
        foreach($cus as $cs){
            if($req->customer_id==$cs->customer_id && $cs->exclude==1){    
                return json_encode(array("Kupon Gagal digunakan! Customer ini tidak dapat menggunakan kupon ini",0,0,0));                                                                                                      
            }
            else if($req->customer_id==$cs->customer_id ){
                $match++;
            }
            if($cs->exclude==0){  
                $count++;  
            }
        }
        if($match!=$count){
            return json_encode(array("Kupon Gagal digunakan! Tidak Terdapat kupon untuk customer ini",0,0,0));                                 
            
        }
        
         $count=0;
        $match=0;
        $label="";                
        $custype = $cup->coupon_customer_type;
    
        foreach($custype as $cs){
            if($cst->customer_type_id==$cs->customer_type_id && $cs->exclude==1){    
                return json_encode(array("Kupon Gagal digunakan! golongan customer ini tidak dapat menggunakan kupon ini",0,0,0));                                                   
            }
            else if($cst->customer_type_id==$cs->customer_type_id ){
                $match++;
            }
            if($cs->exclude==0){  
                if($count!=0){
                    $label.=", ";

                } 
                $tmp = CustomerType::find($cs->customer_type_id);
                $label .=$tmp->name;
                $count++;  
                  
            }
        }
          if($match!=$count){
            return json_encode(array("Kupon Gagal digunakan! Kupon hanya dapat digunakan golongan customer ".$label,0,0,0));                                 
            
        }

        $fac = $cup->coupon_facility;
         $count=0;
        $match=0;
        $label="";        
        $name="";
         foreach($fac as $fcs){
                

            foreach($req->facil as $fas){
                $id= explode("-",$fas);
                if($id[0]=="F"){
                    
                    if($fcs->facs_id==$id[1] && $fcs->exclude==1 && $fcs->facs_type=="App\\Facility"){
                        return json_encode(array("Kupon Gagal digunakan!terdapat Fasilitas yang tidak dapat menggunakan kupon ini",0,0,0));
                    }
                    else if($fcs->facs_id==$id[1]  && $fcs->facs_type=="App\\Facility"){
                        $match++;
                    }
                 
                }
                else{
                                                                                       
                      if($fcs->facs_id==$id[1] && $fcs->exclude==1 && $fcs->facs_type=="App\\Bundle"){
                        return json_encode(array("Kupon Gagal digunakan!terdapat paket yang tidak dapat menggunakan kupon ini",0,0,0));                                                            
                    }
                    else if($fcs->facs_id==$id[1] && $fcs->facs_type=="App\\Bundle"){
                        $match++;
                    }
                }
            }
            if(!isset($req->addons)){
                $req->addons=[];
            }
            foreach($req->addons as $add){
                                                   
                if($fcs->facs_id==$add && $fcs->exclude==1 && $fcs->facs_type=="App\\Addon"){
                    return json_encode(array("Kupon Gagal digunakan! terdapat Tambahan yang tidak dapat menggunakan kupon ini",0,0,0));                                 
                }
                else if($fcs->facs_id==$add && $fcs->facs_type=="App\\Addon"){
                    $match++;
                }
            }
                    if($fcs->exclude==0){
                        if($fcs->facs_type =="App\\Facility"){
                            $tmp = Facility::find($fcs->facs_id);                            
                            $name = $tmp->name;
                        }
                        elseif($fcs->facs_type=="App\\Addon" ){
                             $tmp = Addon::find($fcs->facs_id);  
                                                               
                            $name = $tmp->name;  
                        }
                        else{
                            $tmp = Bundle::find($fcs->facs_id);     
                            $name = $tmp->name; 
                        }
                        if($count!=0){
                            $label.=", ";

                        }    
                                    
                        $label.= $name;
                        $count++;  
                    }
        }
        if($match!=$count){
            return json_encode(array("Kupon Gagal digunakan!Kupon hanya bisa digunakan bila terdapat ".$label,0,0,0));                                 
            
        }
        $count=0;
        $match=0;
        $factype = $cup->coupon_facility_type;
        $label="";
        foreach($factype as $fcs){
                if($fcs->exclude==0){  
                        if($count!=0){
                            $label.=", ";

                        }                
                        $count++;  
                        $factypes = FacilityType::find($fcs->facility_type_id);
                        $label.= $factypes->name;
                        
                    }
                    
            foreach($req->facil as $fas){
                $id= explode("-",$fas);
                if($id[0]=="F"){
                    $faciltyp = Facility::find($id[1]);
                    if($fcs->facility_type_id==$faciltyp->type_id && $fcs->exclude==1){
                        return json_encode(array("Kupon Gagal digunakan!terdapat golongan Fasilitas yang tidak dapat menggunakan kupon ini",0,0,0));
                    }
                    else if($fcs->facility_type_id==$faciltyp->type_id){
                        $match++;
                    }
                    
                }
            }
        }
        if($match!=$count){
            return json_encode(array("Kupon Gagal digunakan! hanya golongan fasilitas ".$label." yang bisa menggunakan kupon ini",0,0,0));                                 
            
        }
        $i=1;
        $dis=0;
        $totall=0;
        if($req->facil != []){        
            foreach($req->facil as $fas){
                    $id= explode("-",$fas);
                    if($id[0]=="F"){
                        $dfas = Facility::find($id[1]);
                        $prs = $req->person[$i];
                        if($req->person[$i]==null){
                            $prs=1;
                        }
                        $totall+= $dfas->calPrice($req->check_in[$i],$req->duration[$i], $prs);                           
                    }
                    else{
                        $dfas = Bundle::find($id[1]);
                        $prs = $req->person[$i];
                        if($req->person[$i]==null){
                            $prs=1;
                        }                       
                        $totall+= $req->duration[$i] * $prs * $dfas->price;                        
                    }
                if($req->discon[$i]!=0 || $req->discon[$i]!=null) {
                    $dis++;
                }
                $i++;
            }
        }
        $i=1;
        if($req->addons != []){
            foreach($req->addons as $fas){
                $dfas = Addon::find($fas);
                $totall+= $req->amount[$i] * $dfas->price;
                    if($req->disca[$i]!=0 || $req->disca[$i]!=null) {
                        $dis++;
                    }
                $i++;
            }
        }
        

        if($totall< $cup->min_spend) {
            return json_encode(array("Kupon Gagal digunakan! Total Pembelanjaan dibawah batas minimum Kupon",0,0,0));                         
        }

        if($totall>$cup->max_spend) {
            return json_encode(array("Kupon Gagal digunakan! Total Pembelanjaan diatas batas maksimum Kupon",0,0,0));             
        }
        if($dis>0 && $cup->exclude_sale==1) {
            return json_encode(array("Kupon Gagal digunakan! Kupon tidak bisa digabungkan dengan barang diskon",0,0,0)); 
        }
        $cuts = $cup->amount;
        if($cup->type==1){
            $cuts = $totall*($cup->amount/100);
        }
        return json_encode(array("Selamat! Kupon Dapat Digunakan",$cup->type, $cuts, $cup->id));
    }
}
