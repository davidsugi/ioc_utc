<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Customer;
use App\CustomerType;
use Carbon\carbon;
use App\Facility;
use App\DetailFacility;
use App\FacilityType;
use App\DetailOrder;
use Barryvdh\Snappy\Facades\SnappyPdf;
use App\Folio;
use Excel;
use App\Bundle;
use H;

class ReportController extends Controller{
    // public function customer(Request $request){
    //     $orders = Order::query();
    //     $filter = [];
    //     if(isset($request->filter)) {
    //         $filter = $request->filter;
    //         foreach ($filter as $key => $value) {
    //             if(!empty($value)) {
    //                 $orders  = $orders->where($key, 'like', '%'.$value.'%');
    //             }
    //         }
    //     }
    //     $orders=$orders->orderBy('id','ASC');
    //     return view('report.index')->with('orders',$orders)->with('filter',$filter);
    // }
    public function index()
    {
        return view('report.index');
    }

    public function customer(Request $request){
        // return $request;
  
        // $notin = Order::whereDate("date","<",$request->filter['start'])->orwhereDate("date",">",$request->filter['end']);
        $cstyp = CustomerType::all();
        $result=[];
        foreach($cstyp as $cust){
            if($cust->name=="External"){
                $cuscat = Customer::where('customer_type_id',$cust->id)->select('customer_type_id','category')->distinct()->get();
                
                foreach( $cuscat as $cat){
                    $cus = Customer::where('customer_type_id',$cust->id)->where('category',$cat->category)->pluck('id');
                    $results = Order::whereIn("customer_id",$cus);
                    if($request->start!=null)
                        $results = $results->whereDate("date",">=",$request->start);
                    if($request->end!=null)
                        $results = $results->whereDate("date","<=",$request->end);                
                    if($request->status){
                        $results = $results->whereIn("status",$request->status);
                    }
                    $results = $results->get();
                    if(count($results)>0)
                        $result = array_add($result,$cat->category_label,$results);   
                }
            }
            else{
                $results = Order::whereIn("customer_id",$cust->customer->pluck("id"));
                if($request->start!=null)
                    $results = $results->whereDate("date",">=",$request->start);
                if($request->end!=null)
                    $results = $results->whereDate("date","<=",$request->end);                
                if($request->status){
                    $results = $results->whereIn("status",$request->status);
                }
                $results = $results->get();
                $result = array_add($result,$cust->name,$results);   
            }
          
        }
        // return $result;
        $datetitle=H::date_title($request['start'],$request['end']);
        // return view('report.customer')->with(['result' => $result, 'dated'=>$dated]);
        $pdf = SnappyPdf::loadView('report.customer', ['result' => $result, 'dated'=>$datetitle]);
        // ->setPaper('letter','landscape');
        return $pdf->download('Laporan Order Per Customer['.$datetitle.'].pdf');
    }

    public function facility(Request $request){
        // return $request;
        // $notin = Order::whereDate("date","<",$request->filter['start'])->orwhereDate("date",">",$request->filter['end']);
        $fcstyp = FacilityType::all();
        $result=[];
        foreach($fcstyp as $facs){
            $results = DetailOrder::join('orders', 'orders.id', '=', 'detail_orders.order_id')->join('customers', 'customers.id', '=', 'orders.customer_id')->where("item_type","LIKE","%Facility")->whereIn("item_id",$facs->facility->pluck("id"));
            if($request->start!=null)
                $results = $results->whereDate("date",">=",$request->start);
            if($request->end!=null)
                $results = $results->whereDate("date","<=",$request->end); 
            if($request->status){
                $results = $results->whereIn("orders.status",$request->status);
            }
            $results = $results->selectRaw("detail_orders.id,price,amount,amount2,item_type,item_id ,detail_orders.disc, orders.date, orders.order_code, customers.name cname")->get();
            $result = array_add($result,$facs->name,$results);
        }
            $rest=[];
            $results = DetailOrder::join('orders', 'orders.id', '=', 'detail_orders.order_id')->join('customers', 'customers.id', '=', 'orders.customer_id')->where("item_type","LIKE","%Bundle");
            if($request->start!=null)
                $results = $results->whereDate("date",">=",$request->start);
            if($request->end!=null)
                $results = $results->whereDate("date","<=",$request->end); 
            if($request->status){
                $results = $results->whereIn("orders.status",$request->status);
            }
            $results = $results->selectRaw("detail_orders.id,price,amount,amount2,item_type,item_id ,detail_orders.disc, orders.date, orders.order_code, customers.name cname")->get();
            $rest = array_add($rest,"Bundle",$results);
        
             $results = DetailOrder::join('orders', 'orders.id', '=', 'detail_orders.order_id')->join('customers', 'customers.id', '=', 'orders.customer_id')->where("item_type","LIKE","%Addon");
            if($request->start!=null)
                $results = $results->whereDate("date",">=",$request->start);
            if($request->end!=null)
                $results = $results->whereDate("date","<=",$request->end); 
            if($request->status){
                $results = $results->whereIn("orders.status",$request->status);
            }
            $results = $results->selectRaw("detail_orders.id,price,amount,amount2,item_type,item_id ,detail_orders.disc, orders.date, orders.order_code, customers.name cname")->get();
            $rest = array_add($rest,"Addon",$results);
        
            // return $result;
        
        // return $result;
        $datetitle=H::date_title($request['start'],$request['end']);
        // return view('report.facility')->with(['result' => $result,'rest' => $rest, 'dated'=>$datetitle]);
        $pdf = SnappyPdf::loadView('report.facility', ['result' => $result,'rest' => $rest, 'dated'=>$dated]);
        // ->setPaper('letter','landscape');
        return $pdf->download('Laporan Order Per Fasilitas['. $datetitle .'].pdf');
        
    }

    public function bkm(Request $request){        
        $datetitle=H::date_title($request->start,$request->end);
        $orders= Order::where('other','<>','');
        if($request->start!=null)
            $orders = $orders->whereDate("date",">=",$request->start);
        if($request->end!=null)
            $orders = $orders->whereDate("date","<=",$request->end);                
        if($request->status){
                        $orders = $orders->whereIn("status",$request->status);
        }
        $orders=$orders->orderBy('date','ASC')->get();
        $results=[];
        
        foreach($orders as $i => $ord){
            $tmp=[];            
            $tmp['No']=$i+1;
            $tmp['Tgl']=$ord->date->format("d/m/Y");
            $jsons=json_decode($ord->other);
            // return $jsons;
            $duplicatevalue=[];
            foreach($jsons as $j => $json){
                $thisamount=0;
                $thisdiscount=0;
                if(in_array($json->item, $duplicatevalue)){
                    continue;
                }
                foreach($jsons as $k => $js){
                    if($js->item==$json->item){
                         if(!in_array($js->item, $duplicatevalue)){
                             $duplicatevalue[]=$js->item;
                         }
                         $thisamount+=$js->jmlitem;
                         $thisdiscount+=$js->diskon;
                    }
                }
                if($j>0){
                    $tmp['No']='';
                    $tmp['Tgl']='';
                }
                $tmp['Pengguna']=$json->pengguna;
                $tmp['Tempat/Barang']=$json->item;
                $tmp['Jml Orang/Barang']=$thisamount;
                $tmp['Jml Hari']=$json->jmlhari;
                $tmp['Biaya Perawatan Unit']=H::rupiah($json->biaya);
                $tmp['Diskon']=H::rupiah($thisdiscount);
                $totals=$thisamount*$json->jmlhari*$json->biaya-$thisdiscount;
                $tmp['Jumlah(Rp)']=H::rupiah($totals);
                $tmp['No BKM']="";
                $results[]=$tmp;
                $tmp=[];
            }
        }
        return Excel::create('LAPORAN BKM '. $datetitle , function($excel) use($results) {
            $excel->sheet('BKM',function($sheet) use($results){
                $sheet->fromArray($results);
            });
        })->download('xls');

    }

    public function usage_index()
    {
        return view('report.usage');
    }

    public function usage(Request $req){
        $datetitle=H::date_title($req->start,$req->end);
        $orders= Order::where('other','<>','');
        if($req->start!=null)
                        $orders = $orders->whereDate("date",">=",$req->start);
        if($req->end!=null)
                        $orders = $orders->whereDate("date","<=",$req->end);                
        if($req->status){
                        $orders = $orders->whereIn("status",$req->status);
        }
        $orders=$orders->orderBy('date','ASC')->get();
        $results=[];
        
        foreach($orders as $i => $ord){
            $tmp=[];            
            $tmp['No']=$i+1;
            $tmp['Tgl']=$ord->date->format("d/m/Y");
            $jsons=json_decode($ord->other);
            // return $jsons;
            $duplicatevalue=[];
            foreach($jsons as $j => $json){
                $thisamount=0;
                if(in_array($json->item, $duplicatevalue)){
                    continue;
                }
                foreach($jsons as $k => $js){
                    if($js->item==$json->item){
                         if(!in_array($js->item, $duplicatevalue)){
                             $duplicatevalue[]=$js->item;
                         }
                         $thisamount+=$js->jmlitem;
                    }
                }
                if($j>0){
                    $tmp['No']='';
                    $tmp['Tgl']='';
                }
                $tmp['Pengguna']=$json->pengguna;
                $tmp['Tempat/Barang']=$json->item;
                $tmp['Jml Orang/Barang']=$thisamount;
                $tmp['Jml Hari']=$json->jmlhari;
                $results[]=$tmp;
                $tmp=[];
            }
        }
        return Excel::create('LAPORAN PENGGUNAAN '.$datetitle , function($excel) use($results) {
            $excel->sheet('PENGGUNAAN',function($sheet) use($results){
                $sheet->fromArray($results);
            });
        })->download('xls');
    }
}
//junk
//  $fac=Bundle::orderBy('id','DESC')->first();

//         $subtot = $fac->calDetprice("2018-10-11",9,2,20,1,1);                    
//         return $subtot;
//         $ord = DetailOrder::join('orders', 'orders.id', '=', 'detail_orders.order_id')->join('customers', 'customers.id', '=', 'orders.customer_id')->selectRaw("detail_orders.id, order_id, item_id, item_type, price,amount,amount2,item_type,item_id ,detail_orders.disc, orders.date, orders.order_code, customers.name cname, detail_orders.other other")->get();
//         // return $ord;
//         $fol= Order::all();
//         $results=[];
//         $added=[];
//         $lastordid=0;
//         $lastperiod=0;
//         $id=1;
//         foreach($ord as $i=>$or){
//             if($or->other==""){
//                 $tmp=[];
//                 $tmp['No']=$lastordid!=$or->order_id ? $id : '';
//                 if($lastordid!=$or->order_id){
//                     $id++;
//                     $added=[];
//                     $lastordid=$or->order_id;
//                     $start=Carbon::parse($or->order->checkinLabel);
//                     $end=Carbon::parse($or->order->checkoutLabel);
//                     $lastperiod= $start->diffInDays($end);
//                 }
//                 $tmp['Tgl']=$or->date;
//                 $tmp['Pengguna']=$or->cname;
//                 $tmp['Tempat/Barang']=$or->nameLabel;
//                 if($or->item->per_person==0){
//                     if(in_array($or->nameLabel, $added)){
//                         continue;
//                     }
//                     $con = DetailOrder::where('order_id',$or->order_id)->where('item_id',$or->item_id)->where('item_type',$or->item_type)->count();
//                     $tmp['Jml Orang/Barang']=$con;    
//                     $added[]=$or->nameLabel;                                    
//                 }
//                 else if($or->item->per_person==1){
//                     $tmp['Jml Orang/Barang']=$or->durasi_atau_jumlah < $or->item->minimum ? $or->item->minimum : str_replace('.00', '', $or->durasi_atau_jumlah);                 
//                 }
//                 else{
//                     $tmp['Jml Orang/Barang']=str_replace('.00', '', $or->durasi_atau_jumlah);                    
//                 }
               
//                 $tmp['Jml Hari']=$lastperiod;
//                 $tmp['Biaya Perawatan Unit']=$or->price;
//                 $tmp['Jumlah(Rp)']=$tmp['Jml Hari']*$tmp['Biaya Perawatan Unit']*$tmp['Jml Orang/Barang'];
//                 $tmp['No BKM']="";
//                 $results[]=$tmp;
//             }
//             else{
//                 $ids=$lastordid!=$or->order_id ? $id : '';
//                 if($lastordid!=$or->order_id){
//                     $id++;
//                     $added=[];
//                     $lastordid=$or->order_id;
//                     $start=Carbon::parse($or->order->checkinLabel);
//                     $end=Carbon::parse($or->order->checkoutLabel);
//                     $lastperiod= $start->diffInDays($end);
//                 }
//                 $details= json_decode($or->other);
//                 foreach($details as $j=>$det){
//                 $tmp=[];
//                 $tmp['No']=$j==0 ? $ids : '';
//                 $tmp['Tgl']=$or->date;
//                 $tmp['Pengguna']=$or->cname;
//                 $tmp['Tempat/Barang']=$det->name;
//                 $tmp['Jml Orang/Barang']=$det->jumlah;
//                 $tmp['Jml Hari']=$det->hari;
//                 $tmp['Biaya Perawatan Unit']=$det->harga;
//                 $tmp['Jumlah(Rp)']=$det->jumlah*$det->hari*$det->harga;
//                 $tmp['No BKM']="";
//                 $results[]=$tmp;
//                 }
//             }
            
//         }
//         // return $results;

//         // $stock = StockCard::where('warehouse_id',Auth::user()->warehouse_id)->join('stocks','stocks.id','stock_cards.stock_id')->selectRaw('stocks.name `Nama Stock`, amount jumlah, null as `Jumlah Fisik`')->get();
//         // return $stock;
//         // $csv = Writer::createFromFileObject(new SplTempFileObject());
//         // $csv->insertOne(['No','Nama Produk','Jumlah virtual', 'Jumlah Aktual']);
//         // $i=0;
//         // foreach ($stock as $key=> $reg) {
//         //     $csv->insertOne([$key+1,$reg->stock->name, $reg->amount, '']);  
//         // }

//         // $csv->output('Stock Opname '. Carbon::now()->format('d-m-y') .'.csv');
//         // return;
