<?php

namespace App\Http\Controllers;

use App\Bundling;
use Illuminate\Http\Request;

class BundlingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bundling = Bundling::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $bundling = $bundling->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");
                }
            }
        }
        
        $bundling = $bundling->paginate(10);
        return view('bundlings.index', compact('bundling', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bundling = new Bundling;
        return view('bundlings.create', compact('bundling'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('image');
        $filename = $file->getClientOriginalName();
        $path = public_path().'/image/';
        $file->move($path, $filename);

        $news = new News;
        $input = $request->except(['categories','image']);
        $news->fill($input);
        $news->image = $filename;

        $news->user_id = Auth::user()->id;
        $news->save();

        foreach($request->categories as $c){
            $news->category()->attach($c);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bundling $bundling
     * @return \Illuminate\Http\Response
     */
    public function show(Bundling $bundling)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bundling $bundling
     * @return \Illuminate\Http\Response
     */
    public function edit(Bundling $bundling)
    {
        return view('bundlings.create', compact('bundling'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Bundling            $bundling
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bundling $bundling)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bundling $bundling
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bundling $bundling)
    {
        //
    }
}
