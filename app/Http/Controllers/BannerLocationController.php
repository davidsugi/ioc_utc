<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BannerLocation;
use DB; 
use Illuminate\Validation\ValidationException; 
use Illuminate\Support\Facades\Validator;

class BannerLocationController extends Controller
{

  public function index(Request $request)
  {
      $banner_locations = BannerLocation::query();
      $filter = [];
      if(isset($request->filter)) {
          $filter = $request->filter;
          foreach ($filter as $key => $value) {
              if(!empty($value)) {
                  $banner_locations  = $banner_locations->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");  
              }
          }
      }
      $banner_locations=$banner_locations->orderBy('id','DESC')->paginate(30);
      return view('banner_location.index')
      ->with('banner_locations', $banner_locations)
      ->with('filter',$filter);
  }
  public function create(){
      $banner_location = new BannerLocation;
      return view('banner_location.create')->with('banner_location', $banner_location);
  }
  public function store(Request $request){
      DB::beginTransaction();
     
      $banner_location = new BannerLocation;

        $check = BannerLocation::orderBy('id','DESC')->first();
        if($check)
            $banner_location->id = $check->id+1;

      $banner_location->fill($request->all());
      $banner_location->save();
      DB::commit();
      $request->session()->flash('toast', 'Lokasi Banner berhasil ditambahkan!');
      return redirect()->route('banner_locations.index');
  }
  public function edit($id){
      $banner_location = BannerLocation::find($id);
      return view('banner_location.create')->with('banner_location',$banner_location);
  }
  public function update($id, Request $request){
      DB::beginTransaction();
      $banner_location = BannerLocation::find($id);
      $banner_location->fill($request->all());
      
      $banner_location->save();
      DB::commit();
      $request->session()->flash('toast', 'Lokasi Banner  berhasil diubah!');
      return redirect()->route('banner_locations.index');
  }
  public function show(BannerLocation $banner_location){
      return view('banner_location.show')->with('banner_location', $banner_location);
  }
  public function destroy(Request $request, BannerLocation $banner_location){
        try {
            $banner_location->delete();
            $request->session()->flash('toast', 'Lokasi Banner berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('error', 'Lokasi Banner gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        return redirect()->route('banner_locations.index');
    }
}
