<?php

namespace App\Http\Controllers;

use App\Customer;
use App\City;
use App\CustomerType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Validation\ValidationException; 
use Illuminate\Database\QueryException;



class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getCity()
    {
        $curl = curl_init();
        curl_setopt_array(
            $curl, array(
            CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "key:0927958fafb5fb0b6187731768a1f6e1"
            ),
            )
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $data = json_decode($response, true);
        for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
            $city = new City;
            $city->city= $data['rajaongkir']['results'][$i]['city_name'];
            $city->type= $data['rajaongkir']['results'][$i]['type'];
            $city->province= $data['rajaongkir']['results'][$i]['province'];
            $city->save();
        }
        curl_close($curl);
    }
    public function index(Request $request)
    {
        $customer = Customer::query();

        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $customer = $customer->where(DB::raw("lower(name)"), "LIKE", "%".strtolower($value)."%");  

                    if($key=='name'){
                        $customer = $customer->orwhere(DB::raw("lower(company_name)"), "LIKE", "%".strtolower($value)."%");  
                    }
                }
            }
        }

        $customer = $customer->orderBy('name','ASC')->paginate(20);
        return view('customers.index', compact('customer', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer = new Customer;
        $city = City::all();
        $city = $city->pluck('full_city', 'id');
        $provinces = City::all()->pluck('province', 'province');
        $customer_type = CustomerType::all();
        $customer_type = $customer_type->pluck('name', 'id');
        return view('customers.create', compact('customer', 'city','provinces' ,'customer_type'));
        
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $chk=Customer::where('card_id',$request->card_id)->first();
        $errors=[];
         if($chk){
            $errors=array_add($errors,"card_id","Nomor Kartu Identitas Sudah Pernah Digunakan!");                                        
            DB::rollBack();
            throw ValidationException::withMessages($errors);
        }

       
        $customer = new Customer;
        $check = Customer::orderBy('id','DESC')->first();
            if($check)
                $customer->id = $check->id+1;
        $customer->fill($request->except('is_company','province_id','active'));
        $customer->blacklist = $request->active ? 0 : 1;
        if($request->is_company==null)
            {$customer->company_name="";}
        $customer->save();
        $request->session()->flash('toast', 'Customer berhasil ditambahkan!');
        
        return redirect('/customers');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return view('customers.show')
        ->with("customer",$customer);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $city = City::all();
        $city = $city->pluck('full_city', 'id');
        $customer_type = CustomerType::all(); 
        $customer_type = $customer_type->pluck('name', 'id');
        $provinces = City::all()->pluck('province', 'province');
        return view('customers.create', compact('customer', 'city','provinces', 'customer_type'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Customer            $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $customer->fill($request->except("is_company",'province_id','active'));
        $customer->blacklist = $request->active ? 0 : 1;
        if(!$request->is_company){
            $customer->company_name="";
        }

        $customer->save();
        $request->session()->flash('toast', 'Customer berhasil diubah!');
        return redirect('/customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Customer $customer)
    {
        try {
            $customer->delete();
            $request->session()->flash('toast', 'Customer berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Customer gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/customers');
    }

    //api
    public function get(Request $request)
    {
        $cus = Customer::where(DB::raw("lower(name)"), "LIKE", "%".strtolower($request->q)."%")->orWhere(DB::raw("lower(company_name)"), "LIKE", "%".strtolower($request->q)."%")->active()->get()->pluck('nameLabel', 'id');
        return $cus;
    }
    public function getData(Request $req)
    {
        $cus = Customer::find($req->id);
        $cus->push("nmlabel",$cus->nameLabel);
        return $cus;
    }
    public function getProvince(Request $req){
        $result = City::where('province','LIKE',"%".$req->q."%")->distinct()->limit(10)->pluck('province','province');
        return $result;
    }

    public function getKota(Request $req){
        $result = City::where('city','LIKE',"%".$req->q."%")->where('province','LIKE',"%".$req->p."%")->limit(10)->pluck('city','id');
        return $result;
    }
}
