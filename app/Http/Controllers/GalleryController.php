<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Facility;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $gallery = Gallery::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $gallery = $gallery->whereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");
                }
            }
        }
        $gallery = $gallery->where('galleriable_type', '=', '')->orWhereNull('galleriable_type');
        $gallery = $gallery->paginate(10);
        return view('galleries.index', compact('gallery', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gallery = new Gallery;
        $facility = Facility::pluck('name', 'id')->all();
        return view('galleries.create', compact('gallery', 'facility'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('image');
        $filename = $file->getClientOriginalName();
        $path = public_path().'/image/';
        $file->move($path, $filename);

        $gallery = new Gallery;
        $check = Gallery::orderBy('id','DESC')->first();
            if($check)
                $gallery->id = $check->id+1;
        $gallery->image_path = $filename;
        $gallery->title = $request->title;
        $gallery->description = $request->description;        
        if($request->description=="")
            $gallery->description = $request->title;
            
        $gallery->save();
        $gallery->slug = str_slug($gallery->id.'_'.$gallery->title, '_');
        $gallery->kode = $request->kode;
        $gallery->save();
        $request->session()->flash('toast', 'Gallery berhasil ditambahkan!');
        
        return redirect('/galleries');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {

        $facility = Facility::pluck('name', 'id')->all();
        return view('galleries.create', compact('gallery', 'facility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Gallery             $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {
        if($request->file('image')){
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/image/';
            $file->move($path, $filename);
            $gallery->image_path = $filename;            
        }

        $gallery->title = $request->title;
        $gallery->description = $request->description;
        $gallery->kode = $request->kode;
        $gallery->slug = str_slug($gallery->id.'_'.$gallery->title, '_');
        $gallery->save();
        $request->session()->flash('toast', 'Gallery berhasil diubah!');
        return redirect('/galleries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Gallery $gallery)
    {
        try {
            $gallery->delete();
            $request->session()->flash('toast', 'Gallery berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Gallery gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/galleries');
    }
}
