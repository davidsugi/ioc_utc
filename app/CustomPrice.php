<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CustomPrice extends Model
{
    protected $guarded =['rangedate'];
    protected $dates = ['startdate','enddate'];
    protected $appends = ['carbonstart','carbonend'];

    
    public function facility()
    {
        return $this->morphTo();
    }
    public function getCarbonstartAttribute()
    {
        $startdate = Carbon::parse($this->startdate)->format('d/m/Y H:i:s');
        return $startdate;
    }
    public function getCarbonendAttribute()
    {
        $enddate = Carbon::parse($this->enddate)->format('d/m/Y H:i:s');
        return $enddate;
    }
  

    public function getNameLabelAttribute()
    {
        $obj = Addon::find($this->price_id);
        if($this->price_type=="App\\Facility") {
            $obj=Facility::find($this->price_id);        
        }
        else if($this->price_type=="App\\Bundle"){
            $obj = Bundle::find($this->price_id);
        }
        if($obj)
            return $obj->name;
        return "-";
    }
}
