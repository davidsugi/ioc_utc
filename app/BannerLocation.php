<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerLocation extends Model
{
     	
    protected $guarded = [];
    public function banner()
    {
        return $this->hasMany('App\Banner', 'banner_location_id');
    }
}
