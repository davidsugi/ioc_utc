<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Addon;
use App\DetailFacility; 
use App\Facility;

class Folio extends Model
{
    protected $dates = ['check_in','check_out'];
    
    public function folio()
    {
        return $this->morphTo();
    }

    public function detail_order()
    {
        return $this->BelongsTo('App\DetailOrder');
    }

    public function getNameLabelAttribute()
    {
        $obj = Addon::find($this->folio_id);
        if($this->folio_type=="App\\DetailFacility") {
            $obj=DetailFacility::find($this->folio_id);
            return $obj->facility->name."-".$obj->name;        
        }
        if($this->folio_type=="App\\Addon") {
            $obj=Addon::find($this->folio_id);
            if($this->description!="")
                return $this->description." - ".$obj->name;
        }
        return $obj->name;        
    }

    public function getcheckOutLabelAttribute()
    {
        if($this->detail_order->order->menginap==1){
            return $this->check_out;
        }
        else{
            return $this->check_out->subDay();
        }
    }

    public function getTypeLabelAttribute(){
        if($this->folio_type=="App\\DetailFacility") {
            $fac = DetailFacility::find($this->folio_id);
            $facs = Facility::find($fac->facility->id);
            return $facs->facilityType->name;        
        }
        return "Tambahan";        
    }

    public function scopeBooked($query, $date1, $date2){
        return $query->whereDate("check_in", "<", $date2)->whereDate("check_out", ">", $date1)->where("active", "1")->where("folio_type", "LIKE", "%DetailFacility");
    }
}
