<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponFacility extends Model
{
    public function facility()
    {
        return $this->BelongsTo('App\Facility');
    }
    public function coupon()
    {
        return $this->BelongsTo('App\Coupon');
    }
}
