<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
         Blade::directive('canany', function ($arguments) {
            list($permissions, $guard) = explode(',', $arguments.',');
        
            $permissions = explode('|', str_replace('\'', '', $permissions));
        
            $expression = "<?php if(auth({$guard})->check() && ( false";
            foreach ($permissions as $permission) {
                $expression .= " || auth({$guard})->user()->can('{$permission}')";
            }
        
            return $expression . ")): ?>";
        });
        
        Blade::directive('endcanany', function () {
            return '<?php endif; ?>';
        });

        //CAN-AND
        Blade::directive('canall', function ($arguments) {
            list($permissions, $guard) = explode(',', $arguments.',');
        
            $permissions = explode('|', str_replace('\'', '', $permissions));
        
            $expression = "<?php if(auth({$guard})->check() && ( adn";
            foreach ($permissions as $permission) {
                $expression .= " && auth({$guard})->user()->can('{$permission}')";
            }
        
            return $expression . ")): ?>";
        });
        
        Blade::directive('endcanall', function () {
            return '<?php endif; ?>';
        });

        Schema::defaultStringLength(191);
        Relation::morphMap(['Facility'=>'App\Facility','EducationTour'=>'App\EducationTour',]);
    
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
