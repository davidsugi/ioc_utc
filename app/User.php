<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function news()
    {
        return $this->hasMany('App\News', 'user_id');
    }

    public function order()
    {
        return $this->hasMany('App\Order');
    }
    public function invoice()
    {
        return $this->hasMany('App\Invoice');
    }
    public function activity()
    {
        return $this->hasMany('App\Activity');
    }
        
    protected $fillable = [
        'name', 'email', 'password','role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRoleNameAttribute()
    {
        $str="";
        if(count($this->roles)>0){
            foreach($this->roles as $role){
                $str.=$role->name.",";
            }
        }
        return rtrim($str,", ");;
    }

    protected $appends = ['role_name'];
    protected $guarded = [];
}
