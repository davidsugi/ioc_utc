<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailInvoice;

class Invoice extends Model
{
    protected $guarded = [];

    protected $dates = ["date"];
    public function user()
    {
        return $this->BelongsTo('App\User','user_id');
    }
    public function order()
    {
        return $this->BelongsTo('App\Order');
    }

    public function getTotalLabelAttribute()
    {
        $detin = DetailInvoice::where("invoice_id", $this->id)->selectRaw("SUM(amount) as tot")->groupBy("invoice_id")->first();
        if($detin)
            return $detin->tot;
        return "0";
    }
    protected $appends=['total_label'];

    public function getdateLabelAttribute()
    {
        //try
        if($this->date==null) {
        }
        else{
            return $this->date->format('d-m-Y');
        }
    }


    public function getspanLabelAttribute()
    {
    $color=["warning","success","primary"];
    $text=["Pending","Dibayar","Ditolak"];
    return "<span class='label label-".$color[$this->status]." pull-right' style='padding:10px'>".$text[$this->status]."</span>";
    }
    public function detail_invoice()
    {
        return $this->hasMany('App\DetailInvoice');
    }
}
