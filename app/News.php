<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $guarded =[];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function category()
    {
        return $this->belongsToMany('App\Category');
    }

    public function getStatusNameAttribute()
    {
        return $this->status == 1 ? "Publish" : "Unpublish";
    }

    protected $appends =['status_name'];


    
}
