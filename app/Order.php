<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailOrder;
use App\Folio;
use App\Order;
use App\Invoice;

class Order extends Model {
    
    protected $guarded = [];

    protected $dates = ["date"];

    public function user()
    {
        return $this->BelongsTo('App\User','user_id');
    }
    public function customer()
    {
        return $this->BelongsTo('App\Customer');
    }
    public function detail_order()
    {
        return $this->hasMany('App\DetailOrder');
    }
    public function invoice()
    {
        return $this->hasMany('App\Invoice');
    }
    public function coupon()
    {
    return $this->BelongsTo('App\Coupon');
    }

    public function scopeActive($query)
    {
        return $query->where('status', '<' ,3);
    }

    public function getmenginapLabelAttribute()
    {
    $color=["danger","success"];
    $text=["tidak menginap","menginap"];
    return "<span class='label label-".$color[$this->menginap]."' style='padding:10px'>".$text[$this->menginap]."</span>";
    }
    public function getcheckInLabelAttribute()
    {
        $dets = DetailOrder::where("order_id", "=", $this->id)->pluck("id");
        $total = Folio::whereIn("detail_order_id", $dets)->where("folio_type", "LIKE", "%Facility")->orderBy("check_in", "ASC")->first();
        //try
        if($total!=null) {
            if($total->check_in==null) {
            }
            else{
                return $total->check_in->format('d-m-Y');
            }
        }
     
    }
    public function getcheckOutLabelAttribute()
    {
        $dets = DetailOrder::where("order_id", "=", $this->id)->pluck("id");
        $total = Folio::whereIn("detail_order_id", $dets)->where("folio_type", "LIKE", "%Facility")->orderBy("check_out", "DESC")->first();
        //try
        if($total!=null) {
            if($total->check_out==null) {
            }
            else{
                return $total->check_out_label->format('d-m-Y');

            }
        }
    
    }
    public function getspanLabelAttribute()
    {
        $color=["warning", "success", "info", "primary", "danger","danger"];
        $text=["Booking", "Konfirmasi", "Check In", "Check Out", "Batal","Batal"];
        return "<span class='label label-" . $color[$this->status] . "' style='padding:10px'>" . $text[$this->status] . "</span>";
    }
    public function getlunasLabelAttribute()
    {
        $color=["warning", "primary", "success",'info'];
        $text=["Belum Dibayar", "Dibayar Sebagian", "Lunas","Lebih Bayar"];
        return "<span class='label label-" . $color[$this->lunas] . "' style='padding:10px'>" . $text[$this->lunas] . "</span>";
    }
    public function getTaxLabelAttribute(){
        $total = DetailOrder::where("order_id", "=", $this->id)->selectRaw("SUM(subtotal) as total")->groupby("order_id")->first();
        if($total)
            {
            $totalnett = ($total->total - $this->disc) * ($this->tax/100);
            return $totalnett;
            }
        return "0";
    }
    public function gettotalLabelAttribute(){
        $total = DetailOrder::where("order_id", "=", $this->id)->selectRaw("SUM(subtotal) as total")->groupby("order_id")->first();
        if($total)
            return $total->total;
        return "0";
    }

    public function getnettLabelAttribute(){
        $tot=$this->totalLabel;
        $tot-=$this->disc;
        $tot+=$this->tax/100*$tot;
        return $tot;
    }
    
    public function gettotalFacilityLabelAttribute(){
        $total = DetailOrder::where("order_id", "=", $this->id)->where("item_type", "LIKE", "%Facility")->orwhere("item_type", "LIKE", "%Bundle")->where("order_id", "=", $this->id)->selectRaw("SUM(subtotal) as total")->groupby("order_id")->first();
        if($total!=null) {
            return 'Rp. ' . number_format($total->total, 0, '', '.');
        }
    }
    public function gettotalAddonLabelAttribute(){
        $total = DetailOrder::where("order_id", "=", $this->id)->where("item_type", "LIKE", "%Addon")->selectRaw("SUM(subtotal) as total")->groupby("order_id")->first();
        if($total!=null) {
            return 'Rp. ' . number_format($total->total, 0, '', '.');
        }
    }
    public function getselectLabelAttribute()
    {
        return $this->id." - ".$this->customer->nameLabel;
    }
    public function getOrderLabelAttribute()
    {
        return $this->id." - ".$this->customer->nameLabel;
    }
    public function getinvLabelAttribute()
    {
        return "Pembayaran untuk Order No.  ".$this->selectLabel;
    }
    public function getpaidLabelAttribute(){
        $paid = Invoice::where("order_id",$this->id)->where('status',1)->join("detail_invoices","invoices.id","detail_invoices.invoice_id")->selectRaw("SUM(detail_invoices.amount) as tot")->groupBy("invoices.order_id")->first();
        if($paid==[]){
            return 0;
        }
        return $paid->tot;
    }
    public function getpayableLabelAttribute(){
        $total = $this->totalLabel - $this->disc - $this->globaldisc_val + $this->taxLabel;  
        
        return $total-$this->paidLabel;
    }
    public function getglobaldiscValAttribute()
    {
        return $this->globaldisc_type==1 ? ($this->totalLabel-$this->disc)*$this->globaldisc/100 : $this->globaldisc;
    }
    public function gettotalNettLabelAttribute()
    {
        return ($this->totalLabel - $this->disc - $this->globaldisc_val) + $this->taxLabel;
    }
    
    public function order_document()
    {
        return $this->hasMany('App\OrderDocument');
    }

    public function getaddonGroupAttribute()
    {
        $det = DetailOrder::where("item_type","LIKE","%Addon")->where("order_id",$this->id)->get();
        return $det;
    }
    public function getFacilityGroupAttribute()
    {
        $det = DetailOrder::where("order_id",$this->id)->where("item_type","LIKE","%Facility")->orwhere("item_type","LIKE","%Bundle")->where("order_id",$this->id)->get();
        return $det;
    }

    public function getinapLabelAttribute()
    {
        if($this->menginap=="")
            $this->menginap=0;
        
        $color=["warning", "primary", "warning"];
        $text=["tidak menginap", "menginap", "Tidak Menginap"];
        return "<span class='label label-" . $color[$this->menginap] . "' style='padding:10px'>" . $text[$this->menginap] . "</span>";
    }

    public function getJsonAttribute()
    {
        foreach($this->detail_order as $det){
            // $det->item->calDetPrice($det->folio()->first()->check_in,$det->folio()->first()->amount);
        }
    }

}
