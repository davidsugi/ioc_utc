<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function customer()
    {
        return $this->hasMany('App\Customer', 'city_id');
    }
    
    public function getFullCityAttribute()
    {
        return $this->province . ', ' . $this->type . ' ' . $this->city;
    }
    protected $appends = ['full_city'];
}