<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationTour extends Model
{
    protected $guarded=[];
    
    public function gallery()
    {
        return $this->morphMany('App\Gallery', 'galleriable');
    }
}
