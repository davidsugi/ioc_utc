<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $guarded = [];
    
    public function user()
    {
        return $this->BelongsTo('App\User','user_id');
    }

    public function getstatusLabelAttribute()
    {
    $color=["warning","success","danger"];
    $text=["draft","Published","Inactive"];
    return "<span class='label label-".$color[$this->status]."' style='padding:10px'>".$text[$this->status]."</span>";
    }
}
