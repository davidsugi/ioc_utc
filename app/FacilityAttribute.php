<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityAttribute extends Model
{
    protected $guarded =[];
    
    public function facilityDetail()
    {
        return $this->hasMany('App\FacilityDetail', 'attribute_id');
    }
    
}
