@extends('layouts.app')

@section('title')
{{ $major->exists ? 'Ubah' : 'Tambah' }} Jurusan
@endsection
@if($major->exists)
@section('actionbtn')
  <a data-href="{{ route('majors.destroy', $major->id) }}" class="btn btn-danger destroy">Hapus Jurusan</a>
@endsection
@endif

@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        {{-- <div class="box-header with-border">
          <h3 class="box-title">Hasil Tes Masuk</h3>
        </div> --}}
        <!-- /.box-header -->
        <!-- form start -->
        @if($major->exists)
        {!! Form::model($major, ['route' => ['majors.update', $major->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($major, ['route' => ['majors.store'], 'role' => 'form']) !!}
        @endif
        {{ csrf_field() }}
        <div class="box-body">
          <div class="row">

            <div class="form-group col-md-12">
              {!! Form::label('name', 'Jurusan'); !!}
              {!! Form::text('name', $major->name, ['class'=> 'form-control', 'placeholder' => 'Jurusan e.g. MIPA', 'required' => 'required']) !!}
              @if ($errors->has('name'))
              <div class="help-block text-red">
                {{ $errors->first('name') }}
              </div>
              @endif
            </div>

          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</section>



@endsection

@include('shared._select2')
@include('shared._deletebtn')
