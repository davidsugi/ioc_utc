@extends('layouts.app')
@section('title')
List Harga Khusus
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-cube'></i> Custom price</a></li>
@endsection


@section('content')
<style>
    input[type="date"]:before {
    content: attr(placeholder) !important;
    color: #aaa;
    margin-right: 0.5em;
    }
    input[type="date"]:focus:before,
    input[type="date"]:valid:before {
    content: "";
    }
</style>
<div class="row">
    <form method="get" id="search-form">
        <div class="col-md-4">
            <div class="input-group no-border">
                <input type="date" class="form-control column-filter" name="filter[startdate]" placeholder="Mulai" value="{{ !empty($filter['startdate']) ? $filter['startdate'] : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group no-border">
                <input type="date" class="form-control column-filter" name="filter[enddate]" placeholder="Selesai" value="{{ !empty($filter['enddate']) ? $filter['enddate'] : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary btn-block">Cari</button>
        </div>
    </form>
</div>
<br>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <a href="{{ route('custom_prices.create',['u'=>'all']) }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Harga Khusus</button></a>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Tanggal Mulai</th>
                            <th width="25%">Tanggal Selesai</th>
                            <th width="25%">Item</th> 
                            <th width="25%">Nilai</th>
                            <th width="25%" colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $custom_prices as $key => $custom_price )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $custom_price->startdate }}</td>
                            <td>{{ $custom_price->enddate }}</td>
                            {{--  <td>{{ $custom_price->type }}</td>  --}}
                            <td>{{ $custom_price->nameLabel }}</td>
                            <td>{{ $custom_price->price }} %</td>
                            <td>
                                <a href="{{route('custom_prices.edit',$custom_price->id)}}">
                                    <button class="btn btn-warning">
                                        Edit
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    @if($custom_prices->count() == 0)
                        <tr>
                            <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                        </tr>
                    @endif
                    </tbody>
            </table>
        </div>
        {{ $custom_prices->links() }}
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
