@extends('layouts.app')

@section('title')
{{ $custom_price->exists ? 'Ubah' : 'Tambah' }} Harga
@endsection
@if($custom_price->exists)
@section('actionbtn')
<a data-href="{{ route('custom_prices.destroy', $custom_price->id) }}" class="btn btn-danger destroy">Hapus Harga</a>
@endsection
@endif

@if($custom_price->price_type == "all")
  @section('breadcrumb')
    <li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
    <li><a href='{{ route("custom_prices.index")}}'><i class='fa fa-cube'></i> Custom price</a></li>
    <li><a href='#' class='active'>{{ $custom_price->exists ? 'Ubah' : 'Tambah'}} Custom price</a></li>
  @endsection
@elseif($custom_price->price_type == "App\\Addon")
  @section('breadcrumb')
    <li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
    <li><a href='{{ route("addons.index")}}'><i class='fa fa-plus'></i> Addon</a></li>
    <li><a href='{{ route("addons.show",$custom_price->price_id)}}'>{{ $custom_price->name_label }}</a></li>
    <li><a href='#' class='active'>{{ $custom_price->exists ? 'Ubah' : 'Tambah'}} Custom price</a></li>
  @endsection
@elseif($custom_price->price_type == "App\\Bundle")
  @section('breadcrumb')
    <li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
    <li><a href='{{ route("bundles.index")}}'><i class='fa fa-star'></i>Paket</a></li>
    <li><a href='{{ route("bundles.show",$custom_price->price_id)}}'>{{ $custom_price->name_label }}</a></li>
    <li><a href='#' class='active'>{{ $custom_price->exists ? 'Ubah' : 'Tambah'}} Custom price</a></li>
  @endsection
@elseif($custom_price->price_type == "App\\Facility")
  @section('breadcrumb')
    <li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
    <li><a href='{{ route("facilities.index")}}'><i class='fa fa-star'></i>Paket</a></li>
    <li><a href='{{ route("facilities.show",$custom_price->price_id)}}'>{{ $custom_price->name_label }}</a></li>
    <li><a href='#' class='active'>{{ $custom_price->exists ? 'Ubah' : 'Tambah'}} Custom price</a></li>
  @endsection
@endif

@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($custom_price->exists)
        {!! Form::model($custom_price, ['route' => ['custom_prices.update', $custom_price->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($custom_price, ['route' => ['custom_prices.store'], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Date and time range:</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
                <input type="text" class="form-control" name ="rangedate" id="reservationtime" value="{{ $custom_price->startdate ? $custom_price->carbonstart : Carbon\carbon::now()->format('d/m/Y 00:00:00') }} - {{ $custom_price->endate ? $custom_price->carbonend : Carbon\carbon::now()->format('d/m/Y 23:59:00') }}" required>
              </div>
              <!-- /.input group -->
            </div>
            {{ Form::hidden('price_type', $custom_price->price_type, ['class' => 'form-control', 'required']) }}              
              @if($custom_price->price_type!="all")
            <div class="form-group col-md-12">
              <label>Fasilitas</label>
                {!! Form::select('price_id', $facility, $custom_price->facility_id, ['class'=> 'form-control select2','required' => 'required']) !!}
            </div>
              @endif
            
            <div class="form-group col-md-12">
              <label>Persentase</label>
             <table>
               <tr>
                 <td>{!! Form::number('price', $custom_price->price, ['class'=> 'form-control','required' => 'required']) !!}</td>
                 <td>%</td>
               </tr>
             </table>
            </div>
            </div>
          </div>

          <!-- /.box-body -->
          
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<script>
  $(function () {
    $('.select2').select2();
    $('#reservationtime').daterangepicker({ timePicker: true, timePicker24Hour: true, timePickerIncrement: 30,locale: {format: 'DD/MM/YYYY H:mm:ss'}})
  })
  
</script>
@endsection
@include('layouts._deletebtn')
