@extends('layouts.app')

@section('title')
Detail Jabatan
@endsection

@section('actionbtn')
  <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-warning">Edit</a>
  <a data-href="{{ route('roles.destroy', $role->id) }}" class="btn btn-danger destroy">Hapus</a>
@endsection

@push('styles')
<style type="text/css">

</style>
@endpush

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='{{ route("roles.index")}}'><i class='fa fa-briefcase'></i> Jabatan</a></li>

<li><a href='#' class='active'>Detail Jabatan</a></li>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Nama</label>
                        <h4>{{ $role->name }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-body">

        <table class="table dataTable">
            <thead>
              <tr>
                <th class="align-middle">No</th>
                <th class="align-middle">Permission</th>
              </tr>
            </thead>
            <tbody>

              @foreach($role->permissions as $permission)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $permission->name }}</td>
              </tr>
              @endforeach
              @if($role->permissions()->count() == 0)
              <tr>
                <td colspan="12"><center><em>Tidak Ada Data.</em></center></td>
              </tr>
              @endif
            </tbody>
        </table>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
