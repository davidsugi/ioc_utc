@extends('layouts.app')
@section('title')
{{ $role->exists ? 'Ubah' : 'Tambah' }} Jabatan
@endsection
@if($role->exists)
@section('actionbtn')
<a data-href="{{ route('roles.destroy', $role->id) }}" class="btn btn-danger destroy">Hapus Jabatan</a>
@endsection
@endif
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='{{ route("roles.index")}}'><i class='fa fa-briefcase'></i> Jabatan</a></li>
<li><a href='#' class='active'>{{ $role->exists ? 'Ubah' : 'Tambah'}} Jabatan</a></li>

@endsection

@section('content')
<div class="row">
    <div class="col-xs-10">
        <div class="box">
            <div class="box-body">
                @if($role->exists)
                    {!! Form::model($role, ['route' => ['roles.update', $role->id], 'method'=>'PATCH', 'files' => true]) !!}
                @else
                    {!! Form::model($role, ['route' => ['roles.store'], 'class' => 'col s12', 'files' => true]) !!}
                @endif
                <div class="form-group col-md-12">
                    {{ Form::label('name','Jabatan') }}
                    {{ Form::text('name', $role->name, [  'placeholder'=>'Masukan Jabatan',  'class' => 'form-control', 'required']) }}
                    @if ($errors->has('name'))
                        <div class="help-block text-red">
                            {{ $errors->first('name') }}
                        </div>
                    @endif
                </div>
                
                <div class="form-group col-md-12">
                    {!! Form::label('permissions[]', 'Hak Akses'); !!}
                    {!! Form::select('permissions[]',$permissions, $role->permissions()->pluck('name'), ['multiple'=>'multiple', 'class' => 'form-control permission', 'style'=>'border:0px none;']) !!}
                </div>

                <div class="box-footer">
                    {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
        $('.permission').select2({
            'width': '100%',
             placeholder: "Pilih Hak Akses",
        });
    });
</script>
@endpush
