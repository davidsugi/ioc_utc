@extends('layouts.app')
@section('title')
List Jabatan
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-briefcase'></i>Jabatan</a></li>

@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <a href="{{ route('roles.create') }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Jabatan</button></a>
            </div>
            <div class="box-body">
                <table id="example" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Jabatan</th>
                            <th width="15%" colspan="3">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $roles as $key => $role )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $role->name }}</td>
                            <td><a href="{{ route('roles.show',['id'=>$role->id]) }}" class="btn btn-primary btn-block">Show</a></td>
                            <td> <a href="{{ route('roles.edit',['id'=>$role->id]) }}" class="btn btn-warning btn-block">
                                   Edit
                                </a></td>
                        </tr>
                    @endforeach
                    @if($roles->count() == 0)
                        <tr>
                            <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                        </tr>
                    @endif
                    </tbody>
            </table>
        </div>
        {{ $roles->links() }}
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
