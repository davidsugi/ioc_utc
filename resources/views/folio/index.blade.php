@extends('layouts.app')
@section('title')
Jadwal
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-book'></i> Folio</a></li>
@endsection
@section('content')
<style>
    input[type="date"]:before {
    content: attr(placeholder) !important;
    color: #aaa;
    margin-right: 0.5em;
    }
    input[type="date"]:focus:before,
    input[type="date"]:valid:before {
    content: "";
    }
</style>
<div class="row">
    <form method="get" id="search-form">
        <div class="col-md-4">
            <div class="input-group no-border">
                <input type="date" class="form-control column-filter" name="filter[check_in]" placeholder="Start: " value="{{ !empty($filter['check_in']) ? $filter['check_in'] : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group no-border">
                <input type="date" class="form-control column-filter" name="filter[check_out]" placeholder="End: " value="{{ !empty($filter['check_out']) ? $filter['check_out'] : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary btn-block">Cari</button>
        </div>
    </form>
</div>
<br>
<div class="row">
    
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <a href="{{ route('folios.print') }}?order={{ isset($order) ? $order : '' }}&start={{ isset($filter['check_in']) ? $filter['check_in'] : '' }}&end={{ isset($filter['check_out']) ? $filter['check_out'] : '' }}"><button class="btn btn-primary"><span>  <i class="fa fa-print"></i> </span>Cetak Folio</button></a>
            </div>
            <div class="box-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Fasilitas</a></li>
                    <li><a data-toggle="tab" href="#menu1">Tambahan</a></li>
                </ul>
                <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <h3>Fasilitas</h3>
                    <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Item</th>
                            <th width="10%">Jenis Item</th>
                            <th width="15%">Estimasi Check in</th>
                            <th width="15%">Estimasi Check Out</th>
                            <th width="25%">Keterangan</th>
                            {{--  <th width="25%">Check in customer</th>
                            <th width="25%">Check out customer</th>  --}}
                            <th width="25%" colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $facs as $key => $folio )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
        			        @can('Mengelola Fasilitas')
                            <td><a href="{{route('detail_facilities.custom_schedules.index',['detail_facilities'=>$folio->folio_id])}}">{{ $folio->nameLabel }}</a></td>
                                                            
                            @else
                            <td> {{ $folio->nameLabel }} </td>
                            @endcan        					
                            <td>{{ $folio->typeLabel }}</td>
                            <td>{{ $folio->check_in->format("d-m-Y") }}</td>
                            <td>{{ $folio->check_out_label->format("d-m-Y") }}</td>
                            <td>{{ $folio->detail_order->note ?: '-' }}</td>
                            <td>
                                @if($folio->detail_order->order->status<3)
                                <a href="{{ route('folios.edit',['id'=>$folio->id]) }}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @endif
                            </td>
                            {{--  <td>{{ $folio->actual_in==null ? '' : $folio->actual_in->format("d-m-Y")  }}</td>  --}}
                            {{--  <td>{{ $folio->actual_out==null ? '' : $folio->actual_out->format("d-m-Y") }}</td>  --}}
                            {{--  <td><a href="{{ route('folios.show',['id'=>$folio->id]) }}"><i class="fa fa-search">open_in_new</i></a>
                                <a href="{{ route('folios.edit',['id'=>$folio->id]) }}">
                                    <i class="fa fa-pencil">mode_edit</i>
                                </a>
                                <a class="destroy" data-href="{{ route('folios.destroy',['id'=>$folio->id]) }}">
                                <i class="fa fa-trash">delete</i>
                                </a>
                            </td>  --}}
                        </tr>
                    @endforeach
                    @if($facs->count() == 0)
                        <tr>
                            <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                        </tr>
                    @endif
                    </tbody>
            </table>
        {{ $facs->appends(array_except(Request::query(), 'page'))->links() }}
            
                </div>
                <div id="menu1" class="tab-pane fade">
                    <h3>Tambahan</h3>
                    <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Tanggal Pesan</th>
                            <th width="25%">Item</th>
                            <th width="25%">Jumlah</th>
                            {{--  <th width="25%">Jenis Item</th>  --}}
                            {{--  <th width="25%" colspan="2">Action</th>  --}}
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $adds as $key => $folio )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $folio->check_in->format("d-m-Y H:i") }}</td>                            
                            <td>{{ $folio->nameLabel }}</td>
                            <td>{{ $folio->amount }}</td>
                            {{--  <td>{{ $folio->typeLabel }}</td>  --}}
                            {{--  <td><a href="{{ route('folios.show',['id'=>$folio->id]) }}"><i class="fa fa-search">open_in_new</i></a>
                                <a href="{{ route('folios.edit',['id'=>$folio->id]) }}">
                                    <i class="fa fa-pencil">mode_edit</i>
                                </a>
                                <a class="destroy" data-href="{{ route('folios.destroy',['id'=>$folio->id]) }}">
                                <i class="fa fa-trash">delete</i>
                                </a>
                            </td>  --}}
                        </tr>
                    @endforeach
                    @if($adds->count() == 0)
                        <tr>
                            <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                        </tr>
                    @endif
                    </tbody>
            </table>
        {{ $adds->appends(array_except(Request::query(), 'add_page'))->links() }}
            
                </div>
                </div>

                
        </div>
        </div>
    </div>
</div>


<div class="box">
<div class="box-body no-padding">
	<!-- THE CALENDAR -->
	<div id="calendar"></div>
</div>
</div>
@endsection
@push('scripts')

<script>
	$(function () {
		ajaxCalendar();
		function ajaxCalendar() {
			$.ajax({
				type: 'GET',
				datatype: 'json',
				url: '{{route("api.get.folio_calendar")}}?id={{ $order }}&start={{ !empty($filter['check_in']) ? $filter['check_in'] : '' }}&end={{ !empty($filter['check_out']) ? $filter['check_out'] : '' }}',
				success: function(data) {
					var header = {
						left  : 'prev,next today',
						center: 'title',
						right : 'month,agendaWeek,agendaDay'
					};
					var buttonText = {
						today: 'today',
						month: 'month',
						week : 'week',
						day  : 'day'
					};
					var events =[];
					var timeFormat = 'H(:mm)t';
					for (var i=0; i<data.length; i++){
						var e = {
							title          : data[i][0],
							start          : $.fullCalendar.moment(data[i][2]),
							end            : $.fullCalendar.moment(data[i][3]),
                            backgroundColor: data[i][1],
                            url: data[i][4],
                            borderColor    : data[i][1],
						};
						events.push(e);
					}
					$('#calendar').fullCalendar({
						header,
						buttonText,
						events,
                        timeFormat,
                        eventLimitText: " Lainnya",
                        timeFormat: 'HH(:mm)'
					})
				}
			})
		}
	})
</script>
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
