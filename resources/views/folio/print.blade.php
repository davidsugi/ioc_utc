@extends('layouts.pdf')
@push('style')
     <style>

    table * {
      font-size: 13px;
      width:340px;
    }
    table th{
      text-transform: uppercase;
      text-align: center;
      
    }
    td{
      text-align: center;
    }

    .kop-surat p{
      font-size: 1.2em;
    }
    table thead { display: table-header-group; }
    table tr { page-break-inside: avoid; }
    section.page
    {
        page-break-after: always;
        page-break-inside: avoid;
    }
    .row{
      padding:20px;
    }
    .holiday{
      background-color: grey;
      color:white;
    }
    table#result { border-collapse: collapse; }
    table#result tr { border: solid thin; }
    table#det th{ border: solid thin; }
    table#det td{ border: solid thin; }

  </style>
@endpush
@section('body')
@php($i=0)
<section class="page">
<div class="row">
<div class="col-xs-12">
  {{--  <center><h3>Folio</h3></center>  --}}
<div class="col-xs-6">
  <h3></h3>
    <h3>List Fasilitas</h3>
    <h5>Tanggal: {{ $dated }}</h5>
</div>
<div class="col-xs-12">
  <table id="det">
      <tr>
        <th>No.</th>
        <th>Tanggal Check-in</th>
        <th>Tanggal Check-out</th>
        <th>Item</th>
        <th>Jenis Item</th>
        <th>Keterangan</th>
        {{--  <th>Total</th>  --}}
      </tr>
      @php($tot=0)
      @foreach($facs as $key => $folio)
      <tr>
        <td>{{ $key+1 }}</td>          
        <td> {{ $folio->nameLabel }} </td>
        <td>{{ $folio->typeLabel }}</td>
        <td>{{ $folio->check_in->format("d-m-Y") }}</td>
        <td>{{ $folio->check_out_label->format("d-m-Y") }}</td>
        <td>{{ $folio->detail_order->note ?: '-'  }}</td>
      </tr>
      @endforeach
   
  </table>
</div>

</div>
</div>
</section>

{{--  ==================================================================  --}}
<section class="page">
<div class="row">
<div class="col-xs-12">
<div class="col-xs-6">
    <h4>List Tambahan</h4>
    <h5>Tanggal: {{ $dated }}</h5>
    
</div>
<div class="col-xs-12">
  <table id="det">
      <tr>
        <th>No.</th>
        <th>Tanggal Pesan</th> 
        <th>Item</th>       
        <th>Jumlah</th>
      </tr>
      @php($tot=0)
      @foreach($addons as $key => $folio)
      <tr>
        <td>{{ $key+1 }}</td>          
        <td>{{ $folio->check_in->format("d-m-Y H:i") }}</td>                            
        <td> {{ $folio->nameLabel }} </td>
        {{--  <td>{{ $folio->typeLabel }}</td>  --}}
        <td>{{ $folio->amount }}</td>
      </tr>
      @endforeach
  </table>
</div>

</div>
</div>
</section>
@endsection

