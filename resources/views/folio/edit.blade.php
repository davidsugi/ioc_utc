@extends('layouts.app')
@section('title')
{{ $folio->exists ? 'Ubah' : 'Tambah' }} Folio
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='{{ route("folios.index")}}'><i class='fa fa-book'></i> Folio</a></li>
<li><a href='#' class='active'>{{ $folio->exists ? 'Ubah' : 'Tambah'}} Folio</a></li>

@endsection
@section('content')
<div class="row">
    <div class="col-xs-10">
        <div class="box">
            <div class="box-body">
                @if($folio->exists)
                    {!! Form::model($folio, ['route' => ['folios.update', $folio->id], 'method'=>'PATCH', 'files' => true]) !!}
                @else
                    {!! Form::model($folio, ['route' => ['folios.store'], 'class' => 'col s12', 'files' => true]) !!}
                @endif
                    <div class="form-group col-md-12">
                        {{ Form::label('folio_id','Fasilitas') }}
                        {{ Form::select('folio_id', empty($facilities) ? array() : $facilities , $folio->folio_id, ['class' => 'form-control select2 select2class', ]) }}
                        @if ($errors->has('folio_id'))
                            <div class="help-block text-red">
                                {{ $errors->first('folio_id') }}
                            </div>
                        @endif
                    </div>
                    
                <div class="box-footer">
                    {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
