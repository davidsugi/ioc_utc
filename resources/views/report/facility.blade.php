@extends('layouts.pdf')
@push('style')
    <style>

    table * {
      font-size: 13px;
      width:340px;
    }
    table th{
      text-transform: uppercase;
      text-align: center;
      
    }
    td{
      text-align: center;
    }

    .kop-surat p{
      font-size: 1.2em;
    }
    table thead { display: table-header-group; }
    table tr { page-break-inside: avoid; }
    section.page
    {
        page-break-after: always;
        page-break-inside: avoid;
    }
    .row{
      padding:20px;
    }
    .holiday{
      background-color: grey;
      color:white;
    }
    table#result { border-collapse: collapse; }
    table#result tr { border: solid thin; }
    table#det th{ border: solid thin; }
    table#det td{ border: solid thin; }

  </style>
@endpush
@section('body')
<h5 align="center">Laporan Order Berdasarkan Fasilitas</h3>
@php($i=0)
@foreach($result as $key=>$res)
<section class="page">

<div class="row">
<div class="col-xs-12">
<div class="col-xs-6">
    <table>
    <tr><td>Jenis Fasilitas</td>
    <td>:</td>
    <td>{{$key}}</td>
    </tr>
    <tr><td>TANGGAL</td>
    <td>:</td>
    <td>{{ $dated }}</td>
    </tr>
    </table>
</div>
<div class="col-xs-12">
  <table id="det">
      <tr>
        <th>No.</th>
        <th>No. Order</th>
        <th>Nama Customer</th>
        <th>Tanggal Order</th>
        <th>Tanggal Check-in</th>
        <th>Tanggal Check-out</th>
        <th>Fasilitas</th>
        <th>Total</th>
      </tr>
      @php($tot=0)
      @foreach($res as $key => $ord)
      <tr>
          <td>{{$key+1}}</td>
          <td>{{$ord->order_code}}</td>
          <td>{{ $ord->cname }}</td>
          <td>{{$ord->date}}</td>
          <td>{{$ord->check_in_label=="-" ? $ord->check_in_label : $ord->check_in_label->format("Y-m-d") }}</td>
          <td>{{$ord->check_out_label=="-" ? $ord->check_out_label : $ord->check_out_label->format("Y-m-d")}}</td>
          <td>{{$ord->nameLabel}}</td>
          <td>{{H::rupiah($ord->totalLabel)}}</td>
          @php($tot+=$ord->totalLabel)
      </tr>
      @endforeach
      <tr>
          <td colspan="7" align="center"><b>Total<b></td>
          <td><b>{{ H::rupiah($tot) }}<b></td>
      </tr>
  </table>
</div>

</div>
</div>

{{--  ==================================================================  --}}

@php($i++)
</section>

 @endforeach
 


@foreach($rest as $key=>$res)
<section class="page">
<div class="row">
<div class="col-xs-12">
<div class="col-xs-6">
    <table>
    <tr><td>Jenis</td>
    <td>:</td>
    <td>{{$key}}</td>
    </tr>
    <tr><td>TANGGAL</td>
    <td>:</td>
    <td>{{ $dated }}</td>
    </tr>
    </table>
</div>
<div class="col-xs-12">
  <table id="det">
      <tr>
        <th>No.</th>
        <th>No. Order</th>
        <th>Nama Customer</th>
        <th>Tanggal Order</th>
        <th>Item</th>
        <th>Total</th>
      </tr>
      @php($tot=0)
      @foreach($res as $key => $ord)
      <tr>
          <td>{{$key+1}}</td>
          <td>{{$ord->order_code}}</td>
          <td>{{ $ord->cname }}</td>
          <td>{{$ord->date}}</td>
          <td>{{$ord->nameLabel}}</td>
          <td>{{H::rupiah($ord->totalLabel)}}</td>
          @php($tot+=$ord->totalLabel)
      </tr>
      @endforeach
      <tr>
          <td colspan="5" align="center"><b>Total<b></td>
          <td><b>{{ H::rupiah($tot) }}<b></td>
      </tr>
  </table>
</div>

</div>
</div>
</section>

{{--  ==================================================================  --}}

@php($i++)
 @endforeach
@endsection
