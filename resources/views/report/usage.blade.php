@extends('layouts.app')
@section('title')
List Laporan
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("reports.index")}}' class='active'><i class='fa fa-book'></i> Laporan</a></li>
@endsection

@section('content')
<style>
    {{--  a {
    display: block;
    width: 100%;
    }  --}}
    input[type="date"]:before {
    content: attr(placeholder) !important;
    color: #aaa;
    margin-right: 0.5em;
    }
    input[type="date"]:focus:before,
    input[type="date"]:valid:before {
    content: "";
    }
</style>
{{--  <div class="row">

</div>  --}}
<br>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <h4>Laporan Penggunaan Barang</h4>
            </div>
            <div class="box-body">
        <form method="get" id="search-form" action="{{ route('reports.usage') }}">
          <div class="col-md-3">
            <div class="input-group no-border">
                   {{ Form::select('status[]', array(0=> 'Booked',1 => 'Payment Confirmed', 2 => 'Check in', 3 => 'Check Out', 4=>'Canceled') , ['class' => 'form-control column-filter select2 multiple', 'multiple'=>'multiple']) }}               
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group no-border">
                <input type="date" class="form-control column-filter" name="start" placeholder="Dari" value="{{ !empty($start) ? $start : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
      
        <div class="col-md-3">
            <div class="input-group no-border">
                <input type="date" class="form-control column-filter" name="end" placeholder="Sampai" value="{{ !empty($end) ? $end : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-3">
            <button type="submit" class="btn btn-primary btn-block"><span>  <i class="fa fa-cube"></i> </span>Unduh Laporan Penggunaan Barang</button>
        </div>
    </form>
                    
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script>
    $(document).ready(function() {
        $('select').attr("multiple","multiple");
        $('select').select2({
            allowClear: true,
            placeholder:"Pilih Status",
            width:"100%",            
        });
        $('select').val("").trigger("change");
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
