@extends('layouts.pdf')
@push('style')
    
  <!-- Required meta tags -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <style>

    table * {
      font-size: 13px;
      width:340px;
    }
    table th{
      text-transform: uppercase;
      text-align: center;
      
    }
    td{
      text-align: center;
    }

    .kop-surat p{
      font-size: 1.2em;
    }
    table thead { display: table-header-group; }
    table tr { page-break-inside: avoid; }
    section.page
    {
        page-break-after: always;
        page-break-inside: avoid;
    }
    .row{
      padding:20px;
    }
    .holiday{
      background-color: grey;
      color:white;
    }
    table#result { border-collapse: collapse; }
    table#result tr { border: solid thin; }
    table#det th{ border: solid thin; }
    table#det td{ border: solid thin; }

  </style>

@endpush
@section('body')    
<h5 align="center">Laporan Order Berdasarkan Pelanggan</h3>

@php($i=0)
@foreach($result as $key=>$res)
<section class="page">
<div class="row">
<div class="col-xs-12">
<div class="col-xs-6">
    <table>
    <tr><td>Jenis Customer</td>
    <td>:</td>
    <td>{{$key}}</td>
    </tr>
    <tr><td>TANGGAL</td>
    <td>:</td>
    <td>{{ $dated }}</td>
    </tr>
    </table>
</div>
<div class="col-xs-12">
  <table id="det">
      <tr>
        <th>No.</th>
        <th>No. Order</th>
        <th>Tanggal Order</th>
        <th>Tanggal Check-in</th>
        <th>Tanggal Check-out</th>
        <th>Nama</th>
        <th>Total</th>
      </tr>
      @php($tot=0)
      @foreach($res as $key => $ord)
      <tr>
          <td>{{$key+1}}</td>
          <td>{{$ord->order_code}}</td>
          <td>{{$ord->date->format("Y-m-d")}}</td>
          <td>{{$ord->check_in_label}}</td>
          <td>{{$ord->check_out_label}}</td>
          <td>{{$ord->customer->nameLabel}}</td>
          <td>{{H::rupiah($ord->nettLabel)}}</td>
          @php($tot+=$ord->nettLabel)
      </tr>
      @endforeach
      <tr>
          <td colspan="6" align="center"><b>Total<b></td>
          <td><b>{{ H::rupiah($tot) }}<b></td>
      </tr>
  </table>
</div>

</div>
</div>
</section>

{{--  ==================================================================  --}}

@php($i++)
 @endforeach
@endsection
