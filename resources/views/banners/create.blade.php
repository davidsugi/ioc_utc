@extends('layouts.app')

@section('title')
{{ $banner->exists ? 'Ubah' : 'Tambah' }} Banner
@endsection
@if($banner->exists)
@section('actionbtn')
<a data-href="{{ route('banners.destroy', $banner->id) }}" class="btn btn-danger destroy">Hapus Banner</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='{{ route("banners.index")}}'><i class='fa fa-image'></i> Banner</a></li>
<li><a href='#' class='active'>{{ $banner->exists ? 'Ubah' : 'Tambah'}} Banner</a></li>

@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($banner->exists)
        {!! Form::model($banner, ['route' => ['banners.update', $banner->id], 'method'=>'PATCH', 'enctype'=>'multipart/form-data','role' => 'form']) !!}
        @else
        {!! Form::model($banner, ['route' => ['banners.store'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            
            <div class="col-md-6">
               <label>Image (Desktop)</label>
                <input type="file" class="form-control" id="imaged" name="image_desktop" onchange="preview_imaged();"/>
            @if ($errors->has('image_desktop'))
              <div class="help-block text-red">
                {{ $errors->first('image_desktop')}}
              </div>
            @endif
              </div>
            <div class="col-md-6">
               <label>Image(Mobile)</label>
                <input type="file" class="form-control" id="image" name="image_mobile" onchange="preview_images();" />
            
            @if ($errors->has('image_mobile'))
              <div class="help-block text-red">
                {{ $errors->first('image_mobile')}}
              </div>
            @endif
            </div>
            <div class="col-md-6">
                <center><div class="row" id="imaged_preview">
                @if($banner->exists)
                  <div class='col-md-3' style='padding-top:20px'><img class='img-responsive' src='{{ asset("image/".$banner->image_desktop_path) }}'></div>
                @endif
                </div></center>
            </div>
            <div class="col-md-6">
                <div class="row" id="image_preview">
                  @if($banner->exists)
                  <div class='col-md-3' style='padding-top:20px'><img class='img-responsive' src='{{ asset("image/".$banner->image_mobile_path) }}'></div>
                @endif
                </div>
            </div>
          
            <div class="form-group col-md-12">
              <label>Title</label>
              {!! Form::text('title',$banner->title, ['placeholder' => 'Title','class'=> 'form-control ']) !!}
              @if ($errors->has('title'))
              <div class="help-block text-red">
                {{ $errors->first('title') }}
              </div>
              @endif
            </div>
            
            <div class="form-group col-md-12">
              <label>Subtitle</label>
              {!! Form::text('subtitle',$banner->subtitle, ['placeholder' => 'Subtitle','class'=> 'form-control ']) !!}
            </div>
            @if ($errors->has('subtitle'))
            <div class="help-block text-red">
              {{ $errors->first('subtitle') }}
            </div>
            @endif
            <div class="form-group col-md-12">
              <label>Description</label>
              {!! Form::text('description',$banner->description, ['placeholder' => 'Deskripsi','class'=> 'form-control ']) !!}
            </div>
            @if ($errors->has('description'))
            <div class="help-block text-red">
              {{ $errors->first('description') }}
            </div>
            @endif
            <div class="form-group col-md-12">
              <label>Link Text</label>
              {!! Form::text('link_text',$banner->link_text, ['placeholder' => 'Link Text','class'=> 'form-control ']) !!}
            </div>
            @if ($errors->has('link_text'))
            <div class="help-block text-red">
              {{ $errors->first('link_text') }}
            </div>
            @endif
            <div class="form-group col-md-12">
              <label>Link URL</label>
              {!! Form::text('link_url',$banner->link_url, ['placeholder' => 'Link URL','class'=> 'form-control ']) !!}
            </div>
            @if ($errors->has('link_url'))
            <div class="help-block text-red">
              {{ $errors->first('link_url') }}
            </div>
            @endif
            <div class="form-group col-md-12">
              <label>Banner Location</label>
              {!! Form::select('banner_location_id', $bannerLocation, $banner->banner_location_id, ['placeholder'=>'Banner Location','class'=> 'form-control location']) !!}
            </div>
            @if ($errors->has('banner_location_id'))
            <div class="help-block text-red">
              {{ $errors->first('banner_location_id') }}
            </div>
            @endif
          </div>
        </div>
        <!-- /.box-body -->
        
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')

@push('scripts')
<form id='destroy-form' method="POST">
	{{ csrf_field() }}
	<input type="hidden" name="_method" value="DELETE">
</form>
<script type="text/javascript">
  function preview_imaged(){
         var total_file=document.getElementById("imaged").files.length;
         for(var i=0;i<total_file;i++){
            $('#imaged_preview').html("<div class='col-md-3' style='padding-top:20px'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
         }
      }
  function preview_images(){
     var total_file=document.getElementById("image").files.length;
     for(var i=0;i<total_file;i++){
        $('#image_preview').html("<div class='col-md-3' style='padding-top:20px'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
     }
  }
	$(document).ready(function() {
      
      $('.location').select2({
        'width': '100%',
         placeholder: "Pilih Lokasi Banner",
      });
	});
</script>
@endpush