@extends('layouts.app')

@section('title')
Manage Banner
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-image'></i> Banner</a></li>
@endsection
@section('content')

<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[title]" placeholder="Title" value="{{ !empty($filter['title']) ? $filter['title'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<br>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-12">
				<a href="{{ route('banners.create')}}" class="btn btn-success">Tambah Banner &nbsp;<span class="fa fa-plus-circle"></span></a>
				<a href="{{ route('banner_locations.index')}}" class="btn btn-primary">Lokasi Banner &nbsp;<span class="fa fa-map-marker"></span></a>
			</div>
		</div>
	</div>

	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th width="15%">Image (Desktop)</th>
                    <th width="15%">Image (Mobile)</th>
                    <th width="10%">Title</th>
                    <th width="15%">Subtitle</th>
                    <th width="25%">Description</th>
					<th width="5%">Link Text</th>
					<th width="5%">Link URL</th>
					<th width="5%">Banner Location</th>
					<th>Edit</th>
				</tr>
			</thead>
			<tbody>
				@foreach($banner as $b)
				<tr>
					<td>{{$loop->iteration }}</td>
					<td><img alt="" title="" style="width:60px;height:60px!important" src="{{ $b->image_desktop_path=="" ? asset('image/na.jpg' ) :  asset('image/'.$b->image_desktop_path ) }} "></td>
					<td><img alt="" title="" style="width:60px;height:60px!important" src="{{ $b->image_mobile_path=="" ? asset('image/na.jpg' ) : asset('image/'.$b->image_mobile_path ) }}"></td>
					<td>{{$b->title}}</td>
					<td>{{$b->subtitle}}</td>
                    <td>{{$b->description}}</td>
                    <td>{{$b->link_text}}</td>
                    <td>{{$b->link_url}}</td>
                    <td>{{$b->bannerLocation->name}}</td>
					<td>
						<a href="{{route('banners.edit',$b->id)}}">
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
				</tr>
				@endforeach
				@if($banner->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $banner->appends($filters)->links() }}
</div>
</div>

@stop


