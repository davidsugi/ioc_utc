  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('image/Logo_Warna_Berdiri_400x400.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <p>{{ Auth::user()->role_name }}</p>
          {{--  <a href="#"><i class="fa fa-circle text-success"></i> Online</a>  --}}
          <br>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MANAJEMEN BACK END</li>
        <li class="{{ active('home') }}">
          <a href="{{ route('home') }}" >
            <i class="fa fa-dashboard"></i>
            <span>Dasboard</span>
          </a>
        </li>
        
        @can('Mengelola Jadwal')
        <li class="treeview {{active('custom_schedules.*')}} {{active('folios.*')}} ">
          <a href="#">
            <i class="fa fa-calendar"></i> <span>Jadwal</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu">
            <li class="{{active('folios.*')}}">
              <a href="{{ route('folios.index') }}" >
                <i class="fa fa-book"></i>
                <span>Jadwal Sewa</span>
              </a>
            </li>
            <li id="customSchedule" class="{{active('custom_schedules.*')}}">
              <a href="{{ route('custom_schedules.index') }}">
                <i class="fa fa-wrench"></i>
                <span>Jadwal Lain-lain</span>
              </a>
            </li>
          </ul>
        </li>
        @endcan

        @can('Mengelola Fasilitas')
          <li id="facilityPage" class="{{active('facilities.*')}}">
          <a href="{{ route('facilities.index') }}">
            <i class="fa fa-building"></i>
            <span>Fasilitas</span>
          </a>
        </li>
        @endcan
        @can('Mengelola Tambahan')
          <li id="addonPage" class="{{active('addons.*')}}">
          <a href="{{ route('addons.index') }}">
            <i class="fa fa-plus"></i>
            <span>Addon/Tambahan</span>
          </a>
        </li>
        @endcan
        @can('Mengelola Paket')
           <li id="Bundle" class="{{active('bundles.*')}}">
          <a href="{{ route('bundles.index') }}">
            <i class="fa fa-star"></i>
            <span>Paket</span>
          </a>
        </li>
        @endcan
       @canany('Mengelola Order|Mengelola Kupon')
         <li class="treeview {{active('orders.*')}} {{active('coupons.*')}}">
          <a href="#">
            <i class="fa fa-shopping-cart"></i> <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu">
            @can('Mengelola Order')
              <li class="{{active('orders.*')}}">
              <a href="{{ route('orders.index') }}" >
                <i class="fa fa-cart-plus"></i>
                <span>Order</span>
              </a>
            </li>    
            @endcan
              @can('Mengelola Kupon')
            <li class="{{active('coupons.*')}}">
              <a href="{{ route('coupons.index') }}" >
                <i class="fa fa-ticket"></i>
                <span>Kupon</span>
              </a>
            </li>
              @endcan

          </ul>
        </li>
       @endcanany
        @can('Mengelola Pelanggan')
            <li id="customerPage" class="{{active('customers.*')}}">
            <a href="{{ route('customers.index') }}">
              <i class="fa fa-users"></i>
              <span>Pelanggan</span>
            </a>
          </li>
          @endcan
       
        @canany('Mengelola Testimoni|Mengelola Aktivitas|Mengelola Berita|Mengelola Eduwisata')
                <li class="header">MANAJEMEN FRONT END</li>
        @endcanany
        @can('Mengelola Testimoni')
          <li class="{{active('testimonies.*')}}">
          <a href="{{ route('testimonies.index') }}" >
            <i class="fa fa-comments"></i>
            <span>Testimoni</span>
          </a>
        </li>
        @endcan
        @can('Mengelola Aktivitas')
          <li class="{{active('activities.*')}}">
          <a href="{{ route('activities.index') }}" >
            <i class="fa fa-compass"></i>
            <span>Aktivitas</span>
          </a>
        </li>
        @endcan
        @can('Mengelola Berita')
           <li  id="newsPage" class="treeview {{active('news.*')}} {{active('categories.*')}}">
          <a href="#">
            <i class="fa fa-newspaper-o"></i> <span>Berita</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu">
            <li id="newsPage" class="{{active('news.*')}}">
              <a href="{{ route('news.index') }}">
                <i class="fa fa-quote-right"></i>
                <span>Berita</span>
              </a>
            </li>

            <li id="categoryPage" class="{{active('categories.*')}}">
              <a href="{{ route('categories.index') }}">
                <i class="fa fa-cube"></i>
                <span>Kategori Berita</span>
              </a>
            </li>
          </ul>
        </li>
        @endcan
       @can('Mengelola Eduwisata')
         <li id="educationTourPage" class="{{active('education_tours.*')}}">
          <a href="{{ route('education_tours.index') }}">
            <i class="fa fa-tree"></i>
            <span>Eduwisata</span>
          </a>
        </li>
       @endcan
       @can('Mengelola Banner')
         <li class="{{active('banners.*')}}">
           <a href="{{ route('banners.index') }}" >
             <i class="fa fa-image"></i>
             <span>Banner</span>
           </a>
         </li>
       @endcan

        <li class="header">MANAJEMEN LAIN-LAIN</li>
      
      @can('Melihat Laporan')
         <li class="{{active('reports.*')}}">
          <a href="{{ route('reports.index') }}" >
            <i class="fa fa-book"></i>
            <span>Laporan</span>
          </a>
        </li>
      @endcan
       @can('Melihat Laporan Penggunaan')
         <li class="{{active('usages.*')}}">
          <a href="{{ route('usages.index') }}" >
            <i class="fa fa-book"></i>
            <span>Laporan Penggunaan</span>
          </a>
        </li>
      @endcan
        @canany('Mengelola Pengaturan Fasilitas|Mengelola Galeri|Mengelola Pengguna')
          <li  id="fasilitasPage" class="treeview  {{active('roles.*')}}{{active('permissions.*')}} {{active('custom_prices.*')}}{{active('customer_types.*')}}{{active('units.*')}}{{active('users.*')}}{{active('galleries.*')}} {{active('facility_attributes.*')}} {{active('facility_details.*')}}{{active('facility_types.*')}}">
          <a href="#">
            <i class="fa fa-cogs"></i> <span>Pengaturan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu">
            @can('Mengelola Pengaturan Fasilitas')
              <li id="attributePage" class="{{active('facility_attributes.*')}}">
              <a href="{{ route('facility_attributes.index') }}">
                <i class="fa fa-cube"></i>
                <span>Atribut Fasilitas</span>
              </a>
            </li>
            <li id="unit" class="{{active('units.*')}}">
              <a href="{{ route('units.index') }}">
                <i class="fa fa-cube"></i>
                <span>Unit</span>
              </a>
            </li>
            <li id="typePage" class="{{active('facility_types.*')}}">
              <a href="{{ route('facility_types.index') }}">
                <i class="fa fa-cube"></i>
                <span>Tipe Fasilitas</span>
              </a>
            </li>
            <li id="typePage" class="{{active('custom_prices.*')}}">
              <a href="{{ route('custom_prices.index') }}">
                <i class="fa fa-cube"></i>
                <span>Harga Khusus</span>
              </a>
            </li>
            @endcan
            @can('Mengelola Pengguna')
              <li id="typePage" class="{{active('customer_types.*')}}">
              <a href="{{ route('customer_types.index') }}">
                <i class="fa fa-cube"></i>
                <span>Tipe Pelanggan</span>
              </a>
            </li>
             <li id="userPage" class="{{active('users.*')}}">
              <a href="{{ route('users.index') }}" >
                <i class="fa fa-user"></i>
                <span>User</span>
              </a>
            </li>
            <li class="{{active('roles.*')}}">
              <a href="{{ route('roles.index') }}" >
                <i class="fa fa-briefcase"></i>
                <span>Jabatan</span>
              </a>
            </li>
            <li class="{{active('permissions.*')}}">
              <a href="{{ route('permissions.index') }}" >
                <i class="fa fa-tasks"></i>
                <span>Hak Akses</span>
              </a>
            </li>
            @endcan
            @can('Mengelola Galeri')
               <li id="galleryPage" class="{{active('galleries.*')}}">
              <a href="{{ route('galleries.index') }}">
                <i class="fa fa-image"></i>
                <span>Galeri</span>
              </a>
            </li>
            @endcan
           
           

          </ul>
        </li>
        @endcanany
        
        

        <li>
          <a href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="fa fa-sign-out"></i>
            <span>Logout</span>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>