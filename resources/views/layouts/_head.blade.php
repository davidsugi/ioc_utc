
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/font-awesome/css/font-awesome.min.css')}}">
{{--  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">  --}}
<!-- Ionicons -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/Ionicons/css/ionicons.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/dist/css/skins/_all-skins.min.css')}}">
<!-- Morris chart -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/morris.js/morris.css')}}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/jvectormap/jquery-jvectormap.css')}}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/select2/dist/css/select2.min.css')}}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
<!-- fullCalendar -->
<link rel="stylesheet" href="{{asset('css/summernote.css')}}">
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/fullcalendar/dist/fullcalendar.print.min.css')}}" media="print">
<link href="{{ asset('css/summernote.css') }}" rel="stylesheet">

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" href="{{asset('css/custom.css')}}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

@stack('styles')