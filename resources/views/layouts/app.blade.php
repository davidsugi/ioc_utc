<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <script>
    window.Laravel = {!! json_encode(['csrfToken' => csrf_token(), 'url' => url('/')]) !!};
  </script>

  <title>{{ config('app.name') }}</title>

  @include("layouts._head")

  
</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="{{ route('home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>{{ env("APP_SHORT","IOC") }}</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>{{ env("APP_SHORT","IOC") }}</b>UTC</span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          {{-- @include('layouts._notification') --}}
          
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              {{--  <img src="{{ asset('images/logo.png') }}" class="user-image" alt="User Image">  --}}
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="{{ asset('image/Logo_Warna_Berdiri_400x400.jpg') }}" class="img-circle" alt="User Image">

                <p>
                  {{--  {{ Auth::user()->name }}  --}}
                  <small>{{ Auth::user()->name }}</small>
                  <small>{{ Auth::user()->roleName }}</small>

                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="{{ route('logout') }}" class="btn btn-danger btn-flat"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                      Log out
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('image/logoutc.png') }}" class="img-circle sidebar-logo" alt="UTCs">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#">{{ Auth::user()->role }}</a>
          <p><small>{{ Auth::user()->name }}</small></p>
        </div>
      </div>

      @include('layouts._sidebar_menu')
      
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row row-eq-height">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <h2 class="title-header">
                @yield('title')
              </h2>
            </div>
            <div class="col-md-6 actionbtn">
              @yield('actionbtn')            
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <ol class="breadcrumb">
                @yield('breadcrumb') 
              </ol>
            </div>
          </div>

        </div>
      </div>

{{--      
        --}}
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li> -->
      {{--  --}}
    </section>

    <!-- Main content -->
    <section class="content">
        @if (Session::has('toast'))
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-check"></i> Success!</h4>
          {!! session('toast') !!}
        </div>
        @endif
       @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-times"></i> Error!</h4>
          {!! session('error') !!}
        </div>
        @endif
      @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @yield('aside')
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
@include('layouts._bottom')
@yield('bottom')


</body>
</html>
