@extends('layouts.guest')
@section('content')
<div class="login-box">
    <div class="login-logo">
        <div class="panel-heading">Reset Password</div>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif

        <form  method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
              <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>

              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group">
            <center>
                <button type="submit" class="btn btn-primary">
                    Send Password Reset Link
                </button>
            </center>
        </div>
    </form>
</div>
</div>
@endsection
