@extends('layouts.app')
@section('title')
{{ $permission->exists ? 'Edit' : 'Create' }} Hak Akses
@endsection
@if($permission->exists)
@section('actionbtn')
<a data-href="{{ route('permissions.destroy', $permission->id) }}" class="btn btn-danger destroy">Hapus Hak Akses</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='{{ route("permissions.index")}}'><i class='fa fa-list'></i> Hak Akses</a></li>
<li><a href='#' class='active'>{{ $permission->exists ? 'Ubah' : 'Tambah'}} Hak Akses</a></li>

@endsection

@section('content')
<div class="row">
    <div class="col-xs-10">
        <div class="box">
            <div class="box-body">
                @if($permission->exists)
                    {!! Form::model($permission, ['route' => ['permissions.update', $permission->id], 'method'=>'PATCH', 'files' => true]) !!}
                @else
                    {!! Form::model($permission, ['route' => ['permissions.store'], 'class' => 'col s12', 'files' => true]) !!}
                @endif
                    <div class="form-group col-md-12">
                        {{ Form::label('name','Hak Akses') }}
                        {{ Form::text('name', $permission->name, [  'placeholder'=>'Masukan Hak Akses',  'class' => 'form-control', 'required']) }}
                        @if ($errors->has('name'))
                            <div class="help-block text-red">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                    </div>
                    
                <div class="box-footer">
                    {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
