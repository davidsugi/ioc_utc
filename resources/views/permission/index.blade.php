@extends('layouts.app')
@section('title')
List Hak Akses
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-tasks'></i> Hak Akses</a></li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">

            <div class="box-body">
                <table id="example" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Hak Akses</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $permissions as $key => $permission )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $permission->name }}</td>
                        </tr>
                    @endforeach
                    @if($permissions->count() == 0)
                        <tr>
                            <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                        </tr>
                    @endif
                    </tbody>
            </table>
        </div>
        {{ $permissions->links() }}
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
