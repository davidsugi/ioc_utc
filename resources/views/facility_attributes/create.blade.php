@extends('layouts.app')

@section('title')
{{ $facility_attribute->exists ? 'Ubah' : 'Tambah' }} Attribute
@endsection
@if($facility_attribute->exists)
@section('actionbtn')
<a data-href="{{ route('facility_attributes.destroy', $facility_attribute->id) }}" class="btn btn-danger destroy">Hapus Attribute</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("facility_attributes.index")}}'><i class='fa fa-cube'></i> Atribut Fasilitas</a></li>
<li><a href='#' class='active'>{{ $facility_attribute->exists ? 'Ubah' : 'Tambah'}} Atribut Fasilitas</a></li>
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($facility_attribute->exists)
        {!! Form::model($facility_attribute, ['route' => ['facility_attributes.update', $facility_attribute->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($facility_attribute, ['route' => ['facility_attributes.store'], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Nama</label>
              {!! Form::text('name', $facility_attribute->name, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Tipe Data</label>
             {!! Form::select('datatype', ['String' => 'String', 'Integer' => 'Integer', 'Boolean' => 'Boolean','Date'=>'Date'], $facility_attribute->datatype,['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              {{ Form::label('icon','Icon') }}
              {{ Form::text('icon', $facility_attribute->icon, ['class' => 'form-control', 'required']) }}
              @if ($errors->has('icon'))
                <div class="help-block text-red">
                  {{ $errors->first('icon') }}
                </div>
              @endif
            </div>
            
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')

