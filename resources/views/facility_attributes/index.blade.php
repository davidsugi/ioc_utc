@extends('layouts.app')

@section('title')
Manage Fasilitas Attribute
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-cube'></i> Atribut Fasilitas</a></li>
@endsection
@section('content')

<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[name]" placeholder="Nama Fasilitas Attribute" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<br>

<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('facility_attributes.create')}}" class="btn btn-success btn-block">Tambah Attribute &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>

	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nama</th>
					<th>Tipe Data</th>
					<th>Icon</th>
					<th>Edit</th>
				</tr>
			</thead>
			<tbody>
				@foreach($facility_attribute as $a)
				<tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$a->name}}</td>
					<td>{{$a->datatype}}</td>
					<td><i class="{{$a->icon}}"></i></td>
					<td>
						<a href="{{route('facility_attributes.edit',$a->id)}}">
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
				</tr>
				@endforeach
				@if($facility_attribute->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $facility_attribute->appends($filters)->links() }}
</div>
</div>

@stop
