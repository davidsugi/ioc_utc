@extends('layouts.app')
@section('title')
List Lokasi Banner
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <a href="{{ route('banner_locations.create') }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Lokasi Banner</button></a>
            </div>
            <div class="box-body">
                <table id="example" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Nama</th>
                            <th width="25%">Kode</th>
                            <th width="25%" colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $banner_locations as $key => $banner_location )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $banner_location->name }}</td>
                            <td>{{ $banner_location->code }}</td>
                            <td><a href="{{ route('banner_locations.edit',['id'=>$banner_location->id]) }}" class="btn btn-warning">
                                    Edit
                                </a>
                                <a data-href="{{ route('banner_locations.destroy',['id'=>$banner_location->id]) }}" class="btn btn-danger destroy">
                                Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    @if($banner_locations->count() == 0)
                        <tr>
                            <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                        </tr>
                    @endif
                    </tbody>
            </table>
        </div>
        {{ $banner_locations->links() }}
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
