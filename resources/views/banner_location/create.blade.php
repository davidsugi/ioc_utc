@extends('layouts.app')
@section('title')
{{ $banner_location->exists ? 'Ubah' : 'Tambah' }} Lokasi Banner
@endsection
@if($banner_location->exists)
@section('actionbtn')
<a data-href="{{ route('banner_locations.destroy', $banner_location->id) }}" class="btn btn-danger destroy">Hapus Lokasi Banner</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='{{ route("banner_locations.index")}}'><i class='fa fa-image'></i> Lokasi Banner </a></li>
<li><a href='#' class='active'>{{ $banner_location->exists ? 'Ubah' : 'Tambah'}} Lokasi Banner </a></li>

@endsection
@section('content')
<div class="row">
    <div class="col-xs-10">
        <div class="box">
            <div class="box-body">
                @if($banner_location->exists)
                    {!! Form::model($banner_location, ['route' => ['banner_locations.update', $banner_location->id], 'method'=>'PATCH', 'files' => true]) !!}
                @else
                    {!! Form::model($banner_location, ['route' => ['banner_locations.store'], 'class' => 'col s12', 'files' => true]) !!}
                @endif
                    <div class="form-group col-md-12">
                        {{ Form::label('name','Lokasi') }}
                        {{ Form::text('name', $banner_location->name, [  'placeholder'=>'Masukan Lokasi',  'class' => 'form-control', 'required']) }}
                        @if ($errors->has('name'))
                            <div class="help-block text-red">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label('code','Kode') }}
                        {{ Form::text('code', $banner_location->kode, [  'placeholder'=>'Masukan Kode',  'class' => 'form-control', 'required']) }}
                        @if ($errors->has('code'))
                            <div class="help-block text-red">
                                {{ $errors->first('code') }}
                            </div>
                        @endif
                    </div>
                    
                <div class="box-footer">
                    {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
