@extends('layouts.app')

@section('title')
{{ $custom_price->exists ? 'Ubah' : 'Tambah' }} Harga
@endsection
@if($custom_price->exists)
@section('actionbtn')
<a data-href="{{ route('facilities.custom_prices.destroy', ['facility'=>$facility->id, 'custom_price'=>$custom_price->id]) }}" class="btn btn-danger destroy">Hapus Harga</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("facilities.index")}}'><i class='fa fa-building'></i> Fasilitas</a></li>
<li><a href='{{ route("facilities.show",["id"=>$facility->id])}}'>{{ $facility->name }}</a></li>
<li><a href='#' class='active'>{{ $custom_price->exists ? 'Ubah' : 'Tambah'}} Harga</a></li>
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($custom_price->exists)
        {!! Form::model($custom_price, ['route' => ['facilities.custom_prices.update', 'facility'=>$facility->id, 'custom_price'=>$custom_price->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($custom_price, ['route' => ['facilities.custom_prices.store',$facility->id], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Date and time range:</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
                <input type="text" class="form-control" name ="rangedate" id="reservationtime" value="{{$custom_price->carbonstart}} - {{$custom_price->carbonend}}" required>
              </div>
              <!-- /.input group -->
            </div>
            <div class="form-group col-md-12">
              <label>Fasilitas</label>
              {!! Form::text('facility_name',$facility->name,['class'=> 'form-control','readonly']) !!}
            </div>
            {{--  {{ $type }}  --}}
            <div class="form-group col-md-12">
              <label>Persentase</label>
              <table>
                <tbody>
                <tr>
                  <td width="50%">{!! Form::number('price', $custom_price->price ?  $custom_price->price : 0, ['class'=> 'form-control','required' => 'required']) !!}</td>
                  <td style="margin-left:5px"> %</td>
                </tr>
                </tbody>
              </table>
            </div>
            </div>
          </div>
          <!-- /.box-body -->
          
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<script>
  $(function () {
    $('#reservationtime').daterangepicker({ timePicker: true, timePicker24Hour: true, timePickerIncrement: 30,locale: {format: 'DD/MM/YYYY H:mm:ss'}})
  })
</script>
@endsection
@include('layouts._deletebtn')
