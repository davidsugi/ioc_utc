@extends('layouts.app')
@section('title')
Dashboard
@endsection
@section('breadcrumb') 
<li><a href="#" class="active"><i class="fa fa-dashboard"></i> Home</a></li>
@endsection

@push('styles')
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endpush

@section('content')
  

      <div class="row">
        <div class="col-md-12">
          <!-- AREA CHART -->
          <div class="box box-danger">
              
            <div class="box-header with-border">
              <h3 class="box-title">Jadwal</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              {{--  <div class="chart">
                <canvas id="areaChart" style="height:250px"></canvas>
              </div>  --}}
                    <div id="calendar"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    </div>
  
      <div class="row">
      @can('Melihat Laporan')
    <div class="col-md-12">
          <!-- BAR CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Order Oleh customer</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="chart">
                            <canvas id="barChart" style="height: 230px; width: 795px;" width="795" height="230" data-source='{{ json_encode($cus) }}'></canvas>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <ul class="chart-legend clearfix">
                            @php($c=1)
                            @foreach($cus as $cs => $cd)
                                @if($c%5==0)
                                    <li style="word-wrap:break-word"><i class="fa fa-circle-o text-gray"></i> {{$cs}}</li>         
                                @elseif($c%4==0)
                                    <li style="word-wrap:break-word"><i class="fa fa-circle-o text-yellow"></i> {{$cs}}</li>         
                                @elseif($c%3==0)
                                    <li style="word-wrap:break-word"><i class="fa fa-circle-o text-green"></i> {{$cs}}</li>  
                                @elseif($c%2==0)
                                    <li style="word-wrap:break-word"><i class="fa fa-circle-o text-red"></i> {{$cs}}</li>                                  
                                @else
                                    <li style="word-wrap:break-word"><i class="fa fa-circle-o text-light-blue"></i> {{$cs}}</li>                                    
                                @endif
                                @php($c++)
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            </div>
            <!-- /.box-body -->
        </div>
      </div>
      <div class="row">

        <!-- /.col (LEFT) -->
        <div class="col-md-12">
          <!-- LINE CHART -->
          <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Order Berdasar Fasilitas</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="chart">
                            <canvas id="pieChart" style="height:250px" data-source='{{ json_encode($facs) }}'></canvas>                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->

        </div>
      </div>
        <!-- /.col (RIGHT) -->
      @endcan
      <!-- /.row -->
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

    </div>
</div>
@endsection


@push('scripts')
<script src="{{asset('AdminLTE-2.4.2/bower_components/chart.js/Chart.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
    $(function () {
        $(".dt").dataTable();
        ajaxCalendar();
		function ajaxCalendar() {
			$.ajax({
				type: 'GET',
				datatype: 'json',
				url: '{{route("API.detailfacilitycalendar")}}',               
				data: ({id: "all", auth:'{{ Auth::user()->id }}'}),                
				success: function(data) {
					var header = {
						left  : 'prev,next today',
						center: 'title',
						right : 'month,agendaWeek,agendaDay'
					};
					var buttonText = {
						today: 'today',
						month: 'month',
						week : 'week',
						day  : 'day'
					};
					var events =[];
          var timeFormat = 'H(:mm)t';
          console.log(data)
					for (var i=0; i<data.length; i++){
						var e = {
							title          : data[i][0],
							start          : $.fullCalendar.moment(data[i][2]),
							end            : $.fullCalendar.moment(data[i][3]),
                            backgroundColor: data[i][1],
                            url: data[i][4],
                            borderColor    : data[i][1],
                            allDay:true
						};
						events.push(e);
					}
					$('#calendar').fullCalendar({
						header,
						buttonText,
						events,
            timeFormat,
            eventLimitText: " Lainnya",
					})
				}
			})
		}
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    $('#barChart').each(function() {
      var chartData = JSON.parse($(this).attr('data-source'));
      var label = [];
      var data = [];
      var c=0;
       $.each(chartData, function(i, val) {
           var tmp =[];
         $.each(val, function(lab, dat) {
             if(c==0){
                label.push(lab);                
             }
            tmp.push(dat);
         });
         c++;
         var color ='#3467cc';
         if(c%5==0)
         {
             color = '#d8d8d8';
         }
         if(c%4==0)
         {
             color = '#f39c12';
         }
         else if(c%3==0)
         {
             color = '#34cc4f';
         }
         else if(c%2==0)
         {
             color = '#c44852';
         }
            var sets={
            label: i,
            fillColor           : color,
            strokeColor         : color,
            pointColor          : color,
            pointStrokeColor    : color,
            pointHighlightFill  : color,
            pointHighlightStroke: color,
            data                : tmp
            }
             data.push(sets);
            
       });
      console.log(data);
       
      var barChartData = {
        labels  : label,
        datasets: data
      }
      //-------------
      //- BAR CHART -
      //-------------
      var barChartCanvas                   = $(this).get(0).getContext('2d')
      var barChart                         = new Chart(barChartCanvas)
      var barChartOptions                  = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero        : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : true,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - If there is a stroke on each bar
        barShowStroke           : true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth          : 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing         : 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing       : 1,
        //String - A legend template
        legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to make the chart responsive
        responsive              : true,
        maintainAspectRatio     : true
      }

      barChartOptions.datasetFill = false
      barChart.Bar(barChartData, barChartOptions);
    });

    $('#pieChart').each(function() {
      var chartData = JSON.parse($(this).attr('data-source'));
      var label = [];
      var data = [];
      var c=0;
       $.each(chartData, function(i, val) {
           var tmp =[];
         c++;
         var color =['#f56954','#00a65a','#f39c12','#00c0ef','#3c8dbc','#d2d6de']
            var sets={
                value    : val,
                color    : color[c%6],
                highlight: color[c%6],
                label    : i
            }
             data.push(sets);
            
       });
      console.log(data);
       
      //-------------
      //- BAR CHART -
      //-------------
      var barChartCanvas                   = $(this).get(0).getContext('2d')
      var barChart                         = new Chart(barChartCanvas)
      var barChartOptions                  = {
    
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      //String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      //Number - The width of each segment stroke
      segmentStrokeWidth   : 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps       : 100,
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    barChart.Doughnut(data, barChartOptions)    
    });

     
  })
</script>
@endpush
