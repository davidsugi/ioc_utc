@extends('layouts.app')
@section('title')
{{ $special_price->exists ? 'Ubah' : 'Tambah' }} Harga
@endsection
@if($special_price->exists)
@section('actionbtn')
<a data-href="{{ route('special_prices.destroy', $special_price->id) }}" class="btn btn-danger destroy">Hapus Harga</a>
@endsection
@endif

@if($special_price->special_type=="App\\Facility")
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("facilities.index")}}'><i class='fa fa-building'></i> Fasilitas</a></li>
<li><a href='{{ route("facilities.show",$special_price->special_id)}}'> {{ $facility[$special_price->special_id] }}</a></li>
<li><a href='#' class='active'>{{ $special_price->exists ? 'Ubah' : 'Tambah'}} Harga</a></li>
@endsection
@elseif($special_price->special_type=="App\\Addon")
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("addons.index")}}'><i class='fa fa-building'></i> Tambahan</a></li>
<li><a href='{{ route("addons.show",$special_price->special_id)}}'> {{ $facility[$special_price->special_id] }}</a></li>
<li><a href='#' class='active'>{{ $special_price->exists ? 'Ubah' : 'Tambah'}} Harga</a></li>
@endsection
@elseif($special_price->special_type=="App\\bundle")
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("bundles.index")}}'><i class='fa fa-star'></i> Paket</a></li>
<li><a href='{{ route("bundles.show",$special_price->special_id)}}'> {{ $facility[$special_price->special_id] }}</a></li>
<li><a href='#' class='active'>{{ $special_price->exists ? 'Ubah' : 'Tambah'}} Harga</a></li>
@endsection
@else
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("special_prices.index")}}'><i class='fa fa-cube'></i> Harga</a></li>
<li><a href='#' class='active'>{{ $special_price->exists ? 'Ubah' : 'Tambah'}} Harga</a></li>
@endsection
@endif

@section('content')
<div class="row">
    <div class="col-xs-10">
        <div class="box">
            <div class="box-body">
                @if($special_price->exists)
                    {!! Form::model($special_price, ['route' => ['special_prices.update', $special_price->id], 'method'=>'PATCH', 'files' => true]) !!}
                @else
                    {!! Form::model($special_price, ['route' => ['special_prices.store'], 'class' => 'col s12', 'files' => true]) !!}
                @endif
                {{ Form::hidden('special_id', $special_price->special_id, ['class' => 'form-control', 'required']) }}                
                {{ Form::hidden('special_type', $special_price->special_type, ['class' => 'form-control', 'required']) }}                
                    <div class="form-group">
                        {{ Form::label('special_id','Item') }}
                        {{ Form::select('special_id',  empty($facility) ? array() : $facility , $special_price->special_id, ['class' => 'form-control select2 facs', 'required','disabled']) }}
                        @if ($errors->has('special_id'))
                            <div class="help-block text-red">
                                {{ $errors->first('special_id') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('customer_type_id','Tipe Pelanggan') }}
                        {{ Form::select('customer_type_id',empty($customer_types) ? array() : $customer_types , $special_price->customer_type_id, ['class' => 'form-control select2 custtype', 'required']) }}
                        @if ($errors->has('customer_type_id'))
                            <div class="help-block text-red">
                                {{ $errors->first('customer_type_id') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('weekend_price','Harga Weekend Menginap') }}
                        {{ Form::number('weekend_price', $special_price->weekend_price, ['class' => 'form-control', 'required']) }}
                        @if ($errors->has('weekend_price'))
                            <div class="help-block text-red">
                                {{ $errors->first('weekend_price') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('price','Harga Weekday Menginap') }}
                        {{ Form::number('price', $special_price->price, ['class' => 'form-control', 'required']) }}
                        @if ($errors->has('price'))
                            <div class="help-block text-red">
                                {{ $errors->first('price') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('no_stay_weekend_price','Harga Weekend Tidak menginap') }}
                        {{ Form::number('no_stay_weekend_price', $special_price->no_stay_weekend_price, ['class' => 'form-control', 'required']) }}
                        @if ($errors->has('no_stay_weekend_price'))
                            <div class="help-block text-red">
                                {{ $errors->first('no_stay_weekend_price') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('no_stay_weekday_price','Harga weekday tidak menginap') }}
                        {{ Form::number('no_stay_weekday_price', $special_price->no_stay_weekday_price, ['class' => 'form-control', 'required']) }}
                        @if ($errors->has('no_stay_weekday_price'))
                            <div class="help-block text-red">
                                {{ $errors->first('no_stay_weekday_price') }}
                            </div>
                        @endif
                    </div>
                    
                    
                <div class="box-footer">
                    {{ Form::submit('Simpan', ['class' => 'btn btn-success']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $(".select2").select2();
         $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush


