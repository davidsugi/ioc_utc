@extends('layouts.app')
@section('title')
List Special Price
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-cube'></i> Special Price</a></li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <a href="{{ route('special_prices.create') }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Special Price</button></a>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2" width="5%">No</th>
                            <th rowspan="2" width="15%">Nama</th>
                            <th rowspan="2" width="10%">Tipe Customer</th>
                            <th colspan="2" width="20%">Harga Menginap</th>
                            <th colspan="2" width="20%">Harga Tidak Menginap</th>
                            <th rowspan="2" width="10%" colspan="2">Action</th>
                        </tr>
                        <tr>
                             <th width="15%">Weekend Price</th>
                            <th width="15%">Weekday Price</th>
                            <th width="15%">Weekend Price</th>
                            <th width="15%">Weekday Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $special_prices as $key => $special_price )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $special_price->nameLabel }}</td>
                            <td>{{ $special_price->typeLabel }}</td>
                            <td>{{ H::rupiah($special_price->weekend_price) }}</td>
                            <td>{{ H::rupiah($special_price->price) }}</td>
                            <td>{{ H::rupiah($special_price->no_stay_weekend_price) }}</td>
                            <td>{{ H::rupiah($special_price->no_stay_weekday_price) }}</td>
                            {{--  <td><a href="{{ route('special_prices.show',['id'=>$special_price->id]) }}"><i class="material-icons black-text">open_in_new</i></a>
                                <a href="{{ route('special_prices.edit',['id'=>$special_price->id]) }}">
                                    <i class="material-icons">mode_edit</i>
                                </a>
                                <i class="material-icons">delete</i>
                                <a class="destroy" data-href="{{ route('special_prices.destroy',['id'=>$special_price->id]) }}">
                                </a>
                            </td>  --}}
                        </tr>
                    @endforeach
                    @if($special_prices->count() == 0)
                        <tr>
                            <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                        </tr>
                    @endif
                    </tbody>
            </table>
        </div>
        {{ $special_prices->links() }}
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
