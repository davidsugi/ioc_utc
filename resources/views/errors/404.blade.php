@extends('errors::layout')

@section('title', 'Not Found')

@section('message', 'Maaf, Halaman Tidak Ditemukan.')
