@extends('errors::layout')

@section('title', 'Unauthorized Page')

@section('message', 'Maaf, Anda tidak memiliki hak akses untuk membuka halaman ini.')
