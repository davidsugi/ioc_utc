@extends('layouts.app')

@section('title')
Manage Fasilitas
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-building'></i> Fasilitas</a></li>
@endsection
@section('content')
<style>
	input[type="date"]:before {
	content: attr(placeholder) !important;
	color: #aaa;
	margin-right: 0.5em;
	}
	input[type="date"]:focus:before,
	input[type="date"]:valid:before {
	content: "";
	}
</style>
<div class='col-md-4'>
  <div class='small-box bg-blue'>
     <div class='inner'>
      <h3>{{ $all}}</h3>
      <p>Total Jenis Fasilitas</p>
    </div>
    <div class='icon'>
      <i class='fa fa-building'></i>
    </div>
    {{--  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>  --}}
  </div>
</div>
<div class='col-md-4'>
  <div class='small-box bg-aqua'>
     <div class='inner'>
      <h3>{{ $detf}}</h3>
      <p>Total Fasilitas</p>
    </div>
    <div class='icon'>
      <i class='fa fa-bed'></i>
    </div>
    {{--  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>  --}}
  </div>
</div>

<div class='col-md-4'>
  <div class='small-box bg-green'>
     <div class='inner'>
      <h3>{{ $ava}}</h3>
      <p>Total Fasilitas Dapat Dipinjam </p>
    </div>
    <div class='icon'>
      <i class='fa fa-check'></i>
    </div>
    {{--  <a href="{{ route('facilities.index',['available'=>'1']) }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>  --}}
  </div>
</div>



<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-2">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[name]" placeholder="Nama" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-2">
			<div class="input-group no-border">
				{!! Form::select('filter[type_id]', $type, null, ['placeholder'=>'Tipe','class'=> 'form-control']) !!}
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-1">
			<div class="input-group no-border">
				<input type="number" class="form-control column-filter" name="filter[capacity]" placeholder="Kapasitas" value="{{ !empty($filter['capacity']) ? $filter['capacity'] : '' }}"/>				
				{{--  <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>  --}}
			</div>
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="date" class="form-control column-filter" name="filter[date]" placeholder="Mulai" value="{{ !empty($filter['date']) ? $filter['date'] : '' }}"/>								
				{{--  {!! Form::select('filter[available]', array("Ketersediaan", "tersedia","tidak Tersedia"), null, ['class'=> 'form-control']) !!}				  --}}
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-1">
			<div class="input-group no-border">
				<input type="number" class="form-control column-filter" name="filter[duration]" placeholder="Durasi" value="{{ !empty($filter['duration']) ? $filter['duration'] : '' }}"/>				
				{{--  <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>  --}}
			</div>
		</div>
		<div class="col-md-2">
			<div class="input-group no-border">
				{!! Form::select('filter[available]', array("Ketersediaan", "tersedia","tidak Tersedia"), null, ['class'=> 'form-control']) !!}				
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>

		<div class="col-md-1">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<br>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('facilities.create')}}" class="btn btn-success btn-block">Tambah Fasilitas &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nama</th>
					<th>Harga</th>
					<th>Harga Weekend</th>
					<th>Minimum</th>
					<th>Kapasitas</th>
					<th>Tipe</th>
					<th>Unit Tersedia</th>
					<th>Edit</th>
					<th>Show Detail</th>
				</tr>
			</thead>
			<tbody>
				@foreach($facility as $f)
				<tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$f->name}}</td>
					<td>{{H::rupiah($f->price)}}</td>
					<td>{{H::rupiah($f->weekend_price)}}</td>
					<td>{{$f->minimum}}</td>
					<td>{{$f->capacity}}</td>
					<td>{{$f->facilityType->name}}</td>
					@if(count($filter)>0)
					<td>{{ count($f->Available($filter['date'] ? $filter['date'] : date("Y-m-d"), $filter['duration']>0 ? $filter['duration'] : 1)) }}</td>
					@else
					<td>{{ count($f->Available(date("Y-m-d"),1)) }}</td>					
					@endif
					<td>
						<a href="{{route('facilities.edit',$f->id)}}">
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
					<td>
						<a href="{{route('facilities.show',$f->id)}}">
							<button class="btn btn-primary">
								Show
							</button>
						</a>
					</td>
				</tr>
				@endforeach
				@if($facility->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
		}
	@endphp
	{{ $facility->appends($filters)->links() }}
</div>
</div>

@stop
