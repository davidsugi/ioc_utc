@extends('layouts.app')

@section('title')
{{ $facility->exists ? 'Ubah' : 'Tambah' }} Fasilitas
@endsection
@if($facility->exists)
@section('actionbtn')
<a data-href="{{ route('facilities.destroy', $facility->id) }}" class="btn btn-danger destroy">Hapus Fasilitas</a>
@endsection
@endif
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("facilities.index")}}'><i class='fa fa-building'></i> Fasilitas</a></li>
<li><a href='#' class='active'>{{ $facility->exists ? 'Ubah' : 'Tambah'}} Fasilitas</a></li>
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
      @if($facility->exists)
      {!! Form::model($facility, ['route' => ['facilities.update', $facility->id], 'method'=>'PATCH','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
      @else
      {!! Form::model($facility, ['route' => ['facilities.store'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
      @endif
      <div class="box-body">
        <div class="row">
          <div class="form-group col-md-12">
            <label>Nama</label>
            {!! Form::text('name', $facility->name, ['class'=> 'form-control','required' => 'required']) !!}
          </div>
          <div class="form-group col-md-12">
            <label>Kode</label>
            {!! Form::text('code', $facility->code, ['class'=> 'form-control','required' => 'required']) !!}
          </div>
          <div class="form-group col-md-12">
            <label>Link Embed</label>
            {!! Form::text('link_embed', $facility->link_embed, ['class'=> 'form-control']) !!}
          </div>
          <div class="form-group col-md-3">
            <label>Harga Menginap Weekend</label>
            {!! Form::number('weekend_price', $facility->weekend_price, ['class'=> 'form-control']) !!}
          </div>  
          <div class="form-group col-md-3">
            <label>Harga Menginap Weekday</label>
            {!! Form::number('price', $facility->price, ['class'=> 'form-control','required' => 'required']) !!}
          </div> 
          <div class="form-group col-md-3">
            <label>Harga Tidak Menginap Weekend</label>
            {!! Form::number('no_stay_weekend_price', $facility->no_stay_weekend_price, ['class'=> 'form-control','required' => 'required']) !!}
          </div>  
          <div class="form-group col-md-3">
            <label>Harga Tidak Menginap Weekday</label>
            {!! Form::number('no_stay_weekday_price', $facility->no_stay_weekday_price, ['class'=> 'form-control']) !!}
          </div>  
          <div class="form-group col-md-3">
            <label>Satuan</label>
            {!! Form::select('unit', ['1'=>'Jam','2'=>'Hari'],$facility->unit, ['class'=> 'form-control select2','required' => 'required']) !!}
          </div>
          <div class="form-group col-md-3" style="padding-top:30px">
            {{ Form::checkbox('per_person', 1,$facility->per_person==1 ? true : false , ['class' => 'field']) }}
            
            <label>
              Per Person
            </label>
          </div>
          <div class="form-group col-md-3">
            <label>Minimum</label>
            {!! Form::number('minimum', $facility->minimum, ['class'=> 'form-control','required' => 'required', 'min'=>0]) !!}
          </div>
          <div class="form-group col-md-3">
            <label>Kapasitas</label>
            {!! Form::number('capacity', $facility->capacity, ['class'=> 'form-control','required' => 'required', 'min'=>0]) !!}
          </div>

          <div class="form-group col-md-6" style="padding-top:30px">
            {{ Form::checkbox('front_end', 1, $facility->front_end==1 ? true : false , ['class' => 'field']) }}
            
            <label>
              Tampilkan pada halaman depan?
            </label>
          </div>
          <div class="form-group col-md-6" style="padding-top:30px">
            {{ Form::checkbox('back_end', 1, $facility->back_end==1 ? true : false , ['class' => 'field']) }}
            
            <label>
              Aktif?
            </label>
          </div>
          <div class="form-group col-md-12">
            <label>Tipe</label>
            {!! Form::select('type_id', $type, $facility->type_id, ['placeholder'=>'Tipe','class'=> 'form-control select2','required' => 'required']) !!}
          </div>
          <div class="form-group col-md-12">
            <label>Deskripsi</label>
            {!! Form::textarea('description', $facility->description, ['id'=>'description','class'=> 'form-control']) !!}
          </div>
          @if(!$facility->exists)
          <div class="col-md-12">
            <input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple/>
          </div>
          <div class="col-md-12">
            <div class="row" id="image_preview"></div>
          </div>
          @endif
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</section>
<!-- CK Editor -->
{{--  <script src="{{asset('AdminLTE-2.4.2/bower_components/ckeditor/ckeditor.js')}}"></script>  --}}

@endsection
@include('layouts._deletebtn')
@push('scripts')
<script type="text/javascript" src="{{ asset('js/plugins/select2/select2.js') }}"></script>
<script src="{{ asset('js/summernote.min.js') }}"></script>

<script>

  $("#description").summernote({
    toolbar: [
	['style', ['bold', 'italic', 'underline', 'clear']],
	['paragraph', ['ol','ul']]
    ]
  });

  $(".select2").select2();

  function preview_images() 
  {
    var total_file=document.getElementById("images").files.length;
    for(var i=0;i<total_file;i++)
    {
      $('#image_preview').append("<div class='col-md-3' style='padding-top:20px'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
    }
  }
</script>
@endpush
