@extends('layouts.app')
@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick-theme.css')}}">
<style>
	
	* {
		box-sizing: border-box;
	}
	
	
	.slick-slide {
		margin: 0px 25px;
	}
	
	.slick-slide img {
		width: 100%;
	}
	
	.slick-prev:before,
	.slick-next:before {
		color: black;
	}
	
	
	.slick-slide {
		transition: all ease-in-out .3s;
		opacity: .2;
	}
	.slick-slide:focus {
		opacity:1;
	}
	
	.slick-active {
		opacity: .5;
	}
	
	
	/* Main carousel style */
	.carousel {
		width: 600px;
	}
	
	/* Indicators list style */
	.article-slide .carousel-indicators {
		bottom: 0;
		left: 0;
		margin-left: 5px;
		width: 100%;
	}
	/* Indicators list style */
	.article-slide .carousel-indicators li {
		border: medium none;
		border-radius: 0;
		float: left;
		height: 54px;
		margin-bottom: 5px;
		margin-left: 0;
		margin-right: 5px !important;
		margin-top: 0;
		width: 100px;
	}
	/* Indicators images style */
	.article-slide .carousel-indicators img {
		border: 2px solid #FFFFFF;
		float: left;
		height: 54px;
		left: 0;
		width: 100px;
	}
	/* Indicators active image style */
	.article-slide .carousel-indicators .active img {
		border: 2px solid #428BCA;
		opacity: 0.7;
	}
</style>
@endpush
@section('title')
Manage Fasilitas
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("facilities.index")}}'><i class='fa fa-building'></i> Fasilitas</a></li>
<li><a href='#' class='active'>Detail Fasilitas</a></li>
@endsection
@section('content')
<div class="row">
	<section class="col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="row">
					
					<div class="col-md-12">
						<h1>Nama: {{$facility->name}}</h1>
					</div>
				</div>
			</div>	
		</div>
	</section>
	
	<section class="col-md-4">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<h2>Tipe: {{$facility->facilityType->name}}</h2>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section class="col-md-12">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Fasilitas
					</div>
				</div>
			</div>	
			<div class="box-body" >
				<div class="row">
					<div class="col-md-12">					
						<span class="label label-info" style="padding:10px; display:{{ $facility->front_end == 1? '' : 'none' }}">Tampil di Halaman Depan</span>
						<span class="label label-primary" style="padding:10px; display:{{ $facility->back_end == 1? '' : 'none' }}" >Dapat dipinjam melalui halaman admin</span>
						<span class="label label-success" style="padding:10px; display:{{ $facility->per_person == 1? '' : 'none' }}" >Perorang</span>
					</div>
				</div>
				<br>
				<div class="row">
					
					<h4>
						
						<div class="col-md-12">
							<div class="col-md-3">Minimal</div>
							<div class="col-md-9">: {{$facility->minimum}}</div>
						</div>
					</h4>
				</div>
				<div class="row">
					<h4>
						<div class="col-md-12">
							<div class="col-md-3">Kapasitas</div>
							<div class="col-md-9">: {{$facility->capacity}}</div>
						</div>
					</h4>
				</div>
				<div class="row">
					<h4>
						<div class="col-md-12">
							<div class="col-md-3">Harga Inap Weekday</div>
							<div class="col-md-9">: {{ H::rupiah($facility->price)}}</div>
						</div>
					</h4>
				</div>
				{{--  @if($facility->weekend_price !=null)  --}}
				<div class="row">
					<h4>
						<div class="col-md-12">
							<div class="col-md-3">Harga Inap Weekend</div>
							<div class="col-md-9">: {{ H::rupiah($facility->weekend_price)}}</div>
						</div>
					</h4>
				</div>
				<div class="row">
					<h4>
						<div class="col-md-12">
							<div class="col-md-3">Harga Weekday Tidak Menginap </div>
							<div class="col-md-9">: {{ H::rupiah($facility->no_stay_weekday_price)}}</div>
						</div>
					</h4>
				</div>
				{{--  @if($facility->weekend_price !=null)  --}}
				<div class="row">
					<h4>
						<div class="col-md-12">
							<div class="col-md-3">Harga Weekend Tidak Menginap </div>
							<div class="col-md-9">: {{ H::rupiah($facility->no_stay_weekend_price)}}</div>
						</div>
					</h4>
				</div>
				{{--  @endif  --}}
				<div class="row">
					<h4>
						<div class="col-md-12">
							<div class="col-md-3">Satuan</div>
							<div class="col-md-9">: {{$facility->unit_name}}</div>
						</div>
					</h4>
				</div>
				<div class="row">
					<h4>
						<div class="col-md-12">
							<div class="col-md-3">Deskripsi</div>
							<div class="col-md-9">: {!!$facility->description!!}</div>
						</div>
					</h4>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-3 col-md-offset-9">
						<a href="{{ route('facilities.edit',$facility->id)}}" class="btn btn-warning btn-block">Ubah Fasilitas &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="col-md-6">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Detail Fasilitas
					</div>
				</div>
			</div>	
			<div class="box-body" style="height: 400px;overflow-y:visible" >
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>Jadwal</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
						@foreach($detail_facility as $a)
						<tr> 
							<td><i class="fa fa-circle text-{{ $a->availLabel}}"></i> {{ $facility->name.' - '.$a->name }}</td>
							
							<td>
								<a href="{{route('detail_facilities.custom_schedules.index',['detail_facilities'=>$a->id])}}">
									<button class="btn btn-primary">
										Jadwal
									</button>
								</a>
							</td>
							<td>
								<a href="{{route('facilities.detail_facilities.edit',['facility'=>$facility->id,'detail_facility'=>$a->id])}}">
									<button class="btn btn-warning">
										Edit
									</button>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{ $detail_facility->links() }}
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-5 pull-right">
						<a href="{{ route('facilities.detail_facilities.create',$facility->id)}}" class="btn btn-success btn-block">Tambah Detail Fasilitas &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="col-md-6">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Attribute
					</div>
				</div>
			</div>
			
			<!-- /.box-header -->
			<div class="box-body" style="height: 400px;overflow-y:visible">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Attribute</th>
							<th>Value</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
						@foreach($facilityDetail as $a)
						<tr> 
							<td>{{ $a->facility_attribute->name }}</td>
							<td>{{ $a->value}}</td>
							<td>
								<a href="{{route('facilities.facility_details.edit',['facility'=>$facility->id,'facility_detail'=>$a->id])}}">
									<button class="btn btn-warning">
										Edit
									</button>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{ $facilityDetail->links() }}
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-5 pull-right">
						<a href="{{ route('facilities.facility_details.create',$facility->id)}}" class="btn btn-success btn-block">Tambah Attribute &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="col-md-12">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Gallery
					</div>
					
					<div class="col-md-3 pull-right">
						<a href="{{ route('facilities.galleries.create',$facility->id)}}" class="btn btn-success btn-block">Tambah Detail Gallery &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>
			
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-7">
						<div class="carousel slide article-slide" id="article-photo-carousel">
							<!-- Wrapper for slides -->
							<div class="carousel-inner cont-slider">
								<div class="item" style="display:none">
								</div>
								@foreach($facility->gallery as $g)<!-- 
									<a href="{{route('galleries.edit',$g->id)}}">
										<button>
											Edit
										</button>
									</a> -->
									@if($loop->first)
									<div class="item active" >
										<img alt="" title="" class="img-responsive" src="{{asset('image/'.$g->image_path)}}">
									</div>
									@else
									<div class="item" >
										<img alt="" title="" class="img-responsive" src="{{asset('image/'.$g->image_path)}}">
									</div>
									@endif
									@endforeach
								</div>
								<!-- Indicators -->
								<ol class="carousel-indicators" >
									<section class="regular slider">
										@foreach($facility->gallery as $key => $g)
										<div data-slide-to={{$key+1}} data-target="#article-photo-carousel">
											<img alt="" class="img-fluid" src="{{asset('image/'.$g->image_path)}}">
										</div>
										@endforeach
									</section>
								</ol>
							</div>
						</div>
						<div class="col-md-5" style="height: 400px;">
							<table id="example1" class="table table-bordered table-striped " >
								<thead>
									<tr>
										<th>Image</th>
										<th>Judul</th>
										<th>Deskripsi</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($gallery as $a)
									<tr> 
										<td><img src="{{asset('image/'.$a->image_path)}}" width="60px" height="40px"></td>
										
										<td>{{ $a->title }}</td>
										<td>{{ $a->description }}</td>
										<td>
											<a href="
											{{route('facilities.galleries.edit',['facility'=>$a->galleriable_id,'gallery'=>$a->id])}}">
											<button class="btn btn-warning">
												Edit
											</button>
										</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						
						{{ $gallery->links() }}
					</div>
				</div>
			</div>
		</div>

	</section>
	<section class="col-md-12">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Custom Harga
					</div>
					
					<div class="col-md-3 pull-right">
						<a href="{{ route('facilities.custom_prices.create',['facility'=>$facility->id,'type'=>'App\Facility'])}}" class="btn btn-success btn-block">Tambah Harga &nbsp;</a>
					</div>
				</div>
			</div>
			
			<!-- /.box-header -->
			<div class="box-body" style="height: 65vh;overflow-y:visible">
				<div class="row">
					<div class="col-md-12" style="height: 400px;">
						<table id="example1" class="table table-bordered table-striped " >
							<thead>
								<tr>
									<th>Rate</th>
									<th>Tanggal Mulai</th>
									<th>Tanggal Akhir</th>
									<th>Edit</th>
								</tr>
							</thead>
							<tbody>
								@foreach($custom_price as $a)
								<tr> 
									<td>{{  $a->price }} %</td>
									<td>{{ $a->carbonstart }}</td>
									<td>{{ $a->carbonend }}</td>
									<td>
										<a href="
										{{route('facilities.custom_prices.edit',['facility'=>$facility->id,'custom_price'=>$a->id])}}">
										<button class="btn btn-warning">
											Edit
										</button>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					
					{{ $custom_price->links() }}
				</div>
			</div>
		</div>
	</div>
</section>
<section class="col-md-12">
	<div class="box">
		<div class="box-header">
			<div class="row">
				<div class="col-md-3">
					Harga Khusus
				</div>
				<div class="col-md-3 pull-right">
					<a href="{{ route('special_prices.create',['id'=>$facility->id, 'u'=>'App\Facility'])}}" class="btn btn-success btn-block">Tambah Harga &nbsp;</a>
				</div>
			</div>
		</div>
		
		<!-- /.box-header -->
		<div class="box-body" style="height: 65vh;overflow-y:visible">
			<div class="row">
				<div class="col-md-12" style="height: 400px;">
					<table id="example1" class="table table-bordered table-striped " >
						<thead>
							<tr>
								<th rowspan="2" width="5%">No</th>
								<th rowspan="2" width="15%">Nama</th>
								<th rowspan="2" width="10%">Tipe Customer</th>
								<th colspan="2" width="20%">Harga Menginap</th>
								<th colspan="2" width="20%">Harga Tidak Menginap</th>
								<th rowspan="2" width="10%" colspan="2">Action</th>
							</tr>
							<tr>
								<th width="15%">Weekend Price</th>
								<th width="15%">Weekday Price</th>
								<th width="15%">Weekend Price</th>
								<th width="15%">Weekday Price</th>
							</tr>
						</thead>
						<tbody>
							@foreach( $special_prices as $key => $special_price )
							<tr height="10">
								<td>{{ $key+1 }}</td>
								<td>{{ $special_price->nameLabel }}</td>
								<td>{{ $special_price->customer_type->name }}</td>
								<td>{{ H::rupiah($special_price->weekend_price) }}</td>
								<td>{{ H::rupiah($special_price->price) }}</td>
								<td>{{ H::rupiah($special_price->no_stay_weekend_price) }}</td>
								<td>{{ H::rupiah($special_price->no_stay_weekday_price) }}</td>
								<td>
									<a href="
									{{route('special_prices.edit',['special_price'=>$special_price->id])}}">
									<button class="btn btn-warning">
										Edit
									</button>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				
				{{ $special_prices->links() }}
			</div>
		</div>
	</div>
</div>
</section>
</div>
@stop
@push('scripts')

<script src="{{asset('slick/slick.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	$(document).on('ready', function() {
		$(".regular").slick({
			dots: false,
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 4
		});
	});
</script>
@endpush