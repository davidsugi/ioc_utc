@extends('layouts.app')

@section('title')
{{ $unit->exists ? 'Ubah' : 'Tambah' }} Unit
@endsection
@if($unit->exists)
@section('actionbtn')
<a data-href="{{ route('units.destroy', $unit->id) }}" class="btn btn-danger destroy">Hapus Unit</a>
@endsection
@endif
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("units.index")}}'><i class='fa fa-cube'></i> Unit</a></li>
<li><a href='#' class='active'>{{ $unit->exists ? 'Ubah' : 'Tambah'}} Unit</a></li>
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($unit->exists)
        {!! Form::model($unit, ['route' => ['units.update', $unit->id], 'method'=>'PATCH','role' => 'form']) !!}
        @else
        {!! Form::model($unit, ['route' => ['units.store'], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Nama</label>
              {!! Form::text('name', $unit->name, ['class'=> 'form-control', 'required' => 'required']) !!}
            </div>
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')

