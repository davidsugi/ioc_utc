@extends('layouts.app')

@push('styles')
<link href="{{ asset('js/plugins/select2/select2.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/summernote.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('title')
{{ $order->exists ? 'Ubah' : 'Tambah' }} Order
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("orders.index")}}'><i class='fa fa-cart-plus'></i> Order</a></li>
<li><a href='#' class='active'>{{ $order->exists ? 'Ubah Order '.$order->order_code : 'Tambah Order'}} </a></li>
@endsection
@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-danger">
      <div class="box-body">
        @if($order->exists)
        {!! Form::model($order, ['route' => ['orders.update', $order->id], 'method'=>'PATCH', 'files' => true]) !!}
        @else
        {!! Form::model($order, ['route' => ['orders.store'], 'class' => 'col s12', 'files' => true]) !!}
        @endif
        
        <div class="form-group col-md-6">
          {{ Form::label('customer_id','Customer') }}
          {{ Form::select('customer_id',$customers, $order->customer_id, ['class' => 'form-control select2 cust', 'required']) }}
          {{-- <a class="btn btn-primary" data-toggle="modal" href='#mymod'>add new</a> --}}
          @if ($errors->has('customer_id'))
          <div class="help-block text-red">
            {{ $errors->first('customer_id') }}
          </div>
          @endif
        </div>
        <div class="form-group col-md-3">
          {{ Form::label('date','Tanggal Order') }}
          {{ Form::date('date', $order->date!=null ? $order->date : date("Y-m-d") , ['class' => 'form-control', 'required']) }}
          @if ($errors->has('date'))
          <div class="help-block text-red">
            {{ $errors->first('date') }}
          </div>
          @endif
        </div>
        <div class="form-group col-md-3">
          {{ Form::label('expiry','Kadaluarsa') }}
          <table>
            <tr>
              <td> {{ Form::number('expiry', $order->expiry ? $order->expiry : H::globs("expiry") , ['class' => 'form-control', 'required']) }}</td>
              <td> hari</td>
            </tr>
          </table>
          @if ($errors->has('expiry'))
          <div class="help-block text-red">
            {{ $errors->first('expiry') }}
          </div>
          @endif
        </div>
        <div class="form-group col-md-6">
          {{ Form::label('guest_name','Guest Name') }}
          {{ Form::text('guest_name', $order->guest_name, ['class' => 'form-control guest-name', 'required']) }}
          @if ($errors->has('guest_name'))
          <div class="help-block text-red">
            {{ $errors->first('guest_name') }}
          </div>
          @endif
        </div>
        
        <div class="form-group col-md-6">
          {{ Form::label('guest_phone','Guest Phone Number') }}
          {{ Form::text('guest_phone', $order->guest_phone, ['class' => 'form-control guest-phone', 'required']) }}
          @if ($errors->has('guest_phone'))
          <div class="help-block text-red">
            {{ $errors->first('guest_phone') }}
          </div>
          @endif
        </div>
        
        <div class="form-group col-md-4">
          {{ Form::label('adult','Jumlah Dewasa') }}
          {{ Form::number('adult', $order->adult ? $order->adult : 1, ['class' => 'form-control','min'=>1]) }}
          @if ($errors->has('adult'))
          <div class="help-block text-red">
            {{ $errors->first('adult') }}
          </div>
          @endif
        </div>
        <div class="form-group col-md-4">
          {{ Form::label('children','Jumlah Anak Anak') }}
          {{ Form::number('children', $order->children ? $order->children : 0, ['class' => 'form-control', 'min'=>0]) }}
          @if ($errors->has('children'))
          <div class="help-block text-red">
            {{ $errors->first('children') }}
          </div>
          @endif
        </div>
        
        <div class="form-group col-md-4" style="margin-top:30px;">
          {{ Form::label('menginap','Menginap?') }}
          {{ Form::checkbox('menginap', 1,$order->menginap==1 ? true : false , ['class' => 'field',"id"=>'inap']) }}
          @if ($errors->has('menginap'))
          <div class="help-block text-red">
            {{ $errors->first('menginap') }}
          </div>
          @endif
        </div>
        
        
        <div class="row">
          <div class="col-md-12">
            <table class="table pelanggaran-table">
              <thead>
                <tr>
                  <th style="width: 10%;">Fasilitas</th>
                  <th style="width: 7%;">Check in</th>
                  <th style="width: 10%;">Orang</th>
                  <th colspan="2" style="width: 10%;" align="center">Durasi</th>
                  <th colspan="2" style="width: 18%;" align="center">Diskon</th>
                  <th style="width: 20%;">Total</th>                    
                  <th style="width: 5%;"></th>
                </tr>
              </thead>
              <tbody>
                <tr class="pelanggaran" id="pel-0" style="display:none">
                  {!! Form::hidden('ids[]',null, ['class'=> 'form-control hiddens']) !!}                    
                  <td>{!! Form::select('facil[]',array(), null, [ 'class'=> 'form-control select2 no-border facils', 'id'=>0]) !!}</td>
                  <td>{{ Form::date('check_in[]', date("Y-m-d") , ['class' => 'form-control dt',"id"=>"dt0"]) }}</td>
                  <td>
                    {{ Form::hidden('per_person[]', '', ['class' => 'form-control perprs',"id"=>"perprs-0", 'min'=>1]) }}
                    {{ Form::number('person[]', '', ['class' => 'form-control prs',"id"=>"prs-0", 'min'=>1]) }}
                  </td>                                        
                  <td>{{ Form::number('duration[]','', ['class' => 'form-control dur',"id"=>"dur-0","min"=>1]) }}</td>
                  <td><div id="d0"> </div></td>
                  <td>{{ Form::number('discon[]',0, ['class' => 'form-control disc',"id"=>"disc-0", 'min'=>0]) }}</td>                  
                  <td>
                    {{ Form::select('fdisc_type[]', array(0=> '%',1 => 'Rp.')  , 0, ['class' => 'form-control  fdisc_type',"id"=>"fdisc_type-0",'required']) }}                    
                  </td>                   
                  <td><div id="price0"></div>
                    {{ Form::hidden('subt[]','', ['class' => 'form-control subt',"id"=>"subt-0",'min'=>0]) }}                  
                  </td>
                  
                  <td>
                    <a href="" class="btn btn-danger delete_line" style="display:none"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                  </td>
                </tr>
                <tr class="note" id="not-0" style="display:none">
                  <td colspan="8">{{ Form::text('note[]','', ['placeholder'=>'isi catataan untuk detail ini','class' => 'form-control notes',"id"=>"note-0"]) }}</td>                    
                </tr>
                {{--  {{ json_encode(old('duration')) }}  --}}
                @php($arrold=old('facil'))
                
                @if(isset($arrold))
                @foreach( $arrold as $key => $fac)
                {{--  {{ json_encode($facs)}}  --}}
                @php($key++)                               
                <tr class="pelanggaran" id="pel-{{$key}}">
                  {{--  {{ $fac }}  --}}
                  {{--  {{ json_encode($facs) }}  --}}
                  {{--  @foreach($facs as $k => $name)  --}}
                  
                  {{--  @endforeach   --}}
                  
                  {{--  {!! Form::hidden('ids[]', $ids[$key], ['class'=> 'form-control hiddens']) !!}  --}}
                  <td>
                    <select required="required" class="form-control select2 facils" id="{{ $key }}" name="facil[]">
                      @foreach($facs as $k => $v)
                      <option value="{{ $k }}" {{ $k == $arrold [$key-1] ? 'selected' : null }}>{{ $v }}</option>
                      @endforeach
                    </select>
                    {{--  {!! Form::select('facil[]',$facs, $fac , ['required' => 'required', 'class'=> 'form-control ', 'id'=>$key]) !!}  --}}
                    
                    @if ($errors->has($key))
                    <div class="help-block text-red">
                      <font size="2">{{ $errors->first($key) }}</font>
                    </div>
                    @endif
                  </td>
                  {{--  <td>{{ Form::date('check_in[]', old('check_in')[$key], ['class' => 'form-control dt',"id"=>"dt".($key), 'required']) }}  --}}
                    <td><input type="date" id="dt{{ $key }}" name="check_in[]" class="form-control dt" required value="{{old('check_in')[$key]}}"></input>                     
                      @if ($errors->has("dt".$key))
                      <div class="help-block text-red">
                        <font size="2">{{ $errors->first("dt".$key) }}</font>
                      </div>
                      @endif
                    </td>
                    {{ Form::hidden('per_person[]', old('per_person')[$key], ['class' => 'form-control perprs',"id"=>"perprs-".$key]) }}
                    
                    @if(old('per_person')[$key]==0)
                    {{--  <td>{{ Form::number('person[]', old('person')[$key], ['class' => 'form-control prs',"id"=>"prs-".($key), 'min'=>1, 'style'=>'display:none']) }}</td>                      --}}
                    <td><input type="number" id="prs-{{ $key }}" name="person[]" class="form-control prs" min="1"  value="{{old('person')[$key]}}" style='display:none '></input></td>                                                 
                    @else
                    {{--  <td>{{ Form::number('person[]', old('person')[$key], ['class' => 'form-control prs',"id"=>"prs-".($key), 'min'=>1]) }}</td>                                              --}}
                    <td><input type="number" id="prs-{{ $key }}" name="person[]" class="form-control prs" min="1"  value="{{old('person')[$key]}}"></input></td>                                                                         
                    @endif
                    {{--  <td>{{ Form::number('duration[]',old('duration')[$key], ["id"=>"dur-".($key),'class' => 'form-control dur', 'required','min'=>1]) }}</td>  --}}
                    <td><input type="number" id="dur-{{ $key }}" class="form-control dur" name="duration[]" min="1" required value="{{old('duration')[$key]}}"></input></td>
                    <td><label id="d{{ $key }}"></label></td>
                    <td>{{ Form::number('discon[]',old('discon')[$key], ['class' => 'form-control disc',"id"=>"disc-".($key),'min'=>0]) }} </td>
                    <td>
                      {{ Form::select('fdisc_type[]', array(0=> '%',1 => 'Rp.')  ,  old('fdisc_type')[$key] , ['class' => 'form-control  fdisc_type',"id"=>"fdisc_type-".($key),'required']) }}                    
                    </td>
                    {{--  {{ var_dump(old('subt')[$key]) }}  --}}
                    <td><div id="price{{ $key}}">{{ H::rupiah((float) old('subt')[$key]) }} </div> 
                      {{ Form::hidden('subt[]', old('subt')[$key] , ['class' => 'form-control subt',"id"=>"subt-".($key),'min'=>0]) }}                  
                    </td>
                    
                    <td>
                      <a href="" class="btn btn-danger delete_line" style="display: {{ $key==0 ? 'none': null }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>                        
                    </td>
                  </tr>
                  <tr class="note" id="not-{{$key}}">
                    <td colspan="8">{{ Form::text('note[]', old('note')[$key] , ['placeholder'=>'isi catataan untuk detail ini','class' => 'form-control notes',"id"=>"note-".$key,"min"=>1]) }}</td>                    
                  </tr>
                  @endforeach
                  
                  @elseif(isset($facd)&& sizeof($facd)>0)
                  {{--  <tr class="pelanggaran" style="display:none">
                    {!! Form::hidden('ids[]',null, ['class'=> 'form-control hiddens']) !!}                    
                    <td>{!! Form::select('facil[]',array(), null, [ 'class'=> 'form-control select2 no-border facils', 'id'=>0]) !!}</td>
                    <td>{{ Form::date('check_in[]', date("Y-m-d") , ['class' => 'form-control dt',"id"=>"dt0"]) }}</td>
                    <td>{{ Form::number('person[]', '', ['class' => 'form-control prs',"id"=>"prs-0", 'min'=>1]) }}</td>                                        
                    <td>{{ Form::number('duration[]','', ['class' => 'form-control dur',"id"=>"dur-0","min"=>1]) }}</td>
                    <td><div id="d0"> </div></td>
                    <td>{{ Form::number('discon[]',0, ['class' => 'form-control disc',"id"=>"disc-0", 'min'=>0]) }}</td><td>%</td>                   
                    <td><div id="price0"></div>
                      {{ Form::hidden('subt[]','', ['class' => 'form-control subt',"id"=>"subt-0",'min'=>0]) }}                  
                    </td>
                    
                    <td>
                      <a href="" class="btn btn-danger delete_line" style="display:none"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>  --}}
                  @foreach($facd as $key=>$fac )
                  <tr class="pelanggaran" id="pel-{{$key+1}}">
                    {!! Form::hidden('ids[]', $ids[$key], ['class'=> 'form-control hiddens']) !!}
                    {{--  <td>{!! Form::select('facil[]', $facs, $fac->idHelperLabel, ['required' => 'required', 'class'=> 'form-control select2 facils', 'id'=>$key+1]) !!}</td>  --}}
                    <td>
                      <select required="required" class="form-control select2 facils" id="{{ $key+1 }}" name="facil[]">
                        @foreach($facs as $k => $v)
                        <option value="{{ $k }}" {{ $k == $fac->idHelperLabel ? 'selected' : null }}>{{ $v }}</option>
                        @endforeach
                      </select>
                    </td>
                    <td>{{ Form::date('check_in[]', $fac->checkinLabel, ['class' => 'form-control dt',"id"=>"dt".($key+1), 'required']) }}</td>
                    
                    <td>
                      {{ Form::hidden('per_person[]', $fac->per_person_label, ['class' => 'form-control perprs',"id"=>"perprs-".($key+1) ]) }}
                      
                      @if($fac->per_person_label==1)
                      {{ Form::number('person[]', $fac->amount2, ['class' => 'form-control prs',"id"=>"prs-".($key+1), 'min'=>1]) }}
                      @else
                      
                      {{ Form::number('person[]', $fac->amount2, ['class' => 'form-control prs',"id"=>"prs-".($key+1), 'min'=>1, 'style'=>'display:none']) }}
                      @endif
                    </td>                    
                    <td>{{ Form::number('duration[]',$fac->durationLabel, ["id"=>"dur-".($key+1),'class' => 'form-control dur', 'required','min'=>1]) }}</td>
                    <td><label id="d{{ $key+1 }}"></label></td>
                    <td>{{ Form::number('discon[]',$fac->disc, ['class' => 'form-control disc',"id"=>"disc-".($key+1),'min'=>0]) }} </td>
                    <td>
                      {{ Form::select('fdisc_type[]', array(0=> '%',1 => 'Rp.')  , $fac->disc_type, ['class' => 'form-control  fdisc_type',"id"=>"fdisc_type-".($key+1),'required']) }}                    
                    </td>
                    <td><div id="price{{ $key+1 }}">{{ H::rupiah($fac->subtotal)}}</div> 
                      {{ Form::hidden('subt[]',$fac->subtotal, ['class' => 'form-control subt',"id"=>"subt-".($key+1),'min'=>0]) }}                  
                    </td>
                    <td>
                      <a href="" class="btn btn-danger delete_line"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  <tr class="note" id="not-{{$key+1}}">
                    <td colspan="8">{{ Form::text('note[]', $fac->note, ['placeholder'=>'isi catataan untuk detail ini','class' => 'form-control notes',"id"=>"note-".$key,"min"=>1]) }}</td>                    
                  </tr>
                  @endforeach
                  @else
                  @for($i=1; $i<2; $i++)
                  <tr class="pelanggaran" id="pel-{{$i}}">
                    <td>{!! Form::select('facil[]',array(), null, ['required' => 'required', 'class'=> 'form-control select2 no-border facils', 'id'=>$i]) !!}</td>
                    <td>{{ Form::date('check_in[]', $order->date!=null ? $order->date : date("Y-m-d") , ['class' => 'form-control dt',"id"=>"dt".$i, 'required']) }}</td>
                    {{ Form::hidden('per_person[]', '', ['class' => 'form-control perprs',"id"=>"perprs-".$i]) }}                    
                    <td>{{ Form::number('person[]', '', ['class' => 'form-control prs',"id"=>"prs-".$i, 'min'=>1]) }}</td>                                        
                    <td>{{ Form::number('duration[]','', ['class' => 'form-control dur',"id"=>"dur-".$i,'required',"min"=>1]) }}</td>
                    <td><div id="d{{ $i }}"> </div></td>
                    <td>{{ Form::number('discon[]',0, ['class' => 'form-control disc',"id"=>"disc-".$i, 'min'=>0]) }}</td>
                    <td>
                      {{ Form::select('fdisc_type[]', array(0=> '%',1 => 'Rp.')  , 0, ['class' => 'form-control fdisc_type',"id"=>"fdisc_type-".$i,'required']) }}                                        
                    </td>                   
                    <td><div id="price{{ $i }}"></div>
                      {{ Form::hidden('subt[]','', ['class' => 'form-control subt',"id"=>"subt-".$i,'min'=>0]) }}                  
                    </td>
                    <td>
                      <a href="" class="btn btn-danger delete_line" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  <tr class="note" id="not-{{$i}}">
                    <td colspan="8">{{ Form::text('note[]','', ['placeholder'=>'isi catataan untuk detail ini','class' => 'form-control notes',"id"=>"note-".$i,"min"=>1]) }}</td>                    
                  </tr>
                  @endfor
                  @endif
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="8"></td>
                    <td><a href="" class="btn btn-success add_line"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></td>
                  </tr>
                </tfoot>
              </table>
            </div>
            {{-- addons --}}
            <div class="col-md-12">
              <table class="table add-table">
                <thead>
                  <tr>
                    <th style="width: 25%;">Add On</th>
                    <th style="width: 15%;">Tanggal</th>
                    <th style="width: 10%;">Jumlah</th>
                    <th style="width: 15%;">Harga Satuan</th>
                    <th colspan="2"  style="width: 20%;" align="center">Diskon</th>
                    <th style="width: 30%;">Total</th>
                    <th style="width: 10%;"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="add"  id="addid-0" style="display:none">
                    <td>{!! Form::select('addons[]',array(), null, [ 'class'=> 'form-control select2a no-border addon', 'id'=>"a-0"]) !!}</td>
                    <td> {{ Form::datetimeLocal('adate[]', Carbon\carbon::now('+07:00','Asia/Jakarta')->format('Y-m-d\TH:i'), ['class' => 'form-control adate', "id"=>"adate-0", 'min'=>0 ]) }}</td>
                    <td> {{ Form::number('amount[]','', ['class' => 'form-control amm', "id"=>"amm-0", 'min'=>0 ]) }}</td>
                    <td> {{ Form::number('price[]','', ['class' => 'form-control add_price', "id"=>"add_price-0", 'min'=>0 ]) }}</td>
                    <td>{{ Form::number('disca[]','', ['class' => 'form-control disca',"id"=>"disca-0",'min'=>0]) }}</td>                                     
                    <td>
                      {{ Form::select('adisc_type[]', array(0=> '%',1 => 'Rp.')  ,0, ['class' => 'form-control  adisc_type',"id"=>"adisc_type-0",'required']) }}                    
                    </td>
                    <td><div id="prica0"></div>
                      {{ Form::hidden('subta[]','', ['class' => 'form-control subta',"id"=>"subta-0",'min'=>0]) }}                                        
                    </td>
                    
                    <td>
                      <a href="" class="btn btn-danger delete_add" style="display:none"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  <tr class="anote" id="anot-0" style="display:none">
                    <td colspan="8">{{ Form::text('anote[]','', ['placeholder'=>'isi catataan untuk detail ini','class' => 'form-control anotes',"id"=>"anote-0"]) }}</td>                    
                  </tr>
                  {{ old('addon') }}
                  @php($addold=old('addons'))
                  @if(isset($addold))
                  @foreach( $addold as $key => $fac)
                  @php($key++)
                  <tr class="add"  id="addid-{{ $key }}" > 
                    <td>  <select required="required" class="form-control select2 select2a addon" id="a-{{ $key }}" name="addons[]">
                      @foreach($adds as $k => $v)
                      <option value="{{ $k }}" {{ $k == $fac ? 'selected' : null }}>{{ $v }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has("a-".$key))
                    <div class="help-block text-red">
                      <font size="2">{{ $errors->first("a-".$key) }}</font>
                    </div>
                    @endif
                  </td>
                  {{--  <td>{!! Form::select('addons[]', $adds, $fac, ['required' => 'required', 'class'=> 'form-control select2a addon', 'id'=>"a-".$key]) !!}</td>  --}}
                  {{--  <td> {{ Form::number('amount[]', old('amount')[$key], ['class' => 'form-control amm', "id"=>"amm-".$key,'min'=>0, 'required']) }}</td>  --}}
                  {{--  <td>{{ Form::number('disca[]',old('disca')[$key] ? old('disca')[$key] :0 , ['class' => 'form-control disca', "id"=>"disca-".$key,'min'=>0]) }}</td><td>%</td>                                         --}}
                  <td><input type="datetime-local" id="adate-{{ $key }}" name="adate[]" class="form-control adate" required value="{{old('adate')[$key]}}"></input></td>                         
                  <td><input type="number" id="amm-{{ $key }}" name="amount[]" class="form-control amm" required value="{{old('amount')[$key]}}"></input></td>                         
                  <td><input type="number" id="add_price-{{ $key }}" name="price[]" class="form-control add_price" required value="{{old('price')[$key]}}"></input></td>                         
                  <td><input type="number" id="disca-{{ $key }}" name="disca[]" class="form-control disca" min="0" required value="{{old('disca')[$key]}}"></input></td>
                  <td>
                    {{ Form::select('adisc_type[]', array(0=> '%',1 => 'Rp.')  , old('adisc_type')[$key] , ['class' => 'form-control  adisc_type',"id"=>"adisc_type-".$key,'required']) }}                    
                  </td>
                  <td><div id="prica{{ $key }}"> {{ H::rupiah((float) old('subta')[$key]) }}</div>
                    {{ Form::hidden('subta[]', old('subta')[$key] , ['class' => 'form-control subta',"id"=>"subta-".$key,'min'=>0]) }}                  
                  </td> 
                  <td>
                    <a href="" class="btn btn-danger delete_line" style="display: {{ $key==0 ? 'none': null }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>                        
                  </td>
                </tr>
                <tr class="anote" id="anot-{{ $key }}">
                  <td colspan="8">{{ Form::text('anote[]',old('anote')[$key] , ['placeholder'=>'isi catataan untuk detail ini','class' => 'form-control anotes',"id"=>"anote-".$key]) }}</td>                    
                </tr>
                @endforeach
                @elseif(isset($addd)&& sizeof($addd)>0)
                @foreach($addd as $key=>$fac )
                <tr class="add" id="addid-{{ $key }}">
                  {!! Form::hidden('ida[]', $ida[$key], ['class'=> 'form-control hiddena']) !!}
                  @php($key++)                    
                  <td>{!! Form::select('addons[]', $adds, $fac->item_id, ['required' => 'required', 'class'=> 'form-control select2a addon', 'id'=>"a-".$key]) !!}</td>
                  <td> {{ Form::datetimeLocal('adate[]', $fac->folio()->orderBy('id','DESC')->first()->check_in, ['class' => 'form-control adate', "id"=>"adate-".$key,'min'=>0, 'required']) }}</td>
                  <td> {{ Form::number('amount[]', intval($fac->amount), ['class' => 'form-control amm', "id"=>"amm-".$key,'min'=>0, 'required']) }}</td>
                  <td> {{ Form::number('price[]',intval($fac->price), ['class' => 'form-control add_price', "id"=>"add_price-".$key, 'min'=>0 , 'required']) }}</td>
                  <td>{{ Form::number('disca[]',$fac->disc, ['class' => 'form-control disca', "id"=>"disca-".$key,'min'=>0]) }}</td>
                  <td>
                    {{ Form::select('adisc_type[]', array(0=> '%',1 => 'Rp.')  ,$fac->disc_type, ['class' => 'form-control  adisc_type',"id"=>"adisc_type-".$key,'required']) }}                    
                  </td>                                   
                  <td><div id="prica{{ $key }}"> {{ H::rupiah($fac->totalLabel)}}</div>
                    {{ Form::hidden('subta[]',$fac->totalLabel, ['class' => 'form-control subta',"id"=>"subta-".$key,'min'=>0]) }}                  
                  </td>
                  
                  <td>
                    <a href="" class="btn btn-danger delete_add"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                  </td>
                </tr>
                <tr class="anote" id="anot-{{ $key }}">
                  <td colspan="8">{{ Form::text('anote[]',$fac->folio()->orderBy('id','DESC')->first()->description, ['placeholder'=>'isi catataan untuk detail ini','class' => 'form-control anotes',"id"=>"anote-".$key]) }}</td>                    
                </tr>
                @endforeach
                
                @endif
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="7"></td>
                  <td><a href="" class="btn btn-success add_lineadds"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></td>
                </tr>
                <tr>
                  <td colspan="2" align="center"><b>Total</b></td>
                  <td colspan="3" align="center"></td>
                  {{--  {{ count($order) }}  --}}
                  <td colspan="2"><div class="total">{{ isset($order->id) >0 ? H::rupiah($order->totalLabel) : ''}}</div></td>
                </tr>
                <tr>
                  <td colspan="2" align="center"><b>Diskon</b></td>
                  {{--  {{ count($order) }}  --}}
                  <td>{{ Form::number('globaldisc', $order->globaldisc==null ? 0 : $order->globaldisc, ['class' => 'form-control globaldisc']) }}</td>
                  <td colspan="2">
                    {{ Form::select('globaldisc_type', array(1=> '%',2 => 'Rp.')  , $order->globaldisc_type, ['class' => 'form-control select2 select2class',"id"=>"globdistype",'required']) }}                    
                  </td>
                  <td colspan="2"><div id="globs"></div></td>
                </tr>
                {{-- <tr>
                  <td colspan="2" align="center"><b>Pajak</b></td>
                  <td colspan="2">{{ Form::number('tax', $order->tax==null ? H::globs("tax") : $order->tax, ['class' => 'form-control tax']) }}</td>
                  <td>%</td>
                  <td colspan="2"><div id="taxs"></div></td>
                </tr> --}}
                
                <tr  id="disconcup" style="display:none;">
                  <td colspan="5" align="center"><b><div id="potlabel" >Potongan Kupon</div>{{ Form::hidden('coupon_id', $order->coupon_id, ['class' => 'form-control',"id"=>"coup_id", 'required']) }}</b></td>
                  {{--  {{ count($order) }}  --}}
                  
                  <td colspan="2"><div id="discup"></div>    
                    {{ Form::hidden('disc', $order->disc, ['class' => 'form-control',"id"=>"dis", 'required']) }}
                    {{ Form::hidden('disc_type', $order->disc_type, ['class' => 'form-control', "id"=> "dis_type", 'required']) }}
                  </td>
                </tr>
                <tr>
                  <td colspan="5" align="center"><b>Total Akhir</b></td>
                  {{--  {{ count($order) }}  --}}
                  <td><div id="totall"></div></td>
                </tr>
              </tfoot>
            </table>
            
            <div class="form-group">
              {{ Form::label('code','Kode Kupon') }}
              <table>
                <tr><th width="90%">
                  {{ Form::text('coden', $order->code, ['class' => 'form-control codes']) }}
                </td><td>
                  <a style="margin-left:20px" href="" class="btn btn-primary check" {{ $order->code=="" ? "disabled='disabled'" : '' }}><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Cek Kode</a>          
                </td>
              </tr>
            </table>
            {{ Form::hidden('code', $order->code, ['class' => 'form-control', "id"=> "code", 'required']) }}
            
            
            @if ($errors->has('code'))
            <div class="help-block text-red">
              {{ $errors->first('code') }}
            </div>
            @endif
          </div>
          
          
          <div class="form-group">
            {{ Form::label('description','Keterangan') }}
            {{ Form::text('description', $order->description, ['class' => 'form-control']) }}
            @if ($errors->has('description'))
            <div class="help-block text-red">
              {{ $errors->first('description') }}
            </div>
            @endif
          </div>
          <div class="box-footer">
            {{ Form::submit('Simpan', ['class' => 'btn btn-success']) }}
          </div>
          {{ Form::close() }}
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    
  </div>
  <!-- /.row -->
  <!-- Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <h4 class="modal-title">Select Time</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-3">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h4 class="box-title">Draggable Events</h4>
                </div>
                <div class="box-body">
                  <!-- the events -->
                  <div id="external-events">
                    
                  </div>
                </div>
                <!-- /.box-body -->
              </div>
              
            </div>
            <!-- /.col -->
            <div class="col-md-9">
              <div class="box box-danger">
                <div class="box-body no-padding">
                  <!-- THE CALENDAR -->
                  <div id="calendar"></div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /. box -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  @endsection
  
  @push('scripts')
  <script type="text/javascript" src="{{ asset('js/plugins/select2/select2.js') }}"></script>
  
  <script src="{{ asset('js/summernote.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $("#inap").on("change",function(e){
        $(".dur").trigger('change');
        $(".amm").trigger('change');
      });
      $(".tax").on("change keyup", function(e){
        total();
      });
      $(".globaldisc").on("change keyup", function(e){
        total();
      });
      $("#globdistype").on("change", function(e){
        total();
      });
      function total(){
        var tots=0;
        $('.pelanggaran-table > tbody  > tr').each(function() {
          tots+= isNaN(parseInt($(this).find(".subt").val())) ? 0: parseInt($(this).find(".subt").val());
        });
        $('.add-table > tbody  > tr').each(function() {
          tots+= isNaN(parseInt($(this).find(".subta").val())) ? 0: parseInt($(this).find(".subta").val());
        });
        var type = $("#globdistype").val();
        var glob= tots * $(".globaldisc").val()/100;        
        if(type==2){
          glob = $(".globaldisc").val();
        }
        var 	number_string = glob.toString(),
        split	= number_string.split(','),
        sisa 	= split[0].length % 3,
        rupiah 	= split[0].substr(0, sisa),
        ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
        
        if (ribuan) {
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        
        $("#globs").html(rupiah ? 'Rp. ' + rupiah : '');
        
        var tax= (tots-glob) * $(".tax").val()/100;
        var 	number_string = tax.toString(),
        split	= number_string.split(','),
        sisa 	= split[0].length % 3,
        rupiah 	= split[0].substr(0, sisa),
        ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
        
        if (ribuan) {
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        
        $("#taxs").html(rupiah ? 'Rp. ' + rupiah : '');
        var 	number_string = tots.toString(),
        split	= number_string.split(','),
        sisa 	= split[0].length % 3,
        rupiah 	= split[0].substr(0, sisa),
        ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
        
        if (ribuan) {
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        
        $(".total").html(rupiah ? 'Rp. ' + rupiah : '')
        var potcup = isNaN(parseInt($("#dis").val())) ? 0 : parseInt($("#dis").val());
        var totall = tots - glob-potcup;
        {{-- console.log("tots"+tots+"glob"+glob+"potcp"+potcup); --}}
        
        var 	number_string = totall.toString();
        split	= number_string.split(',');
        sisa 	= split[0].length % 3;
        rupiah 	= split[0].substr(0, sisa);
        ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
        
        if (ribuan) {
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }
        {{-- console.log(number_string) --}}
        $("#totall").html(rupiah ? 'Rp. ' + rupiah : '')
        
        
      }
      
      var _dat;
      $('.dt, .prs, .dur, .disc, .fdisc_type').bind('keyup change', function (e){
        var disid=parseInt($(this).closest('.pelanggaran').find('select.select2').attr('id'));    
        var chk=0;
        if($("#inap").is(':checked')){
          chk=1;
        }
        $.get("{{ route('api.get.price')}}",{ id:$("#"+disid).val(), t:$("#dt"+disid).val(), d:(isNaN(parseInt($("#dur-"+disid).val())) ? 1 :  parseInt($("#dur-"+disid).val()) ), p:(isNaN(parseInt($("#prs-"+disid).val())) ? 1 :  parseInt($("#prs-"+disid).val()) ),c:(isNaN(parseInt($("#disc-"+disid).val())) ? 0 :  parseInt($("#disc-"+disid).val()) ),dtyp:(isNaN(parseInt($("#fdisc_type-"+disid).val())) ? 1 :  parseInt($("#fdisc_type-"+disid).val()) ), s:chk, cs:(isNaN(parseInt($(".cust").val())) ? 0 :  parseInt($(".cust").val()) ) }, function(data){
          //$.get("{{ route('api.get.price')}}?id="++"&t="++"&d="++"&p="++"&c="+"a,{ name:"Donald", town:"Ducktown" }, function(data){        
            
            {{--  alert("masuk oi")  --}}
            var tot = data;
            $('#subt-'+disid).val(data);
            var 	number_string = tot.toString(),
            split	= number_string.split(','),
            sisa 	= split[0].length % 3,
            rupiah 	= split[0].substr(0, sisa),
            ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
            
            if (ribuan) {
              separator = sisa ? '.' : '';
              rupiah += separator + ribuan.join('.');
            }
            
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            
            $("#price"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
            total();
          });
          
          
        });
        
        
        $('.amm, .disca, .add_price, .adisc_type').bind('keyup change', function (e){
          var disid=parseInt($(this).closest('.add').find('select.select2a').attr('id').split("-")[1]);
          var chk=0;
          if($("#inap").is(':checked')){
            chk=1;
          }
          
          
          //$.get("{{ route('api.get.price')}}?id="++"&t="++"&d="++"&p="++"&c="+"a,{ name:"Donald", town:"Ducktown" }, function(data){        
            var discount=isNaN(parseInt($("#disca-"+disid).val())) ? 0 :  parseInt($("#disca-"+disid).val());
            var ammount=isNaN(parseInt($("#amm-"+disid).val())) ? 0 :  parseInt($("#amm-"+disid).val());
            var per_item =isNaN(parseInt($("#add_price-"+disid).val())) ? 0 :  parseInt($("#add_price-"+disid).val());
            var disc_type=isNaN(parseInt($("#adisc_type-"+disid).val())) ? 0 :  parseInt($("#adisc_type-"+disid).val());
            var tot = per_item * ammount;
     
            if(disc_type==0){
              tot = tot-(tot*discount/100);
            }
            else{
              tot = tot-discount;
            }
            
            $('#subta-'+disid).val(tot);
            
            
            var 	number_string = tot.toString(),
            split	= number_string.split(','),
            sisa 	= split[0].length % 3,
            rupiah 	= split[0].substr(0, sisa),
            ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
            
            if (ribuan) {
              separator = sisa ? '.' : '';
              rupiah += separator + ribuan.join('.');
            }
            
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            
            $("#prica"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
            total();
            
          });
          
          
          $('.codes').on('keyup', function (e){
            if($(this).val()==""){
              $(".check").attr('disabled', 'disabled');  
            }
            else{
              $(".check").removeAttr('disabled');
              
            }
          });
          $('.check').on('click', function (e) {
            e.preventDefault();
            if ($(this).is('[disabled=disabled]'))
            {
              if($('#code').val()!=""){
                
              }
              else{
                alert('Mohon Isi Kode Kupon dahulu');            
              }
              return false;
            }
            var data = $('form').serialize();
            $.get("{{ route('api.get.promo') }}",data).done( function( datas ) {
              $itm = JSON.parse(datas);
              $("#dis").val($itm[2]);
              $("#dis_type").val($itm[1]);
              $("#coup_id").val($itm[3]);
            
              
              if($itm[2]>0){
                $("#code").val($(".codes").val());          
                $(".codes").prop('disabled', true);      
                $(".check").attr('disabled', 'disabled');   
                
                $("#disconcup").show();
                var 	number_string = $itm[2].toString(),
                split	= number_string.split(','),
                sisa 	= split[0].length % 3,
                rupiah 	= split[0].substr(0, sisa),
                ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                
                if (ribuan) {
                  separator = sisa ? '.' : '';
                  rupiah += separator + ribuan.join('.');
                }
                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                
                $("#discup").html(rupiah ? 'Rp. ' + rupiah : '');  
              }
            
              total();
              
            });
            
          });
          $('.facils').select2({
            'width': '100%',
            'val':null,
            placeholder: "Pilih Fasilitas",
            ajax: {
              url: '{{ route('api.get.facility')}}',
              dataType: 'json',
              delay: 250,
              data: function (params) {
                
                var queryParameters = {
                  q: params.term,
                  d: $("#dur-"+$(this).attr("id")).val(),
                  t: $("#dt"+$(this).attr("id")).val(),
                  c: $("#disc-"+$(this).attr("id")).val(),
                  dtyp: $("#fdisc_type-"+$(this).attr("id")).val(),
                  p: $("#prs-"+$(this).attr("id")).val(),
                }
                
                return queryParameters;
              },
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    _dat = data;
                    
                    return {
                      text: item.name,
                      id: item.id,
                      per_person:item.per_person,
                      price:item.price,
                      
                    }
                  })
                };
              },
              cache: true
            }
          });
          $('.facils').trigger('change');
          
          $('.addon').select2({
            'width': '100%',
            placeholder: "Pilih Tambahan",        
            ajax: {
              url: '{{ route('api.get.addon')}}',
              dataType: 'json',
              delay: 250,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.name,
                      id: item.id,
                      price: item.price,
                    }
                  })
                };
              },
              cache: true
            }
          });
          
          $('.addon').on("select2:select", function(e) {
            var disid=parseInt($(this).closest('.add').find('select.select2a').attr('id').split("-")[1]);
            var chk=0;
            if($("#inap").is(':checked')){
              chk=1;
            }
            $.get("{{ route('api.get.addon_price')}}",{ id:$("#a-"+disid).val(), a:(isNaN(parseInt($("#amm-"+disid).val())) ? 1 :  parseInt($("#amm-"+disid).val()) ),d:(isNaN(parseInt($("#disca-"+disid).val())) ? 0 :  parseInt($("#disca-"+disid).val()) ),dtyp:(isNaN(parseInt($("#adisc_type-"+disid).val())) ? 0 :  parseInt($("#adisc_type-"+disid).val()) ), s:chk, cs:(isNaN(parseInt($(".cust").val())) ? 0 :  parseInt($(".cust").val()) ) }, function(data){
              //$.get("{{ route('api.get.price')}}?id="++"&t="++"&d="++"&p="++"&c="+"a,{ name:"Donald", town:"Ducktown" }, function(data){        
                var tot = data;
                var ammount=isNaN(parseInt($("#amm-"+disid).val())) ? 1 :  parseInt($("#amm-"+disid).val());
                $('#add_price-'+disid).val(data/ammount);
                $('#amm-'+disid).val(1);
                
                $('#subta-'+disid).val(data);
                var 	number_string = tot.toString(),
                split	= number_string.split(','),
                sisa 	= split[0].length % 3,
                rupiah 	= split[0].substr(0, sisa),
                ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                
                if (ribuan) {
                  separator = sisa ? '.' : '';
                  rupiah += separator + ribuan.join('.');
                }
                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                
                $("#prica"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
                total();
              });   
            });
            
            
            $('.facils').on("select2:select", function(e) {
              var disid=$(this).attr("id");     
              $("#d"+disid).html("hari");      
              if(e.params!=null){
                $("#perprs-"+disid).val(e.params.data["per_person"]);      
                
                if(e.params.data["per_person"]==0){
                  $("#prs-"+disid).hide();
                  $("#prs-"+disid).val(null);
                }
                else{
                  $("#prs-"+disid).show();                 
                }
                var chk=0;
                if($("#inap").is(':checked')){
                  chk=1;
                }
                $.get("{{ route('api.get.price')}}",{ id:$("#"+disid).val(), t:$("#dt"+disid).val(), d:(isNaN(parseInt($("#dur-"+disid).val())) ? 1 :  parseInt($("#dur-"+disid).val()) ), p:(isNaN(parseInt($("#prs-"+disid).val())) ? 1 :  parseInt($("#prs-"+disid).val()) ),c:(isNaN(parseInt($("#disc-"+disid).val())) ? 0 :  parseInt($("#disc-"+disid).val()) ),dtyp:(isNaN(parseInt($("#fdisc_type-"+disid).val())) ? 1 :  parseInt($("#fdisc_type-"+disid).val()) ), s:chk, cs:(isNaN(parseInt($(".cust").val())) ? 0 :  parseInt($(".cust").val()) ) }, function(data){
                  //$.get("{{ route('api.get.price')}}?id="++"&t="++"&d="++"&p="++"&c="+"a,{ name:"Donald", town:"Ducktown" }, function(data){        
                    var tot = data;
                    
                    $('#prs-'+disid).val(1);
                    $('#dur-'+disid).val(1);
                    $('#subt-'+disid).val(data);
                    var 	number_string = tot.toString(),
                    split	= number_string.split(','),
                    sisa 	= split[0].length % 3,
                    rupiah 	= split[0].substr(0, sisa),
                    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                    
                    if (ribuan) {
                      separator = sisa ? '.' : '';
                      rupiah += separator + ribuan.join('.');
                    }
                    
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    
                    $("#price"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
                    total();
                    
                  });
                }
                
              });
              
              $('.add_line').click(function(e) {
                e.preventDefault();
                var note_tr = $('.pelanggaran-table tbody .note:first').clone();
                
                var pelanggaran_tr = $('.pelanggaran-table tbody tr:first').clone();
                
                var next=parseInt($('.pelanggaran-table tbody tr:last').attr('id').split('-')[1])+1;
                
                pelanggaran_tr.attr("id","pel-"+next)
                note_tr.attr("id","not-"+next)
                pelanggaran_tr.find('.delete_line').show();
                pelanggaran_tr.find('.select2-container').remove();
                pelanggaran_tr.find('select.select2').val("");
                pelanggaran_tr.find('select.select2').attr("id",next);
                pelanggaran_tr.find('select.fdisc_type').val(0);
                pelanggaran_tr.find('select.fdisc_type').attr("id","fdisc_type-"+next);
                pelanggaran_tr.find('#d0').attr("id","d"+next);              
                pelanggaran_tr.find('#dt0').attr("id","dt"+next);              
                pelanggaran_tr.find('#prs-0').attr("id","prs-"+next);              
                pelanggaran_tr.find('#subt-0').attr("id","subt-"+next);              
                pelanggaran_tr.find('#dur-0').attr("id","dur-"+next);              
                pelanggaran_tr.find('#disc-0').attr("id","disc-"+next);              
                pelanggaran_tr.find('#price0').attr("id","price"+next);           
                pelanggaran_tr.find('.hiddens').val("");
                pelanggaran_tr.find('div').html("");                            
                pelanggaran_tr.find('.disc').val("0");
                pelanggaran_tr.find('#prs-'+next).show();                            
                pelanggaran_tr.show();                            
                pelanggaran_tr.find('#subt-'+next).val("");
                pelanggaran_tr.find('#dur-'+next).val("");
                pelanggaran_tr.find('#prs-'+next).val("");
                
                pelanggaran_tr.find('select.select2').select2({
                  'width': '100%',
                  placeholder: "Pilih Fasilitas",
                  ajax: {
                    url: '{{ route('api.get.facility')}}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                      return {
                        results:  $.map(data, function (item) {
                          _dat = data;
                          return {
                            text: item.name,
                            id: item.id,
                            per_person:item.per_person,
                            price:item.price,
                          }
                        })
                      };
                    },
                    cache: true
                  }
                });
                pelanggaran_tr.find('.violation_point').val('');
                $('.pelanggaran-table tbody').append(pelanggaran_tr);
                note_tr.show();              
                
                $('.pelanggaran-table tbody').append(note_tr);  
                
                $('.facils').on("select2:selecting", function(e) {
                });
                
                
                $('.facils').on("select2:select", function(e) {
                  var disid=$(this).attr("id");     
                  $("#d"+disid).html("hari");     
                  $("#perprs-"+disid).val(e.params.data["per_person"]);      
                  
                  if(e.params.data["per_person"]==0){
                    $("#prs-"+disid).hide();
                    $("#prs-"+disid).val(null);
                  }
                  else{
                    $("#prs-"+disid).show();                 
                  }
                  var chk=0;
                  if($("#inap").is(':checked')){
                    chk=1;
                  }
                  $.get("{{ route('api.get.price')}}",{ id:$("#"+disid).val(), t:$("#dt"+disid).val(), d:(isNaN(parseInt($("#dur-"+disid).val())) ? 1 :  parseInt($("#dur-"+disid).val()) ), p:(isNaN(parseInt($("#prs-"+disid).val())) ? 1 :  parseInt($("#prs-"+disid).val()) ),c:(isNaN(parseInt($("#disc-"+disid).val())) ? 1 :  parseInt($("#disc-"+disid).val()) ), s:chk, cs:(isNaN(parseInt($(".cust").val())) ? 0 :  parseInt($(".cust").val()) ) }, function(data){
                    //$.get("{{ route('api.get.price')}}?id="++"&t="++"&d="++"&p="++"&c="+"a,{ name:"Donald", town:"Ducktown" }, function(data){        
                      var tot = data;
                      
                      $('#prs-'+disid).val(1);
                      $('#dur-'+disid).val(1);
                      $('#subt-'+disid).val(data);
                      var 	number_string = tot.toString(),
                      split	= number_string.split(','),
                      sisa 	= split[0].length % 3,
                      rupiah 	= split[0].substr(0, sisa),
                      ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                      
                      if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                      }
                      
                      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                      
                      $("#price"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
                      total();
                    });
                  });
                  
                  
                  $('.prs,.dur,.disc,.fdisc_type').bind('keyup change', function (e){
                    var disid=parseInt($(this).closest('.pelanggaran').find('select.select2').attr('id'));       
                    var chk=0;
                    
                    if($("#inap").is(':checked')){
                      chk=1;
                    }
                    $.get("{{ route('api.get.price')}}",{ id:$("#"+disid).val(), t:$("#dt"+disid).val(), d:(isNaN(parseInt($("#dur-"+disid).val())) ? 1 :  parseInt($("#dur-"+disid).val()) ), p:(isNaN(parseInt($("#prs-"+disid).val())) ? 1 :  parseInt($("#prs-"+disid).val()) ),c:(isNaN(parseInt($("#disc-"+disid).val())) ? 0 :  parseInt($("#disc-"+disid).val()) ),dtyp:(isNaN(parseInt($("#fdisc_type-"+disid).val())) ? 0 :  parseInt($("#fdisc_type-"+disid).val()) ), s:chk, cs:(isNaN(parseInt($(".cust").val())) ? 0 :  parseInt($(".cust").val()) ) }, function(data){
                      //$.get("{{ route('api.get.price')}}?id="++"&t="++"&d="++"&p="++"&c="+"a,{ name:"Donald", town:"Ducktown" }, function(data){        
                        var tot = data;
                        $('#subt-'+disid).val(data);
                        var 	number_string = tot.toString(),
                        split	= number_string.split(','),
                        sisa 	= split[0].length % 3,
                        rupiah 	= split[0].substr(0, sisa),
                        ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                        
                        if (ribuan) {
                          separator = sisa ? '.' : '';
                          rupiah += separator + ribuan.join('.');
                        }
                        
                        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                        
                        $("#price"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
                        total();
                      });
                    });
                  });
                  
                  $('.add_lineadds').click(function(e) {
                    e.preventDefault();
                    var anote_tr = $('.add-table tbody .anote:first').clone();
                    var add_tr = $('.add-table tbody tr:first').clone();
                    var next=parseInt($('.add-table tbody tr:last').attr('id').split('-')[1])+1;
                    add_tr.attr("id","addid-"+next)              
                    anote_tr.attr("id","anot-"+next);
                    anote_tr.find('.anotes').attr("id","anote-"+next);
                    anote_tr.find('#anote-'+next).val("");
                    add_tr.find('.delete_add').show();
                    add_tr.show();
                    add_tr.find('.select2-container').remove();
                    add_tr.find('select.select2a').val("");
                    add_tr.find('select.select2a').attr("id","a-"+next);
                    add_tr.find('select.adisc_type').val(0);
                    add_tr.find('select.adisc_type').attr("id","adisc_type-"+next);
                    add_tr.find('.hiddena').val("");
                    add_tr.find('#add_price-0').attr("id","add_price-"+next);              
                    add_tr.find('#adate-0').attr("id","adate-"+next);              
                    add_tr.find('#amm-0').attr("id","amm-"+next);              
                    add_tr.find('#disca-0').attr("id","disca-"+next);              
                    add_tr.find('#subta-0').attr("id","subta-"+next);
                    add_tr.find('#amm-'+next).val("");
                    add_tr.find('#disca-'+next).val(0);
                    add_tr.find('#subta-0').val("");   
                    add_tr.find('#prica0').attr("id","prica"+next);                         
                    add_tr.find('div').html("");                            
                    
                    add_tr.find('select.select2a').select2({
                      'width': '100%',
                      placeholder: "Pilih Add on",
                      ajax: {
                        url: '{{ route('api.get.addon')}}',
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
                          return {
                            results:  $.map(data, function (item) {
                              return {
                                text: item.name,
                                id: item.id,
                                price: item.price
                              }
                            })
                          };
                        },
                        cache: true
                      }
                    });
                    $('.add-table tbody').append(add_tr);
                    anote_tr.show();              
                    
                    $('.add-table tbody').append(anote_tr);
                    
                    $('.addon').on("select2:select", function(e) {
                      var disid=$(this).attr("id"); 
                      disid = disid.split("-");
                      disid = disid[1];  
                      var chk=0;
                      if($("#inap").is(':checked')){
                        chk=1;
                      }
                      $.get("{{ route('api.get.addon_price')}}",{ id:$("#a-"+disid).val(), a:(isNaN(parseInt($("#amm-"+disid).val())) ? 1 :  parseInt($("#amm-"+disid).val()) ),d:(isNaN(parseInt($("#disca-"+disid).val())) ? 0 :  parseInt($("#disca-"+disid).val()) ),dtyp:(isNaN(parseInt($("#adisc_type-"+disid).val())) ? 0 :  parseInt($("#adisc_type-"+disid).val()) ), s:chk, cs:(isNaN(parseInt($(".cust").val())) ? 0 :  parseInt($(".cust").val()) ) }, function(data){
                        //$.get("{{ route('api.get.price')}}?id="++"&t="++"&d="++"&p="++"&c="+"a,{ name:"Donald", town:"Ducktown" }, function(data){        
                          var tot = data;
                          var ammount=isNaN(parseInt($("#amm-"+disid).val())) ? 1 :  parseInt($("#amm-"+disid).val());
                          $('#add_price-'+disid).val(data/ammount);
                          $('#amm-'+disid).val(1);
                          $('#subta-'+disid).val(data);
                          var 	number_string = tot.toString(),
                          split	= number_string.split(','),
                          sisa 	= split[0].length % 3,
                          rupiah 	= split[0].substr(0, sisa),
                          ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                          
                          if (ribuan) {
                            separator = sisa ? '.' : '';
                            rupiah += separator + ribuan.join('.');
                          }
                          
                          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                          
                          $("#prica"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
                          total();
                        });   
                      });
                      
                      $('.amm, .disca, .add_price, .adisc_type').bind('keyup change', function (e){
                        var disid=parseInt($(this).closest('.add').find('select.select2a').attr('id').split("-")[1]);
                        var disid=parseInt($(this).closest('.add').find('select.select2a').attr('id').split("-")[1]);
                        var chk=0;
                        if($("#inap").is(':checked')){
                          chk=1;
                        }
                        
                        
                        //$.get("{{ route('api.get.price')}}?id="++"&t="++"&d="++"&p="++"&c="+"a,{ name:"Donald", town:"Ducktown" }, function(data){        
                          var discount=isNaN(parseInt($("#disca-"+disid).val())) ? 0 :  parseInt($("#disca-"+disid).val());
                          var ammount=isNaN(parseInt($("#amm-"+disid).val())) ? 1 :  parseInt($("#amm-"+disid).val());
                          var per_item =isNaN(parseInt($("#add_price-"+disid).val())) ? 0 :  parseInt($("#add_price-"+disid).val());
                          var disc_type=isNaN(parseInt($("#adisc_type-"+disid).val())) ? 0 :  parseInt($("#adisc_type-"+disid).val());
                          var tot = per_item * ammount;
                          if(disc_type==0){
                            tot = tot-(tot*discount/100);
                          }
                          else{
                            tot = tot-discount;
                          }
                          
                          $('#subta-'+disid).val(tot);
                          
                          
                          var 	number_string = tot.toString(),
                          split	= number_string.split(','),
                          sisa 	= split[0].length % 3,
                          rupiah 	= split[0].substr(0, sisa),
                          ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                          
                          if (ribuan) {
                            separator = sisa ? '.' : '';
                            rupiah += separator + ribuan.join('.');
                          }
                          
                          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                          
                          $("#prica"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
                          total();
                          
                        });
                        
                      });
                      
                      
                      // $('.summernote').summernote();
                      $('.cust').select2({
                        'width': '100%',
                        ajax: {
                          url: '{{ route('api.get.customer')}}',
                          dataType: 'json',
                          delay: 250,
                          processResults: function (data) {
                            return {
                              results:  $.map(data, function (value, key) {
                                return {
                                  text: value,
                                  id: key
                                }
                              })
                            };
                          },
                          cache: true
                        }
                      });
                      $('.cust').on("select2:select", function(e) {
                        $.get("{{ route('api.get.customer.data')}}",{ id:$(".cust").val() }, function(data){
                          //$.get("{{ route('api.get.price')}}?id="++"&t="++"&d="++"&p="++"&c="+"a,{ name:"Donald", town:"Ducktown" }, function(data){        
                            $(".guest-name").val(data.name);
                            $(".guest-phone").val(data.phone);        
                          });   
                        });
                        $('input, select, subt').on('keydown change', function(e){
                          if($("#code").val()!="" || $("#code").val()!=0){
                            if(confirm('Kupon Yang terpakai akan menjadi tidak valid bila data berubah! Lanjutkan?') ) {
                              $(".check").removeAttr('disabled'); 
                              $("#dis").val("");
                              $("#dis_type").val("");
                              $("#coup_id").val("");
                              $("#code").val("");
                              $("#coden").val("");
                              $(".codes").removeAttr('disabled');      
                            }
                            else{
                              e.preventDefault();
                            }
                          }
                        });
                        
                        $('.pelanggaran-table tbody').on('click', '.delete_line', function (e) {
                          e.preventDefault();
                          var id=$(this).closest('.pelanggaran').attr("id").split("-")[1];
                          $("#not-"+id).remove();            
                          $(this).closest('.pelanggaran').remove();
                          total();
                        });
                        $('.add-table tbody').on('click', '.delete_add', function (e) {
                          e.preventDefault();
                          var id=$(this).closest('.add').attr("id").split("-")[1];
                          $("#anote-"+id).remove();            
                          $(this).closest('.add').remove();
                          total();              
                        });
                        
                        $('.facils').trigger("select2:select");
                        $('.cust').trigger("select2:select");
                      });
                      
                    </script>
@endpush
                    