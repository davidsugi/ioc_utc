@extends('layouts.app')
@section('title')
Manage Order
@endsection

@section('breadcrumb')
  <li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
  <li><a href='#' class='active'><i class='fa fa-cart-plus'></i> Order</a></li>
@endsection 
@section('content')
<style>
  input[type="date"]:before {
  content: attr(placeholder) !important;
  color: #aaa;
  margin-right: 0.5em;
  }
  input[type="date"]:focus:before,
  input[type="date"]:valid:before {
  content: "";
  }
</style>
<div class="row">
  <div class='col-md-4'>
    <div class='small-box bg-blue'>
      <div class='inner'>
        <h3>{{ $all}}</h3>
        <p>Total Order</p>
      </div>
      <div class='icon'><i class='fa fa-cube'></i></div>
      <a href="#" class="small-box-footer">&nbsp;</a>
    </div>
  </div>
  <div class='col-md-4'>
    <div class='small-box bg-yellow'>
      <div class='inner'>
        <h3>{{ $confirmed}}</h3>
        <p>Order Confirmed</p>
      </div>
      <div class='icon'><i class='fa fa-hourglass'></i></div>
      <a href="{{ route('orders.index',['status'=>[1,2]]) }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class='col-md-4'>
    <div class='small-box bg-green'>
      <div class='inner'>
        <h3>{{ $finished}}</h3>
        <p>Order Selesai</p>
      </div>
      <div class='icon'>
        <i class='fa fa-check'></i>
      </div>
      <a href="{{ route('orders.index',['status'=>[3]]) }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>    
  </div>
</div>
</div>


<div class="row">
    <form method="get" id="search-form">
      <div class="col-md-3">
        <div class="input-group no-border">
          <input type="date" class="form-control column-filter" name="filter[start_date]" placeholder="Mulai" value="{{ !empty($filter['start_date']) ? $filter['start_date'] : '' }}" />
          <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
        </div>
      </div>
      <div class="col-md-3">
        <div class="input-group no-border">
          <input type="date" class="form-control column-filter" name="filter[end_date]" placeholder="Akhir" value="{{ !empty($filter['end_date']) ? $filter['end_date'] : '' }}" />
          <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group no-border">
          <input type="name" class="form-control column-filter" name="filter[name]" placeholder="Nama Customer" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}" />
          <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
        </div>
      </div>
      <div class="col-md-2">
        <div class="input-group no-border">
          <select class="form-control select2 status" multiple="multiple" name="status[]">
            <option value="0" {{ array_search(0,$status) ? 'selected' : ''}}>Booked</option>
            <option value="1" {{ array_search(1,$status) ? 'selected' : ''}}>Payment Confrimed</option>
            <option value="2" {{ array_search(2,$status) ? 'selected' : ''}}>Check In</option>
            <option value="3" {{ array_search(3,$status) ? 'selected' : ''}}>Check Out</option>
            <option value="4" {{ array_search(4,$status) ? 'selected' : ''}}>Canceled</option>
          </select>    
          <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
        </div>
      </div>
      <div class="col-md-2">
        <button type="submit" class="btn btn-primary btn-block">Cari</button>
      </div>
    </form>
  </div>
  <br>
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
      <div class="box-header">
        <a href="{{ route('orders.create') }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Order</button></a>
      </div>

        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>
              <th>No Order</th>
              <th width="10%">Status Order</th>
              <th width="10%">Status Pambayaran</th>
              <th width="10%">Operator</th>
              <th width="10%">Customer</th>
              <th width="10%">Check In</th>
              <th width="15%">Check Out</th>
              <th width="25%">Total</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $orders as $key => $order )
            <tr height="10">
              <td>{{ $key+1 }}</td>
              <td>{{ $order->order_code }}</td>
              <td>{!! $order->spanLabel !!}</td>
              <td>{!! $order->lunasLabel !!}</td>
              <td>{{ $order->user ? $order->user->name : '-'}}</td>
              <td>{{ $order->customer->nameLabel }}</td>
              <td>{{ $order->checkInLabel }}</td>
              <td>{{ $order->checkOutLabel }}</td>
              <td>{{ H::rupiah($order->totalLabel-$order->disc+$order->taxLabel) }}</td>
              <td><a href="{{ route('orders.show',['id'=>$order->id]) }}" class="btn btn-primary btn-block">Show</span></a></td>
              </td>
            </tr>
            @endforeach
            @if($orders->count() == 0)
              <tr>
                <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
              </tr>
              @endif
            </tbody>
          </table>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
@endsection

@push('scripts')
<script>
    $(document).ready(function() {  
        $('select').select2({
            allowClear: true,
            placeholder:"Pilih Status",
            width:"100%",            
        });
        $('select').trigger("change");
      });
</script>
@endpush