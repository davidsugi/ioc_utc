@component('mail::message')
<STRONG> KODE ORDER : {{ $ord->order_code}} </STRONG>


@component('mail::panel')

<STRONG>{{ $cus }}</STRONG>{{ $introLines}}

<h4>Berikut Lampiran Transaksi yang dibuat oleh customer:</h4>
@component('mail::table')
|        |          |
| :--------------| :------------|
|  Tanggal Booking:      | {!! $ord->date->format("d-m-Y") !!}|
|  Inap:      | {!! $ord->inapLabel !!}         |
|  Tanggal Inap:      | {!! $ord->checkinLabel !!} s/d {!! $ord->checkoutLabel !!}         |
{{--  |  Total Transaksi:      | {{ H::rupiah($ord->totalNettLabel) }}         |  --}}


@endcomponent

@endcomponent

{{--  "{{ $mes }}"  --}}
<STRONG>Fasilitas</STRONG>
@component('mail::table')
| Fasilitas       | Harga         | Jumlah         |  Durasi       | Diskon  | Subtotal  |
| ------------- |:-------------:| :-----------:| :------------:| :------------:| :--------:|
@foreach($ord->FacilityGroup as $items)
| {{ $items->nameLabel}}      | {{ H::rupiah($items->price) }}      | {!! $items->personLabel !!}      | {{ $items->durationLabel}}      | {{ $items->disc}} %     | <td rowspan=2>{{ H::rupiah($items->subtotal) }}    |
| <td colspan=4>Catatan: {{ $items->note}}    |
@endforeach
|  <td colspan=4>Total Transaksi Fasilitas:      | {{ $ord->totalFacilityLabel }}         |

@endcomponent

<STRONG>Tambahan</STRONG>

@component('mail::table')
| Fasilitas       | Harga         |  jumlah         | Diskon  | Subtotal  |
| :-------------: |:-------------:| :--------:| :--------:| :--------:|
@foreach($ord->AddonGroup as $items)
| {{ $items->nameLabel}}      | {{ $items->priceLabel}}      | {{ $items->amount}}      | {{ $items->disc}} %     | {{ H::rupiah($items->subtotal) }}    |
@endforeach
|  <td colspan=3>Total Transaksi Tambahan:      | {{ $ord->totalAddonLabel }}         |

@endcomponent
@component('mail::panel')

@component('mail::table')
|        |          |           |   |   |
| :-------------: |:-------------:| :--------:| :--------:| :--------:|
|  <td colspan=4>Total Transaksi :      | {{ H::rupiah($ord->totalLabel) }}         |
|  <td colspan=4>Potongan Global {{ $ord->globaldisc_type==1 ? $ord->globaldisc."%" : ""}}:      | {{ H::rupiah($ord->globaldisc_val) }}        |
|  <td colspan=4>Potongan Kupon {{ $ord->code}}:    | {{ H::rupiah($ord->disc) }}        |
|  <td colspan=2>Pajak: | {{$ord->tax}} % |    | {{ H::rupiah($ord->taxLabel) }}        |
|  <td colspan=4><strong>Total Akhir:   |<strong> {{ H::rupiah($ord->total_nett_label) }}       </strong> |

@endcomponent

<strong>Notes: </strong><br>
{{$ord->description}}
@endcomponent

@foreach($outroLines as $line)
	{{"Untuk melihat order sepenuhnya, silahkan klik tombol dibawah ini"}}
@endforeach
@component('mail::button', ['url' => route("orders.show",$ord->id)])
Lihat Order
@endcomponent

Thanks,<br>
Admin {{ config('app.name') }}
@endcomponent


