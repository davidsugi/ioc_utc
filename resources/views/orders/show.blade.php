@extends('layouts.app')
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("orders.index")}}'><i class='fa fa-cart-plus'></i> Order</a></li>
<li><a href='#' class='active'>{{ $order->order_code }}</a></li>
@endsection
@section('title')
Order No. {{ strtoupper($order->order_code) }}
@endsection
@section('actionbtn')
@if($order->status<3)
  @if($order->lunas!=2)
    <a style="margin-right:5px"  href="{{ route('orders.invoices.create', $order->id) }}" class="btn btn-success success">Terima Pembayaran</a>
  @endif
  @if($order->status==1)
    <a style="margin-right:5px" data-href="{{ route('orders.stat', ['id'=>$order->id]) }}" class="btn btn-info info stat">Check In</a>
  @endif
  @if($order->status==2)
    <a style="margin-right:5px" data-href="{{ route('orders.stat', ['id'=>$order->id]) }}" class=" stat btn btn-primary primary">Check Out</a>
  @endif
  <a data-href="{{ route('orders.destroy', $order->id) }}" class="btn btn-danger destroy">Batalkan Order</a>          
@endif

@endsection

@section('content')
<div class="row">
  <div class="col-md-4">
    <div class="small-box bg-blue">
      <div class="inner">
        <h3>{{ $order->customer->nameLabel }}</h3>
        <p>Customer</p>
      </div>
      <div class="icon">
        <i class="fa fa-user"></i>
      </div>
    </div>
  </div>
  @if($order->checkinlabel)
    <div class="col-md-4">
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $order->checkinlabel }}</h3>
          <p>Check In</p>
        </div>
        <div class="icon">
          <i class="fa fa-calendar-alt"></i>
        </div>
      </div>
    </div>
  @endif
  @if($order->checkOutLabel)
  <div class="col-md-4">
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3>{{ $order->checkOutLabel }}</h3>
        <p>Estimasi Check out</p>
      </div>
      <div class="icon">
        <i class="fa fa-calendar-alt"></i>
      </div>
    </div>
  </div>
  @endif

</div>
<div class="row">
  <div class="col-md-12">
    <div class="box">
        <div class="box-body">
          @php($row=4)
          @if($order->customer->company_name!="")
          @php($row=3)
            <div class="col-md-{{ $row }}">
               <h3>{{ $order->customer->name }}</h3>
                <p>Nama Customer</p>
            </div>
          @endif
            <div class="col-md-{{ $row }}">
               <h3>{{ $order->customer->customer_type->name }}</h3>
                <p>Tipe Customer</p>
            </div>
             <div class="col-md-{{ $row }}">
               <h3>{{ $order->customer->phone }}</h3>
                <p>Telpon Customer</p>
            </div>
            <div class="col-md-{{ $row }}">
               <h3>{{ $order->customer->email }}</h3>
                <p>Email Customer</p>
            </div>
            @if($order->user)
            <div class="col-md-4">
               <h3>{{ $order->user->name }}</h3>
                <p>Operator</p>
            </div>
            <div class="col-md-8">
               <h3>{{ $order->description }}</h3>
                <p>Notes</p>
            </div>
            @endif
        </div>
    </div>
  </div>
</div>
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
   
        <div class="box-body">
        {!! $order->spanLabel !!} 
        {!! $order->lunasLabel !!} 
        {!! $order->menginapLabel !!}
        @if($order->status<3)        
        <a style="margin-right:5px"  href="{{ route('orders.edit', $order->id) }}" class="btn btn-primary primary pull-right">Ubah Order</a>          
        @endif
        <a style="margin-right:5px"  href="{{ route('orders.order_documents.index', $order->id) }}" class="btn btn-info info pull-right">Lihat Dokumen</a>          
        <a style="margin-right:5px"  href="{{ route('orders.invoices.index', $order->id) }}" class="btn btn-info info pull-right">Lihat Invoice</a>          
        <a style="margin-right:5px"  href="{{ route('orders.folios.index', $order->id) }}" class="btn btn-info info pull-right">Lihat Folio</a>          
        <a style="margin-right:5px" href="{{ route('invoices.print', $order->id) }}" class="btn btn-success  pull-right"><span>  <i class="fa fa-print"></i> </span>Cetak Invoice </a>        
        
        <h4 style="margin-top: 30px">Utama</h4>
        @if(sizeof($facils)>0)
          <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Fasilitas</th>
              <th width="10%">Harga satuan</th>
              <th width="10%">Jumlah Orang</th>
              <th width="10%">Durasi Pinjam(hari)</th>
              <th width="10%">Diskon</th>
              <th width="20%">Sub total</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            <tr height="10">
              <td>{{ $key+1 }}</td>
              <td>{!! $detail->item_type=="App\\Facility"  ? "<a href='".route('facilities.show',$detail->item_id)."'>" : "<a href='".route('bundles.index',['filter[name]' => $detail->nameLabel])."'>"  !!}{{ $detail->nameLabel }}</a></td>
              <td  align="end">{{ H::rupiah($detail->price) }}</td>
              <td>{!! $detail->personLabel !!}</td>              
              <td>{{ $detail->durationLabel }}</td>              
              <td>{{ $detail->disc_label }}</td>               
              <td rowspan="2"  align="end">{{ H::rupiah($detail->subtotal) }}</td>
            </tr>
            <tr>
              <td>Catatan: </td>
                <td colspan="6">{{ $detail->note }}</td>
            </tr>
            @endforeach
            <tr><b><td colspan="6" align="right"><b>Total</b></td><td align="end"><b>{{ $order->totalFacilityLabel }}</b></td></b></tr>
              
            </tbody>

          </table>

        @else 
        <h5 align="center"><i>{{ "Tidak ada Detail " }}</i></h5>
        @endif
        <h4>Tambahan</h4>
        
        <table id="example2" class="table table-bordered  table-hover">
        @if(sizeof($adds)>0)  
            <thead>
              <tr>
                <th width="5%">No</th>
                <th width="20%">Fasilitas</th>
                <th width="10%">Harga satuan</th>
                <th width="4%">Jumlah</th>
                <th width="10%">Diskon</th>
                <th width="20%" align="end">Sub total</th>
              </tr>
            </thead>
            <tbody>
              @foreach( $adds as $key => $detail )
              <tr height="10">
                <td>{{ $key+1 }}</td>
                <td>{{ $detail->folio()->orderBy("id","DESC")->first()->name_label }}</td>
                <td align="end">{{ $detail->priceLabel }}</td>
                <td>{{ intval($detail->amount) }}</td>
                <td>{{ $detail->disc_label }}</td>              
                <td align="end">{{ H::rupiah($detail->subtotal) }}</td>              
              </tr>
              @endforeach
              <tr><b><td colspan="5" align="center">Total Tambahan: </td><td align="end"><b>{{ $order->totalAddonLabel }}</b></td></b></tr>
        @else 
          <h5 align="center"><i>{{ "Tidak ada tambahan " }}</i></h5>
        @endif            
          <tr><td colspan="5" align="right">Total: </td><td align="end"><b>{{ H::rupiah($order->totalLabel) }}</b></td></tr>            
          <tr><td colspan="5" align="right">Potongan Global {{ $order->globaldisc_type==1 ? $order->globaldisc."%" : ""}}: </td><td align="end"> <b>{{ H::rupiah($order->globaldisc_val) }}</b></td></tr>
          <tr><td colspan="5" align="right">Potongan Kupon {{ $order->code}}: </td><td align="end"><b>{{ H::rupiah($order->disc) }}</b></td></tr>
          {{-- <tr><td colspan="4" align="right">Tax: </td><td style="width: 10%">{{$order->tax}} %</td><td><b>{{ H::rupiah($order->taxLabel) }}</b></td></tr> --}}
          <tr><td colspan="5" align="right">Total Akhir: </td><td align="end"><b>{{ H::rupiah($order->total_nett_label) }}</b></td></tr>            
          <tr><td colspan="5" align="right">Terbayar: </td><td align="end"><b>{{ H::rupiah($order->paidLabel) }}</b></td></tr>            
          <tr><td colspan="5" align="right">Sisa belum Terbayar: </td><td align="end"><b>{{ H::rupiah($order->payableLabel) }}</b></td></tr>            
              
        </tbody>

        </table>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>

{{--     <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi Terima Barang</h4>
      </div>
        {!! Form::model($order, ['route' => ['orders.recieve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      <div class="form-group">
          {{ Form::label('nota','Nomor Nota Supermarket') }}
          {{ Form::text('nota','', ['class' => 'form-control', 'required']) }}
          @if ($errors->has('nota'))
                    <div class="help-block text-red">
                      {{ $errors->first('nota') }}
                    </div>
          @endif
      </div>
      {{ Form::hidden('id', $order->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="25%">Jumlah</th>
              <th width="15%">Harga</th>
              <th width="15%">diterima</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ Form::number('received[]', 0, ['class' => 'form-control','min'=> 0]) }}
                  @if ($errors->has('received'))
                            <div class="help-block text-red">
                              {{ $errors->first('received') }}
                            </div>
                  @endif
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div>

    <div id="myRev" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi draft</h4>
      </div>
        {!! Form::model($order, ['route' => ['orders.approve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      {{ Form::hidden('nota','approved', ['class' => 'form-control', 'required']) }}
      
      {{ Form::hidden('id', $order->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="15%">Jumlah</th>
              <th width="25%">Harga</th>
              <th width="25%">Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            @php( $totals=0)
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ $detail->subTotalLabel}}</td>
              
            </tr>
            @endforeach
            <tr><td colspan="5" align="center">Total:</td><td>{{ $order->totalLabel}}</td></tr>
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div> --}}
  <!-- /.row -->
@endsection

@push('scripts')

<form id='destroy-form' method="POST">
      {{ csrf_field() }}
      <input type="hidden" name="_method" value="DELETE">
  </form>
  <form id='stat-form' method="POST">
      {{ csrf_field() }}
      {{--  <input type="hidden" name="_method">  --}}
  </form>
  <script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });

        $('.stat').click(function() {
              $('#stat-form').attr('action',$(this).data('href'));
              $('#stat-form').submit();
        });
    });
  </script>

@endpush