@extends('layouts.app')

@push('styles')
    <link href="{{ asset('js/plugins/select2/select2.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('css/summernote.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('title')
{{ $order->exists ? 'Ubah' : 'Tambah' }} Order
@endsection
@if($order->exists)
@section('actionbtn')
  <a data-href="{{ route('orders.destroy', $order->id) }}" class="btn btn-danger destroy">Hapus Order</a>
@endsection
@endif

@section('content')
<div class="row">
    <div class="col-xs-10">
      <div class="box">
        <div class="box-body">
          @if($order->exists)
          {!! Form::model($order, ['route' => ['orders.update', $order->id], 'method'=>'PATCH', 'files' => true]) !!}
          @else
          {!! Form::model($order, ['route' => ['orders.store'], 'class' => 'col s12', 'files' => true]) !!}
          @endif
          <div class="form-group">
              {{ Form::label('customer_id','Customer') }}
              {{ Form::select('customer_id',[], $order->customer_id, ['class' => 'form-control select2 cust', 'required']) }}
              {{-- <a class="btn btn-primary" data-toggle="modal" href='#mymod'>add new</a> --}}
              @if ($errors->has('customer_id'))
          <div class="help-block text-red">
              {{ $errors->first('customer_id') }}
          </div>
              @endif
          </div>
          <div class="form-group">
              {{ Form::label('date','check in') }}
              {{ Form::date('date', $order->date!=null ? $order->date : date("Y-m-d") , ['class' => 'form-control', 'required']) }}
              @if ($errors->has('date'))
                        <div class="help-block text-red">
                          {{ $errors->first('date') }}
                        </div>
              @endif
          </div>


          <div class="form-group">
              {{ Form::label('description','Keterangan') }}
              {{ Form::text('description', $order->description, ['class' => 'form-control', 'required']) }}
              @if ($errors->has('description'))
                        <div class="help-block text-red">
                          {{ $errors->first('description') }}
                        </div>
              @endif
          </div>


          <div class="row">
            <div class="col-md-6">
              <table class="table pelanggaran-table">
                <thead>
                  <tr>
                    <th style="width: 15%;">Fasilitas</th>
                    <th style="width: 1%;">Check in</th>
                    <th colspan="2" style="width: 20%;" align="center">Durasi</th>
                    <th style="width: 5%;"></th>
                  </tr>
                </thead>
                <tbody>
                @if(isset($facd)&& sizeof($facd)>0)
                  @foreach($facd as $key=>$fac )
                  <tr class="pelanggaran">
                    {!! Form::hidden('ids[]', $ids[$key], ['class'=> 'form-control hiddens']) !!}
                    <td>{!! Form::select('facil[]', $facs, $key, ['required' => 'required', 'class'=> 'form-control select2 facils', 'id'=>"p".$key]) !!}</td>
                    <td>{{ Form::date('check_in[]', '', ['class' => 'form-control dt',"id"=>"dt".$i, 'required']) }}</td>
                    <td>{{ Form::number('duration[]','', ['class' => 'form-control dur', 'required',]) }}</td>
                    <td><label id="unit{{ $i }}"> dope</label></td>
                    <td>
                    <a href="" class="btn btn-danger delete_line" style="display:none"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  @endforeach
                @else
                  @for($i=0; $i<1; $i++)
                  <tr class="pelanggaran">
                    <td>{!! Form::select('facil[]',array(), null, ['required' => 'required', 'class'=> 'form-control select2 no-border facils', 'id'=>"p".$i]) !!}</td>
                    <td>{{ Form::date('check_in[]', $order->date!=null ? $order->date : date("Y-m-d") , ['class' => 'form-control dt',"id"=>$i, 'required']) }}</td>
                    <td>{{ Form::number('duration[]','', ['class' => 'form-control dur', 'required',"min"=>0]) }}</td>
                    <td><label id="unit{{ $i }}"> dope</label></td>
                    <td>
                      <a href="" class="btn btn-danger delete_line" style="display: {{ $i==0 ? 'none': null }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  @endfor
                @endif
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="4"></td>
                    <td><a href="" class="btn btn-success add_line"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></td>
                  </tr>
                </tfoot>
              </table>
            </div>
              {{-- addons --}}
            <div class="col-md-6">
              <table class="table add-table">
                <thead>
                  <tr>
                    <th style="width: 30%;">Add On</th>
                    <th style="width: 30%;">Jumlah</th>
                    <th style="width: 10%;"></th>
                  </tr>
                </thead>
                <tbody>
                @if(isset($addd)&& sizeof($addd)>0)
                  @foreach($addd as $key=>$fac )
                  <tr class="add">
                    {!! Form::hidden('ida[]', $ida[$key], ['class'=> 'form-control hiddena']) !!}
                      <td>{!! Form::select('addons[]', $adds, $key, ['required' => 'required', 'class'=> 'form-control select2a addon', 'id'=>"a".$key]) !!}</td>
                    <td> {{ Form::number('amount[]', $fac->amount, ['class' => 'form-control amm','min'=>0, 'required']) }}</td>
                    <td>
                    <a href="" class="btn btn-danger delete_add" style="display:none"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  @endforeach
                @else
                  @for($i=0; $i<1; $i++)
                  <tr class="add">
                    <td>{!! Form::select('addons[]',array(), null, ['required' => 'required', 'class'=> 'form-control select2a no-border addon', 'id'=>"a".$i]) !!}</td>
                      <td> {{ Form::number('amount[]','', ['class' => 'form-control amm','min'=>0 , 'required']) }}</td>
                    <td>
                      <a href="" class="btn btn-danger delete_add" style="display: {{ $i==0 ? 'none': null }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  @endfor
                @endif
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2"></td>
                    <td><a href="" class="btn btn-success add_lineadds"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
            <div class="box-footer">
              {{ Form::submit('Simpan', ['class' => 'btn btn-success']) }}
            </div>
          {{ Form::close() }}
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->

  </div>
  <!-- /.row -->
<!-- Modal -->
    <div class="modal fade" id="myModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Select Time</h4>
          </div>
          <div class="modal-body">
                <div class="row">
        <div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h4 class="box-title">Draggable Events</h4>
            </div>
            <div class="box-body">
              <!-- the events -->
              <div id="external-events">
               
              </div>
            </div>
            <!-- /.box-body -->
          </div>
    
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-danger">
            <div class="box-body no-padding">
              <!-- THE CALENDAR -->
              <div id="calendar"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/plugins/select2/select2.js') }}"></script>

<script src="{{ asset('js/summernote.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {

    $('.facils').select2({
        'width': '100%',
          ajax: {
            url: '{{ route('api.get.facility')}}',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (value, key) {
                        return {
                          text: value,
                          id: key
                        }
            })
                };
            },
            cache: true
          }
    });

    $('.addon').select2({
        'width': '100%',
          ajax: {
            url: '{{ route('api.get.addon')}}',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (value, key) {
                        return {
                          text: value,
                          id: key
                        }
            })
                };
            },
            cache: true
          }
    });

    $('.facs').on("select2:selecting", function(e) {
                  var next=parseInt($(this).attr('id'))+1;
                  if($('#'+next).length >0 ){
                    $('#'+next).select2("open");
                  }
                  else{
                    $('.add_line').trigger("click");
                    $('#'+next).select2("open");
                  }
              });

     $('.add_line').click(function(e) {
              e.preventDefault();
              var pelanggaran_tr = $('.pelanggaran-table tbody tr:first').clone();
              var next=parseInt($('.pelanggaran-table tbody tr:last').find('select.select2').attr('id'))+1;
              pelanggaran_tr.find('.delete_line').show();
              pelanggaran_tr.find('.select2-container').remove();
              pelanggaran_tr.find('select.select2').val("");
              pelanggaran_tr.find('select.select2').attr("id","p"+next);
              pelanggaran_tr.find('.hiddens').val("");
              pelanggaran_tr.find('select.select2').select2({
                'width': '100%',
                placeholder: "Pilih Fasilitas",
                  ajax: {
                    url: '{{ route('api.get.facility')}}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                      return {
                        results:  $.map(data, function (value,key) {
                              
                              return {
                                  text: value,
                                  id: key
                              }

                          })
                      };
                    },
                    cache: true
                  }
              });
              pelanggaran_tr.find('.violation_point').val('');
              $('.pelanggaran-table tbody').append(pelanggaran_tr);
               $('.facils').on("select2:selecting", function(e) {
                  var next=parseInt($(this).attr('id'))+1;
                  if($('#'+next).length >0 ){
                    $('#'+next).select2("open");
                  }
                  else{
                    $('.add_line').trigger("click");
                    $('#'+next).select2("open");
                  }
              });

            });

     $('.add_lineadds').click(function(e) {
              e.preventDefault();
              var add_tr = $('.add-table tbody tr:first').clone();
              var next=parseInt($('.add-table tbody tr:last').find('select.select2a').attr('id'))+1;
              add_tr.find('.delete_add').show();
              add_tr.find('.select2-container').remove();
              add_tr.find('select.select2a').val("");
              add_tr.find('select.select2a').attr("id","a"+next);
              add_tr.find('.hiddena').val("");
              add_tr.find('.amm').val("");
              add_tr.find('select.select2a').select2({
                'width': '100%',
                placeholder: "Pilih Add on",
                  ajax: {
                    url: '{{ route('api.get.addon')}}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                      return {
                        results:  $.map(data, function (value,key) {
                              
                              return {
                                  text: value,
                                  id: key
                              }

                          })
                      };
                    },
                    cache: true
                  }
              });
              $('.add-table tbody').append(add_tr);
               $('.addon').on("select2:selecting", function(e) {
                  var next=parseInt($(this).attr('id'))+1;
                  if($('#'+next).length >0 ){
                    $('#'+next).select2("open");
                  }
                  else{
                    $('.add_lineadds').trigger("click");
                    $('#'+next).select2("open");
                  }
              });

            });

     
    // $('.summernote').summernote();
    $('.cust').select2({
        'width': '100%',
          ajax: {
            url: '{{ route('api.get.customer')}}',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (value, key) {
                        return {
                          text: value,
                          id: key
                        }
            })
                };
            },
            cache: true
          }
    });
    $('.pelanggaran-table tbody').on('click', '.delete_line', function (e) {
              e.preventDefault();
              $(this).closest('.pelanggaran').remove();
            });
    $('.add-table tbody').on('click', '.delete_add', function (e) {
              e.preventDefault();
              $(this).closest('.add').remove();
            });
  });

  var url = "{{ route('api.get.schedule.order') }}";
  $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex: 1070,
          revert: true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        });

      });
    }
    
    ini_events($('#external-events div.external-event'));

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();

    $.getJSON(url,function(data) {
        console.log("Data appointment: "+data);
        $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        buttonText: {
          today: 'today',
          month: 'month',
          week: 'week',
          day: 'day'
        },
        //Random default events
        events: data,
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        dayClick: function(date, jsEvent, view, resourceObj) {
                      alert('Date: ' + date.format());
                      alert('Resource ID: ' + resourceObj.id);
        },
        eventClick: function(calEvent, jsEvent, view) {
        alert('Event: ' + calEvent.id);
        // $("#calendar").fullCalendar('removeEvents');

        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        alert('View: ' + view.name);
        $(this).remove();
        // change the border color just for fun

        },
        eventResize: function(event, delta, revertFunc) {
         alert(event.end.format());

        },
        drop      : function (date, allDay) {
        var originalEventObject = $(this).data('eventObject')
        var copiedEventObject = $.extend({}, originalEventObject)
        copiedEventObject.start           = date
        copiedEventObject.allDay          = allDay
        copiedEventObject.backgroundColor = $(this).css('background-color')
        copiedEventObject.borderColor     = $(this).css('border-color')
        $('#calendar').fullCalendar('renderEvent', copiedEventObject,false)

        $(this).remove()
       
      }
      });

    
    });

    /* ADDING EVENTS */
    var currColor = "#3c8dbc"; //Red by default
    //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    $("#color-chooser > li > a").click(function (e) {
      e.preventDefault();
      //Save color
      currColor = $(this).css("color");
      //Add color effect to button
      $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
    });
    $("#add-new-event").click(function (e) {
      e.preventDefault();
      //Get value and make sure it is not null
      var val = $("#new-event").val();
      if (val.length == 0) {
        return;
      }
      //Create events
      var event = $("<div />");
      event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
      event.html(val);
      $('#external-events').prepend(event);

      //Add draggable funtionality
      ini_events(event);

      //Remove event from text input
      $("#new-event").val("");
    });

    $('.dt').on("click", function(e) {
        $('#myModal').modal('show');
      //Get value and make sure it is not null
      var yey=$('#external-events').find( ":contains('"+$(".cust option:selected").text()+": "+$("#p"+$(this).attr('id')+" option:selected").text()+"')" ).length;
      if(yey==0){
                //Create events
      var event = $("<div />");
      event.css({"background-color": '#0073b7', "border-color": '#0073b7', "color": "#fff"}).addClass("external-event");
      event.html($(".cust option:selected").text()+": "+$("#p"+$(this).attr('id')+" option:selected").text());
      $('#external-events').prepend(event);

      //Add draggable funtionality
      ini_events(event);
      
      }

        return false; 
    });
  });
</script>
@endpush
