@extends('layouts.app')

@section('title')
{{ $facility_type->exists ? 'Ubah' : 'Tambah' }} Tipe
@endsection
@if($facility_type->exists)
@section('actionbtn')
<a data-href="{{ route('facility_types.destroy', $facility_type->id) }}" class="btn btn-danger destroy">Hapus Tipe</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("facility_types.index")}}'><i class='fa fa-cube'></i> Tipe Fasilitas</a></li>
<li><a href='#' class='active'>{{ $facility_type->exists ? 'Ubah' : 'Tambah'}} Tipe Fasilitas</a></li>
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($facility_type->exists)
        {!! Form::model($facility_type, ['route' => ['facility_types.update', $facility_type->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($facility_type, ['route' => ['facility_types.store'], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Nama</label>
              {!! Form::text('name', $facility_type->name, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')
