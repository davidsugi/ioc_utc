@extends('layouts.app')
@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick-theme.css')}}">
<style>

	* {
		box-sizing: border-box;
	}


	.slick-slide {
		margin: 0px 25px;
	}

	.slick-slide img {
		width: 100%;
	}

	.slick-prev:before,
	.slick-next:before {
		color: black;
	}


	.slick-slide {
		transition: all ease-in-out .3s;
		opacity: .2;
	}
	.slick-slide:focus {
		opacity:1;
	}

	.slick-active {
		opacity: .5;
	}


	/* Main carousel style */
	.carousel {
		width: 600px;
	}

	/* Indicators list style */
	.article-slide .carousel-indicators {
		bottom: 0;
		left: 0;
		margin-left: 5px;
		width: 100%;
	}
	/* Indicators list style */
	.article-slide .carousel-indicators li {
		border: medium none;
		border-radius: 0;
		float: left;
		height: 54px;
		margin-bottom: 5px;
		margin-left: 0;
		margin-right: 5px !important;
		margin-top: 0;
		width: 100px;
	}
	/* Indicators images style */
	.article-slide .carousel-indicators img {
		border: 2px solid #FFFFFF;
		float: left;
		height: 54px;
		left: 0;
		width: 100px;
	}
	/* Indicators active image style */
	.article-slide .carousel-indicators .active img {
		border: 2px solid #428BCA;
		opacity: 0.7;
	}
</style>
@endpush
@section('title')
Detail bundle
@endsection
@section('actionbtn')

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("bundles.index")}}'><i class='fa fa-star'></i> Paket</a></li>
<li><a href='#' class='active'>Detail Paket</a></li>
@endsection
@section('content')
<style>
  .inner>p{
    font-size:150%;
  }
  .inner>h3{
    font-size:250%;
  }
</style>    
      <div class="row">
          <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-body">
                        <div class="col-xs-6">
                          <div class="col-xs-12">
                              <h4>Name</h4>
                              <h3> {{$bundle->name}}</h3>
                          </div>
                            
                        </div>
                         <div class="col-xs-6">
                         <div class="col-xs-12">
                              <h4>Harga Weekend</h4>
                              <h3>{{H::rupiah($bundle->weekend_price) }}</h3>
                          </div>
                            <div class="col-xs-12">
                              <h4>Harga Weekday</h4>
                              <h3> {{ H::rupiah($bundle->price) }}</h3>
                          </div>
                        </div>
                  </div>
               </div>
          </div>
      </div>

      <div class="row">
          <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-header">
                        <h3>Detail Paket</h3>
                  </div>
                  <div class="box-body">
                      <table id="primary" class="table table-bordered table-hover">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th width="5%">Tipe Fasilitas</th>
                                  <th width="5%">Nama Fasilitas</th>
                              </tr>
                          </thead>
      <tbody>
        @php($total=0)
      @foreach( $bundle->bundle_detail as $key => $detail )
      <tr height="10">
      <td>{{ $key+1 }}</td>
      <td>{!! $detail->nameLabel !!}</td>      
      {{--  <td><a href="{{ route('orders.show',['id'=>$detail->id]) }}">{{ $detail->order_code }}</a></td>  --}}
      {{--  <td>{{ $detail->date->format("d-m-Y") }}</td>  --}}
      <td>{{ $detail->typeLabel }}</td>
      {{--  <td>{{ $detail->check_out_label }}</td>  --}}
      @endforeach
      		@if($bundle->bundle_detail->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
      {{--  <tr>
          <td colspan="2" align="center">Total bundle:</td> 
          <td>{{ H::rupiah($total)}}</td> 
      </tr>  --}}
      </tbody>
      </table>
      </div>
      </div>
      </div>
      
      </div>
      
      <div class="row">
          <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-header">
                      <a href="{{ route('custom_prices.create',['u'=>'App\\Bundle', 'id'=>$bundle->id]) }}" class="btn btn-primary pull-right">Tambah Harga custom</a>
                        <h3>Custom Price</h3>
                  </div>
                  <div class="box-body">
                      <table id="primary" class="table table-bordered table-hover">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  {{--  <th width="5%">Tipe Fasilitas</th>  --}}
                                  <th width="5%">Nilai</th>
                                  <th width="5%">Tanggal Mulai</th>
                                  <th width="5%">Tanggal Selesai</th>
                              </tr>
                          </thead>
      <tbody>
        @php($total=0)
      @foreach( $bundle->custom_price as $key => $detail )
      <tr height="10">
      <td>{{ $key+1 }}</td>
      {{--  <td>{!! $detail->nameLabel !!}</td>        --}}
      {{--  <td><a href="{{ route('orders.show',['id'=>$detail->id]) }}">{{ $detail->order_code }}</a></td>  --}}
      {{--  <td>{{ $detail->date->format("d-m-Y") }}</td>  --}}
      <td>{{ $detail->price }}%</td>
      <td>{{ $detail->enddate->format("d-m-Y") }}</td>
      <td>{{ $detail->startdate->format("d-m-Y") }}</td>
      {{--  <td>{{ $detail->check_out_label }}</td>  --}}
      @endforeach
      		@if($bundle->custom_price->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
      {{--  <tr>
          <td colspan="2" align="center">Total bundle:</td> 
          <td>{{ H::rupiah($total)}}</td> 
      </tr>  --}}
      </tbody>
      </table>
      </div>
      </div>
      </div>

      <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-header">
                      <a href="{{ route('special_prices.create',['u'=>'App\\Bundle', 'id'=>$bundle->id]) }}" class="btn btn-primary pull-right">Tambah Harga Special</a>
                        <h3>Special Price</h3>
                  </div>
                  <div class="box-body">
                     <table id="example1" class="table table-bordered table-striped " >
					 <thead>
                        <tr>
                            <th rowspan="2" width="5%">No</th>
                            <th rowspan="2" width="15%">Nama</th>
                            <th rowspan="2" width="10%">Tipe Customer</th>
                            <th colspan="2" width="20%">Harga Menginap</th>
                            <th colspan="2" width="20%">Harga Tidak Menginap</th>
                            <th rowspan="2" width="10%" colspan="2">Action</th>
                        </tr>
                        <tr>
                             <th width="15%">Weekend Price</th>
                            <th width="15%">Weekday Price</th>
                            <th width="15%">Weekend Price</th>
                            <th width="15%">Weekday Price</th>
                        </tr>
                    </thead>
					<tbody>
                   		 @foreach( $bundle->special_price as $key => $special_price )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $special_price->nameLabel }}</td>
                            <td>{{ $special_price->customer_type->name }}</td>
                            <td>{{ H::rupiah($special_price->weekend_price) }}</td>
                            <td>{{ H::rupiah($special_price->price) }}</td>
                            <td>{{ H::rupiah($special_price->no_stay_weekend_price) }}</td>
                            <td>{{ H::rupiah($special_price->no_stay_weekday_price) }}</td>
							<td>
								<a href="
								{{route('special_prices.edit',['special_price'=>$special_price->id])}}">
								<button class="btn btn-warning">
									Edit
								</button>
							</a>
						</td>
					</tr>
          @endforeach
          	@if($bundle->special_price->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
			@endif
				</tbody>
			</table>
      </div>
      </div>
      <div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Gallery
					</div>

					<div class="col-md-3 pull-right">
						<a href="{{ route('bundles.galleries.create',$bundle->id)}}" class="btn btn-success btn-block">Tambah Detail Gallery &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>

			<!-- /.box-header -->
			<div class="box-body" style="height: 65vh;overflow-y:visible">
				<div class="row">
					<div class="col-xs-7">
						<div class="carousel slide article-slide" id="article-photo-carousel">
							<!-- Wrapper for slides -->
							<div class="carousel-inner cont-slider">
								<div class="item" style="display:none">
								</div>
						@foreach($gallery as $g)
						@if($loop->first)
						<div class="item active" >
							<img alt="" title="" style="width:600px;height:400px!important" src="{{asset('image/'.$g->image_path)}}">
						</div>
						@else
						<div class="item" >
							<img alt="" title="" style="width:600px;height:400px!important" src="{{asset('image/'.$g->image_path)}}">
						</div>
						@endif
						@endforeach
					</div>
					<!-- Indicators -->
					<ol class="carousel-indicators" >
						<section class="regular slider">
            <center>
              
							@foreach($gallery as $key => $g)
							<div data-slide-to={{$key+1}} data-target="#article-photo-carousel">
								<img alt="" src="{{asset('image/'.$g->image_path)}}">
							</div>
              @endforeach
          </center>
              
            </section>
					</ol>
				</div>
			</div>
			<div class="col-xs-5" style="height: 400px;">
				<table id="example1" class="table table-bordered table-striped " >
					<thead>
						<tr>
							<th>Image</th>
							<th>Deskripsi</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($gallery as $a)
						<tr> 
							<td><img src="{{asset('image/'.$a->image_path)}}" width="60px" height="40px"></td>

							<td>{{ $a->description }}</td>
							<td>
								<a href="
								{{route('bundles.galleries.edit',['facility'=>$a->galleriable_id,'gallery'=>$a->id])}}">
								<button class="btn btn-warning">
									Edit
								</button>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			{{ $gallery->links() }}
		</div>
	</div>
</div>
</div>
</section>
      </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>

{{--     <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi Terima Barang</h4>
      </div>
        {!! Form::model($bundle, ['route' => ['bundles.recieve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      <div class="form-group">
          {{ Form::label('nota','Nomor Nota Supermarket') }}
          {{ Form::text('nota','', ['class' => 'form-control', 'required']) }}
          @if ($errors->has('nota'))
                    <div class="help-block text-red">
                      {{ $errors->first('nota') }}
                    </div>
          @endif
      </div>
      {{ Form::hidden('id', $bundle->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-bbundleed table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="25%">Jumlah</th>
              <th width="15%">Harga</th>
              <th width="15%">diterima</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ Form::number('received[]', 0, ['class' => 'form-control','min'=> 0]) }}
                  @if ($errors->has('received'))
                            <div class="help-block text-red">
                              {{ $errors->first('received') }}
                            </div>
                  @endif
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div>

    <div id="myRev" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi draft</h4>
      </div>
        {!! Form::model($bundle, ['route' => ['bundles.approve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      {{ Form::hidden('nota','approved', ['class' => 'form-control', 'required']) }}
      
      {{ Form::hidden('id', $bundle->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-bbundleed table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="15%">Jumlah</th>
              <th width="25%">Harga</th>
              <th width="25%">Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            @php( $totals=0)
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ $detail->subTotalLabel}}</td>
              
            </tr>
            @endforeach
            <tr><td colspan="5" align="center">Total:</td><td>{{ $bundle->totalLabel}}</td></tr>
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div> --}}
  <!-- /.row -->
<section class="col-md-12">
	{{ $gallery->links() }}
</section>
</div>
@stop
@push('scripts')

<script src="{{asset('slick/jquery-2.2.0.min.js')}}" type="text/javascript"></script>
<script src="{{asset('slick/slick.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	$(document).on('ready', function() {
		$(".regular").slick({
			dots: false,
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 4
		});
	});
</script>