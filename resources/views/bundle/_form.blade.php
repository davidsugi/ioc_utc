@extends('layouts.app')
@section('title')
{{ $bundle->exists ? 'Ubah' : 'Tambah' }} Paket
@endsection
@if($bundle->exists)
@section('actionbtn')
<a data-href="{{ route('bundles.destroy', $bundle->id) }}" class="btn btn-danger destroy">Hapus Paket</a>
@endsection
@endif
@section('breadcrumb') 
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="{{ route('bundles.index')}}"><i class="fa fa-star"></i> Paket</a></li>
<li><a href="#" class="active">{{ $bundle->exists ? 'Ubah' : 'Tambah' }} Paket</a></li>
@endsection


@section('content')
<div class="row">
<div class="col-xs-10">
<div class="box">
<div class="box-body">
@if($bundle->exists)
{!! Form::model($bundle, ['route' => ['bundles.update', $bundle->id], 'method'=>'PATCH', 'files' => true]) !!}
@else
{!! Form::model($bundle, ['route' => ['bundles.store'], 'class' => 'col s12', 'files' => true]) !!}
@endif
<div class="form-group">
{{ Form::label('name','Nama Paket') }}
{{ Form::text('name', $bundle->name, ['class' => 'form-control', 'required']) }}
@if ($errors->has('name'))
<div class="help-block text-red">
{{ $errors->first('name') }}
</div>
@endif
</div>

{{--  <div class="form-group">
{{ Form::label('weekend_price','Harga weekend') }}
{{ Form::number('weekend_price', $bundle->weekend_price, ['class' => 'form-control', 'required']) }}
@if ($errors->has('weekend_price'))
<div class="help-block text-red">
{{ $errors->first('weekend_price') }}
</div>
@endif
</div>  --}}

            <div class="form-group col-md-3">
              <label>Harga Weekday(Menginap)</label>
              {!! Form::number('price', $bundle->price, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-3">
              <label>Harga Weekend(Menginap)</label>
              {!! Form::number('weekend_price', $bundle->weekend_price, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-3">
              <label>Harga Weekday Tidak Menginap</label>
              {!! Form::number('no_stay_weekday_price', $bundle->no_stay_weekday_price, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-3">
              <label>Harga Weekend Tidak Menginap</label>
              {!! Form::number('no_stay_weekend_price', $bundle->no_stay_weekend_price, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
<div class="row">
<div class="form-group col-md-6">
{{ Form::label('minimum','Minimum Orang') }}
{{ Form::number('minimum', $bundle->minimum ? $bundle->minimum : 0 , ['class' => 'form-control', 'required']) }}
@if ($errors->has('minimum'))
<div class="help-block text-red">
{{ $errors->first('minimum') }}
</div>
@endif
</div>
<div class="form-group col-md-6">
{{ Form::label('capacity','Kapastias Orang') }}
{{ Form::number('capacity', $bundle->capacity ? $bundle->capacity : 0 , ['class' => 'form-control']) }}
@if ($errors->has('capacity'))
<div class="help-block text-red">
{{ $errors->first('capacity') }}
</div>
@endif
</div>

          <div class="form-group col-md-4" style="padding-top:30px">
            {{ Form::checkbox('frontend', 1, $bundle->frontend==1 ? true : false , ['class' => 'field']) }}
            
            <label>
              Tampilkan pada halaman depan?
            </label>
          </div>
          <div class="form-group col-md-4" style="padding-top:30px">
            {{ Form::checkbox('active', 1, $bundle->active==1 ? true : false , ['class' => 'field']) }}
            
            <label>
              Aktif?
            </label>
          </div>

<div class="form-group col-md-4" style="padding-top:30px">
    {{ Form::checkbox('per_person', 1,$bundle->per_person == 1 ? true : false , ['class' => 'field']) }}
    {{ Form::label('per_person','Per orang?') }}
    
    {{--  {{ Form::checkbox('per_person', $bundle->per_person, ['class' => 'form-control', 'required']) }}  --}}
    @if ($errors->has('per_person'))
        <div class="help-block text-red">
            {{ $errors->first('per_person') }}
        </div>
    @endif
</div>
</div>
          @if(!$bundle->exists)
          <div class="form-group">
            <label>Gambar</label>            
            <input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple/>
          </div>
          <div class="col-md-12">
            <div class="row" id="image_preview"></div>
          </div>
          @endif
          <div class="form-group">
            {{ Form::label('description','Deskripsi') }}
            {{ Form::textarea('description', $bundle->description, ['class' => 'form-control note']) }}
            @if ($errors->has('description'))
              <div class="help-block text-red">
                {{ $errors->first('description') }}
              </div>
            @endif
          </div>
          
{{--  
<div class="form-group">
{{ Form::label('detail[]','Fasiitas/ Tambahan') }}
{{ Form::select('detail[]',empty($details) ? array() : $details , $bundle->bundle_detail->pluck("id_helper_label"), ['class' => 'form-control select2', 'multiple'=>'multiple']) }}
@if ($errors->has('detail'))
<div class="help-block text-red">
{{ $errors->first('detail') }}
</div>
@endif
</div>  --}}


          <div class="row">
            <div class="col-md-12">
              <table class="table pelanggaran-table">
                <thead>
                  <tr>
                    <th style="width: 30%;">Item</th>
                    {{--  <th style="width: 30%;">Amount</th>  --}}
                    <th style="width: 10%;"></th>
                  </tr>
                </thead>
                <tbody>
                {{--  {{ json_encode($facs) }}  --}}
                <tr class="pelanggaran" style="display:none">
                    <td> 
                  @php($i=0)                      
                      <select class="form-control select2 select2a detail" id="det-{{ $i }}" name="detail[]">
                          @foreach($facs as $k => $v)
                            <option value="{{ $k }}">{{ $v }}</option>
                          @endforeach
                        </select>
                       @if ($errors->has("det-".$i))
                        <div class="help-block text-red">
                          <font size="2">{{ $errors->first("det-".$i) }}</font>
                        </div>
                      @endif
                    </td>
                    {{--  <td> {{ Form::number('amount[]', $bundle->amount, ['class' => 'form-control amount', 'required']) }} </td>  --}}
                    <td>
                      <a href="" class="btn btn-danger delete_line"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>

                @if(sizeof($bundle->bundle_detail)>0)
                  @foreach($bundle->bundle_detail as $i=>$fac )
                  @php($i++)
                  <tr class="pelanggaran">
                    {!! Form::hidden('ids[]', isset ($ids) ? $ids[$i] : '', ['class'=> 'form-control hiddens']) !!}
                    {{--  <td>{!! Form::select('facil[]', $dets, $fac->id, ['required' => 'required', 'class'=> 'form-control select2 facs', 'id'=>$i]) !!}</td>  --}}
                    <td>  <select class="form-control select2 select2a detail" id="det-{{ $i }}" name="detail[]">
                          @foreach($facs as $k => $v)
                            <option value="{{ $k }}" {{ $k==$fac->id_helper_label ? 'selected' : '' }}>{{ $v }}</option>
                          @endforeach
                        </select>
                    </td>
                    {{--  <td> {{ Form::text('detail[]', $fac->id_helper_label, ['class' => 'form-control detail', 'required']) }} </td>  --}}
                     {{--  <td> {{ Form::number('amount[]', $fac->amount, ['class' => 'form-control amount', 'required']) }} </td>  --}}
                    <td>
                    <a href="" class="btn btn-danger delete_line"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  @endforeach
                @endif
                
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="1"></td>
                    <td><a href="" class="btn btn-success add_line"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
<div class="box-footer">
{{ Form::submit('Simpan', ['class' => 'btn btn-success']) }}
</div>
{{ Form::close() }}
</div>
</div>
</div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
{{ csrf_field() }}
<input type="hidden" name="_method" value="DELETE">
</form>
<script src="{{ asset('js/summernote.min.js') }}"></script>

<script type="text/javascript">
    function preview_images() 
  {
   var total_file=document.getElementById("images").files.length;
   for(var i=0;i<total_file;i++)
   {
    $('#image_preview').append("<div class='col-md-3' style='padding-top:20px'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
  }
}
$(document).ready(function() {

$('.destroy').click(function() {
if(confirm('Apakah anda yakin?') ) {
$('#destroy-form').attr('action',$(this).data('href'));
$('#destroy-form').submit();
}
});

  $(".note").summernote();  
   $('.add_line').click(function(e) {
              e.preventDefault();
              var pelanggaran_tr = $('.pelanggaran-table tbody tr:first').clone();
              var next=parseInt($('.pelanggaran-table tbody tr:last').find('select.select2').attr('id'))+1;
              pelanggaran_tr.show();              
              pelanggaran_tr.find('.delete_line').show();
              pelanggaran_tr.find('.select2-container').remove();
              pelanggaran_tr.find('select.select2').val("");
              pelanggaran_tr.find('select.select2').attr("id",next);
              pelanggaran_tr.find('.hiddens').val("");
              pelanggaran_tr.find('.detail').val("");
              pelanggaran_tr.find('.amount').val(1);
              pelanggaran_tr.find('select.select2').select2({
                'width': '100%',
                allowClear: true,
                placeholder: "Pilih Fasilitas/Tambahan",
                  ajax: {
                    url: '{{ route('api.get.forbundle')}}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                      return {
                        results:  $.map(data, function (value,i) {
                              
                              return {
                                  text: value,
                                  id: i
                              }

                          })
                      };
                    },
                    cache: true
                  }
              });
             
              pelanggaran_tr.find('.violation_point').val('');
              $('.pelanggaran-table tbody').append(pelanggaran_tr);
                  $(".amount").on("iup change",function (e){
                    total();
                });
               $('.facs').on("select2:selecting", function(e) {
                  var next=parseInt($(this).attr('id'))+1;
                  if($('#'+next).length >0 ){
                    $('#'+next).select2("open");
                  }
                  else{
                    $('.add_line').trigger("click");
                    $('#'+next).select2("open");
                  }
              });

            });
            $('.pelanggaran-table tbody').on('click', '.delete_line', function (e) {
              e.preventDefault();
              $(this).closest('.pelanggaran').remove();
            });
$('.select2').select2({
'width': '100%',
                allowClear: true,

 placeholder: "Pilih Fasilitas/Tambahan", 
ajax: {
url: '{{ route('api.get.forbundle')}}',
dataType: 'json',
delay: 250,
processResults: function (data) {
return {
results:  $.map(data, function (value, i) {
return {
text: value,
id: i
}
})
};
},
cache: true
}
});
});


</script>
@endpush
