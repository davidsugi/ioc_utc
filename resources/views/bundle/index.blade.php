@extends('layouts.app')
@section('title')
Manage Paket
@endsection
@section('breadcrumb') 
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="{{ route('bundles.index')}}" class="active"><i class="fa fa-star"></i> Paket</a></li>
@endsection

@section('content')
<div class="row">
    <form method="get" id="search-form">
        <div class="col-md-6">
            <div class="input-group no-border">
                <input type="text" class="form-control column-filter" name="filter[name]" placeholder="Nama Paket" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary btn-block">Cari</button>
        </div>
    </form>
</div>
<br>
<div class="row">
<div class="col-xs-12">
<div class="box box-danger">
<div class="box-header">
<a href="{{ route('bundles.create') }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Paket</button></a>
</div>
<div class="box-body">
<table id="example1" class="table table-bordered table-hover">
<thead>
<tr>
<th width="5%">No</th>
<th width="25%">Nama</th>
<th width="15%">Harga Weekday</th>
<th width="15%">Harga Weekend</th>
<th width="25%" colspan="2">Action</th>
</tr>
</thead>
<tbody>
@foreach( $bundles as $key => $bundle )
<tr height="10">
<td>{{ $key+1 }}</td>
<td>
{{ $bundle->name }}
{{--  <span data-toggle="collapse" data-target="#demo{{ $key }}" class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
</span>  --}}
{{--  
<div id="demo{{ $key }}" class="collapse">
<table>
<thead>
    <th width="5%">Tipe</th>
    <th width="10%">Nama</th>
<tthr>
@foreach($bundle->bundle_detail as $bund )
<tr>
    <td>{{ $bund->type_label }}</td>
    <td>{{ $bund->name_label }}</td>
<tr>
@endforeach
</table>
</div>  --}}
</td>
<td>{{ H::rupiah($bundle->price) }}</td>
<td>{{ H::rupiah($bundle->weekend_price) }}</td>
<td><a href="{{ route('bundles.edit',['id'=>$bundle->id]) }}"class="btn btn-warning btn-block">Edit</a>
<td><a href="{{ route('bundles.show',['id'=>$bundle->id]) }}"class="btn btn-primary btn-block">Show</a>

</td>
</tr>
@endforeach
@if($bundles->count() == 0)
<tr>
<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
</tr>
@endif
</tbody>
</table>
{{ $bundles->links() }}
</div>
</div>
</div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
{{ csrf_field() }}
<input type="hidden" name="_method" value="DELETE">
</form>
<script>
$(document).ready(function() {
$('.destroy').click(function() {
if(confirm('Apakah anda yakin?') ) {
$('#destroy-form').attr('action',$(this).data('href'));
$('#destroy-form').submit();
}
});
});
</script>
@endpush
