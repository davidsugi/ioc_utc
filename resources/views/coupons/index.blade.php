@extends('layouts.app')
@section('title')
Manage Coupon
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-ticket'></i> Kupon</a></li>
@endsection
@section('content')
<style>
    input[type="date"]:before {
    content: attr(placeholder) !important;
    color: #aaa;
    margin-right: 0.5em;
    }
    input[type="date"]:focus:before,
    input[type="date"]:valid:before {
    content: "";
    }
</style>
<div class="row">
    <form method="get" id="search-form">
        <div class="col-md-3">
            <div class="input-group no-border">
                <input type="text" class="form-control column-filter" name="filter[code]" placeholder="kode" value="{{ !empty($filter['code']) ? $filter['code'] : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group no-border">
                <input type="date" class="form-control column-filter" name="filter[start]" placeholder="start" value="{{ !empty($filter['start']) ? $filter['start'] : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group no-border">
                <input type="date" class="form-control column-filter" name="filter[expiry]" placeholder="expiry" value="{{ !empty($filter['expiry']) ? $filter['expiry'] : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-3">
            <button type="submit" class="btn btn-primary btn-block">Cari</button>
        </div>
    </form>
</div>
<br>
<div class="row">
<div class="col-xs-12">
<div class="box box-danger">
<div class="box-header">
<a href="{{ route('coupons.create') }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Coupon</button></a>
</div>
<div class="box-body">
<table id="example1" class="table table-bordered table-hover">
<thead>
<tr>
<th width="5%">No</th>
<th width="25%">kode</th>
<th width="25%">Tanggal Mulai</th>
<th width="25%">Tanggal Kadaluarsa</th>
<th width="25%">deskripsi</th>
<th width="25%" colspan="2">Action</th>
</tr>
</thead>
<tbody>
@foreach( $coupons as $key => $coupon )
<tr height="10">
<td>{{ $key+1 }}</td>
<td>{{ $coupon->code }}</td>
<td>{{ $coupon->startLabel }}</td>
<td>{{ $coupon->expiryLabel }}</td>
<td>{{ $coupon->description }}</td>
<td><a href="{{ route('coupons.edit',['id'=>$coupon->id]) }}" class="btn btn-warning btn-block">Edit</a></td>
<td><a href="{{ route('coupons.show',['id'=>$coupon->id]) }}" class="btn btn-primary btn-block">Show</span></a></td>

</tr>
@endforeach
@if($coupons->count() == 0)
<tr>
<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
</tr>
@endif
</tbody>
</table>
</div>
</div>
</div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
{{ csrf_field() }}
<input type="hidden" name="_method" value="DELETE">
</form>
<script>
$(document).ready(function() {
$('.destroy').click(function() {
if(confirm('Apakah anda yakin?') ) {
$('#destroy-form').attr('action',$(this).data('href'));
$('#destroy-form').submit();
}
});
});
</script>
@endpush
