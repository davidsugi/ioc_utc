@extends('layouts.app')

@section('title')
coupon No. {{ strtoupper($coupon->id) }}
@endsection
@section('actionbtn')

@endsection

@section('content')
<div class="row">
<div class="col-md-12">
<div class="box">

<div class="box-body">
<div class="col-md-4">
            <div class="inner">
              <h6>Valid date</h6>
              <h3>{{ $coupon->startlabel }} ~ {{ $coupon->expiryLabel }}</h3>
            </div>
    </div>
    <div class="col-md-4">
            <div class="inner">
              <h6>Coupon Code</h6>            
              <h3>{{ $coupon->code }}</h3>            
            </div>
    </div>
    <div class="col-md-4">
            <div class="inner">
              <h6>Diskon</h6>            
              <h3>{{ $coupon->diskonLabel }}</h3>             
            </div>
    </div> 
    <div class="col-md-3">
            <div class="inner">
              <h6>Minimum Transaksi</h6>            
              <h3>{{ H::rupiah($coupon->min_spend) }}</h3>             
            </div>
    </div> 
    <div class="col-md-3">
            <div class="inner">
              <h6>Maksimum Transaksi</h6>            
              <h3>{{ H::rupiah($coupon->max_spend) }}</h3>             
            </div>
    </div> 
</div>

</div>
</div>
</div>




 <div class="row">
 <div class="col-xs-6">
  <div class="box box-danger">
  <div class="box-header">
    Include List
  </div>
 <div class="box-body">
 <table id="primary" class="table table-bordered table-hover">
 <thead>
 <tr>
 <th width="10%">Kategori</th>
 <th width="10%">Nama</th>
 </tr>
 </thead>
 <tbody>
 @foreach( $coupon->includeList as $key => $value )
 <tr height="10">
 <td>{{ $key }}</td>
 <td>{{ $value }}</td>
 </tr>
 @endforeach
 </tbody>
 </table>
 </div>
 </div>
 </div>

  <div class="col-xs-6">
  <div class="box box-danger">
  <div class="box-header">
    Exclude List
  </div>
 <div class="box-body">
 <table id="primary" class="table table-bordered table-hover">
 <thead>
 <tr>
 <th width="10%">Kategori</th>
 <th width="10%">Nama</th>
 </tr>
 </thead>
 <tbody>
 @foreach( $coupon->excludeList as $key => $value )
 <tr height="10">
 <td>{{ $key }}</td>
 <td>{{ $value }}</td>
 </tr>
 @endforeach
 </tbody>
 </table>
 </div>
 </div>
 </div>

 
 </div>
    

{{--     <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi Terima Barang</h4>
      </div>
        {!! Form::model($coupon, ['route' => ['coupons.recieve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      <div class="form-group">
          {{ Form::label('nota','Nomor Nota Supermarket') }}
          {{ Form::text('nota','', ['class' => 'form-control', 'required']) }}
          @if ($errors->has('nota'))
                    <div class="help-block text-red">
                      {{ $errors->first('nota') }}
                    </div>
          @endif
      </div>
      {{ Form::hidden('id', $coupon->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-bcouponed table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="25%">Jumlah</th>
              <th width="15%">Harga</th>
              <th width="15%">diterima</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ Form::number('received[]', 0, ['class' => 'form-control','min'=> 0]) }}
                  @if ($errors->has('received'))
                            <div class="help-block text-red">
                              {{ $errors->first('received') }}
                            </div>
                  @endif
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div>

    <div id="myRev" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi draft</h4>
      </div>
        {!! Form::model($coupon, ['route' => ['coupons.approve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      {{ Form::hidden('nota','approved', ['class' => 'form-control', 'required']) }}
      
      {{ Form::hidden('id', $coupon->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-bcouponed table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="15%">Jumlah</th>
              <th width="25%">Harga</th>
              <th width="25%">Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            @php( $totals=0)
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ $detail->subTotalLabel}}</td>
              
            </tr>
            @endforeach
            <tr><td colspan="5" align="center">Total:</td><td>{{ $coupon->totalLabel}}</td></tr>
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div> --}}
  <!-- /.row -->
@endsection
