@extends('layouts.app')

@section('title')
{{ $coupon->exists ? 'Ubah' : 'Tambah' }} Kupon
@endsection
@if($coupon->exists)
@section('actionbtn')
<a data-href="{{ route('coupons.destroy', $coupon->id) }}" class="btn btn-danger destroy">Hapus Kupon</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("coupons.index")}}'><i class='fa fa-ticket'></i> Kupon</a></li>
<li><a href='#' class='active'>{{ $coupon->exists ? 'Ubah' : 'Tambah'}} Kupon</a></li>
@endsection
@section('content')
<div class="row">
<div class="col-xs-10">
<div class="box">
<div class="box-body">
@if($coupon->exists)
{!! Form::model($coupon, ['route' => ['coupons.update', $coupon->id], 'method'=>'PATCH', 'files' => true]) !!}
@else
{!! Form::model($coupon, ['route' => ['coupons.store'], 'class' => 'col s12', 'files' => true]) !!}
@endif

<div class="form-group">
{{ Form::label('code','Kode') }}
{{ Form::text('code', $coupon->code, ['class' => 'form-control', 'required']) }}
@if ($errors->has('code'))
<div class="help-block text-red">
{{ $errors->first('code') }}
</div>
@endif
</div>
<div class="form-group">
{{ Form::label('description','Deskripsi') }}
{{ Form::textarea('description', $coupon->description, [ 'class' => 'form-control']) }}
@if ($errors->has('description'))
<div class="help-block text-red">
{{ $errors->first('description') }}
</div>
@endif
</div>
<div class="form-group">
{{ Form::label('type','Tipe diskon') }}
{{ Form::select('type', array(1=> 'Diskon Persentase',2 => 'Diskon Fix') , $coupon->type, ['class' => 'form-control select2', 'required']) }}
@if ($errors->has('type'))
<div class="help-block text-red">
{{ $errors->first('type') }}
</div>
@endif

<div class="form-group">
{{ Form::label('amount','Nilai Kupon') }}
{{ Form::number('amount', $coupon->amount, ['min'=>0, 'class' => 'form-control', 'required']) }}
@if ($errors->has('amount'))
<div class="help-block text-red">
{{ $errors->first('amount') }}
</div>
@endif
</div>
<div class="form-group">
{{ Form::label('start','Tanggal Berlaku') }}
{{ Form::date('start', $coupon->start==null ? date('Y-m-d') : $coupon->start, ['class' => 'form-control', 'required']) }}
@if ($errors->has('start'))
<div class="help-block text-red">
{{ $errors->first('start') }}
</div>
@endif
</div>

<div class="form-group">
{{ Form::label('expiry','Tanggal Berakhir') }}
{{ Form::date('expiry', $coupon->expiry==null ? date('Y-m-d') : $coupon->expiry, ['class' => 'form-control', 'required']) }}
@if ($errors->has('expiry'))
<div class="help-block text-red">
{{ $errors->first('expiry') }}
</div>
@endif
</div>

<div class="form-group">
{{ Form::label('max_spend','Harga Maksimum') }}
{{ Form::number('max_spend', $coupon->max_spend, ['class' => 'form-control','min'=>0, 'value'=>0]) }}
@if ($errors->has('max_spend'))
<div class="help-block text-red">
{{ $errors->first('max_spend') }}
</div>
@endif
</div>
<div class="form-group">
{{ Form::label('min_spend','Harga Minimum') }}
{{ Form::number('min_spend', $coupon->min_spend, ['class' => 'form-control','min'=>0, 'value'=>0]) }}
@if ($errors->has('min_spend'))
<div class="help-block text-red">
{{ $errors->first('min_spend') }}
</div>
@endif
</div>

<div class="form-group">
{{ Form::label('indiv_use','Tidak Boleh digabung dengan kupon lain?') }}
{{ Form::checkbox('indiv_use',1,$coupon->indiv_use==null||$coupon->indiv_use==0 ? false:true) }}
@if ($errors->has('indiv_use'))
<div class="help-block text-red">
{{ $errors->first('indiv_use') }}
</div>
@endif
</div>
<div class="form-group">
{{ Form::label('exclude_sale','Hanya Boleh digunakan bila tidak ada barang diskon?') }}
{{ Form::checkbox('exclude_sale',1, $coupon->exclude_sale==null||$coupon->exclude_sale==0 ? false:true ) }}
@if ($errors->has('exclude_sale'))
<div class="help-block text-red">
{{ $errors->first('exclude_sale') }}
</div>
@endif
</div>
<div class="form-group">
{{ Form::label('facility','Fasilitas') }}
{{ Form::select('facility[]', array() ,old('facility') ? old('facility')  : $coupon->facility, ['class' => 'form-control fas','multiple']) }}
@if ($errors->has('facility'))
<div class="help-block text-red">
{{ $errors->first('facility') }}
</div>
@endif
</div>

<div class="form-group">
{{ Form::label('ex_facil','Kecuali Fasilitas') }}
{{ Form::select('ex_facil[]', array() , old('ex_facil') ? old('ex_facil')  : $coupon->ex_facil, ['class' => 'form-control fas','multiple']) }}
@if ($errors->has('ex_facil'))
<div class="help-block text-red">
{{ $errors->first('ex_facil') }}
</div>
@endif
</div>

<div class="form-group">
{{ Form::label('ftype','Tipe Fasilitas') }}
{{ Form::select('ftype[]', array() , old('ftype') ? old('ftype')  : $coupon->ftype, ['class' => 'form-control type','multiple']) }}
@if ($errors->has('ftype'))
<div class="help-block text-red">
{{ $errors->first('ftype') }}
</div>
@endif
</div>
<div class="form-group">
{{ Form::label('ex_ftype','Kecuali Tipe Fasilitas') }}
{{ Form::select('ex_ftype[]', array() , old('ex_ftype') ? old('ex_ftype')  : $coupon->ex_ftype, ['class' => 'form-control type','multiple']) }}
@if ($errors->has('ex_ftype'))
<div class="help-block text-red">
{{ $errors->first('ex_ftype') }}
</div>
@endif
</div>

<div class="form-group">
{{ Form::label('user','Pelanggan') }}
{{ Form::select('user[]', array() , old('user') ? old('user')  : $coupon->user, ['class' => 'form-control cus','multiple']) }}
@if ($errors->has('user'))
<div class="help-block text-red">
{{ $errors->first('user') }}
</div>
@endif
</div>

<div class="form-group">
{{ Form::label('ex_user','Kecuali Pengguna') }}
{{ Form::select('ex_user[]', array() ,  old('ex_user') ? old('ex_user')  :$coupon->ex_user, ['class' => 'form-control cus','multiple']) }}
@if ($errors->has('ex_user'))
<div class="help-block text-red">
{{ $errors->first('ex_user') }}
</div>
@endif
</div>

<div class="form-group">
{{ Form::label('role','Kelompok Pelanggan') }}
{{ Form::select('role[]', array() , old('role') ? old('role')  : $coupon->role, ['class' => 'form-control role','multiple']) }}
@if ($errors->has('role'))
<div class="help-block text-red">
{{ $errors->first('role') }}
</div>
@endif
</div>

<div class="form-group">
{{ Form::label('ex_role','Kecuali Kelompok Pelanggan') }}
{{ Form::select('ex_role[]', array() ,old('ex_role') ? old('ex_role')  : $coupon->ex_role, ['class' => 'form-control role','multiple']) }}
@if ($errors->has('ex_role'))
<div class="help-block text-red">
{{ $errors->first('ex_role') }}
</div>
@endif
</div>
       




</div>
<div class="box-footer">
{{ Form::submit('Simpan', ['class' => 'btn btn-success']) }}
</div>
{{ Form::close() }}
</div>
</div>
</div>
</div>
@endsection
@include('layouts._deletebtn')

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $(".select2").select2();

    $('.fas').select2({
    'width': '100%',
     placeholder: "Pilih Fasilitas", 
    ajax: {
    url: '{{ route('api.get.buyable')}}',
    dataType: 'json',
    delay: 250,
    processResults: function (data) {
    return {
    results:  $.map(data, function (item) {
    return {
    text: item.name,
    id: item.id
    }
    })
    };
    },
    cache: true
    }
    }); 
    $('.fas').trigger("change");

    $('.cus').select2({
    'width': '100%',
     placeholder: "Pilih Pelanggan", 
    ajax: {
    url: '{{ route('api.get.customer')}}',
    dataType: 'json',
    delay: 250,
    processResults: function (data) {
    return {
    results:  $.map(data, function (value, key) {
    return {
    text: value,
    id: key
    }
    })
    };
    },
    cache: true
    }
    });
    $('.cus').trigger("change");    

    $('.role').select2({
    'width': '100%',
     placeholder: "Pilih Kelompok Pengguna", 
    ajax: {
    url: '{{ route('api.get.customer_tpye')}}',
    dataType: 'json',
    delay: 250,
    processResults: function (data) {
    return {
    results:  $.map(data, function (value, key) {
    return {
    text: value,
    id: key
    }
    })
    };
    },
    cache: true
    }
    });
    $('.role').trigger("change");    
    

    $('.type').select2({
    'width': '100%',
     placeholder: "Tipe Fasilitas", 
    ajax: {
    url: '{{ route('api.get.facility_type')}}',
    dataType: 'json',
    delay: 250,
    processResults: function (data) {
    return {
    results:  $.map(data, function (value, key) {
    return {
    text: value,
    id: key
    }
    })
    };
    },
    cache: true
    }
    });
    $('.type').trigger("change");    
    
});
</script>
@endpush
