@extends('layouts.app')

@section('title')
{{ $customer->exists ? 'Ubah' : 'Tambah' }} Customer
@endsection
@if($customer->exists)
@section('actionbtn')
<a data-href="{{ route('customers.destroy', $customer->id) }}" class="btn btn-danger destroy">Hapus Customer</a>
@endsection
@endif


@section('breadcrumb') 
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="{{ route('customers.index')}}"><i class="fa fa-users"></i> Customer</a></li>
<li><a href="#" class="active">{{ $customer->exists ? 'Ubah' : 'Tambah' }} Customer</a></li>
@endsection

@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($customer->exists)
        {!! Form::model($customer, ['route' => ['customers.update', $customer->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($customer, ['route' => ['customers.store'], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
          <div class="form-group col-md-6" style="margin-top:15px">
              {{ Form::label('active','Aktif?') }}
            {{ Form::checkbox('active', 1,$customer->blacklist=="0" ? true : false , ['class' => 'field']) }}
              @if ($errors->has('active'))
                <div class="help-block text-red">
                  {{ $errors->first('active') }}
                </div>
              @endif
            </div>
            
            <div class="form-group col-md-12">
              <label>Nama</label>
              {!! Form::text('name', $customer->name, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-6" style="margin-top:15px">
              {{ Form::label('is_company','Gunakan Nama Perusahaan') }}
            {{ Form::checkbox('is_company', 1,$customer->company_name!="" ? true : false , ['class' => 'field', 'id'=>'chk']) }}
              @if ($errors->has('is_company'))
                <div class="help-block text-red">
                  {{ $errors->first('is_company') }}
                </div>
              @endif
            </div>
            
            <div class="form-group col-md-6" id="nama_perusahaan" style="visibility:{{ $customer->company_name!="" ? '' : 'hidden' }}">
              {{ Form::label('company_name','Nama Perusahaan') }}
              {{ Form::text('company_name', $customer->company_name, ['class' => 'form-control' ,"id"=>"company_name"]) }}
              @if ($errors->has('company_name'))
                <div class="help-block text-red">
                  {{ $errors->first('company_name') }}
                </div>
              @endif
            </div>
            <div class="form-group col-md-4">
              <label>Tipe</label>
              {!! Form::select('customer_type_id', $customer_type, $customer->customer_type_id, ['placeholder'=>'Tipe','class'=> 'form-control select2','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-4">
              <label>Bidang Usaha</label>
              {!! Form::select('category', array('2'=>'Swasta/ Perusahaan','3'=>'BUMN/Bank/Dinas','4'=>'Pendidikan','1'=>'Pribadi'), $customer->category, ['placeholder'=>'Bidang Usaha','class'=> 'form-control select2','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-4">
              {{ Form::label('job','Pekerjaaan') }}
              {{ Form::text('job', $customer->job, ['class' => 'form-control']) }}
              @if ($errors->has('job'))
                <div class="help-block text-red">
                  {{ $errors->first('job') }}
                </div>
              @endif
            </div>
            
            <div class="form-group col-md-12">
              <label>Email</label>
              {!! Form::email('email', $customer->email, ['class'=> 'form-control']) !!}
              @if ($errors->has('email'))
              <div class="help-block text-red">
                {{ $errors->first('email') }}
              </div>
              @endif
            </div>
            <div class="form-group col-md-12">
              <label>Alamat</label>
              {!! Form::text('address', $customer->address, ['class'=> 'form-control']) !!}
            </div>
            <div class="form-group col-md-12">
              {{ Form::label('province_id','Provinsi') }}
              {{ Form::select('province_id',  empty($provinces) ? array() : $provinces , $customer->province_id, ['class' => 'form-control select2 prov', 'required']) }}
              @if ($errors->has('province_id'))
                <div class="help-block text-red">
                  {{ $errors->first('province_id') }}
                </div>
              @endif
            </div>
            
            <div class="form-group col-md-12">
              <label>Kota</label>
              {!! Form::select('city_id', $city, $customer->city_id, ['placeholder'=>'Kota','class'=> 'form-control select2 city','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Kode Pos</label>
              {!! Form::text('zip', $customer->zip, ['class'=> 'form-control']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>No Identitas</label>
              {!! Form::number('card_id', $customer->card_id, ['class'=> 'form-control' ]) !!}
                @if ($errors->has('card_id'))
                <div class="help-block text-red">
                  {{ $errors->first('card_id') }}
                </div>
              @endif
            </div>
            <div class="form-group col-md-12">
              <label>Telpon</label>
              {!! Form::text('phone', $customer->phone, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script src="{{ asset('AdminLTE-2.4.2/bower_components/jquery/dist/jquery.min.js') }}"></script> 

<script type="text/javascript">

  $(document).ready(function(){
    $("#chk").on("change",function(e){
        if ($(this).is(':checked')) {
          $("#nama_perusahaan").css("visibility", "visible");
        }
        else{
          $("#nama_perusahaan").css("visibility", "hidden");

        }
    });
    $('.city').select2({
      'width': '100%',
       placeholder: "Pilih Kota", 
        ajax: {
          url: '{{ route('api.get.kota')}}',
          dataType: 'json',
          delay: 250,
          data: function (params) {
            var queryParameters = {
              q: params.term,
              p: $(".prov").val(),
            }

            return queryParameters;
          },
          processResults: function (data) {
          return {
            results:  $.map(data, function (value, key) {
            return {
              text: value,
              id: key
              }
            })
          };
        },
        cache: true
      }
    });

    $('.prov').select2({
      'width': '100%'});
    //init();

//isi kota ke database
function init() {
  $.ajax({
    type: 'GET',
    dataType: 'text',
    url: '{{route("API.getCity")}}', 
    success: function(data) {
      alert(data);

    }
  });
}
});

</script>
</section>
@endsection
@include('layouts._deletebtn')

