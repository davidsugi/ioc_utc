@extends('layouts.app')

@section('title')
Detail Customer: {{ $customer->name }}
@endsection
@section('actionbtn')

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("customers.index")}}'><i class='fa fa-users'></i> Customer</a></li>
<li><a href='#' class='active'>Detail Customer</a></li>
@endsection
@section('content')
<style>
  .inner>p{
    font-size:150%;
  }
  .inner>h3{
    font-size:250%;
  }
</style>    
      <div class="row">
          <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-body">
                        <div class="col-xs-6">
                          <div class="col-xs-12">
                              <h4>Name</h4>
                              <h3> {{$customer->name}}</h3>
                          </div>
                          <div class="col-xs-6">
                              <h4>Pekerjaan</h4>
                              <h3> {{$customer->job}}</h3>
                          </div>
                          @if($customer->company_name!="")
                            <div class="col-xs-6">
                              <h4>Company Name</h4>
                              <h3> {{$customer->company_name}}</h3>
                          </div>
                          @endif
                        </div>
                         <div class="col-xs-6" style="margin-left: -15px !important;">
                         <div class="col-xs-12">
                              <h4>Phone</h4>
                              <h3> {{$customer->phone}}</h3>
                          </div>
                            <div class="col-xs-12">
                              {{--  <h3>Phone: {{$customer->phone}}</h3>  --}}
                              <h4>Email</h4>
                              <h3> {{$customer->email}}</h3>
                          </div>
                          <div class="col-xs-12">
                              <p>Address</p>
                              <h4>{{$customer->address }}</h4>
                          </div>
                        </div>
                         <div class="col-xs-12">
                           <div class="col-xs-3">
                              <p>Type</p>
                              <h4>{{$customer->customer_type->name}}</h4>
                          </div>
                          <div class="col-xs-3">
                              <p>Kategori</p>
                              <h4>{{$customer->categoryLabel}}</h4>
                          </div>
                          <div class="col-xs-3 col-offset-xs-1">
                              <p>Card ID</p>
                              <h4>{{$customer->card_id}}</h4>
                          </div>

                          

                          <div class="col-xs-3">
                              <h4>Kota</h4>
                              <p>{{ $customer->city ? $customer->city->full_city : "-" }}</p>
                          </div>
                          
                            
                        </div>
                  </div>
               </div>
          </div>
      </div>

      <div class="row">
          <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-header">
                        <h3>Order History</h3>
                  </div>
                  <div class="box-body">
                      <table id="primary" class="table table-bordered table-hover">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th width="5%">status</th>
                                  <th width="10%">Order No.</th>
                                  <th width="10%">Order Date</th>
                                  <th width="10%">Estimated Check in</th>
                                  <th width="10%">Estimated Check out</th>
                              </tr>
                          </thead>
      <tbody>
        @php($total=0)
      @foreach( $customer->order as $key => $detail )
      <tr height="10">
      <td>{{ $key+1 }}</td>
      <td>{!! $detail->spanLabel !!}</td>      
      <td><a href="{{ route('orders.show',['id'=>$detail->id]) }}">{{ $detail->order_code }}</a></td>
      <td>{{ $detail->date->format("d-m-Y") }}</td>
      <td>{{ $detail->check_in_label }}</td>
      <td>{{ $detail->check_out_label }}</td>
      @endforeach
      		@if($customer->order->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
      {{--  <tr>
          <td colspan="2" align="center">Total customer:</td> 
          <td>{{ H::rupiah($total)}}</td> 
      </tr>  --}}
      </tbody>
      </table>
      </div>
      </div>
      </div>
      </div>
      

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>

{{--     <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi Terima Barang</h4>
      </div>
        {!! Form::model($customer, ['route' => ['customers.recieve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      <div class="form-group">
          {{ Form::label('nota','Nomor Nota Supermarket') }}
          {{ Form::text('nota','', ['class' => 'form-control', 'required']) }}
          @if ($errors->has('nota'))
                    <div class="help-block text-red">
                      {{ $errors->first('nota') }}
                    </div>
          @endif
      </div>
      {{ Form::hidden('id', $customer->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-bcustomered table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="25%">Jumlah</th>
              <th width="15%">Harga</th>
              <th width="15%">diterima</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ Form::number('received[]', 0, ['class' => 'form-control','min'=> 0]) }}
                  @if ($errors->has('received'))
                            <div class="help-block text-red">
                              {{ $errors->first('received') }}
                            </div>
                  @endif
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div>

    <div id="myRev" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi draft</h4>
      </div>
        {!! Form::model($customer, ['route' => ['customers.approve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      {{ Form::hidden('nota','approved', ['class' => 'form-control', 'required']) }}
      
      {{ Form::hidden('id', $customer->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-bcustomered table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="15%">Jumlah</th>
              <th width="25%">Harga</th>
              <th width="25%">Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            @php( $totals=0)
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ $detail->subTotalLabel}}</td>
              
            </tr>
            @endforeach
            <tr><td colspan="5" align="center">Total:</td><td>{{ $customer->totalLabel}}</td></tr>
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div> --}}
  <!-- /.row -->
@endsection
