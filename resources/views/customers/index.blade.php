@extends('layouts.app')

@section('title')
Manage Customer
@endsection

@section('breadcrumb') 
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="#" class="active"><i class="fa fa-users"></i> Customer</a></li>
@endsection
@section('content')

<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[name]" placeholder="Nama" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[address]" placeholder="Alamat" value="{{ !empty($filter['address']) ? $filter['address'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[card_id]" placeholder="No. Identitas" value="{{ !empty($filter['card_id']) ? $filter['card_id'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<br>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('customers.create')}}" class="btn btn-success btn-block">Tambah Customer &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>

	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nama</th>
					{{--  <th>Tipe</th>  --}}
					<th>Alamat</th>
					<th>Perusahaan</th>
					<th>Telpon</th>
					<th>Identitas</th>
					{{--  <th>Kota</th>  --}}
					<th>Email</th>
					<th>Edit</th>
					<th>Show</th>
				</tr>
			</thead>
			<tbody>
				@foreach($customer as $c)
				<tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$c->name}}</td>
					{{--  <td>{{$c->customer_type->name}}</td>  --}}
					<td>{{$c->address}}</td>
					<td><center>{{$c->company_name?:"-" }}</center></td>
					<td>{{$c->phone}}</td>
					<td>{{$c->card_id}}</td>
					{{--  <td>{{$c->city->full_city}}</td>  --}}
					<td>{{$c->email}}</td>
					<td>
						<a href="{{route('customers.edit',$c->id)}}">
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
						<td>
						<a href="{{route('customers.show',$c->id)}}">
							<button class="btn btn-primary">
								Show
							</button>
						</a>
					</td>
				</tr>
				@endforeach
				@if($customer->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $customer->appends($filters)->links() }}
</div>
</div>
@stop