@extends('layouts.app')

@section('title')
{{ $detail_facility->exists ? 'Ubah' : 'Tambah' }} List Fasilitas
@endsection
@if($detail_facility->exists)
@section('actionbtn')
<a data-href="{{ route('facilities.detail_facilities.destroy', ['facility'=>$facility->id,'detail_facility'=>$detail_facility->id]) }}" class="btn btn-danger destroy">Hapus List Fasilitas</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("facilities.index")}}'><i class='fa fa-building'></i> Fasilitas</a></li>
<li><a href='{{ route("facilities.show",["id"=>$facility->id])}}'>{{ $facility->name}}</a></li>
<li><a href='#' class='active'>{{ $detail_facility->exists ? 'Ubah' : 'Tambah'}} List Fasilitas {{ $detail_facility->exists ? ": ".$detail_facility->name : ''}}</a></li>
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($detail_facility->exists)
        {!! Form::model($detail_facility, ['route' => ['facilities.detail_facilities.update', 'facility'=>$facility->id, 'detail_facility'=>$detail_facility->id], 'method'=>'PATCH','role' => 'form']) !!}
        @else
        {!! Form::model($detail_facility, ['route' => ['facilities.detail_facilities.store',$facility->id], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Fasilitas</label>
              {!! Form::text('facility_name',$facility->name,['class'=> 'form-control','readonly']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Name</label>
              {!! Form::text('name',$detail_facility->name,['class'=> 'form-control','require'=>'required']) !!}
            </div>
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')


