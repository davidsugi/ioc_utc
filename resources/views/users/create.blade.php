@extends('layouts.app')

@section('title')
{{ $user->exists ? 'Ubah' : 'Tambah' }} User
@endsection
@if($user->exists)
@section('actionbtn')
<a data-href="{{ route('users.destroy', $user->id) }}" class="btn btn-danger destroy">Hapus User</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("users.index")}}'><i class='fa fa-user'></i> User</a></li>
<li><a href='#' class='active'>{{ $user->exists ? 'Ubah' : 'Tambah'}} User</a></li>
@endsection
@section('content')


<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($user->exists)
        {!! Form::model($user, ['route' => ['users.update', $user->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($user, ['route' => ['users.store'], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Nama</label>
              {!! Form::text('name', $user->name, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Email</label>
              {!! Form::email('email', $user->email, ['class'=> 'form-control','required' => 'required']) !!}
              @if ($errors->has('email'))
              <div class="help-block text-red">
                {{ $errors->first('email') }}
              </div>
              @endif
            </div>
            @if(!$user->exists)
              <div class="form-group col-md-12">
                <label>Password</label>
                {!! Form::password('password', ['class'=> 'form-control','required' => 'required']) !!}
                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
              </div>
              <div class="form-group col-md-12">
                <label>Confirm Password</label>
                {!! Form::password('password_confirmation', ['class'=> 'form-control','required' => 'required']) !!}
              </div>
            @endif
            <div class="form-group col-md-12">
              <label>Role</label>
              {!! Form::select('role[]', $roles ,  $user->roles()->pluck('name'),['class'=> 'form-control role','required' => 'required','multiple'=>'multiple']) !!}
            </div>

            <div class="form-group col-md-12">
              {{ Form::label('emailable','Kirim Email Saat Ada Perubahan Status Transaksi?') }}
            {{ Form::checkbox('emailable', 1,$user->emailable==1 ? true : false , ['class' => 'field']) }}              
              {{--  {{ Form::checkobx('emailable', $loopervar->emailable, ['class' => 'form-control', 'required']) }}  --}}
              @if ($errors->has('emailable'))
                <div class="help-block text-red">
                  {{ $errors->first('emailable') }}
                </div>
              @endif
            </div>
            
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')

@push('scripts')
<form id='destroy-form' method="POST">
  {{ csrf_field() }}
  <input type="hidden" name="_method" value="DELETE">
</form>
<script type="text/javascript">
  $(document).ready(function() {
    $('.role').select2({
      'width': '100%',
    });
  });
</script>
@endpush
