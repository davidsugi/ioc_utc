@extends('layouts.app')

@section('title')
Detail User
@endsection

@section('actionbtn')
  <a href="{{ route('users.edit', $user->id) }}" class="btn btn-warning">Edit</a>
  <a href="{{ route('users.change_password_view', $user->id) }}" class="btn btn-info">Ganti Password</a>
  <a data-href="{{ route('users.destroy', $user->id) }}" class="btn btn-danger destroy">Hapus</a>
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='{{ route("users.index")}}'><i class='fa fa-user'></i> User</a></li>

<li><a href='#' class='active'>Detail User</a></li>
@endsection
@push('styles')
<style type="text/css">

</style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Nama</label>
                        <p>{{ $user->name }}</p>
                    </div>
                    <div class="col-md-12">
                        <label>Email</label>
                        <p>{{ $user->email }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-body">
        <table class="table table-striped table-bordered dataTable mt-1">
            <thead>
              <tr>
                <th class="align-middle">No</th>
                <th class="align-middle">Role</th>
              </tr>
            </thead>
            <tbody>

              @foreach($user->roles as $role)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $role->name }}</td>
              </tr>
              @endforeach
              @if($user->roles()->count() == 0)
              <tr>
                <td colspan="2"><center><em>Tidak Ada Data.</em></center></td>
              </tr>
              @endif
            </tbody>
        </table>
            </div>
        </div>
    </div>
</div>

@endsection
@include('layouts._deletebtn')