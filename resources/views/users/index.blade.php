@extends('layouts.app')

@section('title')
Manage User
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-user'></i> User</a></li>
@endsection

@section('content')

<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[name]" placeholder="Nama" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				<select name="filter[role]" class="form-control column-filter">
					@if(!empty($filter['role']))
					<option value="0" {{ ($filter['role'])==0 ? 'selected' : '' }}>All</option>
					<option value="1" {{ ($filter['role'])==1 ? 'selected' : '' }}>Administrator</option>
					<option value="2" {{ ($filter['role'])==2 ? 'selected' : '' }}>Analyst</option>
					<option value="3" {{ ($filter['role'])==3 ? 'selected' : '' }}>Staff</option>
					@else
					<option value="0" >All</option>
					<option value="1" >Administrator</option>
					<option value="2" >Analyst</option>
					<option value="3" >Staff</option>
					@endif
				</select>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<br>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('users.create')}}" class="btn btn-success btn-block">Tambah User &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>

	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nama</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($user as $u)
				<tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$u->name}}</td>
					<td>{{$u->email}}</td>
					<td>
						<a href="{{route('users.show',$u->id)}}">
							<button class="btn btn-primary">
								Show
							</button>
						</a>
						<a href="{{route('users.edit',$u->id)}}">
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
				</tr>
				@endforeach
				@if($user->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $user->appends($filters)->links() }}
</div>
</div>

@stop
