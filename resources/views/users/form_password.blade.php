@extends('layouts.app')

@section('title')
Ubah Password User
@endsection

@section('content')

<section class="form-user">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($user, ['route' => ['users.change_password_store', $user->id], 'method'=>'POST', 'role' => 'form']) !!}
        {{ csrf_field() }}
        <div class="box-body">
          <div class="row">
            
            <div class="form-group col-md-12">
              {!! Form::label('password', 'Password'); !!}
              @if($user->exists)
                <p>Silahkan isi untuk mengubah password.</p>
              @endif
              {!! Form::password('password', ['class'=> 'form-control', 'placeholder' => 'Password', 'rows' => 2]) !!}
              @if ($errors->has('password'))
              <div class="help-block text-red">
                {{ $errors->first('password') }}
              </div>
              @endif
            </div>

            <div class="form-group col-md-12">
              {!! Form::label('password_confirmation', 'Konfirmasi Password'); !!}
              {!! Form::password('password_confirmation', ['class'=> 'form-control', 'placeholder' => 'Konfirmasi password', 'rows' => 2]) !!}
              @if ($errors->has('password_confirmation'))
              <div class="help-block text-red">
                {{ $errors->first('password_confirmation') }}
              </div>
              @endif
            </div>

          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</section>



@endsection

@include('layouts._deletebtn')
