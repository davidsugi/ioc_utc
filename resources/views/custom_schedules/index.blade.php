@extends('layouts.app')

@section('title')
View Jadwal
@endsection
@push('styles')

<!-- fullCalendar -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/fullcalendar/dist/fullcalendar.print.min.css')}}" media="print">
@endpush
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-wrench'></i> Jadwal Lain-lain</a></li>
@endsection
@section('content')
<style>
input[type="date"]:before {
content: attr(placeholder) !important;
color: #aaa;
margin-right: 0.5em;
}
input[type="date"]:focus:before,
input[type="date"]:valid:before {
content: "";
}
</style>
<div class="box">
<div class="box-body no-padding">
	<!-- THE CALENDAR -->
	<div id="calendar"></div>
</div>
</div>
<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="date" class="form-control column-filter" name="filter[startdate]" placeholder="Start: " value="{{ !empty($filter['startdate']) ? $filter['startdate'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
			
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="date" class="form-control column-filter" name="filter[enddate]" placeholder="End: " value="{{ !empty($filter['enddate']) ? $filter['enddate'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<br>
<div class="box">
	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Fasilitas</th>
					<th>Deskripsi</th>     										
					<th>Edit</th>
				</tr>
			</thead>
			<tbody>
				@foreach($custom_schedule as $c)
				<tr>
					<td>{{$loop->iteration }}</td>
					<td>{{$c->carbonstart}}</td>
					<td>{{$c->carbonend}}</td>
					<td>{{$c->detailFacility->name}}</td>
					<td>{{$c->description}}</td>   										
					<td>
						<a href="{{route('detail_facilities.custom_schedules.edit',['detail_facility'=>$c->detailFacility->id,'custom_schedule'=>$c->id])}}">						
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
				</tr>
				@endforeach
				@if($custom_schedule->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $custom_schedule->appends($filters)->links() }}
</div>
</div>

@stop

@push('scripts')
<script>
	$(function () {
		ajaxCalendar();
		function ajaxCalendar() {
			$.ajax({
				type: 'GET',
				datatype: 'json',
				url: '{{route("API.calendar")}}',
				success: function(data) {
					var header = {
						left  : 'prev,next today',
						center: 'title',
						right : 'month,agendaWeek,agendaDay'
					};
					var buttonText = {
						today: 'today',
						month: 'month',
						week : 'week',
						day  : 'day'
					};
					var events =[];
					var timeFormat = 'H(:mm)t';
					for (var i=0; i<data.length; i++){
						var e = {
							title          : data[i][0]+'\nDeskripsi : '+data[i][1],
							start          : $.fullCalendar.moment(data[i][2]),
							end            : $.fullCalendar.moment(data[i][3]),
							backgroundColor: '#f56954',
							borderColor    : '#f56954'
						};
						events.push(e);
					}
					$('#calendar').fullCalendar({
						header,
						buttonText,
						events,
						timeFormat
					})
				}
			})
		}
	})
</script>
@endpush