@extends('layouts.app')

@section('title')
{{ $custom_schedule->exists ? 'Ubah' : 'Tambah' }} Jadwal
@endsection
@if($custom_schedule->exists)
@section('actionbtn')
<a data-href="{{ route('custom_schedules.destroy', $custom_schedule->id) }}" class="btn btn-danger destroy">Hapus Jadwal</a>
@endsection
@endif
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("custom_schedules.index")}}'><i class='fa fa-wrench'></i> Jadwal</a></li>
<li><a href='#' class='active'>{{ $custom_schedule->exists ? 'Ubah' : 'Tambah'}} Jadwal</a></li>

@endsection
@section('content')
<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($custom_schedule->exists)
        {!! Form::model($custom_schedule, ['route' => ['custom_schedules.update', $custom_schedule->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($custom_schedule, ['route' => ['custom_schedules.store'], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Date and time range:</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
                <input type="text" class="form-control" name ="rangedate" id="reservationtime" value="{{$custom_schedule->carbonstart}} - {{$custom_schedule->carbonend}}" required>
              </div>
              <!-- /.input group -->
            </div>
            <div class="form-group col-md-12">
              <label>Fasilitas</label>
              {!! Form::text('detail_facility_id', null, ['placeholder'=>'Fasilitas','class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Deskripsi</label>
              {!! Form::textarea('description', $custom_schedule->description, ['class'=> 'form-control','required' => 'required']) !!}</div>
            </div>
          </div>
          <!-- /.box-body -->
          
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<script>
  $(function () {
    $('#reservationtime').daterangepicker({ timePicker: true, timePicker24Hour: true, timePickerIncrement: 30,locale: {format: 'DD/MM/YYYY H:mm:ss'}})
  })
</script>
@endsection
@include('layouts._deletebtn')
