@extends('layouts.app')
@section('title')
List Setting
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <a href="{{ route('food_settings.create',['type'=>1,'dev'=>ENV('DEV_KEY','')]) }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Setting</button></a>
            </div>
            <div class="box-body">
                <table id="example" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Jenis</th>
                            <th width="25%">Tipe Menu</th>
                            <th width="25%">Tambahan hari(addDays)</th>
                            <th width="25%">Jam Default</th>
                            <th width="25%" colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $menus as $key => $menu )
                       <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $menu[0]==1 ? 'Food' : 'Snack'  }}</td>
                            <td>{{ $menu[1]  }}</td>
                            <td>{{ $menu[2] }}</td>
                            <td>{{ $menu[3] }}</td>
                            <td><a href="{{ route('food_settings.edit',['id'=>$key,'type'=>1]) }}">
                                    <i class="material-icons">mode_edit</i>
                                </a>
                                <a class="destroy" data-href="{{ route('food_settings.destroy',['id'=>$key,'type'=>1]) }}">
                                <i class="material-icons">delete</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    @if(count($menus) == 0)
                        <tr>
                            <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                        </tr>
                    @endif
                    </tbody>
            </table>
        </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <a href="{{ route('food_settings.create',['type'=>0,'dev'=>ENV('DEV_KEY','')]) }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Setting</button></a>
            </div>
            <div class="box-body">
                <table id="example" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Nama</th>
                            <th width="25%" colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $lastdayMenus as $key => $menu )
                      <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $menu[0]==1 ? 'Food' : 'Snack'  }}</td>
                            <td>{{ $menu[2] }}</td>
                            <td>{{ $menu[3] }}</td>
                            <td><a href="{{ route('food_settings.edit',['id'=>$key,'type'=>0]) }}">
                                    <i class="material-icons">mode_edit</i>
                                </a>
                                <a class="destroy" data-href="{{ route('food_settings.destroy',['id'=>$key,'type'=>0]) }}">
                                <i class="material-icons">delete</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    @if(count($lastdayMenus) == 0)
                        <tr>
                            <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                        </tr>
                    @endif
                    </tbody>
            </table>
        </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
