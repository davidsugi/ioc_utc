@extends('layouts.app')

@section('title')
Detail addon: {{ $addon->name }}
@endsection
@section('actionbtn')
@if($addon->type==1||$addon->type==2)
<a href='{{ route("addons.menus.index",$addon->id)}}'  class="btn btn-success success"><i class='fa fa-list'></i> Menu</a>
@endif
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("addons.index")}}'><i class='fa fa-plus'></i> addon</a></li>
<li><a href='#' class='active'>Detail addon</a></li>
@endsection
@section('content')
<style>
  .inner>p{
    font-size:150%;
  }
  .inner>h3{
    font-size:250%;
  }
</style>    
      <div class="row">
          <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-body">
                        <div class="col-xs-6">
                          <div class="col-xs-12">
                              <h4>Name</h4>
                              <h3> {{$addon->name}}</h3>
                          </div>
                     
                        </div>
                         <div class="col-xs-6">
                        @if($addon->limited ==1)
                         <div class="col-xs-12">
                              <h4>Stok</h4>
                              <h3>{{$addon->stock}} {{$addon->unit->name}}</h3>
                          </div>
                          @endif
                            <div class="col-xs-12">
                              {{--  <h3>Phone: {{$addon->phone}}</h3>  --}}
                              <h4>Dipinjam/dipakai</h4>
                              <h3> {{$addon->borrowed}}</h3>
                          </div>
                        </div>
                            <div class="col-xs-12">
                              <div class="col-xs-6">
                                {{--  <h3>Phone: {{$addon->phone}}</h3>  --}}
                                <h4>Harga Weekday</h4>
                                <h3> {{ H::rupiah($addon->price) }}</h3>
                            </div>
                            {{--  <div class="col-xs-3">
                                <h4>Harga Weekend</h4>
                                <h3> {{ H::rupiah($addon->weekend_price) }}</h3>
                            </div>  --}}
                           <div class="col-xs-6">
                                {{--  <h3>Phone: {{$addon->phone}}</h3>  --}}
                                <h4>Harga Weekday Tidak Menginap</h4>
                                <h3> {{ H::rupiah($addon->no_stay_weekday_price) }}</h3>
                            </div>
                            {{--  <div class="col-xs-3">
                                <h4>Harga Weekend Tidak Menginap</h4>
                                <h3> {{ H::rupiah($addon->no_stay_weekend_price) }}</h3>
                            </div>  --}}
                          </div>
                  </div>
               </div>
          </div>
      </div>
      <div class="row">

            <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-header">
                    {{--  <a href="{{ route('special_prices.create',['u'=>'App\\Addon','id'=>$addon->id]) }}" class="btn btn-primary pull-right" >Tambah Special Price</a>  --}}
                      
                    <h3>Order History List</h3>
                  </div>
                  
                  <div class="box-body">
<table id="example1" class="table table-bordered table-striped " >
					 <thead>
                        <tr>
                            <th rowspan="2" width="5%">No</th>
                            {{--  <th rowspan="2" width="15%">status</th>  --}}
                            <th rowspan="2" width="15%">Tanggal</th>
                            <th rowspan="2" width="10%">Jumlah</th>
                            <th colspan="2" width="20%">Harga</th>
                        </tr>
                    </thead>
					<tbody>
                   		 @foreach( $addon->detail_order as $key => $det )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            {{--  <td>{!! $det->order->spanLabel !!}</td>  --}}
                            <td>{{ $det->order->date }}</td>
                            <td>{{ $det->amount }}</td>
                            <td>{{ H::rupiah($det->price) }}</td>
							<td>
						</td>
					</tr>
          @endforeach
          	@if($addon->detail_order->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
			@endif
				</tbody>
			</table>
      </div>
      </div>
      </div>

      </div>

      <div class="row">
      <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-header">
                    <a href="{{ route('custom_prices.create',['u'=>'App\\Addon','id'=>$addon->id]) }}" class="btn btn-primary pull-right" >Tambah Custom Price</a>
                      
                    <h3>Custom Price List</h3>
                  </div>
                  
                  <div class="box-body">
                      <table id="primary" class="table table-bordered table-hover">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  {{--  <th width="5%">item</th>  --}}
                                  <th width="10%">Nilai</th>
                                  <th width="10%">Tanggal Mulai</th>
                                  <th width="10%">Tanggal Selesai</th>
                                  <th width="10%">Action</th>
                              </tr>
                          </thead>
                <tbody>
                    @php($total=0)
                @foreach( $addon->custom_price as $key => $detail )
                    <tr height="10">
                    <td>{{ $key+1 }}</td>
                    {{--  <td>{!! $detail->nameLabel !!}</td>        --}}
                    {{--  <td><a href="{{ route('orders.show',['id'=>$detail->id]) }}">{{ $detail->order_code }}</a></td>  --}}
                    <td>{{ $detail->price }}%</td>
                    <td>{{ $detail->startdate->format("d-m-Y") }}</td>
                    <td>{{ $detail->enddate->format("d-m-Y") }}</td>
                    						<td>
								<a href="
								{{route('custom_prices.edit',['u'=>'App\\Addon' ,'id'=>$detail->price_id ,'custom_price'=>$detail->id])}}">
								<button class="btn btn-warning">
									Edit
								</button>
							</a>
						</td>
                @endforeach
      		@if($addon->custom_price->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
			@endif
      {{--  <tr>
          <td colspan="2" align="center">Total addon:</td> 
          <td>{{ H::rupiah($total)}}</td> 
      </tr>  --}}
      </tbody>
      </table>
      </div>
      </div>
      </div>

      <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-header">
                    <a href="{{ route('special_prices.create',['u'=>'App\\Addon','id'=>$addon->id]) }}" class="btn btn-primary pull-right" >Tambah Special Price</a>
                      
                    <h3>Special Price List</h3>
                  </div>
                  
                  <div class="box-body">
<table id="example1" class="table table-bordered table-striped " >
					 <thead>
                        <tr>
                            <th rowspan="2" width="5%">No</th>
                            <th rowspan="2" width="15%">Nama</th>
                            <th rowspan="2" width="10%">Tipe Customer</th>
                            <th colspan="2" width="20%">Harga Menginap</th>
                            <th colspan="2" width="20%">Harga Tidak Menginap</th>
                            <th rowspan="2" width="10%" colspan="2">Action</th>
                        </tr>
                        <tr>
                             <th width="15%">Weekend Price</th>
                            <th width="15%">Weekday Price</th>
                            <th width="15%">Weekend Price</th>
                            <th width="15%">Weekday Price</th>
                        </tr>
                    </thead>
					<tbody>
                   		 @foreach( $addon->special_price as $key => $special_price )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $special_price->nameLabel }}</td>
                            <td>{{ $special_price->customer_type->name }}</td>
                            <td>{{ H::rupiah($special_price->weekend_price) }}</td>
                            <td>{{ H::rupiah($special_price->price) }}</td>
                            <td>{{ H::rupiah($special_price->no_stay_weekend_price) }}</td>
                            <td>{{ H::rupiah($special_price->no_stay_weekday_price) }}</td>
							<td>
								<a href="
								{{route('special_prices.edit',['special_price'=>$special_price->id])}}">
								<button class="btn btn-warning">
									Edit
								</button>
							</a>
						</td>
					</tr>
          @endforeach
          	@if($addon->special_price->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
			@endif
				</tbody>
			</table>
      </div>
      </div>
      </div>
      </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>

{{--     <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi Terima Barang</h4>
      </div>
        {!! Form::model($addon, ['route' => ['addons.recieve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      <div class="form-group">
          {{ Form::label('nota','Nomor Nota Supermarket') }}
          {{ Form::text('nota','', ['class' => 'form-control', 'required']) }}
          @if ($errors->has('nota'))
                    <div class="help-block text-red">
                      {{ $errors->first('nota') }}
                    </div>
          @endif
      </div>
      {{ Form::hidden('id', $addon->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-baddoned table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="25%">Jumlah</th>
              <th width="15%">Harga</th>
              <th width="15%">diterima</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ Form::number('received[]', 0, ['class' => 'form-control','min'=> 0]) }}
                  @if ($errors->has('received'))
                            <div class="help-block text-red">
                              {{ $errors->first('received') }}
                            </div>
                  @endif
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div>

    <div id="myRev" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi draft</h4>
      </div>
        {!! Form::model($addon, ['route' => ['addons.approve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      {{ Form::hidden('nota','approved', ['class' => 'form-control', 'required']) }}
      
      {{ Form::hidden('id', $addon->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-baddoned table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="15%">Jumlah</th>
              <th width="25%">Harga</th>
              <th width="25%">Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            @php( $totals=0)
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ $detail->subTotalLabel}}</td>
              
            </tr>
            @endforeach
            <tr><td colspan="5" align="center">Total:</td><td>{{ $addon->totalLabel}}</td></tr>
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div> --}}
  <!-- /.row -->
@endsection
