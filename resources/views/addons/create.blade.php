@extends('layouts.app')

@section('title')
{{ $addon->exists ? 'Ubah' : 'Tambah' }} Add On
@endsection
@if($addon->exists)
@section('actionbtn')
<a data-href="{{ route('addons.destroy', $addon->id) }}" class="btn btn-danger destroy">Hapus Add On</a>
@endsection
@endif


@section('breadcrumb') 
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="{{ route('addons.index')}}"><i class="fa fa-plus"></i> Addon</a></li>
<li><a href="#" class="active">{{ $addon->exists ? 'Ubah' : 'Tambah' }} Addon</a></li>
@endsection

@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($addon->exists)
        {!! Form::model($addon, ['route' => ['addons.update', $addon->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($addon, ['route' => ['addons.store'], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Nama</label>
              {!! Form::text('name', $addon->name, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Harga Weekday(Menginap)</label>
              {!! Form::number('price', $addon->price, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            {{--  <div class="form-group col-md-6">
              <label>Harga Weekend(Menginap)</label>
              {!! Form::number('weekend_price', $addon->weekend_price, ['class'=> 'form-control','required' => 'required']) !!}
            </div>  --}}
             {{--  <div class="form-group col-md-6">
              <label>Harga Weekday Tidak Menginap</label>
              {!! Form::number('no_stay_weekday_price', $addon->no_stay_weekday_price, ['class'=> 'form-control','required' => 'required']) !!}
            </div>  --}}
            {{--  <div class="form-group col-md-3">
              <label>Harga Weekend Tidak Menginap</label>
              {!! Form::number('no_stay_weekend_price', $addon->no_stay_weekend_price, ['class'=> 'form-control','required' => 'required']) !!}
            </div>   --}}
            <div class="form-group col-md-12">
            <label>Satuan</label>
              {!! Form::select('unit_id', $unit, $addon->unit_id, ['placeholder'=>'Satuan','class'=> 'form-control','required' => 'required']) !!}
            </div>

            <div class="form-group col-md-12">
              {{ Form::label('stock','Stok') }}
              {{ Form::number('stock', $addon->stock ? $addon->stock : 0 , ['class' => 'form-control', 'required']) }}
              @if ($errors->has('stock'))
                <div class="help-block text-red">
                  {{ $errors->first('stock') }}
                </div>
              @endif
            </div>

           

            <div class="form-group col-md-12">
              <label>Tipe Tambahan</label>
              {!! Form::select('type',['0'=>'Tambahan','1'=>'Makanan','2'=>'Snack'], $addon->type, ['class'=> 'form-control','required' => 'required']) !!}
            </div>

          <div class="form-group col-md-4" style="padding-top:30px">
            {{ Form::checkbox('front_end', 1, $addon->front_end==1 ? true : false , ['class' => 'field']) }}
            
            <label>
              Tampilkan pada halaman depan?
            </label>
          </div>
          <div class="form-group col-md-4" style="padding-top:30px">
            {{ Form::checkbox('active', 1, $addon->active==1 ? true : false , ['class' => 'field']) }}
            
            <label>
              Aktif?
            </label>
          </div>
          <div class="form-group col-md-4" style="padding-top:30px">
              {{ Form::checkbox('limited', 1,$addon->limited==1 ? true : false , ['class' => 'field']) }}
            <label>
              Terbatas?
            </label>
          </div>
            
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')

@push('scripts')
<script>
  $(document).ready(function (){
      $('select').select2();

  });
</script>
@endpush

