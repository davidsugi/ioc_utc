@extends('layouts.app')
@section('title')
Setting Menu
@endsection

@section('content')
<div class="row">
    <div class="col-xs-10">
        <div class="box">
            <div class="box-body">
                @if(!empty($menu))
                    {!! Form::model($menu, ['route' => ['food_settings.update', $menu[0]], 'method'=>'PATCH', 'files' => true]) !!}
                @else
                    {!! Form::model($menu, ['route' => ['food_settings.store'], 'class' => 'col s12', 'files' => true]) !!}
                @endif
                    {{ Form::hidden('typef',$type, [  'class' => 'form-control', 'required']) }}

                <div class="form-group col-md-12">
                    {{ Form::label('type','Tipe Menu') }}
                    {{ Form::select('type', array(0=> 'Snack',2 => 'Makanan') , 0, ['class' => 'form-control  select2class', ]) }}
                    @if ($errors->has('type'))
                        <div class="help-block text-red">
                            {{ $errors->first('type') }}
                        </div>
                    @endif
                </div>
                
                <div class="form-group col-md-12">
                    {{ Form::label('title','Title') }}
                    {{ Form::text('title','Lunch', [  'class' => 'form-control', 'required']) }}
                    @if ($errors->has('title'))
                        <div class="help-block text-red">
                            {{ $errors->first('title') }}
                        </div>
                    @endif
                </div>
                <div class="form-group col-md-12">
                    {{ Form::label('day','Add Days') }}
                    {{ Form::number('day',0, [  'class' => 'form-control', 'required']) }}
                    @if ($errors->has('day'))
                        <div class="help-block text-red">
                            {{ $errors->first('day') }}
                        </div>
                    @endif
                </div>
                
                <div class="form-group col-md-12">
                    {{ Form::label('time','Default Time') }}
                    {{ Form::text('time', date('h:i'), [  'class' => 'form-control', 'required']) }}
                    @if ($errors->has('day'))
                        <div class="help-block text-red">
                            {{ $errors->first('day') }}
                        </div>
                    @endif
                </div>
                
                <div class="box-footer">
                    {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
