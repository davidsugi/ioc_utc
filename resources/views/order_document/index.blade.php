@extends('layouts.app')
@section('title')
List Dokumen Order
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("orders.index")}}'><i class='fa fa-cart-plus'></i> Order</a></li>
<li><a href='{{ route("orders.show",$order->id)}}'>{{ $order->order_code }}</a></li>
<li><a href='#' class='active'>Dokumen Order</a></li>
@endsection
@section('content')
<div class="row">
    <form method="get" id="search-form">
        <div class="col-md-6">
            <div class="input-group no-border">
                <input type="text" class="form-control column-filter" name="filter[path]" placeholder="Masukan Nama File" value="{{ !empty($filter['path']) ? $filter['path'] : '' }}" />
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary btn-block">Cari</button>
        </div>
    </form>
</div>
<br>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <a href="{{ route('orders.order_documents.create',['order'=>$order]) }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Dokumen Order</button></a>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="25%">Keterangan</th>
                            <th width="25%">File</th>
                            <th width="25%" colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $ord as $key => $orders )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $orders->notes }}</td>
                            <td><a href="{{ asset('document/'.$orders->path.'') }}" target="_blank">{{ $orders->path }}</a></td>
                            <td>
                                <a class="destroy" data-href="{{ route('orders.order_documents.destroy',['id'=>$orders->id, 'order'=>$order]) }}" > <button class="btn btn-danger"> Hapus</button></a>
                            </td>
                        </tr>
                    @endforeach
                    @if($ord->count() == 0)
                        <tr>
                            <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                        </tr>
                    @endif
                    </tbody>
            </table>
        </div>
        {{ $ord->links() }}
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
