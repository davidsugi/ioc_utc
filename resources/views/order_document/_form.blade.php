@extends('layouts.app')
@section('title')
{{ $order_document->exists ? 'Ubah' : 'Tambah' }} Dokumen Order
@endsection
@if($order_document->exists)
@section('actionbtn')
<a data-href="{{ route('orders.order_documents.destroy', $order_document->id) }}" class="btn btn-danger destroy">Hapus Dokumen Order</a>
@endsection
@endif

@section('content')
<div class="row">
    <div class="col-xs-10">
        <div class="box">
            <div class="box-body">
                @if($order_document->exists)
                    {!! Form::model($order_document, ['route' => ['orders.order_documents.update', $order_document->id], 'method'=>'PATCH', 'files' => true]) !!}
                @else
                    {!! Form::model($order_document, ['route' => ['orders.order_documents.store', $order], 'class' => 'col s12', 'files' => true]) !!}
                @endif
                    <div class="form-group">
                        {{ Form::label('notes','Catatan') }}
                        {{ Form::textarea('notes', $order_document->notes, ['class' => 'form-control']) }}
                        @if ($errors->has('notes'))
                            <div class="help-block text-red">
                                {{ $errors->first('notes') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('path','File') }}
                        <input name="path" type="file" id="path">
                        @if ($errors->has('path'))
                            <div class="help-block text-red">
                                {{ $errors->first('path') }}
                            </div>
                        @endif
                    </div>
                    
                <div class="box-footer">
                    {{ Form::submit('Simpan', ['class' => 'btn btn-success']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
    });
</script>
@endpush
