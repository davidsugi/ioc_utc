@extends('layouts.app')

@section('title')
{{ $category->exists ? 'Ubah' : 'Tambah' }} Kategori
@endsection
@if($category->exists)
@section('actionbtn')
<a data-href="{{ route('categories.destroy', $category->id) }}" class="btn btn-danger destroy">Hapus Kategori</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("categories.index")}}'><i class='fa fa-cube'></i> Kategori</a></li>
<li><a href='#' class='active'>{{ $category->exists ? 'Ubah' : 'Tambah'}} Kategori</a></li>
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($category->exists)
        {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($category, ['route' => ['categories.store'], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Nama</label>
              {!! Form::text('name', $category->name, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')

