@extends('layouts.app')

@section('title')
{{ $facility_detail->exists ? 'Ubah' : 'Tambah' }} Detail Fasilitas
@endsection
@if($facility_detail->exists)
@section('actionbtn')
<a data-href="{{ route('facilities.facility_details.destroy', ['facility'=>$facility->id,'facility_detail'=>$facility_detail->id]) }}" class="btn btn-danger destroy">Hapus Detail Fasilitas</a>
@endsection
@endif
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("facilities.index")}}'><i class='fa fa-building'></i> Fasilitas</a></li>
<li><a href='{{ route("facilities.show",["id"=>$facility->id]) }}'>{{ $facility->name }}</a></li>
<li><a href='#' class='active'>{{ $facility_detail->exists ? 'Ubah' : 'Tambah'}}  Detail Fasilitas</a></li>

@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($facility_detail->exists)
        {!! Form::model($facility_detail, ['route' => ['facilities.facility_details.update', 'facility'=>$facility->id, 'facility_detail'=>$facility_detail->id], 'method'=>'PATCH','role' => 'form']) !!}
        @else
        {!! Form::model($facility_detail, ['route' => ['facilities.facility_details.store',$facility->id], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Fasilitas</label>
              {!! Form::text('facility_name',$facility->name,['class'=> 'form-control','readonly']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Attribute</label>
              {!! Form::select('attribute_id',$attribute, null, ['placeholder' => 'Pilih Fasilitas','class'=> 'form-control js-example-basic-single','onchange' => 'changeType(value)','required' => 'required']) !!}
              <input type="hidden" id="tipe" name="tipe" >
            </div>
            <div class="form-group col-md-12">
              <div id="Integer" style="display:none">
                <label>Value</label>
                {!! Form::number('value[]',$facility_detail->value,['placeholder' => 'Jumlah Item','class'=> 'form-control'])!!}
              </div>
              <div id="String" style="display:none">
                <label>Value</label>
                {!! Form::text('value[]',$facility_detail->value,['placeholder' => 'Deskripsi Item','class'=> 'form-control'])!!}
              </div>
              <div id="Date" style="display:none">
                <label>Value</label>
                {!! Form::number('value[]',$facility_detail->value,['placeholder' => 'Tanggal item','class'=> 'form-control js-example-basic-single','onchange' => 'changeType(value)'])!!}
              </div>
              <div id="Boolean" style="display:none">
                <label>Value</label>
                {!! Form::select('value[]',['Ada'=>'Ada','Tidak'=>'Tidak'],$facility_detail->value,['placeholder' => 'Pilih Ketersediaan','class'=> 'form-control','onchange' => 'changeType(value)'])!!}
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')
@push('scripts'){
<script type="text/javascript">
  $(document).ready(function(){
      init({{$facility_detail->attribute_id}});
    function init(value) {
      $.ajax({
        type: 'GET',
        datatype: 'json',
        data: {
          id: value
        },
        url: '{{route("API.datatype")}}', 
        success: function(data) {
          $('#Integer').css('display','none');
          $('#String').css('display','none');
          $('#Boolean').css('display','none');
          $('#Date').css('display','none');
          $('#'+data).css('display','block');
          if(data=="Integer"){
            document.getElementById('tipe').value=0;
          }
          else if(data=="String"){
            document.getElementById('tipe').value=1;
          }
          else if(data=="Date"){
            document.getElementById('tipe').value=2;
          }
          else if(data=="Boolean"){
            document.getElementById('tipe').value=3;
          }
        }
      });
    }
  });
  function changeType(value) {
    $.ajax({
      type: 'GET',
      datatype: 'json',
      data: {
        id: value
      },
      url: '{{route("API.datatype")}}', 
      success: function(data) {
        $('#Integer').css('display','none');
        $('#String').css('display','none');
        $('#Boolean').css('display','none');
        $('#Date').css('display','none');
        $('#'+data).css('display','block');
        if(data=="Integer"){
          document.getElementById('tipe').value=0;
        }
        else if(data=="String"){
          document.getElementById('tipe').value=1;
        }
        else if(data=="Date"){
          document.getElementById('tipe').value=2;
        }
        else if(data=="Boolean"){
          document.getElementById('tipe').value=3;
        }
      }
    });
  }
</script>
@endpush

