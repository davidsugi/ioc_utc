@extends('layouts.app')

@section('title')
{{ $custom_schedule->exists ? 'Ubah' : 'Tambah' }} Jadwal
@endsection
@if($custom_schedule->exists)
@section('actionbtn')
<a data-href="{{ route('detail_facilities.custom_schedules.destroy',['detail_facility'=>$detail_facility->id, 'custom_schedule'=>$custom_schedule->id]) }}" class="btn btn-danger destroy">Hapus Jadwal</a>
@endsection
@endif
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("facilities.index")}}'><i class='fa fa-building'></i> Fasilitas</a></li>
<li><a href='{{ route("facilities.show",["id"=>$detail_facility->facility->id])}}'> {{$detail_facility->facility->name}}</a></li>
<li><a href="{{ route('detail_facilities.custom_schedules.index',['detail_facility'=>$detail_facility->id]) }}">Jadwal {{ $detail_facility->name }}</a></li>
<li><a href='#' class='active'>{{ $custom_schedule->exists ? 'Ubah' : 'Tambah'}} Jadwal</a></li>

@endsection

@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($custom_schedule->exists)
        {!! Form::model($custom_schedule, ['route' => ['detail_facilities.custom_schedules.update','detail_facility'=>$detail_facility->id, 'custom_schedule'=>$custom_schedule->id], 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($custom_schedule, ['route' => ['detail_facilities.custom_schedules.store',$detail_facility->id], 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Date and time range:</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              @if($custom_schedule->exists)  
                <input type="text" class="form-control" name ="rangedate" id="reservationtime" value="{{ $custom_schedule->startdate->format('d/m/Y 00:00:00') }} - {{ $custom_schedule->enddate->format('d/m/Y 00:00:00') }}" required>                                            
              @else
                <input type="text" class="form-control" name ="rangedate" id="reservationtime" value="{{ Carbon\carbon::now()->format('d/m/Y 00:00:00')}} - {{ Carbon\carbon::now()->format('d/m/Y 23:59:00') }}" required>              
              @endif
              </div>
              <!-- /.input group -->
            </div>
            <div class="form-group col-md-12">
              <label>Fasilitas</label>
              {!! Form::text('detail_facility_id',$detail_facility->facility->name.' '.$detail_facility->name,['class'=> 'form-control','readonly']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Deskripsi</label>
              {!! Form::textarea('description', $custom_schedule->description, ['class'=> 'form-control']) !!}</div>
            </div>
          </div>
          <!-- /.box-body -->
          
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>


@endsection

@push('scripts')
<script>

  $(function () {
    $('#reservationtime').daterangepicker({ timePicker: true, timePicker24Hour: true, timePickerIncrement: 30,locale: {format: 'DD/MM/YYYY H:mm:ss'}})
  })
</script>
@endpush
@include('layouts._deletebtn')
