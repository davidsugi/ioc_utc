@extends('layouts.app')

@section('title')
Manage Jadwal
@endsection
@push('styles')

<!-- fullCalendar -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.2/bower_components/fullcalendar/dist/fullcalendar.print.min.css')}}" media="print">
@endpush
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("facilities.index")}}'><i class='fa fa-building'></i>Facility</a></li>
<li><a href='{{ route("facilities.show",["id"=>$detail_facility->facility->id])}}'>{{ $detail_facility->facility->name }}</a></li>
<li><a href='#' class="active">Jadwal {{ $detail_facility->name }}</a></li>
{{--  <li><a href='#' class='active'>DetailFacility</a></li>  --}}
@endsection
@section('content')
<style>
	input[type="date"]:before {
	content: attr(placeholder) !important;
	color: #aaa;
	margin-right: 0.5em;
	}
	input[type="date"]:focus:before,
	input[type="date"]:valid:before {
	content: "";
	}
</style>
<div class="box">
	<div class="box-body no-padding">
		<!-- THE CALENDAR -->
		<div id="calendar"></div>
	</div>
</div>
<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="date" class="form-control column-filter" name="filter[startdate]" placeholder="Start" value="{{ !empty($filter['startdate']) ? $filter['startdate'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
			
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="date" class="form-control column-filter" name="filter[enddate]" placeholder="End" value="{{ !empty($filter['enddate']) ? $filter['enddate'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<br>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('detail_facilities.custom_schedules.create',$detail_facility->id)}}" class="btn btn-success btn-block">Tambah Jadwal &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Check in</th>
					<th>Check out</th>
					<th>Fasilitas</th>
					<th>Deskripsi</th>       										
					<th>Edit</th>
				</tr>
			</thead>
			<tbody>
				@foreach($custom_schedule as $c)
				<tr>
					<td>{{$loop->iteration }}</td>
					<td>{{ Carbon\carbon::parse($c->start)->format("d-m-Y H:i") }}</td>
					@if ($c->subs==1)
					<td>{{ Carbon\carbon::parse($c->end)->format("d-m-Y H:i") }}</td>
					@else
					<td>{{ Carbon\carbon::parse($c->end)->subDay()->format("d-m-Y H:i") }}</td>
					@endif
					<td>{{$c->facility }}</td>
					<td>
					@if($c->order_id!=null)						
						<a href="{{ route('orders.show',['id'=>$c->order_id]) }}">Order No.{{$c->order_code.", Oleh ".$c->description}}</a>
					@else
					{{$c->description}}
					@endif
					</td>      										
					<td>
						@if($c->id!=null)
							<a href="{{route('detail_facilities.custom_schedules.edit',['detail_facility'=>$detail_facility->id,'custom_schedule'=>$c->id])}}">
							<button class="btn btn-warning">
								Edit
							</button>
							</a>
						@endif
					</td>
					
				</tr>
				@endforeach
				@if($custom_schedule->count() == 0)
<tr>
<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
</tr>
@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{--  {{ $custom_schedule->appends($filters)->links() }}  --}}
</div>
</div>
@stop
@push('scripts')
<script>
	var id = {{$detail_facility->id}};
	$(function () {
		ajaxCalendar();
		function ajaxCalendar() {
			$.ajax({
				type: 'GET',
				datatype: 'json',
				url: '{{route("API.detailfacilitycalendar")}}',
				data: ({id: id}),
				success: function(data) {
					var header = {
						left  : 'prev,next today',
						center: 'title',
						right : 'month,agendaWeek,agendaDay'
					};
					var buttonText = {
						today: 'today',
						month: 'month',
						week : 'week',
						day  : 'day'
					};
					var events =[];
					var timeFormat = 'H(:mm)t';
					for (var i=0; i<data.length; i++){
						var e = {
							title          : data[i][0],
							start          : $.fullCalendar.moment(data[i][2]),
							end            : $.fullCalendar.moment(data[i][3]),
                            backgroundColor: data[i][1],
                            url: data[i][4],
							borderColor    : data[i][1]
						};
						events.push(e);
					}
					$('#calendar').fullCalendar({
						eventLimit: 5,
						header,
						buttonText,
						events,
						timeFormat
					})
				}
			})
		}
	})
</script>
@endpush