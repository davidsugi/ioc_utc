@extends('layouts.app')
@section('title')
Manage Detail Fasilitas 
@endsection

@section('content')

<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				{!! Form::select('filter[facility_id]', $facility, null, ['placeholder'=>'Fasilitas','class'=> 'form-control select2']) !!}
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				{!! Form::select('filter[attribute_id]', $attribute, null, ['placeholder'=>'Attribute','class'=> 'form-control select2']) !!}
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[value]" placeholder="Value" value="{{ !empty($filter['value']) ? $filter['value'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('facility_details.create')}}" class="btn btn-success btn-block">Tambah Detail Fasilitas &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>

	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Fasilitas</th>
					<th>Attribute</th>
					<th>Jumlah</th>
					<th>Edit</th>
				</tr>
			</thead>
			<tbody>
				@foreach($facility_detail as $a)
				<tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$a->facility->name}}</td>
					<td>{{$a->facility_attribute->name}}</td>
					<td>{{$a->value}}</td>
					<td>
						<a href="{{route('facility_details.edit',$a->id)}}">
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
				</tr>
				@endforeach
					@if($facility_detail->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $facility_detail->appends($filters)->links() }}
</div>
</div>

@stop
@push('scripts')
<script type="">
	$('.select2').select2()
</script>
@endpush
