@extends('layouts.app')

@section('title')
{{ $news->exists ? 'Ubah' : 'Tambah' }} Berita
@endsection
@if($news->exists)
@section('actionbtn')
<a data-href="{{ route('news.destroy', $news->id) }}" class="btn btn-danger destroy">Hapus Berita</a>
@endsection
@endif
@section('content')

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("news.index")}}'><i class='fa fa-quote-right'></i> Berita</a></li>
<li><a href='#' class='active'>{{ $news->exists ? 'Ubah' : 'Tambah'}} Berita</a></li>
@endsection

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($news->exists)
        {!! Form::model($news, ['route' => ['news.update', $news->id], 'enctype'=>'multipart/form-data', 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($news, ['route' => ['news.store'], 'enctype'=>'multipart/form-data', 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Judul</label>
              {!! Form::text('title', $news->title, ['class'=> 'form-control','required' => 'required']) !!}
              @if ($errors->has('title'))
              <div class="help-block text-red">
                {{ $errors->first('title') }}
              </div>
              @endif
            </div>
            <div class="form-group col-md-12">
              <label>Kategori</label>
              {!! Form::select('categories[]',$category, $news->category()->pluck('categories.id'), ['class'=> 'form-control select2','required' => 'required','multiple'=>'multiple']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Status</label>
              {!! Form::select('status',['1'=>'Publish','2'=>'UnPublish'], $news->active, ['class'=> 'form-control select2','required' => 'required']) !!}
              @if ($errors->has('status'))
              <div class="help-block text-red">
                {{ $errors->first('status') }}
              </div>
              @endif
            </div>
            <div class="form-group col-md-12">
              <label>Konten</label>
              {!! Form::textarea('content', $news->content, ['id'=>'description', 'class'=> 'form-control', 'required' => 'required', 'rows'=>10]) !!}
              @if ($errors->has('content'))
              <div class="help-block text-red">
                {{ $errors->first('content') }}
              </div>
              @endif
            </div>
            @if(!$news->exists)
            <div class="col-md-12">
                {!! Form::file('image', ['id'=>'image', 'class'=> 'form-control', 'required' => 'required', 'onchange'=>'preview_images();']) !!}
            </div>
            <div class="col-md-12">
              <div class="row" id="image_preview">
                <div class='col-md-3' style='padding-top:20px'>
                  @if($news->exists)
                    <img class='img-responsive' src="{{ asset($news->image) }}"/>
                  @endif
                </div>
              </div>
            </div>
            @endif

          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- CK Editor -->
<script>
</script>
</section>
@endsection
@include('layouts._deletebtn')

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
  $("#description").summernote({height: 300});
  $('.select2').select2();
  function preview_images() {
    $('#image_preview div').html("<img class='img-responsive' src='"+URL.createObjectURL(event.target.files[0])+"'>");
  }
});
</script>
@endpush
