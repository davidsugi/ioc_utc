@extends('layouts.app')
@section('title')
Manage News
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("news.index")}}' class='active'><i class='fa fa-quote-right'></i> News</a></li>
<li><a href='#' class='active'>Detail News</a></li>
@endsection
@section('content')
<div class="row">
	<section class="col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<h1>{{$news->title}}</h1>
					</div>
				</div>
			</div>	
		</div>
	</section>

	<section class="col-md-4">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<h2>Publisher: {{$news->user->name}}</h2>
					</div>
				</div>
			</div>	
		</div>
	</section><section class="col-md-12">
		<div class="box">
			
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Image
					</div>
				</div>
			</div>	
			<div class="box-body" style="height: 400px;overflow-y:visible" >
				<div class="row">
					<div class="col-md-12">
						<center>
						<img src="{{asset('image/'.$news->image)}}" width="550px">
						</center>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-3 col-md-offset-9">
						<a href="{{ route('news.editImage',$news->id)}}" class="btn btn-warning btn-block">Ubah Gambar &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="col-md-12">
		<div class="box">
			
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Content
					</div>
				</div>
			</div>	
			<div class="box-body" style="height: 400px;overflow-y:visible" >
				<div class="row">
					<div class="col-md-12">
						{!! ($news->content) !!}
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-3 col-md-offset-9">
						<a href="{{ route('news.edit',$news->id)}}" class="btn btn-warning btn-block">Ubah News &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@stop
