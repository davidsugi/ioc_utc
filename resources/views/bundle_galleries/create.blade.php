@extends('layouts.app')

@section('title')
{{ $gallery->exists ? 'Ubah' : 'Tambah' }} Gallery
@endsection
@if($gallery->exists)
@section('actionbtn')
<a data-href="{{ route('bundles.galleries.destroy', ['bundle'=>$bundle->id,'gallery'=>$gallery->id]) }}" class="btn btn-danger destroy">Hapus Gallery</a>
@endsection
@endif
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($gallery->exists)
        {!! Form::model($gallery, ['route' => ['bundles.galleries.update',$bundle->id, $gallery->id], 'method'=>'PATCH', 'enctype'=>'multipart/form-data','role' => 'form']) !!}
        @else
        {!! Form::model($gallery, ['route' => ['bundles.galleries.store',$bundle->id], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>ID Eduwisata</label>
              {!! Form::text('bundle_name',$bundle->name,['class'=> 'form-control','readonly']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Judul</label>
              {!! Form::text('title',$gallery->title, ['placeholder' => 'Judul gambar','class'=> 'form-control ','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Deskripsi</label>
              {!! Form::text('description',$gallery->description, ['placeholder' => 'Deskripsi gambar','class'=> 'form-control ','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Image</label>
            <input type="file" class="form-control" id="image" name="image" onchange="preview_images();" multiple/>              
              {{--  {!! Form::file('image', ['class'=> 'form-control', 'required' => 'required']) !!}  --}}
            </div>
          <div class="col-md-12">
            <div class="row" id="image_preview">
            @if($gallery->exists)       
              <div class='col-md-3' style='padding-top:20px'>       
                <img alt="" title="" style="width:600px;height:400px!important" src="{{asset('image/'.$gallery->image_path)}}">            
              </div>
            @endif
            </div>
          </div>

          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script> 

{{--  <script src="{{asset('js/summernote.min.js')}}"></script>  --}}
<script>
  function preview_images() 
  {
   var total_file=document.getElementById("image").files.length;
   for(var i=0;i<total_file;i++)
   {
    $('#image_preview').html("");
     
    $('#image_preview').append("<div class='col-md-3' style='padding-top:20px'><img alt='' title='' style='width:600px;height:400px!important' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
  }
}
</script>
@endpush