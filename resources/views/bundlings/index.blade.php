@extends('layouts.app')

@section('title')
Manage Bundling
@endsection

@section('content')

<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[name]" placeholder="Nama" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				{!! Form::select('filter[type_id]', $type, null, ['placeholder'=>'Tipe','class'=> 'form-control']) !!}
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('bundlings.create')}}" class="btn btn-success btn-block">Tambah Bundling &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nama</th>
					<th>Harga</th>
					<th>Kapasitas</th>
					<th>Minimum</th>
					<th>Satuan</th>
					<th>Edit</th>
					<th>Show Detail</th>
				</tr>
			</thead>
			<tbody>
				@foreach($bundling as $f)
				<tr>
					<td>{{$loop->iteration }</td>
					<td>{{$f->name}}</td>
					<td>{{$f->price}}</td>
					<td>{{$f->capacity}}</td>
					<td>{{$f->minimum}}</td>
					<td>{{$f->unit_name}}</td>
					<td>
						<a href="{{route('bundlings.edit',$f->id)}}">
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
					<td>
						<a href="{{route('bundlings.show',$f->id)}}">
							<button class="btn btn-primary">
								Show
							</button>
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $facility->appends($filters)->links() }}
</div>
</div>

@stop
