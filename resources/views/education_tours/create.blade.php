@extends('layouts.app')

@section('title')
{{ $education_tour->exists ? 'Ubah' : 'Tambah' }} Eduwisata
@endsection
@if($education_tour->exists)
@section('actionbtn')
<a data-href="{{ route('education_tours.destroy', $education_tour->id) }}" class="btn btn-danger destroy">Hapus Eduwisata</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("education_tours.index")}}'><i class='fa fa-tree'></i> Eduwisata</a></li>
<li><a href='#' class='active'>{{ $education_tour->exists ? 'Ubah' : 'Tambah'}} Eduwisata</a></li>
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($education_tour->exists)
        {!! Form::model($education_tour, ['route' => ['education_tours.update', $education_tour->id], 'method'=>'PATCH','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
        @else
        {!! Form::model($education_tour, ['route' => ['education_tours.store'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Judul</label>
              {!! Form::text('title', $education_tour->title, ['class'=> 'form-control','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Deskripsi</label>
              {!! Form::textarea('description', $education_tour->description, ['id'=>'description','class'=> 'form-control','required' => 'required']) !!}
            </div>
            @if(!$education_tour->exists)
            <div class="col-md-12">
              <input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple/>
            </div>
            <div class="col-md-12">
              <div class="row" id="image_preview"></div>
            </div>
            @endif
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
<!-- CK Editor -->

{{--  <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description')
  })
</script>  --}}
@endsection
@include('layouts._deletebtn')
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script> 

{{--  <script src="{{asset('js/summernote.min.js')}}"></script>  --}}
<script>
  $("#description").summernote();
  function preview_images() 
  {
   var total_file=document.getElementById("images").files.length;
   for(var i=0;i<total_file;i++)
   {
    $('#image_preview').append("<div class='col-md-3' style='padding-top:20px'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
  }
}
</script>
@endpush
