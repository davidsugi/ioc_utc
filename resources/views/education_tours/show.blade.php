@extends('layouts.app')
@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick-theme.css')}}">
<style>

	* {
		box-sizing: border-box;
	}


	.slick-slide {
		margin: 0px 25px;
	}

	.slick-slide img {
		width: 100%;
	}

	.slick-prev:before,
	.slick-next:before {
		color: black;
	}


	.slick-slide {
		transition: all ease-in-out .3s;
		opacity: .2;
	}
	.slick-slide:focus {
		opacity:1;
	}

	.slick-active {
		opacity: .5;
	}


	/* Main carousel style */
	.carousel {
		width: 600px;
	}

	/* Indicators list style */
	.article-slide .carousel-indicators {
		bottom: 0;
		left: 0;
		margin-left: 5px;
		width: 100%;
	}
	/* Indicators list style */
	.article-slide .carousel-indicators li {
		border: medium none;
		border-radius: 0;
		float: left;
		height: 54px;
		margin-bottom: 5px;
		margin-left: 0;
		margin-right: 5px !important;
		margin-top: 0;
		width: 100px;
	}
	/* Indicators images style */
	.article-slide .carousel-indicators img {
		border: 2px solid #FFFFFF;
		float: left;
		height: 54px;
		left: 0;
		width: 100px;
	}
	/* Indicators active image style */
	.article-slide .carousel-indicators .active img {
		border: 2px solid #428BCA;
		opacity: 0.7;
	}
</style>
@endpush
@section('title')
Manage Eduwisata
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("education_tours.index")}}'><i class='fa fa-tree'></i> Eduwisata</a></li>
<li><a href='#' class='active'>Detail Eduwisata</a></li>
@endsection
@section('content')
<div class="row">
	<section class="col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<h1>Nama: {{$education_tour->title}}</h1>
					</div>
				</div>
			</div>	
		</div>
	</section>
	<section class="col-md-12">
		<div class="box">
			
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Deskripsi
					</div>
				</div>
			</div>	
			<div class="box-body" >
				<div class="row">
					<h4>
						<div class="col-md-12">
							<div class="col-md-9">{!! $education_tour->description!!}</div>
						</div>
					</h4>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-3 col-md-offset-9">
						<a href="{{ route('education_tours.edit',$education_tour->id)}}" class="btn btn-warning btn-block">Ubah Deskripsi &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="col-md-12">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Gallery
					</div>

					<div class="col-md-3 pull-right">
						<a href="{{ route('education_tours.galleries.create',$education_tour->id)}}" class="btn btn-success btn-block">Tambah Detail Gallery &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>

			<!-- /.box-header -->
			<div class="box-body" style="height: 65vh;overflow-y:visible">
				<div class="row">
					<div class="col-md-7">
						<div class="carousel slide article-slide" id="article-photo-carousel">
							<!-- Wrapper for slides -->
							<div class="carousel-inner cont-slider">
								<div class="item" style="display:none">
								</div>
						@foreach($education_tour->gallery as $g)
						@if($loop->first)
						<div class="item active" >
							<img alt="" title="" style="width:600px;height:400px!important" src="{{asset('image/'.$g->image_path)}}">
						</div>
						@else
						<div class="item" >
							<img alt="" title="" style="width:600px;height:400px!important" src="{{asset('image/'.$g->image_path)}}">
						</div>
						@endif
						@endforeach
					</div>
					<!-- Indicators -->
					<ol class="carousel-indicators" >
						<section class="regular slider">
							@foreach($education_tour->gallery as $key => $g)
							<div data-slide-to={{$key+1}} data-target="#article-photo-carousel">
								<img alt="" src="{{asset('image/'.$g->image_path)}}">
							</div>
							@endforeach
						</section>
					</ol>
				</div>
			</div>
			<div class="col-md-5" style="height: 400px;">
				<table id="example1" class="table table-bordered table-striped " >
					<thead>
						<tr>
							<th>Image</th>
							<th>Deskripsi</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($gallery as $a)
						<tr> 
							<td><img src="{{asset('image/'.$a->image_path)}}" width="60px" height="40px"></td>

							<td>{{ $a->description }}</td>
							<td>
								<a href="
								{{route('education_tours.galleries.edit',['facility'=>$a->galleriable_id,'gallery'=>$a->id])}}">
								<button class="btn btn-warning">
									Edit
								</button>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			{{ $gallery->links() }}
		</div>
	</div>
</div>
</div>
</section>
<section class="col-md-12">
	{{ $gallery->links() }}
</section>
</div>
@stop
@push('scripts')

<script src="{{asset('slick/jquery-2.2.0.min.js')}}" type="text/javascript"></script>
<script src="{{asset('slick/slick.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	$(document).on('ready', function() {
		$(".regular").slick({
			dots: false,
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 4
		});
	});
</script>
@endpush