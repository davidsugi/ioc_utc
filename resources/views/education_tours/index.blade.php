@extends('layouts.app')

@section('title')
Manage Eduwisata
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-tree'></i> Education Tour</a></li>
@endsection

@section('content')

<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[title]" placeholder="Judul" value="{{ !empty($filter['title']) ? $filter['title'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<br>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('education_tours.create')}}" class="btn btn-success btn-block">Tambah Eduwisata &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>
	
	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Judul</th>
					{{--  <th>Deskripsi</th>  --}}
					<th>Edit</th>
					<th>Show Detail</th>
				</tr>
			</thead>
			<tbody>
				@foreach($education_tour as $f)
				<tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$f->title}}</td>
					{{--  <td>{{$f->description}}</td>  --}}
					<td>
						<a href="{{route('education_tours.edit',$f->id)}}">
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
					<td>
						<a href="{{route('education_tours.show',$f->id)}}">
							<button class="btn btn-primary">
								Show
							</button>
						</a>
					</td>
				</tr>
				@endforeach
				@if($education_tour->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $education_tour->appends($filters)->links() }}
</div>
</div>

@stop
