@extends('layouts.app')

@section('title')
{{ $testimony->exists ? 'Ubah' : 'Tambah' }} Testimoni
@endsection
@if($testimony->exists)
@section('actionbtn')
<a data-href="{{ route('testimonies.destroy', $testimony->id) }}" class="btn btn-danger destroy">Hapus Testimoni</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("testimonies.index")}}'><i class='fa fa-comments'></i> Testimoni</a></li>
<li><a href='#' class='active'>{{ $testimony->exists ? 'Ubah' : 'Tambah'}} Testimoni</a></li>

@endsection 
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($testimony->exists)
        {!! Form::model($testimony, ['route' => ['testimonies.update', $testimony->id], 'method'=>'PATCH', 'enctype'=>'multipart/form-data','role' => 'form']) !!}
        @else
        {!! Form::model($testimony, ['route' => ['testimonies.store'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">

            <div class="form-group col-md-12">
              <label>Nama</label>
              {!! Form::text('name',$testimony->name, ['placeholder' => 'Nama','class'=> 'form-control ','required' => 'required']) !!}
            </div>
            <div class="form-group col-md-12">
              <label>Isi</label>
              {!! Form::text('content',$testimony->content, ['placeholder' => 'Isi','class'=> 'form-control ','required' => 'required']) !!}
            </div>
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')