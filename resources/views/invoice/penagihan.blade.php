@extends('layouts.pdf')

@push('style')
         <style>

    table {
      font-size: 13px;
      background-color: white;
    }
    
    table th{
      text-transform: uppercase;
      text-align: center;
      
    }
    table.header{
       width:400px;
    }
    table.header td{
       text-align: start;
    }
    td{
      text-align: center;
    }

    .kop-surat p{
      font-size: 1.2em;
    }
    table thead { display: table-header-group; }
    table tr { page-break-inside: avoid; }
    section.page
    {
        page-break-after: always;
        page-break-inside: avoid;
    }
    .row{
      padding:10px;
    }
    .holiday{
      background-color: grey;
      color:white;
    }

    .dethead{
      background-color: #d6d7d8;
    }
     
    table#result {  width:340px;border-collapse: collapse; }
    table#result tr { border: solid thin; }
    table#det th{  width:340px;border: solid thin; }
    table#det td{ border: solid thin; }
    .sum{
        text-align: end;
    }
  </style>
@endpush

@section('title')
Invoice
@endsection

@section('body')

@php($i=0)
<section class="page">
<div class="row">
<div class="col-xs-12">
<div class="col-xs-12">
    <table class="header">
      <tr><td>Nama/Group Pengguna</td><td>:<b>{{ $order->customer->nameLabel }}</b></td></tr>
      <tr><td>Penanggung Jawab</td><td>:<b>{{ $order->customer->name }}</b></td></tr>
      <tr><td>Alamat</td><td>:{{ $order->customer->address }}</td></tr>
      
      <tr><td>Telp</td><td>:<b>{{ $order->customer->phone }}</b></td></tr>
      {{--  <tr><td>Fax</td><td>:{{ "-"}}</td></tr>  --}}
        

    </table>
</div>

<div class="col-xs-12">
    <center><h5>Biaya Penggantian Perawatan dan Pmemeliiharaan</h5>
    <h5>Tanggal {{ $order->checkinLabel}} s/d {{ $order->checkoutLabel}}</h5>
    <h5><b>Ubaya Training Center</b> </h5>

    </center>

    <table id="det">
      
            <thead>
              <tr><th class="dethead" colspan="7"><b>Order Fasilitas</b></th></tr>

            <tr>
              
              <th width="5%">No</th>
              <th width="10%">Uraian</th>
              <th width="10%">Tarif</th>
              <th width="10%">Orang</th>
              <th width="10%">Hari/Kali</th>
                <th width="18%">Diskon</th>
              <th width="20%">Jumlah(Rp)</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $order->detail_order()->where("item_type", "LIKE", "%Facility%")->orwhere("item_type", "LIKE", "%Bundle%")->where("order_id", "=", $order->id)->get() as $key => $detail )
            <tr height="10">
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->nameLabel }}</a></td>
              <td>{{ H::rupiah($detail->price) }}</td>
              <td>{!! $detail->personLabel !!}</td>              
              <td>{{ $detail->durationLabel }}</td>           
              <td>{{ $detail->disc_label }}</td>           
              <td>{{ H::rupiah($detail->subtotal) }}</td>
            </tr>
            {{-- <tr>
              <td>Catatan: </td>
                <td colspan="6">{{ $detail->note }}</td>
            </tr> --}}
            @endforeach
            <tr><b><th colspan="6" class="sum" align="right"><b>Total Fasilitas &nbsp;</b></th><td><b>{{ $order->totalFacilityLabel }}</b></td></b></tr>
              
            </tbody>
              <tr><th class="dethead" colspan="7"><b>Order Tambahan</b></th></tr>
             <thead>
              <tr>
                <th width="5%">No</th>
                <th width="4%">Item</th>
                <th width="18%">Harga satuan</th>
                <th width="4%" colspan="2">Unit/Pax</th>
                <th width="18%">Diskon</th>
                <th width="20%">Sub total</th>
              </tr>
            </thead>
            <tbody>
            @foreach( $order->detail_order()->where("item_type", "LIKE", "%Addon%")->get() as $key => $detail )
              <tr height="10">
                <td>{{ $key+1 }}</td>
                <td>{{ $detail->nameLabel }}</td>
                <td>{{ $detail->priceLabel }}</td>
                <td colspan="2">{{ $detail->amount }}</td>
                <td>{{ $detail->disc_label }}</td>              
                <td>{{ H::rupiah($detail->subtotal) }}</td>              
              </tr>
              @endforeach
              <tr><b><th colspan="6" class="sum" align="center"><b>Total Tambahan &nbsp;</b></th><td><b>{{ $order->totalAddonLabel }}</b></td></b></tr>
            </tbody>

            <tr><th class="dethead sum" colspan="6" align="right"><b>Total Order &nbsp; </b></th><td class="dethead" ><b>{{ H::rupiah($order->totalLabel) }}</b></td></tr>            
          <tr><td colspan="6" class="sum" align="end">Potongan Global {{ $order->globaldisc_type==1 ? $order->globaldisc."%" : ""}} &nbsp;</td><td><b>{{ H::rupiah($order->globaldisc_val) }}</b></td></tr>
          <tr><td colspan="6" class="sum" align="end">Potongan Kupon {{ $order->code}} &nbsp; </td><td><b>{{ H::rupiah($order->disc) }}</b> &nbsp;</td></tr>
          {{-- <tr><td colspan="5" align="right">Tax: </td><td style="width: 10%">{{$order->tax}} %</td><td><b>{{ H::rupiah($order->taxLabel) }}</b></td></tr> --}}
          <tr><th colspan="6" class="dethead sum" align="right">Total Akhir &nbsp; </th><th class="dethead" ><b>{{ H::rupiah($order->total_nett_label) }}</b></th></tr>            
          <tr><th colspan="6" class="dethead sum" align="right">Terbayar &nbsp;</th><th class="dethead" ><b>{{ H::rupiah($order->paidLabel) }}</b></th></tr>            
          <tr><th colspan="6" class="dethead sum" align="right">Sisa/Kurang &nbsp; </th><th class="dethead" ><b>{{ H::rupiah($order->payableLabel) }}</b></th></tr>            
         
          </table>
</div>
<center>
<div class="col-xs-12">
    <div class="row">
        <div class="col-xs-6">
            <p>Mengetahui,</p>
            <br>
            <br>
            <br>
            <br>
            <br>
            <p>(____________________)</p>
            <p><b>Ubaya Training Center</b></p>
        </div>
        <div class="col-xs-6">
            <p>Trawas,{{date("d M Y")}} </p>
            <br>
            <br>
            <br>
            <br>
            <br>
            <p>(____________________)</p>
            <p><b>{{$order->customer->nameLabel}}</b></p>
        </div>
    </div>
</div>
</center>
</div>
</div>
</section>


@endsection