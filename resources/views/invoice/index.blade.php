@extends('layouts.app')
@section('title')
Manage Invoice
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("orders.show",["id"=>$order])}}'><i class='fa fa-cart-plus'></i>{{ $ordcod->order_code }}</a></li>
<li><a href='#' class="active">invoice</a></li>
@endsection

@section('content')
<style>
  input[type="date"]:before {
  content: attr(placeholder) !important;
  color: #aaa;
  margin-right: 0.5em;
  }
  input[type="date"]:focus:before,
  input[type="date"]:valid:before {
  content: "";
  }
</style>
<div class="row">
    <form method="get">
      <div class="col-md-4">
        <div class="input-group no-border">
          <input type="date" class="form-control column-filter" name="filter[start]" placeholder="Mulai" value="{{ !empty($filter['start']) ? $filter['start'] : '' }}" />
          <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
        </div>
      </div>
        <div class="col-md-4">
        <div class="input-group no-border">
          <input type="date" class="form-control column-filter" name="filter[end]" placeholder="Selesai" value="{{ !empty($filter['end']) ? $filter['end'] : '' }}" />
          <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
        </div>
      </div>
      <div class="col-md-4">
        <button type="submit" class="btn btn-primary btn-block">Cari</button>
      </div>
    </form>
  </div>
<br>

  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
      <div class="box-header">
        <a href="{{ route('orders.show', $order) }}"><button class="btn btn-info"><span>  <i class="fa fa-arrow-left"></i> </span>Kembali ke Order</button></a>        
        @if($ordcod->status<3)
        <a href="{{ route('orders.invoices.create', $order) }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Invoice</button></a>
        @endif

       
      </div>

        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>
              <th width="5%">Operator</th>
              <th width="5%">Status</th>
              <th width="25%">Tanggal invoice</th>
              <th width="25%">Total Keseluruhan</th>
              <th width="25%">Action</th>
            </tr>
            </thead>
            <tbody>
              @php($total=0)
            @foreach( $invoices as $key => $invoice )
            <tr height="10">
              <td>{{ +1 }}</td>
              <td>{!! $invoice->user ? $invoice->user->name : '-'  !!}</td>
              <td>{!! $invoice->spanLabel !!}</td>
              <td>{{ $invoice->dateLabel }}</td>
              <td>{{ H::rupiah($invoice->TotalLabel) }}</td>
              <td><a href="{{ route('invoices.show',['id'=>$invoice->id]) }}" class="btn btn-primary">Show</span></a>
                @if($invoice->status==0)
              <a href="{{ route('invoices.edit',['id'=>$invoice->id]) }}"  class="btn btn-warning">Edit</span></a>
            @endif</td>
              </td>
            </tr>
            @php($total+=$invoice->TotalLabel)
            @endforeach
            <tr>
                <td colspan="3"><center>Total</center></td><td>{{ H::rupiah($total) }}</td>
              </tr>
            @if($invoices->count() == 0)
              <tr>
                <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
              </tr>
              @endif
            </tbody>
          </table>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
@endsection
@push('scripts')

<form id='destroy-form' method="POST">
      {{ csrf_field() }}
      <input type="hidden" name="_method" value="DELETE">
  </form>
  <script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
  </script>

@endpush