@extends('layouts.app')

@section('title')
invoice No. {{ strtoupper($invoice->id) }}
@endsection
@section('actionbtn')
@if($invoice->status==0)
<a href="{{ route('orders.invoice.stat',['invoice'=>$invoice,'stat'=>1]) }}"><button class="btn btn-success">Bayar Invoice</button></a>
<a href="{{ route('invoices.edit',['invoice'=>$invoice]) }}"><button class="btn btn-info">Ubah Invoice</button></a>
<a href="{{ route('orders.invoice.stat',['invoice'=>$invoice,'stat'=>2]) }}"><button class="btn btn-danger">Tolak Invoice</button></a>
@endif
<a href="{{ route('invoices.tagih',['order'=>$invoice->order_id]) }}"><button class="btn btn-warning">Cetak Tagihan</button></a>

@endsection


@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("orders.show",["id"=>$invoice->order->id])}}'><i class='fa fa-cart-plus'></i> {{ $invoice->order->order_code }}</a></li>
<li><a href='{{ route("orders.invoices.index",["id"=>$invoice->order->id])}}'>Invoice</a></li>
<li><a href='#' class='active'>Detail Invoice</a></li>
@endsection

@section('content')
<style>
  .inner>p{
    font-size:150%;
  }
  .inner>h3{
    font-size:250%;
  }
</style>
 <div class="col-md-4">
        <div class="small-box bg-navy">
            <div class="inner">
              <h3>{{ $invoice->invoice_code }}</h3>
              <p>No. Invoice</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
          </div>
      </div>
 <div class="col-md-4">
        <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{ $invoice->order->order_code }}</h3>
              <p>No. Order</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
          </div>
      </div>

 <div class="col-md-4">
        <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $invoice->dateLabel }}</h3>
              <p>Invoice Date</p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar"></i>
            </div>
          </div>
      </div>

      <div class="row">
          <div class="col-xs-12">
               <div class="box box-danger">
                  <div class="box-body">
              {!! $invoice->span_label !!}
                    
                      <table id="primary" class="table table-bordered table-hover">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th width="10%">Keterangan</th>
                                  <th width="10%">Jumlah</th>
                              </tr>
                          </thead>
      <tbody>
        @php($total=0)
      @foreach( $invoice->detail_invoice as $key => $detail )
      <tr height="10">
      <td>{{ $key+1 }}</td>
      <td>{{ $detail->description }}</td>
      <td>{{ H::rupiah($detail->amount) }}</td>
      </tr>
      @php($total+=$detail->amount)
      @endforeach
      <tr>
          <td colspan="2" align="center">Total Invoice:</td> 
          <td>{{ H::rupiah($total)}}</td> 
      </tr>
      </tbody>
      </table>
      </div>
      </div>
      </div>
      </div>
      

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>

{{--     <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi Terima Barang</h4>
      </div>
        {!! Form::model($invoice, ['route' => ['invoices.recieve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      <div class="form-group">
          {{ Form::label('nota','Nomor Nota Supermarket') }}
          {{ Form::text('nota','', ['class' => 'form-control', 'required']) }}
          @if ($errors->has('nota'))
                    <div class="help-block text-red">
                      {{ $errors->first('nota') }}
                    </div>
          @endif
      </div>
      {{ Form::hidden('id', $invoice->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-binvoiceed table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="25%">Jumlah</th>
              <th width="15%">Harga</th>
              <th width="15%">diterima</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ Form::number('received[]', 0, ['class' => 'form-control','min'=> 0]) }}
                  @if ($errors->has('received'))
                            <div class="help-block text-red">
                              {{ $errors->first('received') }}
                            </div>
                  @endif
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div>

    <div id="myRev" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfrimasi draft</h4>
      </div>
        {!! Form::model($invoice, ['route' => ['invoices.approve'], 'class' => 'col s12', 'files' => true]) !!}
        
      <div class="modal-body">
      {{ Form::hidden('nota','approved', ['class' => 'form-control', 'required']) }}
      
      {{ Form::hidden('id', $invoice->id, ['class' => 'form-control', 'required']) }}
       <table id="example2" class="table table-binvoiceed table-hover">
            <thead>
            <tr>
              <th width="5%">No</th>

              <th width="10%">Kode Barang</th>
              <th width="25%">Barang</th>
              <th width="15%">Jumlah</th>
              <th width="25%">Harga</th>
              <th width="25%">Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $facils as $key => $detail )
            @php( $totals=0)
            <tr height="10">
              {{ Form::hidden('ids[]',$detail->id,[]) }}
              {{ Form::hidden('stock[]',$detail->stock_id,[]) }}
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->stock->kode }}</td>
              <td>{{ $detail->stock->nama }}</td>
              <td>{{ $detail->jumlah }}</td>
              <td>{{ $detail->hargaLabel }}</td>
              <td>{{ $detail->subTotalLabel}}</td>
              
            </tr>
            @endforeach
            <tr><td colspan="5" align="center">Total:</td><td>{{ $invoice->totalLabel}}</td></tr>
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
      {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::close() }}

      </div>
    </div>

  </div>
</div> --}}
  <!-- /.row -->
@endsection
