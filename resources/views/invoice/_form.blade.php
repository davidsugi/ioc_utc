@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ asset('css/summernote.css') }}">
@endpush

@section('title')
{{ $invoice->exists ? 'Ubah' : 'Tambah' }} Tagihan untuk order {{ $ords[0]["order_code"] }}
@endsection
@if($invoice->exists)
@section('actionbtn')
  <a data-href="{{ route('invoices.destroy', $invoice->id) }}" class="btn btn-danger destroy">Hapus Tagihan</a>
@endsection
@endif

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("orders.show",["id"=>$ords[0]["id"]])}}'><i class='fa fa-cart-plus'></i> {{ $ords[0]["order_code"] }}</a></li>
<li><a href='{{ route("orders.invoices.index",["id"=>$ords[0]["id"]])}}'>Invoice</a></li>
<li><a href='#' class='active'>{{ $invoice->exists ? 'Ubah Invoice: '.$invoice->invoice_code : 'Tambah Invoice'}}</a></li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-10">
      <div class="box">
        <div class="box-body">
          @if($invoice->exists)
          {!! Form::model($invoice, ['route' => ['invoices.update', $invoice->id], 'method'=>'PATCH', 'files' => true]) !!}
          {{--  @elseif($request->exists)
          {!! Form::model($invoice, ['route' => ['invoices.store', "checkout"=> $request->checkout], 'class' => 'col s12', 'files' => true]) !!}            --}}
          @else
          {!! Form::model($invoice, ['route' => ['invoices.store'], 'class' => 'col s12', 'files' => true]) !!}
          @endif
          <div class="form-group">
              {{ Form::label('order_id','Order') }}
              {{ Form::select('order_id', $orders, $invoice->order_id, ['class' => 'form-control select2 order',"id"=>"order_id", 'required']) }}
              @if ($errors->has('order_id'))
                <div class="help-block text-red">
                    {{ $errors->first('order_id') }}
                </div>
              @endif
          </div>
          <div class="form-group">
              {{ Form::label('date','Tanggal Pembayaran') }}
              {{ Form::date('date', $invoice->date==null? date("Y-m-d") : $invoice->date, ['class' => 'form-control', 'required']) }}
              @if ($errors->has('date'))
                <div class="help-block text-red">
                  {{ $errors->first('date') }}
                </div>
              @endif
          </div>
          <div class="row">
            <div class="col-md-12">
              <table class="table pelanggaran-table">
                <thead>
                  <tr>
                    <th style="width: 30%;">Deskripsi</th>
                    <th style="width: 30%;">Jumlah</th>
                    <th style="width: 10%;"></th>
                  </tr>
                </thead>
                <tbody>
                {{--  {{ json_encode($facs) }}  --}}
                @if(sizeof($invoice->detail_invoice)>0)
                  @foreach($invoice->detail_invoice as $key=>$fac )
                  <tr class="pelanggaran">
                    {!! Form::hidden('ids[]', isset ($ids) ? $ids[$key] : '', ['class'=> 'form-control hiddens']) !!}
                    {{--  <td>{!! Form::select('facil[]', $dets, $fac->id, ['required' => 'required', 'class'=> 'form-control select2 facs', 'id'=>$key]) !!}</td>  --}}
                    <td> {{ Form::text('descriptions[]', $fac->description, ['class' => 'form-control desc', 'required']) }} </td>
                     <td> {{ Form::number('amount[]', $fac->amount, ['class' => 'form-control amount', 'required']) }} </td>
                    <td>
                    <a href="" class="btn btn-danger delete_line" style="display:none"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  @endforeach
                 @elseif(isset($ords)&& sizeof($ords)>0)
                  @foreach($ords as $key=>$fac )
                  <tr class="pelanggaran">
                    {{--  {!! Form::hidden('ids[]', isset ($ids) ? $ids[$key] : '', ['class'=> 'form-control hiddens']) !!}  --}}
                    {{--  <td>{!! Form::select('facil[]', $dets, $fac->id, ['required' => 'required', 'class'=> 'form-control select2 facs', 'id'=>$key]) !!}</td>  --}}
                    <td> {{ Form::text('description[]', $fac->invLabel, ['class' => 'form-control desc', 'required']) }} </td>
                     <td> {{ Form::number('amount[]', $fac->payableLabel, ['class' => 'form-control amount', 'required']) }} </td>
                    <td>
                    <a href="" class="btn btn-danger delete_line" style="display:none"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  @endforeach
                @else
                  @for($i=0; $i<1; $i++)
                  <tr class="pelanggaran">
                    {{--  <td>{!! Form::select('facil[]',array(), null, ['required' => 'required', 'class'=> 'form-control select2 no-border facs', 'id'=>$i]) !!}</td>  --}}
                    <td> {{ Form::text('descriptions[]', $invoice->description, ['class' => 'form-control desc', 'required']) }} </td>
                    <td> {{ Form::number('amount[]', $invoice->amount, ['class' => 'form-control amount', 'required']) }} </td>
                    <td>
                      <a href="" class="btn btn-danger delete_line" style="display: {{ $i==0 ? 'none': null }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                  </tr>
                  @endfor
                @endif
                
                </tbody>
                <tfoot>
                  <tr>
                  <td align="center"><b>Total</b></td>
                  <td><div class="total"></div></td>
                </tr>
                  <tr>
                    <td colspan="2"></td>
                    <td><a href="" class="btn btn-success add_line"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
            <div class="box-footer">
              {{ Form::submit('Simpan', ['class' => 'btn btn-success']) }}
            </div>
          {{ Form::close() }}
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
@endsection
@include('layouts._deletebtn')

@push('scripts')
<script src="{{ asset('js/summernote.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    function total(){
        var tots=0;
       $('.pelanggaran-table > tbody  > tr').each(function() {
         tots+= isNaN(parseInt($(this).find(".amount").val())) ? 0: parseInt($(this).find(".amount").val());
   
        });
       var 	number_string = tots.toString(),
          split	= number_string.split(','),
          sisa 	= split[0].length % 3,
          rupiah 	= split[0].substr(0, sisa),
          ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
          
        if (ribuan) {
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        

       $(".total").html("<h1>"+rupiah ? 'Rp. ' + rupiah : ''+"</h1>")
    }
   $(".amount").on("keyup change",function (e){  
      total();
   }); 
   $('.add_line').click(function(e) {
              e.preventDefault();
              var pelanggaran_tr = $('.pelanggaran-table tbody tr:first').clone();
              var next=parseInt($('.pelanggaran-table tbody tr:last').find('select.select2').attr('id'))+1;
              pelanggaran_tr.find('.delete_line').show();
              pelanggaran_tr.find('.select2-container').remove();
              pelanggaran_tr.find('select.select2').val("");
              pelanggaran_tr.find('select.select2').attr("id",next);
              pelanggaran_tr.find('.hiddens').val("");
              pelanggaran_tr.find('.amount').val("");
              pelanggaran_tr.find('.desc').val("");
              pelanggaran_tr.find('select.select2').select2({
                'width': '100%',
                placeholder: "Pilih Pesanan",
                  ajax: {
                    url: '{{ route('api.get.det.order')}}',
                    dataType: 'json',
                    delay: 250,
                    data: function(params){
                        var bor_id = $("#order_id").val();
                        
                        return{
                          b: bor_id,
                          //searchbox results is params.term
                          q: params.term
                        };
                    },
                    processResults: function (data) {
                      return {
                        results:  $.map(data, function (value,key) {
                              
                              return {
                                  text: value,
                                  id: key
                              }

                          })
                      };
                    },
                    cache: true
                  }
              });
             
              pelanggaran_tr.find('.violation_point').val('');
              $('.pelanggaran-table tbody').append(pelanggaran_tr);
                  $(".amount").on("keyup change",function (e){
                    total();
                });
               $('.facs').on("select2:selecting", function(e) {
                  var next=parseInt($(this).attr('id'))+1;
                  if($('#'+next).length >0 ){
                    $('#'+next).select2("open");
                  }
                  else{
                    $('.add_line').trigger("click");
                    $('#'+next).select2("open");
                  }
              });

            });

     
    $('.summernote').summernote();
    $('.order').select2({
        'width': '100%',
          ajax: {
            url: '{{ route('api.get.order')}}',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (value, key) {
                        return {
                          text: value,
                          id: key
                        }
            })
                };
            },
            cache: true
          }
    });
    $('.pelanggaran-table tbody').on('click', '.delete_line', function (e) {
              e.preventDefault();
              $(this).closest('.pelanggaran').remove();
            });
    $('.facs').select2({
        'width': '100%',
          ajax: {
            url: '{{ route('api.get.det.order')}}',
            dataType: 'json',
            delay: 250,
            data: function(params){
                        var bor_id = $("#order_id").val();
                        
                        return{
                          b: bor_id,
                          //searchbox results is params.term
                          q: params.term
                        };
                    },
            processResults: function (data) {
                return {
                    results:  $.map(data, function (value, key) {
                        return {
                          text: value,
                          id: key
                        }
            })
                };
            },
            cache: true
          }
    });
    $('.facs').on("select2:selecting", function(e) {
                  var next=parseInt($(this).attr('id'))+1;
                  if($('#'+next).length >0 ){
                    $('#'+next).select2("open");
                  }
                  else{
                    $('.add_line').trigger("click");
                    $('#'+next).select2("open");
                  }
              });
              $(".amount").trigger('change');
              total();

  });
</script>
@endpush
