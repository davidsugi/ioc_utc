@extends('layouts.pdf')
@push('style')    
<style>

    table {
      font-size: 13px;
      background-color: white;
    }
    
    table th{
      text-transform: uppercase;
      text-align: center;
      
    }
    table.header{
       width:340px;
    }
    table.header td{
       text-align: start;
    }
    td{
      text-align: center;
    }

    .kop-surat p{
      font-size: 1.2em;
    }
    table thead { display: table-header-group; }
    table tr { page-break-inside: avoid; }
    section.page
    {
        page-break-after: always;
        page-break-inside: avoid;
    }
    .row{
      padding:20px;
    }
    .holiday{
      background-color: grey;
      color:white;
    }

    .dethead{
      background-color: #d6d7d8;
    }
     
    table#result {  width:340px;border-collapse: collapse; }
    table#result tr { border: solid thin; }
    table#det th{  width:340px;border: solid thin; }
    table#det td{ border: solid thin; }

  </style>
@endpush


@section('body')
@php($i=0)
<section class="page">
<div class="row">
<div class="col-xs-12">
<div class="col-xs-6">
    <h3>Order</h3>
    <table class="header">
      <tr><td>Tanggal Order: </td><td>{{ $order->date->format("d/m/Y") }}</td></tr>
      <tr><td>Kode Order: </td><td>{{ $order->order_code }}</td></tr>
      <tr><td>Tanggal Check-in: </td><td>{{ $order->checkinlabel }}</td></tr>
      <tr><td>Tanggal Check-out: </td><td>{{ $order->checkOutLabel }}</td></tr>
        

    </table>
</div>
<div class="col-xs-6">
    <h3>Data Pelanggan</h3>
    <table class="header">
      <tr><td>Nama: </td><td>{{ $order->customer->nameLabel }}</td></tr>
      <tr><td>Email: </td><td>{{ $order->customer->email }}</td></tr>
      <tr><td>Telepon: </td><td>{{ $order->customer->phone }}</td></tr>
        

    </table>
</div>

<div class="col-xs-12">
    <h3>Detail Order</h3>

    <table id="det">
      
            <thead>
              <tr><th class="dethead" colspan="7"><b>Order Fasilitas</b></th></tr>

            <tr>
              
              <th width="5%">No</th>
              <th width="10%">Fasilitas</th>
              <th width="10%">Harga satuan</th>
              <th width="10%">Jumlah Orang</th>
              <th width="10%">Durasi Pinjam(hari)</th>
              <th width="10%">Diskon</th>
              <th width="20%">Sub total</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $order->detail_order()->where("item_type", "LIKE", "%Facility%")->orwhere("item_type", "LIKE", "%Bundle%")->where("order_id", "=", $order->id)->get() as $key => $detail )
            <tr height="10">
              <td>{{ $key+1 }}</td>
              <td>{{ $detail->nameLabel }}</a></td>
              <td>{{ H::rupiah($detail->price) }}</td>
              <td>{!! $detail->personLabel !!}</td>              
              <td>{{ $detail->durationLabel }}</td>              
              <td>{{$detail->disc_label }}</td>               
              <td>{{ H::rupiah($detail->subtotal) }}</td>
            </tr>
            {{-- <tr>
              <td>Catatan: </td>
                <td colspan="6">{{ $detail->note }}</td>
            </tr> --}}
            @endforeach
            <tr><b><td colspan="6" align="right"><b>Total Fasilitas:</b></td><td><b>{{ $order->totalFacilityLabel }}</b></td></b></tr>
              
            </tbody>
              <tr><th class="dethead" colspan="7"><b>Order Tambahan</b></th></tr>
             <thead>
              <tr>
                <th width="5%">No</th>
                <th width="4%">Item</th>
                <th width="18%">Harga satuan</th>
                <th width="4%" colspan="2">Jumlah</th>
                <th width="18%">Diskon</th>
                <th width="20%">Sub total</th>
              </tr>
            </thead>
            <tbody>
            @foreach( $order->detail_order()->where("item_type", "LIKE", "%Addon%")->get() as $key => $detail )
              <tr height="10">
                <td>{{ $key+1 }}</td>
                <td>{{ $detail->nameLabel }}</td>
                <td>{{ $detail->priceLabel }}</td>
                <td colspan="2">{{ $detail->amount }}</td>
                <td>{{ $detail->disc_label }}</td>              
                <td>{{ H::rupiah($detail->subtotal) }}</td>              
              </tr>
              @endforeach
              <tr><b><td colspan="6" align="center"><b>Total Tambahan:</b></td><td><b>{{ $order->totalAddonLabel }}</b></td></b></tr>
            </tbody>

            <tr><th class="dethead" colspan="6" align="right"><b>Total Order: </b></th><td class="dethead" ><b>{{ H::rupiah($order->totalLabel) }}</b></td></tr>            
          <tr><td colspan="6" align="right">Potongan Global {{ $order->globaldisc_type==1 ? $order->globaldisc."%" : ""}}: </td><td><b>{{ H::rupiah($order->globaldisc_val) }}</b></td></tr>
          <tr><td colspan="6" align="right">Potongan Kupon {{ $order->code}}: </td><td><b>{{ H::rupiah($order->disc) }}</b></td></tr>
          {{-- <tr><td colspan="5" align="right">Tax: </td><td style="width: 10%">{{$order->tax}} %</td><td><b>{{ H::rupiah($order->taxLabel) }}</b></td></tr> --}}
          <tr><th colspan="6" class="dethead" align="right">Total Akhir: </th><th class="dethead" ><b>{{ H::rupiah($order->total_nett_label) }}</b></th></tr>            
         
          </table>
</div>

<div class="col-xs-12">
    <h3>Riwayat Pembayaran</h3>

  <table id="det">
      <tr>
        <th>No.</th>
        <th>Kode</th>
        <th>Tanggal</th>
        <th>Deskripsi</th>
        <th>Jumlah Pembayaran</th>
        {{--  <th>Total</th>  --}}
      </tr>
      @php($tot=0)
      @foreach($result as $key => $folio)
      <tr>
        <td>{{ $key+1 }}</td>          
        <td> {{ $folio->invoice_code }} </td>
        <td>{{ $folio->date->format("d-m-Y") }}</td>
        <td>{{ $folio->description }}</td>
        <td>{{ H::rupiah($folio->amount) }}</td>
      </tr>
      @php($tot+=$folio->amount)
      @endforeach
      <tr><th class="dethead" colspan="4"><b>Total Sudah Dibayar</b></td><th class="dethead" >{{ H::rupiah($tot) }}</th></tr>
       <tr><th class="dethead" colspan="4" align="right">Total belum Terbayar: </th><th class="dethead" ><b>{{ H::rupiah($order->payableLabel) }}</b></th></tr>            
              
    </table>
</div>

</div>
</div>
</section>
@endsection

