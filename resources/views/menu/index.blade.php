@extends('layouts.app')
@section('title')
List Menu
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='{{ route("addons.index")}}'><i class='fa fa-plus'></i> Addon</a></li>
<li><a href='{{ route("addons.show",$addon->id)}}' <i class='fa fa-plus'></i> {{ $addon->name }}</a></li>
<li><a href='#' class='active'>Menu {{ $addon->name }}</a></li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
                <a href="{{ route('addons.menus.create',$addon->id) }}"><button class="btn btn-primary"><span>  <i class="fa fa-plus"></i> </span>Tambah Menu</button></a>
            </div>
            <div class="box-body">
                <table id="example" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            {{-- <th class='center'>Addon</th> --}}
                            <th class='center'>Menu</th>
                            <th class='center'>Description</th>
                            
                            <th width="25%" colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $menus as $key => $menu )
                        <tr height="10">
                            <td>{{ $key+1 }}</td>
                            {{-- <td>{{ $menu->addon_id }}</td> --}}
                            <td>{{ $menu->name }}</td>
                            <td>{{ $menu->description_label }}</td>
                            
                            <td><a href="{{ route('addons.menus.edit',['id'=>$menu->id,'addons'=>$addon->id]) }}" class="btn btn-primary">
                                Edit
                            </a>
                            <a class="destroy btn btn-danger" data-href="{{ route('addons.menus.destroy',['id'=>$menu->id,'addons'=>$addon->id]) }}" >
                                Delete
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @if($menus->count() == 0)
                    <tr>
                        <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        {{ $menus->links() }}
    </div>
</div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script>
    $(document).ready(function() {
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
