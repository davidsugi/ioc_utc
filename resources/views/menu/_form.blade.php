@extends('layouts.app')
@section('title')
{{ $menu->exists ? 'Ubah' : 'Tambah' }} Menu
@endsection
@if($menu->exists)
@section('actionbtn')
<a data-href="{{ route('addons.menus.destroy', ['id'=>$menu->id,'addon'=>$addon->id] ) }}" class="btn btn-danger destroy">Hapus Menu</a>
@endsection
@endif


@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-dashboard'></i> Home</a></li>
<li><a href='{{ route("addons.index")}}'><i class='fa fa-plus'></i> Addon</a></li>
<li><a href='{{ route("addons.show",$addon->id)}}' <i class='fa fa-plus'></i> {{ $addon->name }}</a></li>
<li><a href='{{ route("addons.menus.index",$addon->id)}}' >Menu {{ $addon->name }}</a></li>
<li><a href='#' class='active'>{{ $menu->exists ? 'Ubah' : 'Tambah' }} Menu {{ $addon->name }}</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-10">
        <div class="box">
            <div class="box-body">
                @if($menu->exists)
                {!! Form::model($menu, ['route' => ['addons.menus.update', 'id'=>$menu->id,'addon'=>$addon->id], 'method'=>'PATCH', 'files' => true]) !!}
                @else
                {!! Form::model($menu, ['route' => ['addons.menus.store','id'=>$menu->id,'addon'=>$addon->id], 'class' => 'col s12', 'files' => true]) !!}
                @endif
                
                <div class='form-group col-md-12'>
                    {{ Form::label('addon_id','Addon') }}
                    {{ Form::select('addon_id',empty($addons) ? array() : $addons , $menu->addon_id, ['class' => 'form-control select2 addon_id' ,'required']) }}
                    @if ($errors->has('addon_id'))
                    <div class='help-block text-red'>
                        {{ $errors->first('addon_id') }}
                    </div>
                    @endif
                </div>
                
                <div class='form-group col-md-12'>
                    {{ Form::label('name','Menu') }}
                    {{ Form::text('name', $menu->name, ['placeholder'=>'Masukan Judul Menu',  'class' => 'form-control','required']) }}
                    @if ($errors->has('name'))
                    <div class='help-block text-red'>
                        {{ $errors->first('name') }}
                    </div>
                    @endif
                </div>
                
                <div class="form-group col-md-12">
                    {{ Form::label('description[]','Makanan') }}
                    {{ Form::select('description[]', $makanan , $menu->item_label, ['class' => 'form-control select2 select2class','multiple'=>'multiple']) }}
                    @if ($errors->has('description'))
                    <div class="help-block text-red">
                        {{ $errors->first('description') }}
                    </div>
                    @endif
                </div>
                
                <div class="box-footer">
                    {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<form id='destroy-form' method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2class ').select2({
            'width': '100%',
            placeholder: "Tambah Menu Makanan", 
            tags:true,
        });
        $('.destroy').click(function() {
            if(confirm('Apakah anda yakin?') ) {
                $('#destroy-form').attr('action',$(this).data('href'));
                $('#destroy-form').submit();
            }
        });
    });
</script>
@endpush
