@extends('layouts.app')

@section('title')
{{ $activity->exists ? 'Ubah' : 'Tambah' }} Aktivitas
@endsection
@if($activity->exists)
@section('actionbtn')
<a data-href="{{ route('activities.destroy', $activity->id) }}" class="btn btn-danger destroy">Hapus Aktivitas</a>
@endsection
@endif
@section('content')

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("activities.index")}}'><i class='fa fa-compass'></i> Aktivitas</a></li>
<li><a href='#' class='active'>{{ $activity->exists ? 'Ubah' : 'Tambah'}} Aktivitas</a></li>
@endsection

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        @if($activity->exists)
        {!! Form::model($activity, ['route' => ['activities.update', $activity->id], 'enctype'=>'multipart/form-data', 'method'=>'PATCH', 'role' => 'form']) !!}
        @else
        {!! Form::model($activity, ['route' => ['activities.store'], 'enctype'=>'multipart/form-data', 'method'=>'POST', 'role' => 'form']) !!}
        @endif
        <div class="box-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label>Judul</label>
              {!! Form::text('title', $activity->title, ['class'=> 'form-control']) !!}
              @if ($errors->has('title'))
              <div class="help-block text-red">
                {{ $errors->first('title') }}
              </div>
              @endif
            </div>
           
            <div class="form-group col-md-12">
              <label>Status</label>
              {!! Form::select('status',['1'=>'Published','2'=>'Inactive'], $activity->active, ['class'=> 'form-control select2']) !!}
              @if ($errors->has('status'))
              <div class="help-block text-red">
                {{ $errors->first('status') }}
              </div>
              @endif
            </div>
            <div class="form-group col-md-12">
              <label>Deskripsi</label>
              {!! Form::textarea('description', $activity->description, ['id'=>'description', 'class'=> 'form-control', 'rows'=>10]) !!}
              @if ($errors->has('description'))
              <div class="help-block text-red">
                {{ $errors->first('description') }}
              </div>
              @endif
            </div>
            <div class="form-group col-md-12">
              {{ Form::label('sort','Prioritas') }}
              {{ Form::number('sort', $activity->sort, ['min'=>0 ,  'placeholder'=>'Masukan Skala Prioritas Tampil',  'class' => 'form-control', 'required']) }}
              @if ($errors->has('sort'))
                <div class="help-block text-red">
                  {{ $errors->first('sort') }}
                </div>
              @endif
            </div>
            
            @if(!$activity->exists)
            <div class="col-md-12">
                {!! Form::file('image', ['id'=>'image', 'class'=> 'form-control',  'onchange'=>'preview_images();']) !!}
            </div>
            <div class="col-md-12">
              <div class="row" id="image_preview">
                <div class='col-md-3' style='padding-top:20px'>
                  @if($activity->exists)
                    <img class='img-responsive' src="{{ asset($activity->image) }}"/>
                  @endif
                </div>
              </div>
            </div>
            @endif

          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- CK Editor -->
<script>
</script>
</section>
@endsection
@include('layouts._deletebtn')

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
  $('.select2').select2();
  function preview_images() {
    $('#image_preview div').html("<img class='img-responsive' src='"+URL.createObjectURL(event.target.files[0])+"'>");
  }
});
</script>
@endpush
