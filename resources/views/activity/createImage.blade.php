@extends('layouts.app')

@section('title')
Ubah Gambar
@endsection

@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("activities.index")}}'><i class='fa fa-compass'></i> Aktivitas</a></li>
<li><a href='#' class='active'>Ubah Gambar</a></li>
@endsection
@section('content')

<section class="form-candidate">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-danger">
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($activtiy, ['route' => ['activities.updateImage', $activtiy->id], 'method'=>'put', 'enctype'=>'multipart/form-data','role' => 'form']) !!}
        <div class="box-body">
          <div class="row">

            <div class="form-group col-md-12">
              <label>Image</label>
              {!! Form::file('image', ['class'=> 'form-control', 'required' => 'required']) !!}
            </div>
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
@endsection
@include('layouts._deletebtn')

