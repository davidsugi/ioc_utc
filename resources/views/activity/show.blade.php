@extends('layouts.app')
@section('title')
Manage activities
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='{{ route("activities.index")}}' class='active'><i class='fa fa-compass'></i> Aktivitas</a></li>
<li><a href='#' class='active'>Detail Aktivitas</a></li>
@endsection
@section('content')
<div class="row">
	<section class="col-md-8">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<h1>{{$activity->title}}</h1>
					</div>
				</div>
			</div>	
		</div>
	</section>

	<section class="col-md-4">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<h2>Publisher: {{$activity->user->name}}</h2>
					</div>
				</div>
			</div>	
		</div>
	</section><section class="col-md-12">
		<div class="box">
			
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Image
					</div>
				</div>
			</div>	
			<div class="box-body" style="height: 400px;overflow-y:visible" >
				<div class="row">
					<div class="col-md-12">
						<center>
						<img src="{{asset('image/')}}/{{ $activity->image ? $activity->image : ENV('PLACEHOLDER_IMG','Na.jpg')  }}" width="550px">
						</center>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-3 col-md-offset-9">
						<a href="{{ route('activities.editImage',$activity->id)}}" class="btn btn-warning btn-block">Ubah Gambar &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="col-md-12">
		<div class="box">
			
			<div class="box-header">
				<div class="row">
					<div class="col-md-3">
						Deskripsi
					</div>
				</div>
			</div>	
			<div class="box-body" style="height: 400px;overflow-y:visible" >
				<div class="row">
					<div class="col-md-12">
						{{ ($activity->description) }}
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-3 col-md-offset-9">
						<a href="{{ route('activities.edit',$activity->id)}}" class="btn btn-warning btn-block">Ubah Aktivitas &nbsp;<span class="fa fa-plus-circle"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@stop
