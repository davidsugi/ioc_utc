@extends('layouts.app')

@section('title')
Manage Aktivitas
@endsection
@section('breadcrumb')
<li><a href='{{ route("home") }}'><i class='fa fa-home'></i> Home</a></li>
<li><a href='#' class='active'><i class='fa fa-compass'></i> Aktivitas</a></li>
@endsection
@section('content')

<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[title]" placeholder="Judul" value="{{ !empty($filter['title']) ? $filter['title'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<div class="input-group no-border">
				<select name="filter[status]" class="form-control column-filter">
					<option value="" {{ empty($filter['status']) ? 'selected' : '' }}>All</option>
					<option value="1" {{ !empty($filter['status']) && $filter['status'] == 1 ? 'selected' : '' }}>Publish</option>
					<option value="2" {{ !empty($filter['status']) && $filter['status'] == 2 ? 'selected' : '' }}>Inactive</option>
				</select>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<br>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('activities.create')}}" class="btn btn-success btn-block">Tambah Aktivitas &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>

	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Judul</th>
					<th>Gambar</th>
					<th>Status</th>
					<th>Edit</th>
					<th>Show</th>
				</tr>
			</thead>
			<tbody>
				@foreach($activitys as $a)
				<tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$a->title}}</td>
					
					<td><img src="{{asset('image/')}}/{{ $a->image ? $a->image : ENV('PLACEHOLDER_IMG','Na.jpg')  }}"  width="60px" height="40px"></td>
					<td>{!! $a->statusLabel !!}</td>
					<td>
						<a href="{{route('activities.edit',$a->id)}}">
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
					<td>
						<a href="{{route('activities.show',$a->id)}}">
							<button class="btn btn-primary">
								Show
							</button>
						</a>
					</td>
				</tr>
				@endforeach
					@if($activitys->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $activitys->appends($filters)->links() }}
</div>
</div>

@stop
