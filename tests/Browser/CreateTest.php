<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
class CreateTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $links = [
                    "/facilities"=>[
                        "Tambah Fasilitas"=>[
                            "name"=>"text",
                            "code"=>"code",
                            "link_embed"=>"url",
                            "weekend_price"=>"price",
                            "price"=>"price",
                            "no_stay_weekend_price"=>"price",
                            "no_stay_weekday_price"=>"price",
                            "unit"=>"select",
                            "per_person"=>"check",
                            "minimum"=>"number",
                            "capacity"=>"number",
                            "type_id"=>"select",
                            ".note-editable"=>"text",
                        ]
                    ],
                    // "/customers"=>[
                    //     "Tambah Customer"=>[
                    //         "name"=>"text",
                    //         "is_company"=>"check",
                    //         "company_name"=>"text",
                    //         "customer_type_id"=>"select",
                    //         "job"=>"text",
                    //         "email"=>"email",
                    //         "address"=>"price",
                    //         "province_id"=>"select2",
                    //         "city_id"=>"select",
                    //         "zip"=>"number",
                    //         "card_id"=>"number",
                    //         "phone"=>"number",
                    //     ]
                    // ],
                    "/addons"=>[
                        "Tambah Addon"=>[
                            "name"=>"text",
                            "weekend_price"=>"price",
                            "price"=>"price",
                            "no_stay_weekend_price"=>"price",
                            "no_stay_weekday_price"=>"price",
                            "unit_id"=>"select",
                            "stock"=>"number",
                            "active"=>"select",
                            "limited"=>"check",
                        ]
                    ],
                    "/bundles"=>[
                        "Tambah Paket"=>[
                            "name"=>"text",
                            "weekend_price"=>"price",
                            "price"=>"price",
                            "no_stay_weekend_price"=>"price",
                            "no_stay_weekday_price"=>"price",
                            // "unit_id"=>"select",
                            "minimum"=>"number",
                            "per_person"=>"check",
                        ]
                    ]

            ];
            $prc = rand(1,100);
            $fakers = ["text"=>"Laravel dusk Rocks!","email"=>"dusk@laravel.com", "code"=>"LRVLDSKRCKZ", "url"=>"https://laravel.com/", "price"=>$prc."000","number"=>$prc, "textarea"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum eu ante cursus ultricies. Praesent vitae pulvinar magna. Nullam maximus congue dui, sed vestibulum eros pharetra sed. Phasellus et purus sed orci dictum tempus non quis tellus. Proin ut purus ut augue ullamcorper maximus et at ex. Duis quis commodo libero. Phasellus risus sapien, egestas vel imperdiet sit amet, pulvinar a nisl. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ex mi, ultricies quis velit eget, mattis molestie risus. Quisque sagittis, nunc at laoreet elementum, elit ante porta urna, a laoreet ante libero non odio.Donec fermentum maximus velit, quis bibendum quam lacinia at. Fusce hendrerit lectus purus, sit amet gravida libero suscipit eu. Nulla a ex arcu."];
            $browser->LoginAs(User::where("role",1)->first());
                    foreach ($links as $url => $link) {
                        $browser->visit($url);
                        foreach($link as $menu=>$fields){
                            $browser->clickLink($menu)
                            ->assertSee($menu);
                            foreach ($fields as $field => $value) {
                                if($value=="radio"){

                                }
                                else if($value=="select"){
                                    $browser->select($field);
                                }
                                else if($value=="select2"){
                                    $browser->select2($field);
                                }
                                else if($value=="check"){
                                    $browser->check($field);
                                }
                                else if($value=="uncheck"){
                                    $browser->uncheck($field);
                                }
                                else{
                                    $prc = rand(1,100);                                    
                                    $fakers["price"]=$prc."000";                                    
                                    $fakers["number"]=$prc;                                    
                                    $browser->type($field,$fakers[$value]);
                                }
                            }
                            $browser->press('Simpan')->assertSee('Success!');
                        }
                        
                    }
                    

        });
    }
}
