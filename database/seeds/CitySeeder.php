<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $curl = curl_init();
        curl_setopt_array(
            $curl, array(
            CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "key:0927958fafb5fb0b6187731768a1f6e1"
            ),
            )
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $data = json_decode($response, true);
        for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
            DB::table('cities')->insert(
                [
                'city' => $data['rajaongkir']['results'][$i]['city_name'],
                'type' => $data['rajaongkir']['results'][$i]['type'],
                'province' => $data['rajaongkir']['results'][$i]['province'],
                ]
            );
        }
        curl_close($curl);
    }
}
