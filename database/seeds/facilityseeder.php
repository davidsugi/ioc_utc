<?php

use Illuminate\Database\Seeder;

class facilityseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $sets = ['jam','hari','buah','orang'];
        foreach($sets as $set){
            DB::table('units')->insert(
                [ 
                'name' => $set,
                ]
            );
        }
         $sets = [['Breakfast','12000',4],['Pool Ticket','19000',4],['Lunch','14000',4],['Dinner','15000',4]];
        foreach($sets as $set){
            DB::table('addons')->insert(
                [ 
                'name' => $set[0],
                'price' => $set[1],
                'unit_id' => $set[2],
                'active'=>true,
                'slug'=>$set[0]
                ]
            );
        }
        $sets = ['room','dorm','pool','meeting room','camping ground'];
        foreach($sets as $set){
            DB::table('facility_types')->insert(
                [ 
                'name' => $set,
                'slug' => $set,
                ]
            );
        }
        $sets = [['bed','integer'],['wifi','integer']];
        foreach($sets as $set){
            DB::table('facility_attributes')->insert(
                [ 
                'name' => $set[0],
                'datatype' => $set[1],
                'slug'=> $set[0],
                ]
            );
        }
        factory(App\Facility::class, 50)->create();
        factory(App\DetailFacility::class, 300)->create();
        factory(App\FacilityDetail::class, 5000)->create();

        
    }
}
