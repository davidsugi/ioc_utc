<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('customer_types')->insert(
        //     [ 
        //     'name' => "local",
        //     ]
        // );
        // DB::table('customer_types')->insert(
        //     [ 
        //     'name' => "Ubaya",
        //     ]
        // );
        DB::table('users')->insert(
            [ 
                'name' => "admin",
                'role' => 1,
                'email' => "admin@ioc.com",
                'password' => bcrypt("secret"),
            ]
        );
        // DB::table('cities')->insert(
        //     [ 
        //             'city' => "surabaya",
        //             'type' => "kota",
        //             'province' => "Jawa Timur",
        //     ]
        // );
        // DB::table('global_settings')->insert(
        //     [ 
        //             'key' => "tax",
        //             'value' => "10",
        //     ]
        // );
         // DB::table('global_settings')->insert(
        //     [ 
        //             'key' => "expiry",
        //             'value' => "3",
        //     ]
        // );
        // factory(App\Customer::class, 200)->create();

    }
}
