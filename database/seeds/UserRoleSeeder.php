<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = ['Admin'];

         $roles = ['Admin', 'Analyst', 'Staff'];


        $permissions = [
            'Mengelola Jadwal','Mengelola Pelanggan',
            'Mengelola Testimoni','Mengelola Aktivitas',
            'Mengelola Berita', 'Mengelola Eduwisata',
            'Mengelola Fasilitas','Mengelola Tambahan',
            'Mengelola Paket','Mengelola Order',
            'Mengelola Kupon','Mengelola Pengaturan Fasilitas',
            'Mengelola Galeri','Mengelola Pengguna',
            'Melihat Laporan','Mengelola Banner',
            'Mengelola Pengaturan Pelanggan','Melihat Laporan Penggunaan'
        ];

            foreach($permissions as $val){
                $item = Permission::firstOrCreate(array('name' => $val));
                $item->save();
            }

        foreach($roles as $val){
            $item = Role::firstOrCreate(array('name' => $val));
            $item->save();
            if($val == 'Admin'){
                $item->syncPermissions(Permission::all());
            }
        }

        foreach($users as $user){
            // $item = User::firstOrCreate(array('name' => $user[0], 'email' => $user[1], 'password' => bcrypt($user[2]),'role' => 0));
            // $item->save();
            $item = User::where('name',$user)->first();
            if($item->name == "Admin"){
                $item->syncRoles('Admin');
            }
        } 
               app()['cache']->forget('spatie.permission.cache');

    }
}
