<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDetailOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('detail_orders', function($table) {
            $table->decimal('amount', 12, 2)->change();
            $table->decimal('price', 12, 2)->change();
            $table->decimal('subtotal', 12, 2)->change();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_orders', function($table) {
            $table->integer('amount')->change();
            $table->integer('price')->change();
            $table->integer('subtotal')->change();
        });
    }
}
