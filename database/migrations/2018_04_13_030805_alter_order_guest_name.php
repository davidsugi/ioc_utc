<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderGuestName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('orders', function($table) {
            $table->string('guest_name')->nullable();
            $table->string('guest_phone')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {         
        Schema::table('orders', function($table) {
            $table->string('guest_name')->nullable();
            $table->string('guest_phone')->nullable();
        });
    }
}
