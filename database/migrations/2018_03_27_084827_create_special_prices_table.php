<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('special_id');
            $table->string('special_type');
            $table->integer('customer_type_id')->unsigned();
            $table->integer('weekend_price')->default(0);
            $table->integer('price')->default(0);
            $table->integer('no_stay_weekend_price')->default(0);
            $table->integer('no_stay_weekday_price')->default(0);

            $table->foreign('customer_type_id')->references('id')->on('customer_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_prices');
    }
}
