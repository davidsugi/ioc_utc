<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'banners', function (Blueprint $table) {
                $table->increments('id');
                $table->string('image_desktop_path');
                $table->string('image_mobile_path');
                $table->string('title');
                $table->string('subtitle');
                $table->text('description');
                $table->string('link_text');
                $table->string('link_url');
                $table->unsignedInteger('banner_location_id');
                $table->foreign('banner_location_id')->references('id')->on('banner_locations');                         
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'banners', function (Blueprint $table) {
                //
            }
        );
    }
}
