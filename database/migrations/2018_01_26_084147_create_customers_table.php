<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'customers', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('card_id')->unique();
                $table->string('address')->nullable();
                $table->unsignedInteger('city_id')->nullable();
                $table->foreign('city_id')->references('id')->on('cities');
                $table->string('zip')->nullable();
                $table->string('email');
                $table->string('phone');
                $table->string('company_name')->nullable();
                $table->string('job')->nullable();
                $table->integer('customer_type_id')->unsigned()->nullable();    
                $table->timestamps();
                $table->foreign('customer_type_id')->references('id')->on('customer_types');
                //
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'customers', function (Blueprint $table) {
                //
            }
        );
    }
}
