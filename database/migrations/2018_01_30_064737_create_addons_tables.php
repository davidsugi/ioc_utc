<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddonsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'addons', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug')->unique();
                // $table->integer('price');
                $table->integer('weekend_price')->default(0);
                $table->integer('price')->default(0);
                $table->integer('no_stay_weekend_price')->default(0);
                $table->integer('no_stay_weekday_price')->default(0);
                $table->integer('stock')->default(0);
                $table->integer('borrowed')->default(0);
                $table->integer('limited')->default(0);
                $table->boolean('active');
                $table->unsignedInteger('unit_id');
                $table->foreign('unit_id')->references('id')->on('units');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'addons', function (Blueprint $table) {
                //
            }
        );
    }
}
