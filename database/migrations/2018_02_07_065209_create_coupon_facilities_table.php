<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'coupon_facilities', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('facs_id');
                $table->string('facs_type');
                $table->integer('coupon_id')->unsigned();
                $table->integer('exclude')->default(0);
                $table->timestamps();
                $table->foreign('coupon_id')->references('id')->on('coupons');

            
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_facilities');
    }
}
