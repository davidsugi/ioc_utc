<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomschedulesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'custom_schedules', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('detail_facility_id')->unsigned();
                $table->foreign('detail_facility_id')->references('id')->on('detail_facilities');
                $table->timestamp('startdate');
                $table->timestamp('enddate');
                $table->string('description')->nullable();
                $table->timestamps();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'custom_schedules', function (Blueprint $table) {
                //
            }
        );
    }
}
