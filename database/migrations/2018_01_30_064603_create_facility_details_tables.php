<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilitydetailsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'facility_details', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('facility_id')->unsigned();
                $table->integer('attribute_id')->unsigned();
                $table->foreign('facility_id')->references('id')->on('facilities');
                $table->foreign('attribute_id')->references('id')->on('facility_attributes');
                $table->string('value');
                $table->timestamps();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'facility_details', function (Blueprint $table) {
                //
            }
        );
    }
}
