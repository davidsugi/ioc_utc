<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailFacilitiesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'detail_facilities', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->unsignedInteger('facility_id');
                $table->foreign('facility_id')->references('id')->on('facilities');
                $table->timestamps();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'detail_facilities', function (Blueprint $table) {
                //
            }
        );
    }
}
