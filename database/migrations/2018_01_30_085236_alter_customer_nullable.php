<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'customers', function (Blueprint $table) {
                $table->string('card_id')->nullable()->change();
                $table->string('address')->nullable()->change();
                $table->string('email')->nullable()->change();
                $table->string('phone')->nullable()->change();
            }
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
