<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilitiesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('code');
                $table->string('slug')->unique();
                $table->integer('price');
                $table->integer('weekend_price')->nullable();
                $table->integer('no_stay_weekend_price')->default(0);
                $table->integer('no_stay_weekday_price')->default(0);
                $table->integer('unit');
                $table->boolean('per_person');
                $table->integer('minimum');
                $table->integer('capacity');
                $table->text('link_embed')->nullable();
                $table->integer('type_id')->unsigned();
                $table->foreign('type_id')->references('id')->on('facility_types');
                $table->text('description')->nullable();
                $table->timestamps();
                //
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'facilities', function (Blueprint $table) {
                //
            }
        );
    }
}
