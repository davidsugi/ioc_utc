<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/', 'HomeController@index'
);

Route::Group(['middleware'=>'auth'], function () {
    Route::get('l0gslux0d3v12345', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::resource('/food_settings','FoodSettingController');
    Route::group(['middleware' => ['permission:Mengelola Order']], function () {
        Route::get('invoices/print/{order}','InvoiceController@print')->name("invoices.print");
        Route::get('invoices/penagihan/{order}','InvoiceController@tagih')->name("invoices.tagih");

        Route::resource('orders.invoices','InvoiceController');        
        Route::resource('/orders', 'OrderController');
        Route::resource('/invoices', 'InvoiceController');

        Route::get('/invoice/stat','InvoiceController@stat')->name('orders.invoice.stat');
        Route::get('/order/{order}/folios', 'OrderController@folio')->name('orders.folios.index');
        Route::resource('orders.order_documents','OrderDocumentController');
        Route::post('/order/stat','OrderController@stat')->name("orders.stat");
    });

    Route::group(['middleware' => ['permission:Melihat Laporan']], function () {
        Route::resource('/reports','ReportController');
        Route::get('/report/customer', 'ReportController@customer')->name('reports.customer');
        Route::get('/report/facility', 'ReportController@facility')->name('reports.facility');
        Route::get('/report/bkm', 'ReportController@bkm')->name('reports.bkm');
    });

     Route::group(['middleware' => ['permission:Melihat Laporan Penggunaan']], function () {
        Route::get('/usage_index', 'ReportController@usage_index')->name('usages.index');
        Route::get('/report/usage', 'ReportController@usage')->name('reports.usage');
    });

    Route::group(['middleware' => ['permission:Melihat Laporan Penggunaan']], function () {
        Route::get('/report/usage', 'ReportController@usage')->name('reports.usage');
    });

    Route::group(['middleware' => ['permission:Mengelola Jadwal']], function () {
        Route::get('/pint/folio', 'FolioController@print')->name('folios.print');
        Route::resource('/custom_schedules', 'CustomScheduleController');
       Route::resource('/folios', 'FolioController');

    });
    Route::group(['middleware' => ['permission:Mengelola Pengguna']], function () {
        Route::resource('/roles','RoleController');
        Route::resource('/permissions','PermissionController');
        Route::resource('/users', 'UserController');
        Route::get('users/{user}/change_password', 'UserController@change_password_view')->name('users.change_password_view');
        Route::post('users/{user}/change_password_store', 'UserController@change_password_store')->name('users.change_password_store');
   
    });
    Route::group(['middleware' => ['permission:Mengelola Pengaturan Fasilitas']], function () {
        Route::resource('/facility_details', 'FacilityDetailController');
        Route::resource('/facility_attributes', 'FacilityAttributeController');
        Route::resource('/customer_types', 'CustomerTypeController');
        Route::resource('/facility_types', 'FacilityTypeController');
        Route::resource('/categories', 'CategoryController');
        Route::resource('/units', 'UnitController');
        
    });
    Route::group(['middleware' => ['permission:Mengelola Fasilitas']], function () {
        Route::resource('/facilities', 'FacilityController');
        Route::resource('/facilities.galleries', 'FacilityGalleryController');
        Route::resource('/facilities.facility_details', 'FacilityFacilityDetailController');
        Route::resource('/facilities.detail_facilities', 'FacilityDetailFacilityController');
        Route::resource('/facilities.custom_prices', 'FacilityCustomPriceController');
        Route::resource('/detail_facilities.custom_schedules', 'DetailFacilityCustomScheduleController');

    });

    Route::group(['middleware' => ['permission:Mengelola Eduwisata']], function () {
        Route::resource('/education_tours', 'EducationTourController');
        Route::resource('/education_tours.galleries', 'EducationTourGalleryController');
    });
    Route::group(['middleware' => ['permission:Mengelola Berita']], function () {
        Route::resource('/news', 'NewsController');
        Route::get('/news_image/{news}', 'NewsController@editImage')->name('news.editImage');
        Route::put('/news_image/{news}', 'NewsController@updateImage')->name('news.updateImage');
                
    });
    Route::group(['middleware' => ['permission:Mengelola Paket']], function () {
        Route::resource('/bundles.galleries', 'BundleGalleryController');
        Route::resource('/bundles', 'BundleController'); 
        Route::resource('/bundlings', 'BundlingController');
        Route::resource('/bundlings.facilities', 'BundlingFacilityController');
        
    });
    Route::group(['middleware' => ['permission:Mengelola Aktivitas']], function () {
        Route::resource('/activities','ActivtiyController');
        Route::get('/activtiy_image/{activtiy}', 'ActivtiyController@editImage')->name('activities.editImage');
        Route::put('/activtiy_image/{activtiy}', 'ActivtiyController@updateImage')->name('activities.updateImage');
    });
    Route::group(['middleware' => ['permission:Mengelola Fasilitas|Mengelola Tambahan|Mengelola Paket|Mengelola Pengaturan Fasilitas']], function () {
        Route::resource('/custom_prices', 'CustomPriceController')->middleware('permission: Mengelola Fasilitas|Mengelola Pengaturan Fasilitas');
        Route::resource('/special_prices', 'SpecialPriceController')->middleware('permission:Mengelola Fasilitas|Mengelola Tambahan|Mengelola Paket|Mengelola Pengaturan Fasilitas');
    });
    Route::group(['middleware' => ['permission:Mengelola Banner']], function () {
        Route::resource('/banners', 'BannerController');
        Route::resource('/banner_locations','BannerLocationController');
      
    });
    Route::group(['middleware' => ['permission:Mengelola Tambahan']], function () {
        Route::resource('/addons', 'AddonController');
        Route::resource('addons.menus', 'MenuController');
    });
        Route::resource('/customers', 'CustomerController')->middleware('permission:Mengelola Pelanggan');
        Route::resource('/galleries', 'GalleryController')->middleware('permission:Mengelola Galeri');
        // ->middleware('permission:Mengelola Tambahan');

        Route::resource('/coupons', 'CouponController')->middleware('permission:Mengelola Kupon');

        Route::resource('/testimonies', 'TestimonyController')->middleware('permission:Mengelola Testimoni');
    
       
    }
);
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');


Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/order/{id}/invoices/create/{invoice]', 'InvoiceController@create')->name('order.invoice.create.ord');
Route::post('/order/stat','OrderController@stat')->name("orders.stat");
