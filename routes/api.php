<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/datatype', 'FacilityAttributeController@getDataType')->name('API.datatype');
Route::middleware('auth:api')->get(
    '/user', function (Request $request) {
        return $request->user();
    }
);
Route::get('/calendar', 'CustomScheduleController@getCalendar')->name('API.calendar');
Route::get('/city', 'CustomerController@getCity')->name('API.getCity');
Route::get('/get/kota','CustomerController@getKota')->name('api.get.kota');
Route::get('/get/province','CustomerController@getProvince')->name('api.get.province');
Route::get('/detailfacilitycalendar', 'DetailFacilityCustomScheduleController@getCalendar')->name('API.detailfacilitycalendar');
//Route::get('/asa','UserController@ap')->name('API.asa');
Route::get('/get/customers', 'CustomerController@get')->name('api.get.customer');
Route::get('/get/customer/data', 'CustomerController@getData')->name('api.get.customer.data');
Route::get('/get/facility', 'FacilityController@get')->name('api.get.facility');
Route::get('/get/buyable', 'FacilityController@getBuyable')->name('api.get.buyable');
Route::get('/get/addons', 'AddonController@get')->name('api.get.addon');
Route::get('/get/order', 'OrderController@get')->name('api.get.order');
Route::get('/get/detail_order', 'OrderController@getDet')->name('api.get.det.order');
Route::get('/get/schedule/detail_order', 'OrderController@getSch')->name('api.get.schedule.order');
Route::get('/get/facility/get_units', 'FacilityController@getUnit')->name('api.get.facility.unit');
Route::get('/get/facility/type', 'FacilityTypeController@get')->name('api.get.facility_type');
Route::get('/get/customer_type', 'CustomerTypeController@get')->name('api.get.customer_tpye');
Route::get('/get/price','OrderController@getPrice')->name('api.get.price');
Route::get('/get/addon_price','OrderController@getAddonPrice')->name('api.get.addon_price');
Route::get('/get/promo', 'CouponController@get')->name('api.get.promo');
// Route::get('/calendar', 'CustomScheduleController@getCalendar')->name('API.calendar');
Route::get('/get/folio_calendar','FolioController@getCalendar')->name('api.get.folio_calendar');
Route::get('/city', 'CustomerController@getCity')->name('API.getCity');

Route::get('/get/forbundle', 'BundleController@getforbundle')->name('api.get.forbundle');
Route::get('/get/order','OrderController@getOrder')->name('api.get.order')->middleware('client');

Route::group(['middleware' => ['AuthToCore']], function () {
});

Route::post('menus/create', 'MenuController@api_create');
Route::post('menus/show', 'MenuController@api_show');
Route::post('menus/update/{id}', 'MenuController@api_update');
Route::delete('menus/delete/{id}', 'MenuController@api_delete');